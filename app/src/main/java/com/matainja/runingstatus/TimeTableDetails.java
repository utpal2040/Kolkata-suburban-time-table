package com.matainja.runingstatus;

import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.app.Activity;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.matainja.runingstatus.Adapter.TimeTableDetailsAdapter;
import com.matainja.runingstatus.Adapter.TimeTableDetailsRecyclerAdapter;
import com.matainja.runingstatus.Adapter.TimeTableListAdapter;
import com.matainja.runingstatus.Common.ConnectionDetector;
import com.matainja.runingstatus.Database.DatabaseAdapter;
//import com.matainja.runingstatus.JSONParser;

import com.matainja.runingstatus.GoogleAnalyticsApp.TrackerName;
import com.matainja.runingstatus.Model.StationListBean;

public class TimeTableDetails extends AppCompatActivity {
	ListView list;
    TextView source;
    TextView arrival;
    TextView departure;
    TextView status;
    Button Btngetdata;
    TextView constatus ;
    ArrayList<HashMap<String, String>> oslist = new ArrayList<HashMap<String, String>>();
    //URL to get JSON Array
    private static String url = "";
    //JSON Node Names
	SharedPreferences sp;
    ProgressDialog pDialog;
    public static final int DIALOG_DOWNLOAD_PROGRESS = 1;
    Bundle extras;
    Integer trainId;
    Integer  trainNo;
    String tostationcode;
    private DatabaseAdapter dbHelper;
    Cursor traincursor;
    Cursor stationList;
	RecyclerView lv;
    private ProgressDialog mProgressDialog;
    ArrayList<StationListBean> stationListView;
    LayoutInflater infalter;
	 ViewGroup header;
	 TimeTableListAdapter adapter;
	 TextView train_name;
	 TextView deststation;
	 String trainName;
	 String fromstation;
	 String tostation;
	 int sun;
	 int mon;
	 int tue;
	 int wed;
	 int thu;
	 int fri;
	 int sat;
	 TextView days;
	 String TrainNo;
	 int currentapiVersion=0;
	 LinearLayout adviewlayout ;
	 ConnectionDetector cd;
	 Button livestatus;
	 LinearLayout livestatuslayout ;
	 Context context;
	 Toolbar toolbar;
	RelativeLayout RLay;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	//Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
		setContentView(R.layout.timetabledetails);
		overridePendingTransition(R.anim.anim_slide_in_left,
				R.anim.anim_slide_out_left);
        context = getApplicationContext();
        //livestatus =(Button)findViewById(R.id.livestatus);
       // Log.v("start activity","ooo");

		//Toast.makeText(TimeTableDetails.this,"TIME TABLE DETAILS",Toast.LENGTH_SHORT).show();
		sp = getSharedPreferences("themeColor", Context.MODE_PRIVATE);
     // Database Connection Open
	    dbHelper = new DatabaseAdapter(this);
		dbHelper.open();

		toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		RLay = (RelativeLayout) findViewById(R.id.RLay);

	      //adviewlayout = (LinearLayout)findViewById(R.id.admoblayout);
	      /*livestatuslayout = (LinearLayout)findViewById(R.id.livestatuslayout);
	      livestatuslayout.setVisibility(View.GONE);*/
		     /* offline remove ads */
		     
		  cd = new ConnectionDetector(getApplicationContext());
	         
	         // Check if Internet present
	         if (!cd.isConnectingToInternet()) {
	        	 
	        	 //adviewlayout.setVisibility(View.GONE);
	        	 
	        	// livestatuslayout.setVisibility(View.GONE);
				 RLay.setVisibility(View.GONE);
	         } else{
				 AdView mAdView = (AdView) findViewById(R.id.adView);
				 AdRequest adRequest = new AdRequest.Builder().build();
				 mAdView.loadAd(adRequest);
			 }


		/*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			Window window = getWindow();
			window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
			window.setStatusBarColor(getResources().getColor(R.color.colorLocalStatus));
		}*/

		if(sp.getInt("localColor",0)!=0) {
			toolbar.setBackgroundColor(sp.getInt("localColor",0));

			int[] demo_colors = getResources().getIntArray(R.array.demo_colors);
			int[] demo_colors_mat = getResources().getIntArray(R.array.demo_colors_mat);

			ArrayList<String> colorString = new ArrayList<String>();
			ArrayList<String> colorString_mat = new ArrayList<String>();

			for (int i = 0; i < demo_colors.length; i++) {
				Log.e("Test", demo_colors[i] + "");
				//Log.e("Test", demo_colors[i] + "");
				colorString.add(String.valueOf(demo_colors[i]));

			}

			for (int i = 0; i < demo_colors_mat.length; i++) {
				Log.e("Test", demo_colors[i] + "");
				//Log.e("Test", demo_colors[i] + "");
				colorString_mat.add(String.valueOf(demo_colors_mat[i]));

			}
			Log.e("position ",colorString.indexOf(String.valueOf(sp.getInt("localColor",0))) + "");
			int color_mat_val = Integer.parseInt(colorString_mat.get(colorString.indexOf(String.valueOf(sp.getInt("localColor", 0)))));

			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
				Window window = getWindow();
				window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
				window.setStatusBarColor(color_mat_val);
			}
		}else{
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
				Window window = getWindow();
				window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
				window.setStatusBarColor(getResources().getColor(R.color.colorLocalStatus));
			}
		}

	      currentapiVersion = android.os.Build.VERSION.SDK_INT;
	        /*
	        Typeface custom_font = Typeface.createFromAsset(getAssets(),
	        	      "fonts/Bodoni-Italic.TTF");
	        TextView homepage_header_text = (TextView) findViewById(R.id.homepage_header_text);
	        homepage_header_text.setTypeface(custom_font);*/
	        extras = getIntent().getExtras();
		      if(extras !=null) {
		    	  trainId = extras.getInt("trainId");
		    	  trainNo =extras.getInt("trainNo"); 
		    	  fromstation = extras.getString("fromstation");
		    	  tostation = extras.getString("tostation");
				}
		      stationListView = new ArrayList<StationListBean>();
		      
		      Handler handler = new Handler();
		        Runnable r = new Runnable() {
		            public void run() {
		            	 /* TRacking Code */
		                currentapiVersion = android.os.Build.VERSION.SDK_INT;
		                if(currentapiVersion>=android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH)
		                {
		                	/* TRacking Code */
		      		      
		      		      Tracker t = ((GoogleAnalyticsApp) getApplication()).getTracker(TrackerName.APP_TRACKER);
		      				t.setScreenName(trainNo+"- Details Localtrain");
		      				t.send(new HitBuilders.AppViewBuilder().build());


		                }      
		            }
		        };
		        handler.postDelayed(r, 200);
		      
		      
				
	      /// Train Info retrireve
	      
		      traincursor=  dbHelper.getLocalTrain(trainId);
		      if(traincursor.getCount()>0)
	 	  	  {	 
	 	     	 
	 	     	  while (traincursor.moveToNext()) {
	 	 				
	 	     		
	 	 				
	 	     		TrainNo =  traincursor.getString(traincursor.getColumnIndexOrThrow("trainNO"));
	 	  				 trainName = traincursor.getString(traincursor.getColumnIndexOrThrow("trainName"));
	 	  				
	 	  				sun =traincursor.getInt(traincursor.getColumnIndexOrThrow("sun"));
	 	  				mon =traincursor.getInt(traincursor.getColumnIndexOrThrow("mon"));
	 	  				tue =traincursor.getInt(traincursor.getColumnIndexOrThrow("tue"));
	 	  				wed =traincursor.getInt(traincursor.getColumnIndexOrThrow("wed"));
	 	  				thu =traincursor.getInt(traincursor.getColumnIndexOrThrow("thu"));
	 	  				fri =traincursor.getInt(traincursor.getColumnIndexOrThrow("fri"));
	 	  				sat =traincursor.getInt(traincursor.getColumnIndexOrThrow("sat"));
	 	  				 
	 	  				
	 	  				
	 	  				
	 	  				
	 	 			}
	 	  	 } 
		      
		      /*livestatus.setOnClickListener(new View.OnClickListener() {
		        	
		            @Override
		            public void onClick(View v) {
		                // TODO Auto-generated method stub
		            	
		            	*//*Intent intent = new Intent(context, locallivestatus.class);
		            	
		    			intent.putExtra("trainno", TrainNo);
		    			
		    			intent.putExtra("trainName", trainName);
		    			
	                    startActivity(intent);*//*
		            }
		      });*/
		      
		      String runningdays ="Running Days: ";
		      if(sun==1)
		      {
		    	  runningdays=runningdays+"| Sun";
		      }
		      if(mon==1)
		      {
		    	  runningdays=runningdays+" | Mon";
		      }
		      if(tue==1)
		      {
		    	  runningdays=runningdays+" | Tue";
		      }
		      if(wed==1)
		      {
		    	  runningdays=runningdays+" | Wed";
		      }
		      if(thu==1)
		      {
		    	  runningdays=runningdays+" | Thu";
		      }
		      if(fri==1)
		      {
		    	  runningdays=runningdays+" | Fri";
		      }
		      if(sat==1)
		      {
		    	  runningdays=runningdays+" | Sat ";
		      }
		 	 /// Listview for train
		        lv = (RecyclerView)findViewById(R.id.list);
				RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(TimeTableDetails.this);
				lv .setLayoutManager(mLayoutManager);
		//lv.setNestedScrollingEnabled(false);

		        train_name =   (TextView) findViewById(R.id.train_name);
		        days = (TextView) findViewById(R.id.runningday);
		        //days.startAnimation((Animation)AnimationUtils.loadAnimation(getApplicationContext(),R.anim.textviewanimation));
				days.setEllipsize(TextUtils.TruncateAt.MARQUEE);
				days.setSingleLine(true);
                days.setSelected(true);
		        train_name.setText(trainName);
		    	   days.setText(runningdays);
		    	   
			          // return null;
		       
		        
		       
		    	   traincursor.close();
	    
		        stationList = dbHelper.getStationList(trainId);
	    
	     
		        if(stationList.getCount()>0)
		 	  	  {	 
		 	     	 
		 	     	  while (stationList.moveToNext()) {
		 	 				
		 	     		
		 	     		
		 	 				
		 	 				String arrival   = stationList.getString(stationList.getColumnIndexOrThrow("arrival"));
		 	  				String stationName   = stationList.getString(stationList.getColumnIndexOrThrow("stationName"));
		 	  				String stationCode  = stationList.getString(stationList.getColumnIndexOrThrow("stationCode"));
		 	  				
		 	  				
		 	  				int iscolorBG=0;
		 	  				
		 	  				if(fromstation.equalsIgnoreCase(stationCode) || tostation.equalsIgnoreCase(stationCode))
		 	  				{
		 	  					iscolorBG=1;	
		 	  				}
		 	  				
		 	  				
		 	  				stationListView.add(new StationListBean(arrival,stationName,stationCode,fromstation,tostation,iscolorBG,""));
		 	 			}
		 	  	 } 
		        stationList.close();

        Log.e("TimeTableDetailsRecyclerAdapter","TRUE");
		TimeTableDetailsRecyclerAdapter adapter = new TimeTableDetailsRecyclerAdapter(this, stationListView, "local");
		       	
		       	// lv.setOnItemClickListener(this);
		       	 lv.setAdapter(adapter);
		       	// lv.setOnClickListener(new ClickListenerList());
		       	/*lv.setOnItemClickListener(new OnItemClickListener() {
		       	      public void onItemClick(AdapterView<?> parent, View view,
		       	          int position, long id) {
		       	        // When clicked, show a toast with the TextView text
		       	    	
		       	      }
		       	    });*/
		       	
		       	//ImageView  	menuback =(ImageView)findViewById(R.id.close);
		         /*menuback.setOnClickListener(new View.OnClickListener() {
		         	
		             @Override
		             public void onClick(View v) {
		                 // TODO Auto-generated method stub
		            	 //overridePendingTransition(R.anim.lefttoright, R.anim.righttoleft);
		             	finish();
		             	 //overridePendingTransition(R.anim.lefttoright, R.anim.righttoleft);
		             }
		         });*/
		         
		         ImageView error =(ImageView)findViewById(R.id.error);
		         error.setOnClickListener(new View.OnClickListener() {
		         	
		             @Override
		             public void onClick(View v) {
		                 // TODO Auto-generated method stub

							String adminemail= getResources().getString(R.string.adminemail) ;
							String subject = "Time Schedule problems Train No : "+TrainNo;
							String Message = "HI The train No : "+TrainNo +" Problem in Schedule Please check Time";
							String[] TO = {adminemail};
						     // String[] CC = {"mcmohd@gmail.com"};
						      Intent emailIntent = new Intent(Intent.ACTION_SEND);
						      emailIntent.setData(Uri.parse("mailto:"));
						      emailIntent.setType("text/plain");


						      emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
						      //emailIntent.putExtra(Intent.EXTRA_CC, CC);
						      emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
						      emailIntent.putExtra(Intent.EXTRA_TEXT, Message);
						      
						      try {
							         startActivity(Intent.createChooser(emailIntent, "Sending email..."));
							         
							         
							        
							       //  successAlert();
							         //Log.i("Finished sending email...", "");
							      } catch (android.content.ActivityNotFoundException ex) {
							         Toast.makeText(TimeTableDetails.this, 
							         "There is no email client installed.", 

Toast.LENGTH_SHORT).show();
					    		CharSequence text = "success";
								int duration = Toast.LENGTH_SHORT;

								Toast toast = Toast.makeText(getApplicationContext(), text, 

duration);
								toast.setGravity(Gravity.CENTER, 0, 0);
								toast.show();	
					    	}
						      
						     	
				    	
		             }
		         });
		       
        
        
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //overridePendingTransition(R.anim.lefttoright, R.anim.righttoleft);
        overridePendingTransition(R.anim.anim_slide_in_right,
                R.anim.anim_slide_out_right);
    }
    
    
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
		case DIALOG_DOWNLOAD_PROGRESS:
			mProgressDialog = new ProgressDialog(this);
			mProgressDialog.setMessage("Loading Train List...");
			mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			mProgressDialog.setCancelable(false);
			mProgressDialog.show();
			return mProgressDialog;
		default:
			return null;
        }
        
       
   }
    
    
    @Override
    public void onDestroy()
    {
        super.onDestroy();
        dbHelper.close();
    }  
  
   
}