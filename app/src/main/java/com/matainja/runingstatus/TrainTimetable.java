package com.matainja.runingstatus;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.matainja.runingstatus.Adapter.TimeTableListAdapter;
import com.matainja.runingstatus.Common.ConnectionDetector;
import com.matainja.runingstatus.CustomView.WrapContentHeightViewPager;
import com.matainja.runingstatus.Database.DatabaseAdapter;
import com.matainja.runingstatus.Database.DatabaseAdapterEx;
import com.matainja.runingstatus.Database.DatabaseAdapterSaveSearch;
import com.matainja.runingstatus.Fragment.LocalTrainListFragment;
import com.matainja.runingstatus.Fragment.TrainListViewPagerAdapter;
import com.matainja.runingstatus.GoogleAnalyticsApp.TrackerName;
import com.matainja.runingstatus.Model.TimetableListBean;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;


public class TrainTimetable extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
	ListView list;
    TextView source;
    TextView arrival;
    TextView departure;
    TextView status;
    Button Btngetdata;
    TextView constatus ;
    ArrayList<HashMap<String, String>> oslist = new ArrayList<HashMap<String, String>>();
    //URL to get JSON Array
    private static String url = "";
    //JSON Node Names
    int norecrod=0;
    ProgressDialog pDialog;
    public static final int DIALOG_DOWNLOAD_PROGRESS = 1;
    Bundle extras;
    String fromstationcode,fromstationname;
    String tostationcode,tostationname;
    int filterbytime;
    String timefilter;
    private DatabaseAdapter dbHelper;
    private DatabaseAdapterSaveSearch dbHelpersave;
    private DatabaseAdapterEx dbHelperex;
    Cursor traincursor;
    ListView lv;
    private ProgressDialog mProgressDialog;
    ArrayList<TimetableListBean> listOftrain;
    LayoutInflater infalter;
    ViewGroup header;
    TimeTableListAdapter adapter;
    TextView sourcestation;
    public static TextView stationname;
    public static TextView totaltrain;
	 Integer validstationfrom=0;
	 Integer validstationto=0;
	 Cursor fromstation;
	 Cursor tostation;
	 String fromStationNAme="";
     String tostation_ = "";
     String fromStationCode ="";
     String toStationCode ="";
    //ImageView savesearch;
     Button expressBttn;
     LinearLayout expresslayout;
     int currentapiVersion=0;
     LinearLayout adviewlayout;
     Context context;
     ConnectionDetector cd;
     Toolbar toolbar;
     DrawerLayout drawer;
    RelativeLayout headerRLay;
    private TabLayout tabLayout;
    private WrapContentHeightViewPager viewPager;
    private AppBarLayout appbar;
    TrainListViewPagerAdapter viewadapter;
    RelativeLayout viewpagerLay;
    SharedPreferences sp;
    CircleImageView profile_image;
    TextView nameTxt;
    RelativeLayout headerFrame;
    String email;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	//Remove title bar
    	  this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.anim_slide_in_left,
                R.anim.anim_slide_out_left);
        setContentView(R.layout.localtimetablelist);
        context = getApplicationContext();
        currentapiVersion = android.os.Build.VERSION.SDK_INT;
        dbHelper = new DatabaseAdapter(this);
        dbHelper.open();

        dbHelperex = new DatabaseAdapterEx(getApplicationContext());
        dbHelperex.open();

        sp = getSharedPreferences("themeColor", Context.MODE_PRIVATE);

        stationname = (TextView) findViewById(R.id.stationname);
        totaltrain = (TextView) findViewById(R.id.totaltrain);

        viewpagerLay = (RelativeLayout) findViewById(R.id.viewpagerLay);

        LocalTrainListFragment.callfrom = getIntent().getStringExtra("callfrom");
        /*if(currentapiVersion>=android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH)
        {
            Tracker t = ((GoogleAnalyticsApp) getApplication()).getTracker(TrackerName.APP_TRACKER);
            t.setScreenName(fromStationNAme + " To "+ tostation_+"- Localtrain");
            t.send(new HitBuilders.AppViewBuilder().build());

            AdView mAdView = (AdView) findViewById(R.id.adView);
            AdRequest adRequest = new AdRequest.Builder().build();
            mAdView.loadAd(adRequest);

        }*/



        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorLocalStatus));
        }*/
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);

        /*getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);*/


        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View headerview = navigationView.getHeaderView(0);
        profile_image = (CircleImageView) headerview.findViewById(R.id.profile_image);
        nameTxt = (TextView) headerview.findViewById(R.id.name);
        headerFrame = (RelativeLayout) headerview.findViewById(R.id.headerFrame);
        headerFrame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(email.equalsIgnoreCase("")){
                    Intent i = new Intent(TrainTimetable.this, WelcomeActivity.class);
                    startActivity(i);
                    finish();
                }else {
                    Intent i = new Intent(TrainTimetable.this, SettingsActivity.class);
                    startActivity(i);
                }
            }
        });
        ApplyUserInfo();


        /*NavigationView search_view = (NavigationView) findViewById(R.id.search_view);
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, findViewById(R.id.search_view));*/
        extras = getIntent().getExtras();
        if(extras !=null) {
            fromstationcode = extras.getString("fromstation");
            tostationcode = extras.getString("tostation");
            timefilter = extras.getString("timefilter");
            Log.e("timefilter==",""+timefilter);
            filterbytime = extras.getInt("filterbytime");
            tostationname = extras.getString("tostationname");
            fromstationname = extras.getString("fromstationname");

            //stationname.setText(fromstationname+" TO "+tostationname);

            getSupportActionBar().setTitle(fromstationcode+" TO "+tostationcode);
        } else{
            getSupportActionBar().setTitle("Train List");
        }

        //back icon Click Event
        /*toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 finish();
            }
        });*/

        new IsExpress().execute();
        headerRLay = (RelativeLayout) findViewById(R.id.headerRLay);
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        viewPager = (WrapContentHeightViewPager) findViewById(R.id.view_pager);
        appbar = (AppBarLayout) findViewById(R.id.appbar);

        tabLayout.addTab(tabLayout.newTab().setText("Local Train"));
        tabLayout.addTab(tabLayout.newTab().setText("Express Train"));

        tabLayout.setSelectedTabIndicatorColor(Color.WHITE);
        tabLayout.setSelectedTabIndicatorHeight(5);

        if(sp.getInt("localColor",0)!=0) {
            toolbar.setBackgroundColor(sp.getInt("localColor",0));
            tabLayout.setBackgroundColor(sp.getInt("localColor",0));
            headerRLay.setBackgroundColor(sp.getInt("localColor",0));

            int[] demo_colors = getResources().getIntArray(R.array.demo_colors);
            int[] demo_colors_mat = getResources().getIntArray(R.array.demo_colors_mat);

            ArrayList<String> colorString = new ArrayList<String>();
            ArrayList<String> colorString_mat = new ArrayList<String>();

            for (int i = 0; i < demo_colors.length; i++) {
                Log.e("Test", demo_colors[i] + "");
                //Log.e("Test", demo_colors[i] + "");
                colorString.add(String.valueOf(demo_colors[i]));

            }

            for (int i = 0; i < demo_colors_mat.length; i++) {
                Log.e("Test", demo_colors[i] + "");
                //Log.e("Test", demo_colors[i] + "");
                colorString_mat.add(String.valueOf(demo_colors_mat[i]));

            }
            Log.e("position ",colorString.indexOf(String.valueOf(sp.getInt("localColor",0))) + "");
            int color_mat_val = Integer.parseInt(colorString_mat.get(colorString.indexOf(String.valueOf(sp.getInt("localColor", 0)))));

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(color_mat_val);
            }
        }else{
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(getResources().getColor(R.color.colorAccent));
            }
        }

        viewadapter = new TrainListViewPagerAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount(),fromstationcode,tostationcode,timefilter,filterbytime);
        viewPager.setAdapter(viewadapter);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                //Toast.makeText(BookmarkSessionActivity.this, ""+tab.getPosition(), Toast.LENGTH_LONG).show();

                if(tab.getPosition()==1) {
                    //mTitle.setText("Notes - "+getIntent().getStringExtra("SUB_MENU_NAME"));
                    /*toolbar.setBackgroundColor(getResources().getColor(R.color.colorExpress));
                    tabLayout.setBackgroundColor(getResources().getColor(R.color.colorExpress));
                    headerRLay.setBackgroundColor(getResources().getColor(R.color.colorExpress));
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        Window window = getWindow();
                        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                        window.setStatusBarColor(getResources().getColor(R.color.colorExpressStatus));
                    }*/

                    if(sp.getInt("expressColor",0)!=0) {
                        toolbar.setBackgroundColor(sp.getInt("expressColor", 0));
                        tabLayout.setBackgroundColor(sp.getInt("expressColor", 0));
                        headerRLay.setBackgroundColor(sp.getInt("expressColor", 0));

                        int[] demo_colors = getResources().getIntArray(R.array.demo_colors);
                        int[] demo_colors_mat = getResources().getIntArray(R.array.demo_colors_mat);

                        ArrayList<String> colorString = new ArrayList<String>();
                        ArrayList<String> colorString_mat = new ArrayList<String>();

                        for (int i = 0; i < demo_colors.length; i++) {
                            Log.e("Test", demo_colors[i] + "");
                            //Log.e("Test", demo_colors[i] + "");
                            colorString.add(String.valueOf(demo_colors[i]));

                        }

                        for (int i = 0; i < demo_colors_mat.length; i++) {
                            Log.e("Test", demo_colors[i] + "");
                            //Log.e("Test", demo_colors[i] + "");
                            colorString_mat.add(String.valueOf(demo_colors_mat[i]));

                        }
                        Log.e("position ",colorString.indexOf(String.valueOf(sp.getInt("expressColor",0))) + "");
                        int color_mat_val = Integer.parseInt(colorString_mat.get(colorString.indexOf(String.valueOf(sp.getInt("expressColor", 0)))));

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            Window window = getWindow();
                            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                            window.setStatusBarColor(color_mat_val);
                        }

                    }else{
                        toolbar.setBackgroundColor(getResources().getColor(R.color.colorExpress));
                        tabLayout.setBackgroundColor(getResources().getColor(R.color.colorExpress));
                        headerRLay.setBackgroundColor(getResources().getColor(R.color.colorExpress));
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            Window window = getWindow();
                            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                            window.setStatusBarColor(getResources().getColor(R.color.colorExpressStatus));
                        }
                    }
                }
                else{
                    /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        Window window = getWindow();
                        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                        window.setStatusBarColor(getResources().getColor(R.color.colorLocalStatus));
                    }
                    //mTitle.setText("Bookmark - "+getIntent().getStringExtra("SUB_MENU_NAME"));
                    headerRLay.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                    toolbar.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                    tabLayout.setBackgroundColor(getResources().getColor(R.color.colorAccent));*/

                    if(sp.getInt("localColor",0)!=0) {

                        int[] demo_colors = getResources().getIntArray(R.array.demo_colors);
                        int[] demo_colors_mat = getResources().getIntArray(R.array.demo_colors_mat);

                        ArrayList<String> colorString = new ArrayList<String>();
                        ArrayList<String> colorString_mat = new ArrayList<String>();

                        for (int i = 0; i < demo_colors.length; i++) {
                            Log.e("Test", demo_colors[i] + "");
                            //Log.e("Test", demo_colors[i] + "");
                            colorString.add(String.valueOf(demo_colors[i]));

                        }

                        for (int i = 0; i < demo_colors_mat.length; i++) {
                            Log.e("Test", demo_colors[i] + "");
                            //Log.e("Test", demo_colors[i] + "");
                            colorString_mat.add(String.valueOf(demo_colors_mat[i]));

                        }
                        Log.e("position ",colorString.indexOf(String.valueOf(sp.getInt("localColor",0))) + "");
                        int color_mat_val = Integer.parseInt(colorString_mat.get(colorString.indexOf(String.valueOf(sp.getInt("localColor", 0)))));


                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            Window window = getWindow();
                            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                            window.setStatusBarColor(color_mat_val);
                        }
                        //mTitle.setText("Bookmark - "+getIntent().getStringExtra("SUB_MENU_NAME"));
                        headerRLay.setBackgroundColor(sp.getInt("localColor",0));
                        toolbar.setBackgroundColor(sp.getInt("localColor",0));
                        tabLayout.setBackgroundColor(sp.getInt("localColor",0));
                    }else{
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            Window window = getWindow();
                            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                            window.setStatusBarColor(getResources().getColor(R.color.colorLocalStatus));
                        }
                        //mTitle.setText("Bookmark - "+getIntent().getStringExtra("SUB_MENU_NAME"));
                        headerRLay.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                        toolbar.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                        tabLayout.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                    }
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        fromstation =dbHelper.getStationName(fromstationcode);
        tostation =dbHelper.getStationName(tostationcode);

        if(fromstation.getCount()>0)
        {

            while (fromstation.moveToNext()) {


                validstationfrom=1;

                fromStationNAme = fromstation.getString(fromstation.getColumnIndexOrThrow("stationName"));
                fromStationCode = fromstation.getString(fromstation.getColumnIndexOrThrow("stationCode"));


            }
        }
        fromstation.close();

        if(tostation.getCount()>0)
        {

            while (tostation.moveToNext()) {


                validstationto=1;

                tostation_ = tostation.getString(tostation.getColumnIndexOrThrow("stationName"));
                toStationCode = tostation.getString(tostation.getColumnIndexOrThrow("stationCode"));



            }
        }
        tostation.close();

        if(currentapiVersion>=android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH)
        {
            Tracker t = ((GoogleAnalyticsApp) getApplication()).getTracker(TrackerName.APP_TRACKER);
            t.setScreenName(fromStationNAme + " To "+ tostation_+"- Localtrain");
            t.send(new HitBuilders.AppViewBuilder().build());

            ConnectionDetector cd = new ConnectionDetector(getApplicationContext());

            // Check if Internet present
            if (!cd.isConnectingToInternet()) {
                //Toast.makeText(getApplicationContext(),"Internet Problem!", Toast.LENGTH_SHORT).show();
            }
            else
            {
                AdView mAdView = (AdView) findViewById(R.id.adView);
                AdRequest adRequest = new AdRequest.Builder().build();
                mAdView.loadAd(adRequest);

                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)viewpagerLay.getLayoutParams();
                params.setMargins(0, 0, 0, 100);
                viewpagerLay.setLayoutParams(params);
                viewpagerLay.requestLayout();
            }


        }

        /*savesearch =(ImageView)findViewById(R.id.savesearch);
        savesearch.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // Database Connection Open
                dbHelpersave = new DatabaseAdapterSaveSearch(getApplicationContext());
                dbHelpersave.open();
                boolean createSuccessful = false;
                if(dbHelpersave.getcount(fromstationcode, tostationcode,1)==0 && validstationfrom==1 && validstationto==1 )
                {
                    createSuccessful=dbHelpersave.InsertStationSave(fromStationNAme+" To "+tostation_, fromstationcode, tostationcode, 1);
                    if(createSuccessful)
                        SavesuccessAlert();
                    else
                        SaveErrorSAlert();
                }
                else
                {
                    SaveErrorAlert();
                }
                dbHelpersave.close();

            }
        });*/

    }

    public void ApplyUserInfo(){
        SharedPreferences userPref = getSharedPreferences("userCred",MODE_PRIVATE);
        String name = userPref.getString("name","Guest User");
        email = userPref.getString("email","");
        String imageUrl = userPref.getString("image_value","");

        Log.e("imageUrl ",imageUrl);
        nameTxt.setText(name);
        if(!imageUrl.equalsIgnoreCase("")) {
            Picasso.with(context).load(imageUrl).memoryPolicy(MemoryPolicy.NO_CACHE).into(profile_image);
        }
    }

    public void SavesuccessAlert()
    {

        AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(
                TrainTimetable.this);

        // Setting Dialog Title
        alertDialog2.setTitle("Alert");

        // Setting Dialog Message
        alertDialog2.setMessage("Successfully Save your search.");

        // Setting Icon to Dialog


        // Setting Positive "Yes" Btn
        alertDialog2.setNeutralButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog




                    }
                });
        alertDialog2.show();

    }

    public void SaveErrorSAlert()
    {

        AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(
                TrainTimetable.this);

        // Setting Dialog Title
        alertDialog2.setTitle("Alert");

        // Setting Dialog Message
        alertDialog2.setMessage("Something is Wrong");

        // Setting Icon to Dialog


        // Setting Positive "Yes" Btn
        alertDialog2.setNeutralButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog




                    }
                });
        alertDialog2.show();

    }

    public void SaveErrorAlert()
    {

        AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(
                TrainTimetable.this);

        // Setting Dialog Title
        alertDialog2.setTitle("Alert");

        // Setting Dialog Message
        alertDialog2.setMessage("Already Added!!");

        // Setting Icon to Dialog


        // Setting Positive "Yes" Btn
        alertDialog2.setNeutralButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog




                    }
                });
        alertDialog2.show();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.train_list_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_saveroute) {
            //Toast.makeText(TrainTimetable.this,"CLICKED",Toast.LENGTH_SHORT).show();

            dbHelpersave = new DatabaseAdapterSaveSearch(getApplicationContext());
            dbHelpersave.open();
            boolean createSuccessful = false;
            if(dbHelpersave.getcount(fromstationcode, tostationcode,1)==0 && validstationfrom==1 && validstationto==1 )
            {
                createSuccessful=dbHelpersave.InsertStationSave(fromStationNAme+" To "+tostation_, fromstationcode, tostationcode, 1);
                if(createSuccessful)
                    SavesuccessAlert();
                else
                    SaveErrorSAlert();
            }
            else
            {
                SaveErrorAlert();
            }
            dbHelpersave.close();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the camera action
            Intent intent = new Intent(getApplicationContext(), LocalTrain.class);
            intent.putExtra("callfrom","other");
            startActivity(intent);
            finish();

        } else if (id == R.id.nav_runningstatus) {

            Intent intent = new Intent(getApplicationContext(), Homepage.class);
            startActivity(intent);
            finish();

        } else if (id == R.id.nav_cancelled) {

            Intent intent = new Intent(getApplicationContext(), ActivityCancelTrain.class);
            startActivity(intent);
            finish();

        }  else if (id == R.id.nav_pnrstatus) {

            Intent intent = new Intent(getApplicationContext(), Pnr.class);
            startActivity(intent);
            finish();

        } else if (id == R.id.nav_saveroute) {

            Intent intent = new Intent(getApplicationContext(), SaveSearch.class);
            startActivity(intent);
            finish();

        } else if (id == R.id.nav_theme) {

            if(email.equalsIgnoreCase("")){
                Intent i = new Intent(TrainTimetable.this, WelcomeActivity.class);
                startActivity(i);
                finish();
            }else {
                Intent i = new Intent(TrainTimetable.this, SettingsActivity.class);
                startActivity(i);
            }


        } else if (id == R.id.nav_feedback) {
            Intent intent = new Intent(getApplicationContext(), feedback.class);
            startActivity(intent);
            finish();

        } else if (id == R.id.nav_fblike) {

            Intent notificationIntent =null;
            notificationIntent = new Intent(Intent.ACTION_VIEW);
            notificationIntent.setData(Uri.parse("https://www.facebook.com/localtimetable"));
            startActivity(notificationIntent);

        } else if (id == R.id.nav_rate) {

            Intent notificationIntent =null;
            notificationIntent = new Intent(Intent.ACTION_VIEW);
            notificationIntent.setData(Uri.parse("market://details?id="+getApplicationContext().getPackageName()));
            startActivity(notificationIntent);

        } else if (id == R.id.nav_aboutus) {

            Intent intent = new Intent(getApplicationContext(), BuildWebsite.class);
            startActivity(intent);

        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private class IsExpress extends AsyncTask<String, String, Integer> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Integer doInBackground(String... arg0) {



            Cursor isExpress = dbHelperex.isExtrainAvailable(fromstationcode, tostationcode);

            Integer isexcount =isExpress.getCount();
            isExpress.close();

            return isexcount;
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);

            if(result>0)
            {
                //expresslayout.setVisibility(View.VISIBLE);
                Log.e("IS EXPREE==","TRUE");

            } else{
                Log.e("IS EXPREE==","FALSE");
                tabLayout.setVisibility(View.GONE);
                tabLayout.removeTab(tabLayout.getTabAt(1));


                viewPager.setPagingEnabled(false);

            }
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(TrainTimetable.this, LocalTrain.class);
        i.putExtra("callfrom","other");
        startActivity(i);
        finish();
        overridePendingTransition(R.anim.anim_slide_in_right,
                R.anim.anim_slide_out_right);

    }
    @Override
    protected void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        if(currentapiVersion>=android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH)
        {
            GoogleAnalytics.getInstance(TrainTimetable.this).reportActivityStart(this);
        }
    }


    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        if(currentapiVersion>=android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH)
        {
            GoogleAnalytics.getInstance(TrainTimetable.this).reportActivityStop(this);
        }
    }
}