package com.matainja.runingstatus;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.matainja.runingstatus.Common.ConnectionDetector;
import com.matainja.runingstatus.Connectivity.HttpConnection;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

/**
 * Created by matainja on 10-Aug-17.
 */

public class WelcomeActivity extends AppCompatActivity implements View.OnClickListener,GoogleApiClient.OnConnectionFailedListener{

    LinearLayout facebookBtn,googleBtn;
    CallbackManager callbackManager;
    String f_uid,firstname,lastname,gender,email,uname;
    URL image_value = null;
    private GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 007;
    String TAG = "RESPONSE";
    private ProgressDialog mProgressDialog;
    String name;
    TextView skip;
    SharedPreferences userPref;
    String personPhotoUrl = "";
    ConnectionDetector cd;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(WelcomeActivity.this);
        setContentView(R.layout.layout_welcome);

        cd = new ConnectionDetector(getApplicationContext());
        skip = (TextView) findViewById(R.id.skip);
        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i3 = new Intent(WelcomeActivity.this, LocalTrain.class);
                i3.putExtra("callfrom","other");
                startActivity(i3);
                finish();
            }
        });
        userPref = getSharedPreferences("userCred", Context.MODE_PRIVATE);
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.matainja.runingstatus",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }

        if (Build.VERSION.SDK_INT > 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }

        facebookBtn = (LinearLayout) findViewById(R.id.facebookBtn);
        facebookBtn.setOnClickListener(this);
        //LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile"));

        googleBtn = (LinearLayout) findViewById(R.id.googleBtn);
        googleBtn.setOnClickListener(this);

        LoginWithFaceBook();

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestId()
                .requestProfile()
                .requestId()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        //Auth.GoogleSignInApi.signOut(mGoogleApiClient);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }

    }
    public void LoginWithFaceBook(){
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                Log.e("loginResult",""+loginResult.getAccessToken());

                Bundle bundle = new Bundle();
                bundle.putString("fields", "first_name,last_name,gender,email");
                new GraphRequest(
                        AccessToken.getCurrentAccessToken(),
                        "me",
                        bundle,
                        HttpMethod.GET,
                        new GraphRequest.Callback() {
                            public void onCompleted(GraphResponse response) {
                                // handle the result
                                JSONObject fresponse = response.getJSONObject();
                                Log.e("result", fresponse.toString() + "");
                                try {
                                    f_uid = fresponse.getString("id");
                                    //if(fresponse.has("email")){

                                    //}

                                    if (fresponse.has("first_name")) {
                                        firstname = fresponse.getString("first_name");
                                    } else {
                                        firstname = " ";
                                    }
                                    if (fresponse.has("last_name")) {
                                        lastname = fresponse.getString("last_name");
                                    } else {
                                        lastname = " ";
                                    }
                                    if (fresponse.has("gender")) {
                                        gender = fresponse.getString("gender");
                                    } else {
                                        gender = " ";
                                    }

                                    if (fresponse.has("email")) {
                                        email = fresponse.getString("email");
                                        Log.e("EMAIL-----", email);
                                    } else {
                                        email = " ";
                                    }
                                    if (fresponse.has("name")) {
                                        uname = fresponse.getString("name");
                                    } else {
                                        uname = firstname;
                                    }

                                    Log.e("CRED ** ",uname+" "+firstname);


                                    try {
                                        personPhotoUrl = "https://graph.facebook.com/" + f_uid + "/picture?type=large";
                                        image_value = new URL("https://graph.facebook.com/" + f_uid + "/picture?type=large");
                                        Log.e("image_value **",""+image_value);

                                    } catch (MalformedURLException e) {
                                        e.printStackTrace();
                                    }

                                } catch (JSONException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                } catch(Exception e){
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }

                                name = firstname+" "+lastname;
                                new StoreUserAuth().execute(email,name,personPhotoUrl);
                                //"http://graph.facebook.com/1545956945466472/picture?type=large"
                                //new ProcessFbRegister().execute();
                               /* Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {*/
                                        Log.e("image_value **",""+image_value);
                                        /*Intent i = new Intent(WelcomeActivity.this,LocalTrain.class);
                                        i.putExtra("callfrom","other");
                                        i.putExtra("name",firstname+" "+lastname);
                                        i.putExtra("email",email);
                                        i.putExtra("image_value",image_value.toString());
                                        startActivity(i);
                                        finish();*/
                                    /*}
                                },3000);*/

                            }
                        }
                ).executeAsync();
            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Log.e("FacebookException",""+exception);
            }
        });
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.e(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            Log.e(TAG, "display name: " + acct.getDisplayName());
            name = acct.getDisplayName();
            String personName = acct.getDisplayName();
            String email = acct.getEmail();

            try {
                personPhotoUrl = acct.getPhotoUrl().toString();
                image_value = new URL(personPhotoUrl);

            }catch(NullPointerException e){
                e.printStackTrace();   
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            Log.e(TAG, "Name: " + personName + ", email: " + email
                    + ", Image: " + personPhotoUrl);

            new StoreUserAuth().execute(email,name,personPhotoUrl);


            /*txtName.setText(personName);
            txtEmail.setText(email);
            Glide.with(getApplicationContext()).load(personPhotoUrl)
                    .thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imgProfilePic);

            updateUI(true);*/
        } else {
            // Signed out, show unauthenticated UI.
            //updateUI(false);
        }
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.facebookBtn:
                if (cd.isConnectingToInternet()) {
                    LoginManager.getInstance().logInWithReadPermissions(WelcomeActivity.this, Arrays.asList("public_profile", "email"));
                }else{
                    Toast.makeText(WelcomeActivity.this,"Please Connect To Internet",Toast.LENGTH_LONG).show();
                }
                break;

            case R.id.googleBtn:
                if (cd.isConnectingToInternet()) {
                    signIn();
                }else{
                    Toast.makeText(WelcomeActivity.this,"Please Connect To Internet",Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(TAG, "onConnectionFailed:" + connectionResult);
    }

    @Override
    public void onStart() {
        super.onStart();

        /*OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
        if (opr.isDone()) {
            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
            // and the GoogleSignInResult will be available instantly.
            Log.e(TAG, "Got cached sign-in");
            GoogleSignInResult result = opr.get();
            handleSignInResult(result);
        } else {
            // If the user has not previously signed in on this device or the sign-in has expired,
            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
            // single sign-on will occur in this branch.
            showProgressDialog();
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(GoogleSignInResult googleSignInResult) {
                    hideProgressDialog();
                    handleSignInResult(googleSignInResult);
                }
            });
        }*/
    }

    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("Please Wait...");
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }


    public class StoreUserAuth extends AsyncTask<String, Void, String>{

        @Override
        protected void onPreExecute(){
            showProgressDialog();
        }
        @Override
        protected String doInBackground(String... params) {

            HttpConnection con = new HttpConnection();
            JSONObject json = con.AuthenticateUser(params[0],params[1],params[2]);

            return json.toString();
        }

        @Override
        protected void onPostExecute(String result) {

            try {
                JSONObject response = new JSONObject(result);
                hideProgressDialog();
                if (response.getString("error_no").equalsIgnoreCase("0")) {
                    String user = response.getString("user");
                    JSONObject userObj = new JSONObject(user);
                    Log.e("name",name);
                    SharedPreferences.Editor editor = userPref.edit();
                    editor.putString("name", name);
                    editor.putString("email", userObj.getString("email"));
                    editor.putString("image_value", personPhotoUrl);
                    editor.commit();

                    Intent i = new Intent(WelcomeActivity.this, LocalTrain.class);
                    i.putExtra("callfrom", "other");

                    startActivity(i);
                    finish();
                } else {

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        /*String tag_json_obj = "json_obj_req";

        showProgressDialog();

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                "http://www.demoinja.com/suhrit/auth.php", null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e(TAG, response.toString());
                        hideProgressDialog();

                        try {
                            if(response.getString("error_no").equalsIgnoreCase("0")) {
                                SharedPreferences.Editor editor = userPref.edit();
                                editor.putString("name", name);
                                editor.putString("email", email);
                                editor.putString("image_value", image_value.toString());
                                editor.commit();

                                Intent i = new Intent(WelcomeActivity.this, LocalTrain.class);
                                i.putExtra("callfrom", "other");

                                startActivity(i);
                                finish();
                            }else{

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERROR REPO",""+error);
                VolleyLog.e(TAG, "Error: " + error.getMessage());
                hideProgressDialog();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");

                Log.e("HEADER LOG",""+params);
                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", email);
                params.put("token", "timetable0592");
                params.put("phone", "");
                params.put("iemi", "");
                params.put("notify_key", "");
                params.put("os_type", "");

                Log.e("PARAM == ",""+params);
                return params;
            }

        };

// Adding request to request queue
        GoogleAnalyticsApp.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);*/
        }
    }
}
