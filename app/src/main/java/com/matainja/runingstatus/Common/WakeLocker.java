package com.matainja.runingstatus.Common;

import android.app.KeyguardManager;
import android.content.Context;
import android.os.PowerManager;

public abstract class WakeLocker {
    private static PowerManager.WakeLock wakeLock;

    @SuppressWarnings("deprecation")
	public static void acquire(Context context) {
    	
        if (wakeLock != null)
        	wakeLock.release();
        if (wakeLock == null)
        {
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        wakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK |
                PowerManager.ACQUIRE_CAUSES_WAKEUP |
                PowerManager.ON_AFTER_RELEASE, "WakeLock");
        wakeLock.acquire();
        }
        
        
        
    }

    public static void release() {
        if (wakeLock != null) wakeLock.release(); wakeLock = null;
    }
    
    public static boolean isheld() {
        return wakeLock.isHeld();
    }
    
    public static boolean islock(Context context) {


KeyguardManager myKeyManager = (KeyguardManager)context.getSystemService(Context.KEYGUARD_SERVICE);

if( myKeyManager.inKeyguardRestrictedInputMode()) {

 return true;

} else {

	return false;
 //screen is not locked

}


    }
    
}
