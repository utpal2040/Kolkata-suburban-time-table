package com.matainja.runingstatus.Common;

/**
 * Created by matainja on 23-Feb-17.
 */

public class Constants {
    public static boolean isSelectedFrom = false;
    public static boolean isSelectedto = false;

    public static boolean isSelectedFromEx = false;
    public static boolean isSelectedtoEx = false;

    public static boolean timechkd = false;
    //public static boolean isSelectedtoEx = false;

    //public static ArrayList<RecentSearch>= null;
    public static String USER_SHARED_PREF_VAR = "UserPref";

    public static String ERR_FROMSTATION = "Enter Source Station Name";
    public static String ERR_TOSTATION = "Enter Destination Station Name";

    public static String FROMSTATIONCODE_LOCAL = "";
    public static String TOSTATIONCODE_LOCAL = "";

    public static String EX_FROMSTATIONCODE = "";
    public static String EX_TOSTATIONCODE = "";

    public static String EX_GETSTATUS_URL = "http://matainja.com/androidApp/Api/v3/livetrain.php";

    public static String EX_GETCANCEL_URL = "http://matainja.com/androidApp/Api/v3/cancel.php";

    public static String CREATE_USER = "http://matainja.com/androidApp/Api/v3/createuser.php";
}
