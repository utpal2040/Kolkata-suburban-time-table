package com.matainja.runingstatus;


import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.matainja.runingstatus.Adapter.CustomAdapterClass;
import com.matainja.runingstatus.Adapter.CustomAdapterTypetckt;
import com.matainja.runingstatus.Common.ConnectionDetector;
import com.matainja.runingstatus.GoogleAnalyticsApp.TrackerName;
import com.matainja.runingstatus.Model.TypeClass;
import com.matainja.runingstatus.Model.TypeTicket;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;




public class AvailableActivity extends AppCompatActivity implements View.OnClickListener{
	
	AvailableActivity activity;
	public  ArrayList<TypeClass> TypeclassSpinner = new ArrayList<TypeClass>();
	
	 public  ArrayList<TypeTicket> TypetcktSpinner = new ArrayList<TypeTicket>();
	 CustomAdapterClass spinnerClassadapter;
	
	 CustomAdapterTypetckt spinnertcktadapter;
	 String mClass="SL";
     SharedPreferences sp;
	 String mTicket="GN";
	 private int year, month, day;
	 private Calendar calendar;
	 private DatePicker datePicker;
	 TextView jdate;
	 Bundle extras;
	 String trainName;
	 String fromstation;
	 String tostation,tomDate;
	 int trainId;
	 String trainNo,mjourneydate,jsonerror,todayAsString,tomorrowAsString;
	 Button av_get;
	TextView Ltrainname,Lstation,Ldstation,jhdate,train_no,todaydatetext,tomotextdatetext,tomodaytext,todaydaytext;
	int currentapiVersion=0;
	Toolbar toolbar;
	RelativeLayout RlayAd;

    RelativeLayout FCRLayuout,E3RLayout,A1RLayout,A3RLayout,A2RLayout,CCRlayout,S2RLayout,SLLLayout;
    TextView FC,E3,A1,A3,A2,CC,S2,SL;

    CheckBox gen,tat,pre_tat,phy_handi,ladQ,seniCity;
    ImageView imageView2;
    RelativeLayout DateRLay,TomoRLay,TodayRLay;
    TextView todaytext,tomotext,jourtext;
    View horizonView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	//Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.available_box_frame);
        overridePendingTransition(R.anim.anim_slide_in_left,
                R.anim.anim_slide_out_left);
		toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
                overridePendingTransition(R.anim.anim_slide_in_right,
                        R.anim.anim_slide_out_right);
			}
		});

        sp = getSharedPreferences("themeColor", Context.MODE_PRIVATE);
		/*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			Window window = getWindow();
			window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
			window.setStatusBarColor(getResources().getColor(R.color.colorExpress));
		}*/
		//displayNotification();
        // view width calculate
		RlayAd = (RelativeLayout) findViewById(R.id.RlayAd);
        jdate = (TextView)findViewById(R.id.jdate);
        //jhdate  = (TextView)findViewById(R.id.jhdate);
        Ltrainname = (TextView)findViewById(R.id.Ltrainname);
		train_no = (TextView) findViewById(R.id.trainId);
        Lstation = (TextView)findViewById(R.id.Lstation);
        Ldstation = (TextView)findViewById(R.id.Ldstation);
        av_get = (Button)findViewById(R.id.av_get);

        FCRLayuout = (RelativeLayout) findViewById(R.id.FCRLayuout);
        E3RLayout = (RelativeLayout) findViewById(R.id.E3RLayout);
        A1RLayout = (RelativeLayout) findViewById(R.id.A1RLayout);
        A3RLayout = (RelativeLayout) findViewById(R.id.A3RLayout);
        A2RLayout = (RelativeLayout) findViewById(R.id.A2RLayout);
        CCRlayout = (RelativeLayout) findViewById(R.id.CCRlayout);
        S2RLayout = (RelativeLayout) findViewById(R.id.S2RLayout);
        SLLLayout = (RelativeLayout) findViewById(R.id.SLLLayout);

        FC = (TextView) findViewById(R.id.FC);
        E3 = (TextView) findViewById(R.id.E3);
        A1 = (TextView) findViewById(R.id.A1);
        A3 = (TextView) findViewById(R.id.A3);
        A2 = (TextView) findViewById(R.id.A2);
        CC = (TextView) findViewById(R.id.cc);
        S2 = (TextView) findViewById(R.id.s2);
        SL = (TextView) findViewById(R.id.sl);

        FCRLayuout.setOnClickListener(this);
        E3RLayout.setOnClickListener(this);
        A1RLayout.setOnClickListener(this);
        A3RLayout.setOnClickListener(this);
        A2RLayout.setOnClickListener(this);
        CCRlayout.setOnClickListener(this);
        S2RLayout.setOnClickListener(this);
        SLLLayout.setOnClickListener(this);

		todaydatetext = (TextView) findViewById(R.id.todaydatetext);
		tomotextdatetext = (TextView) findViewById(R.id.tomotextdatetext);
        todaydaytext = (TextView) findViewById(R.id.todaydaytext);
        tomodaytext = (TextView) findViewById(R.id.tomodaytext);

        todaytext = (TextView) findViewById(R.id.todaytext);
        tomotext = (TextView) findViewById(R.id.tomotext);
        jourtext = (TextView) findViewById(R.id.jourtext);

        horizonView = (View)findViewById(R.id.horizonView);
        gen = (CheckBox) findViewById(R.id.gen);
        tat = (CheckBox) findViewById(R.id.tat);
        pre_tat = (CheckBox) findViewById(R.id.pre_tat);
        phy_handi = (CheckBox) findViewById(R.id.phy_handi);
        ladQ = (CheckBox) findViewById(R.id.ladQ);
        seniCity = (CheckBox) findViewById(R.id.seniCity);

        DateRLay = (RelativeLayout) findViewById(R.id.DateRLay);
        TomoRLay = (RelativeLayout) findViewById(R.id.TomoRLay);
        TodayRLay = (RelativeLayout) findViewById(R.id.TodayRLay);
        imageView2 = (ImageView) findViewById(R.id.imageView2);



        if(sp.getInt("expressColor",0)!=0) {
            toolbar.setBackgroundColor(sp.getInt("expressColor",0));
            av_get.setBackgroundColor(sp.getInt("expressColor",0));
            Lstation.setTextColor(sp.getInt("expressColor",0));
            Ldstation.setTextColor(sp.getInt("expressColor",0));
            imageView2.setColorFilter(sp.getInt("expressColor",0));
            TodayRLay.setBackgroundColor(sp.getInt("expressColor",0));
            horizonView.setBackgroundColor(sp.getInt("expressColor",0));
            SLLLayout.setBackgroundColor(sp.getInt("expressColor",0));

            int[] demo_colors = getResources().getIntArray(R.array.demo_colors);
            int[] demo_colors_mat = getResources().getIntArray(R.array.demo_colors_mat);

            ArrayList<String> colorString = new ArrayList<String>();
            ArrayList<String> colorString_mat = new ArrayList<String>();

            for (int i = 0; i < demo_colors.length; i++) {

                colorString.add(String.valueOf(demo_colors[i]));

            }

            for (int i = 0; i < demo_colors_mat.length; i++) {

                colorString_mat.add(String.valueOf(demo_colors_mat[i]));

            }
            Log.e("position ",colorString.indexOf(String.valueOf(sp.getInt("expressColor",0))) + "");
            int color_mat_val = Integer.parseInt(colorString_mat.get(colorString.indexOf(String.valueOf(sp.getInt("expressColor", 0)))));

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(color_mat_val);


            }
        }else{
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(getResources().getColor(R.color.colorExpressStatus));
            }
        }

        /*---------------- TICKET TYPE EVENTS -----------------*/
        gen.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    // perform logic
                    mTicket = "GN";

                    tat.setChecked(false);
                    pre_tat.setChecked(false);
                    phy_handi.setChecked(false);
                    ladQ.setChecked(false);
                    seniCity.setChecked(false);
                }

            }
        });

        tat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    // perform logic
                    mTicket = "CK";

                    gen.setChecked(false);
                    pre_tat.setChecked(false);
                    phy_handi.setChecked(false);
                    ladQ.setChecked(false);
                    seniCity.setChecked(false);
                }

            }
        });

        pre_tat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    // perform logic
                    mTicket = "PT";

                    gen.setChecked(false);
                    tat.setChecked(false);
                    phy_handi.setChecked(false);
                    ladQ.setChecked(false);
                    seniCity.setChecked(false);
                }

            }
        });

        phy_handi.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    // perform logic
                    mTicket = "HP";

                    gen.setChecked(false);
                    tat.setChecked(false);
                    pre_tat.setChecked(false);
                    ladQ.setChecked(false);
                    seniCity.setChecked(false);
                }

            }
        });

        ladQ.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    // perform logic
                    mTicket = "LD";

                    gen.setChecked(false);
                    tat.setChecked(false);
                    pre_tat.setChecked(false);
                    phy_handi.setChecked(false);
                    seniCity.setChecked(false);
                }

            }
        });

        seniCity.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    // perform logic
                    mTicket = "SS";

                    gen.setChecked(false);
                    tat.setChecked(false);
                    pre_tat.setChecked(false);
                    phy_handi.setChecked(false);
                    ladQ.setChecked(false);
                }

            }
        });


		activity = this;

         calendar = Calendar.getInstance();
         year = calendar.get(Calendar.YEAR);
         month = calendar.get(Calendar.MONTH);
         day = calendar.get(Calendar.DAY_OF_MONTH);

        int mnt = (month+1);
        String newmnt = null;
        if(mnt < 10){
            newmnt = "0"+mnt;
        } else{
            newmnt = String.valueOf(mnt);
        }
        Log.e("DATE====",""+day+"-"+newmnt+"-"+year);

        mjourneydate = day+"-"+newmnt+"-"+year;

        Date today = calendar.getTime();
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        Date tomorrow = calendar.getTime();

        String current_day = new SimpleDateFormat("EEEE", Locale.ENGLISH).format(today.getTime());
        String tomorrow_day = new SimpleDateFormat("EEEE", Locale.ENGLISH).format(tomorrow.getTime());

        DateFormat dateFormat = new SimpleDateFormat("dd MMM");

        DateFormat dateFormat_tom = new SimpleDateFormat("dd-MM-yyyy");
        tomDate = dateFormat_tom.format(tomorrow);

        Log.e("tomDate====",tomDate);

        todayAsString = dateFormat.format(today);
        tomorrowAsString = dateFormat.format(tomorrow);

        Log.e("todayAsString",""+year);
        Log.e("todayAsString",tomorrow_day);

		todaydatetext.setText(todayAsString.toUpperCase());
		tomotextdatetext.setText(tomorrowAsString.toUpperCase());

        todaydaytext.setText(current_day+" "+year);
        tomodaytext.setText(tomorrow_day+" "+year);

         extras = getIntent().getExtras();
	      if(extras !=null) {
	    	  trainId = extras.getInt("trainId");
	    	  trainNo =extras.getString("trainNo"); 
	    	  fromstation = extras.getString("fromstation");
	    	  tostation = extras.getString("tostation");
	    	  trainName = extras.getString("trainName");
			}
	      if(trainName!=null)
	      {
	    	  Ltrainname.setText(trainName);
			  train_no.setText(trainNo);
	    	  Lstation.setText(fromstation);
	    	  Ldstation.setText(tostation);
	      }

		ConnectionDetector cd = new ConnectionDetector(getApplicationContext());

		// Check if Internet present
		if (!cd.isConnectingToInternet()) {
			RlayAd.setVisibility(View.GONE);
		}
	      Handler handler = new Handler();
	        Runnable r = new Runnable() {
	            public void run() {
	            	 /* TRacking Code */
	                currentapiVersion = android.os.Build.VERSION.SDK_INT;
	                if(currentapiVersion>=android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	                {
	                	/* TRacking Code */
	      		      
	      		      Tracker t = ((GoogleAnalyticsApp) getApplication()).getTracker(TrackerName.APP_TRACKER);
	      				t.setScreenName(trainNo+"- Details Localtrain");
	      				t.send(new HitBuilders.AppViewBuilder().build());

						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								AdView mAdView = (AdView) findViewById(R.id.adView);
								AdRequest adRequest = new AdRequest.Builder().build();
								mAdView.loadAd(adRequest);
							}
						});

	                }      
	            }
	        };
	        handler.postDelayed(r, 200);
	      
	      /*ImageView  	menuback =(ImageView)findViewById(R.id.backbutton);
	         menuback.setOnClickListener(new View.OnClickListener() {
	         	
	             @Override
	             public void onClick(View v) {
	                 // TODO Auto-generated method stub
	            	 //overridePendingTransition(R.anim.lefttoright, R.anim.righttoleft);
		             	finish();
		             	 //overridePendingTransition(R.anim.lefttoright, R.anim.righttoleft);
	             }
	         });*/
			
        /* class drop down */
		 //Spinner  Spinnerclass = (Spinner)findViewById(R.id.typeclass);
		
		 //setListData();
		// Resources passed to adapter to get image
	        
		 //spinnerClassadapter = new CustomAdapterClass(activity, R.layout.spinner_row, TypeclassSpinner);
	        
		// Set adapter to spinner
		 /*Spinnerclass.setAdapter(spinnerClassadapter);
		 Spinnerclass.setOnItemSelectedListener(new OnItemSelectedListener() {
	            @Override
	            public void onItemSelected(AdapterView<?> parentView, View v, int position, long id) {
	                // your code here
	                 
	                // Get selected row data to show on screen
	                
	                TypeClass classtypeval = (TypeClass) TypeclassSpinner.get(position);
	                mClass=classtypeval.getValue();
	                Log.e("Select Item","hh="+mClass);
	            }
	 
	            @Override
	            public void onNothingSelected(AdapterView<?> parentView) {
	                // your code here
	            }
	 
	        });*/
		 /* class drop  End down */
		 
		 

		 /* Ticket type drop down */
		 //Spinner  Spinnertckt = (Spinner)findViewById(R.id.tckttype);
		
		 //setTicketType();
		// Resources passed to adapter to get image
	        
		//spinnertcktadapter = new CustomAdapterTypetckt(activity, R.layout.spinner_row, TypetcktSpinner);
	        
		// Set adapter to spinner
		 /*Spinnertckt.setAdapter(spinnertcktadapter);
		 Spinnertckt.setOnItemSelectedListener(new OnItemSelectedListener() {
	            @Override
	            public void onItemSelected(AdapterView<?> parentView, View v, int position, long id) {
	                // your code here
	                 
	                // Get selected row data to show on screen
	              //  String Company    = ((TextView) v.findViewById(R.id.name)).getText().toString();
	               
	            	TypeTicket classtypeval = (TypeTicket) TypetcktSpinner.get(position);
	            	mTicket=classtypeval.getValue();
	            }
	 
	            @Override
	            public void onNothingSelected(AdapterView<?> parentView) {
	                // your code here
	            }
	 
	        });*/
		 /* ticket type  End down */
		 
		 
		  av_get.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					
					 /*if(jhdate.getText().length()== 0)
					{
			    		CharSequence text = "Please Select Journey Date";
						int duration = Toast.LENGTH_SHORT;

						Toast toast = Toast.makeText(getApplicationContext(), text, duration);
						toast.setGravity(Gravity.CENTER, 0, 0);
						toast.show();
					}*/
					 /*else if(mClass.length()== 0)
					{
			    		CharSequence text = "Please Select  Class!";
						int duration = Toast.LENGTH_SHORT;

						Toast toast = Toast.makeText(getApplicationContext(), text, duration);
						toast.setGravity(Gravity.CENTER, 0, 0);
						toast.show();
					}*/
					
					/*else if(mTicket.length()== 0)
					{
			    		CharSequence text = "Please Select Ticket Type!";
						int duration = Toast.LENGTH_SHORT;

						Toast toast = Toast.makeText(getApplicationContext(), text, duration);
						toast.setGravity(Gravity.CENTER, 0, 0);
						toast.show();
					}*/
					
					
					/*else
					{
					*/
                    Log.e("mjourneydate", mjourneydate);
                    Log.e("mClass", mClass);
                    Log.e("fromstation", fromstation);
                    Log.e("mTicket", mTicket);
                    Log.e("trainNo", trainNo);
                    Log.e("tostation", tostation);

						Intent intent = new Intent(activity, AvailabilityResult.class);
		            	intent.putExtra("mjourneydate", mjourneydate);
		    			intent.putExtra("mClass", mClass);
		    			intent.putExtra("fromstation", fromstation);
		    			intent.putExtra("mTicket", mTicket);
		    			intent.putExtra("trainNo", trainNo);
		    			intent.putExtra("tostation", tostation);
                        intent.putExtra("trainName", trainName);
	                    startActivity(intent);
						
						// JSONObject json = searchFunctions.getTrainAvailability(mjourneydate, mClass, fromstation, mTicket, trainNo, tostation);

						//new GetAvailable().execute();
					//}
				}
		  });
			 
    	}
	
    
   
    @SuppressWarnings("deprecation")
    public void setDate(View view) {
       showDialog(999);

        jourtext.setTextColor(getResources().getColor(R.color.white));
        jdate.setTextColor(getResources().getColor(R.color.white));
        if(sp.getInt("expressColor",0)!=0) {
            DateRLay.setBackgroundColor(sp.getInt("expressColor",0));
        }else {
            DateRLay.setBackgroundColor(getResources().getColor(R.color.colorExpress));
        }
        jdate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_date_range_white, 0);

        tomotext.setTextColor(getResources().getColor(R.color.cardview_dark_background));
        tomotextdatetext.setTextColor(getResources().getColor(R.color.cardview_dark_background));
        tomodaytext.setTextColor(getResources().getColor(R.color.cardview_dark_background));
        TomoRLay.setBackgroundColor(getResources().getColor(R.color.background_layout));

        todaytext.setTextColor(getResources().getColor(R.color.cardview_dark_background));
        todaydatetext.setTextColor(getResources().getColor(R.color.cardview_dark_background));
        todaydaytext.setTextColor(getResources().getColor(R.color.cardview_dark_background));
        TodayRLay.setBackgroundColor(getResources().getColor(R.color.background_layout));

    }

    public void setDateTomo(View view) {

        mjourneydate = tomDate;
        tomotext.setTextColor(getResources().getColor(R.color.white));
        tomotextdatetext.setTextColor(getResources().getColor(R.color.white));
        tomodaytext.setTextColor(getResources().getColor(R.color.white));
        if(sp.getInt("expressColor",0)!=0) {
            TomoRLay.setBackgroundColor(sp.getInt("expressColor",0));
        }else {
            TomoRLay.setBackgroundColor(getResources().getColor(R.color.colorExpress));
        }
        jdate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_date_range, 0);

        jourtext.setTextColor(getResources().getColor(R.color.cardview_dark_background));
        jdate.setTextColor(getResources().getColor(R.color.cardview_dark_background));
        DateRLay.setBackgroundColor(getResources().getColor(R.color.background_layout));

        todaytext.setTextColor(getResources().getColor(R.color.cardview_dark_background));
        todaydatetext.setTextColor(getResources().getColor(R.color.cardview_dark_background));
        todaydaytext.setTextColor(getResources().getColor(R.color.cardview_dark_background));
        TodayRLay.setBackgroundColor(getResources().getColor(R.color.background_layout));
    }

    public void setDateToday(View view) {

        todaytext.setTextColor(getResources().getColor(R.color.white));
        todaydatetext.setTextColor(getResources().getColor(R.color.white));
        todaydaytext.setTextColor(getResources().getColor(R.color.white));
        if(sp.getInt("expressColor",0)!=0) {
            TodayRLay.setBackgroundColor(sp.getInt("expressColor",0));
        }else {
            TodayRLay.setBackgroundColor(getResources().getColor(R.color.colorExpress));
        }
        jdate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_date_range, 0);

        jourtext.setTextColor(getResources().getColor(R.color.cardview_dark_background));
        jdate.setTextColor(getResources().getColor(R.color.cardview_dark_background));
        DateRLay.setBackgroundColor(getResources().getColor(R.color.background_layout));

        tomotext.setTextColor(getResources().getColor(R.color.cardview_dark_background));
        tomotextdatetext.setTextColor(getResources().getColor(R.color.cardview_dark_background));
        tomodaytext.setTextColor(getResources().getColor(R.color.cardview_dark_background));
        TomoRLay.setBackgroundColor(getResources().getColor(R.color.background_layout));

    }

    @Override
    protected Dialog onCreateDialog(int id) {
       // TODO Auto-generated method stub
       if (id == 999) {
          return new DatePickerDialog(this, myDateListener, year, month, day);
       }
       return null;
    }

    private DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {
       

	@Override
	public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		 showDate(arg1, arg2+1, arg3);
	}
    };
    private void showDate(int year, int month, int day) {
    	String day_f=String.valueOf(day);
    	String month_f =String.valueOf(month);
    	if(day>0 && day<=9)
    		day_f ="0"+String.valueOf(day);
    	if(month>0 && month<=9)
    		month_f ="0"+String.valueOf(month);
    	
    	/*jhdate.setText(new StringBuilder().append(day_f).append("/")
    	        .append(month_f).append("/").append(year));*/
    	mjourneydate = day_f+"-"+month_f+"-"+String.valueOf(year);
    	jdate.setText(new StringBuilder().append(day_f).append("/")
        .append(month_f).append("/").append(year));

        mjourneydate = day_f+"-"+month_f+"-"+year;
        Log.e("mjourneydate---",""+mjourneydate);
     }
    /****** Function to set data in ArrayList *************/
    /*public void setListData()
    {
    	*//*1A or 2A or 3A or 3E or CC or FC or SL or 2S*//*
    	String[][] typeclass = {
    			{ "Select", "" },
    			 { "Sleeper Class(SL)", "SL" },
    			 { "Second Sitting(2S)", "2S" },
    			 { "AC chair Car(CC)", "CC" },
    	        { "AC 2 Tier sleeper(2A)", "2A" },
    	        { "AC 3 Tier(3A)", "3A" },
    	        { "First class Air-Conditioned(1A)", "1A" },
    	        { "AC 3 Tier Economy(3E)", "3E" },
    	        { "First class(FC)", "FC" },
    	        };
        // Now i have taken static values by loop.
        // For further inhancement we can take data by webservice / json / xml;
         
        for (int ic = 0; ic < typeclass.length; ic++) {
        	
        	
        	final TypeClass sched = new TypeClass();
            
            *//******* Firstly take data in model object ******//*
        	sched.setName(typeclass[ic][0]);
             sched.setValue(typeclass[ic][1]);
             
             TypeclassSpinner.add(sched);
             
            
              
                
            *//******** Take Model Object in ArrayList **********//*
               
        }
         
    }*/
   
    
   
    // add items into spinner dynamically
   
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        
        finish();
        overridePendingTransition(R.anim.anim_slide_in_right,
                R.anim.anim_slide_out_right);
    }
    public void NotificationAlert(String Message)
    {
  	
  	  AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(
 		        AvailableActivity.this);

 		// Setting Dialog Title
 		alertDialog2.setTitle("Message");

 		// Setting Dialog Message
 		alertDialog2.setMessage(Message);

 		// Setting Icon to Dialog
 		

 		// Setting Positive "Yes" Btn
 		alertDialog2.setNeutralButton("OK",
 		        new DialogInterface.OnClickListener() {
 		            public void onClick(DialogInterface dialog, int which) {
 		                // Write your code here to execute after dialog
 		            
 		            	
 	                 

 		                
 		            }
 		        });
 		alertDialog2.show();
 		
    } 
    
   
 
   
    
    /*public void setTicketType()
    {
    	*//*GN|CK|PT for General, Tatkal, Premium Tatkal respectively.*//*
    	String[][] tickettype = {
    			{ "Select", "" },
    			 { "General", "GN" },
    			 { "Tatkal", "CK" },
    	       
    	        { "Premium Tatkal", "PT" },
    	        { "PHYSICALLY HANDICAP", "HP" },
    	        { "Ladies Quota", "LD" },
    	        { "Senior Citizen", "SS" },
    	        
    	        
    	        };
        // Now i have taken static values by loop.
        // For further inhancement we can take data by webservice / json / xml;
         
        for (int ic = 0; ic < tickettype.length; ic++) {
        	
        	
        	final TypeTicket sched = new TypeTicket();
            
            *//******* Firstly take data in model object ******//*
        	sched.setName(tickettype[ic][0]);
             sched.setValue(tickettype[ic][1]);
             
             TypetcktSpinner.add(sched);
             
            
              
                
            *//******** Take Model Object in ArrayList **********//*
               
        }
         
    }*/
    
   
    @Override
    public void onDestroy()
    {
       
        
        
		 super.onDestroy();
      
    }
    
    @Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		
	}


	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		
	}

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.FCRLayuout:
                if(sp.getInt("expressColor",0)!=0) {
                    FCRLayuout.setBackgroundColor(sp.getInt("expressColor",0));
                }else {
                    FCRLayuout.setBackgroundColor(getResources().getColor(R.color.colorExpress));
                }
                FC.setTextColor(getResources().getColor(R.color.white));

                E3RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                A1RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                A3RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                A2RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                CCRlayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                S2RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                SLLLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));

                E3.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                A1.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                A3.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                A2.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                CC.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                S2.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                SL.setTextColor(getResources().getColor(R.color.cardview_dark_background));

                mClass = "FC";
                break;

            case R.id.E3RLayout:
                if(sp.getInt("expressColor",0)!=0) {
                    E3RLayout.setBackgroundColor(sp.getInt("expressColor",0));
                }else {
                    E3RLayout.setBackgroundColor(getResources().getColor(R.color.colorExpress));
                }
                E3.setTextColor(getResources().getColor(R.color.white));

                FCRLayuout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                A1RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                A3RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                A2RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                CCRlayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                S2RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                SLLLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));

                FC.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                A1.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                A3.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                A2.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                CC.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                S2.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                SL.setTextColor(getResources().getColor(R.color.cardview_dark_background));

                mClass = "E3";
                break;

            case R.id.A1RLayout:
                if(sp.getInt("expressColor",0)!=0) {
                    A1RLayout.setBackgroundColor(sp.getInt("expressColor",0));
                }else{
                    A1RLayout.setBackgroundColor(getResources().getColor(R.color.colorExpress));
                }
                A1.setTextColor(getResources().getColor(R.color.white));

                FCRLayuout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                E3RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                A3RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                A2RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                CCRlayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                S2RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                SLLLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));

                FC.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                E3.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                A3.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                A2.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                CC.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                S2.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                SL.setTextColor(getResources().getColor(R.color.cardview_dark_background));

                mClass = "A1";
                break;

            case R.id.A3RLayout:
                if(sp.getInt("expressColor",0)!=0) {
                    A3RLayout.setBackgroundColor(sp.getInt("expressColor",0));
                }else{
                    A3RLayout.setBackgroundColor(getResources().getColor(R.color.colorExpress));
                }
                A3.setTextColor(getResources().getColor(R.color.white));

                FCRLayuout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                E3RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                A1RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                A2RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                CCRlayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                S2RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                SLLLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));

                FC.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                E3.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                A1.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                A2.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                CC.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                S2.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                SL.setTextColor(getResources().getColor(R.color.cardview_dark_background));

                mClass = "A3";
                break;

            case R.id.A2RLayout:
                if(sp.getInt("expressColor",0)!=0) {
                    A2RLayout.setBackgroundColor(sp.getInt("expressColor",0));
                }else{
                    A2RLayout.setBackgroundColor(getResources().getColor(R.color.colorExpress));
                }
                A2.setTextColor(getResources().getColor(R.color.white));

                FCRLayuout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                E3RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                A1RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                A3RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                CCRlayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                S2RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                SLLLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));

                FC.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                E3.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                A1.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                A3.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                CC.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                S2.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                SL.setTextColor(getResources().getColor(R.color.cardview_dark_background));

                mClass = "A2";
                break;

            case R.id.CCRlayout:
                if(sp.getInt("expressColor",0)!=0) {
                    CCRlayout.setBackgroundColor(sp.getInt("expressColor",0));
                }else{
                    CCRlayout.setBackgroundColor(getResources().getColor(R.color.colorExpress));
                }
                CC.setTextColor(getResources().getColor(R.color.white));

                FCRLayuout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                E3RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                A1RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                A3RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                A2RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                S2RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                SLLLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));

                FC.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                E3.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                A1.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                A3.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                A2.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                S2.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                SL.setTextColor(getResources().getColor(R.color.cardview_dark_background));

                mClass = "CC";
                break;

            case R.id.S2RLayout:
                if(sp.getInt("expressColor",0)!=0) {
                    S2RLayout.setBackgroundColor(sp.getInt("expressColor",0));
                }else{
                    S2RLayout.setBackgroundColor(getResources().getColor(R.color.colorExpress));
                }
                S2.setTextColor(getResources().getColor(R.color.white));

                FCRLayuout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                E3RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                A1RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                A3RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                A2RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                CCRlayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                SLLLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));

                FC.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                E3.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                A1.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                A3.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                A2.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                CC.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                SL.setTextColor(getResources().getColor(R.color.cardview_dark_background));

                mClass = "S2";
                break;

            case R.id.SLLLayout:
                if(sp.getInt("expressColor",0)!=0) {
                    SLLLayout.setBackgroundColor(sp.getInt("expressColor",0));
                }else{
                    SLLLayout.setBackgroundColor(getResources().getColor(R.color.colorExpress));
                }
                SL.setTextColor(getResources().getColor(R.color.white));

                FCRLayuout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                E3RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                A1RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                A3RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                A2RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                CCRlayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                S2RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));

                FC.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                E3.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                A1.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                A3.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                A2.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                CC.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                S2.setTextColor(getResources().getColor(R.color.cardview_dark_background));

                mClass = "SL";
                break;
        }
    }
}