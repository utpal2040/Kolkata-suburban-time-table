package com.matainja.runingstatus;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.matainja.runingstatus.Adapter.SuggestionStationsAdapter;
import com.matainja.runingstatus.Database.DatabaseAdapter;
import com.matainja.runingstatus.Model.SuggestStationBean;

import java.util.ArrayList;


public class SugesstionStation extends AppCompatActivity {
	
    
    ProgressDialog pDialog;
    public static final int DIALOG_DOWNLOAD_PROGRESS = 1;
    Bundle extras;
   
    private DatabaseAdapter dbHelper;
    SuggestionStationsAdapter adapter;
    ListView lv;
	Toolbar toolbar;
  
    ArrayList<SuggestStationBean> stationlist ;
	 
	 String tostation;
	 String tostationName;
	SharedPreferences sp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	//Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        this.overridePendingTransition(R.anim.lefttoright,
                R.anim.righttoleft);
        setContentView(R.layout.suggestionstationlist);

		sp = getSharedPreferences("themeColor", Context.MODE_PRIVATE);
		if(sp.getInt("expressColor",0)!=0) {

			int[] demo_colors = getResources().getIntArray(R.array.demo_colors);
			int[] demo_colors_mat = getResources().getIntArray(R.array.demo_colors_mat);

			ArrayList<String> colorString = new ArrayList<String>();
			ArrayList<String> colorString_mat = new ArrayList<String>();

			for (int i = 0; i < demo_colors.length; i++) {
				Log.e("Test", demo_colors[i] + "");
				//Log.e("Test", demo_colors[i] + "");
				colorString.add(String.valueOf(demo_colors[i]));

			}

			for (int i = 0; i < demo_colors_mat.length; i++) {
				Log.e("Test", demo_colors[i] + "");
				//Log.e("Test", demo_colors[i] + "");
				colorString_mat.add(String.valueOf(demo_colors_mat[i]));

			}
			Log.e("position ",colorString.indexOf(String.valueOf(sp.getInt("expressColor",0))) + "");
			int color_mat_val = Integer.parseInt(colorString_mat.get(colorString.indexOf(String.valueOf(sp.getInt("expressColor", 0)))));


			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
				Window window = getWindow();
				window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
				window.setStatusBarColor(color_mat_val);
			}
			//mTitle.setText("Bookmark - "+getIntent().getStringExtra("SUB_MENU_NAME"));
			toolbar.setBackgroundColor(sp.getInt("expressColor",0));

		}else {

			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
				Window window = getWindow();
				window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
				window.setStatusBarColor(getResources().getColor(R.color.colorLocalStatus));
			}
		}

       // Log.v("start activity","ooo");
        
     // Database Connection Open
	     dbHelper = new DatabaseAdapter(this);
	     dbHelper.open();

		toolbar = (Toolbar) findViewById(R.id.toolbar);

		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);

		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				Intent intent = new Intent(getApplicationContext(), LocalTrain.class);
				intent.putExtra("callfrom","other");
				startActivity(intent);
				finish();


			}
		});

	      stationlist = new ArrayList<SuggestStationBean>();
	      
	      lv = (ListView)findViewById(R.id.list);
	       
	       
	        extras = getIntent().getExtras();
		      if(extras !=null) {
		    	  
		    	  tostation = extras.getString("tostation");
		    	 
		    	  tostationName = extras.getString("tostationName");
				}
		      
  			String HeadText = "The Train starts from  listed station and reach to "+tostationName;

			TextView sourcestation = (TextView) findViewById(R.id.train_name);
			     

			sourcestation.setText(HeadText);

		    new SuggestionSource().execute();
        
    }


    private class SuggestionSource extends AsyncTask<Void, Void, Void> {
    	  
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(SugesstionStation.this);
            pDialog.setMessage("Loading Source station List ...");
            pDialog.setCancelable(false);
            pDialog.show();
 
        }
 
        @Override
        protected Void doInBackground(Void... arg0) {
        	 stationlist = dbHelper.getSuggestion(tostation,tostationName);
        	 
        	
			return null;
        	
        	
        }
 
        @Override
        protected void onPostExecute(Void result) {
        	// Toast.makeText(getApplicationContext(), tostation+"=="+tostation+"tostationcode="+tostationName+"isempty="+!stationlist.isEmpty(), Toast.LENGTH_SHORT).show();
        	if(!stationlist.isEmpty())
        	{
        		//Collections.sort(stationlist);
        	 adapter = new SuggestionStationsAdapter(getApplicationContext(), stationlist);
		       	
		       	// lv.setOnItemClickListener(this);
		       	 lv.setAdapter(adapter);
		       	// lv.setOnClickListener(new ClickListenerList());
		       	lv.setOnItemClickListener(new OnItemClickListener() {
		       	      public void onItemClick(AdapterView<?> parent, View view,
		       	          int position, long id) {
		       	        // When clicked, show a toast with the TextView text
		       	    	 final SuggestStationBean entry = stationlist.get(position);
		       	    	Intent intent = new Intent(getApplicationContext(), TrainTimetable.class);
		    	    	//StringBuffer urlString = new StringBuffer();
		    	    	//Clinets parentActivity = (Clinets)getParent();
		    	    	//replaceContentView("addclients", intent);
		    	    	// 1 for Add Clients
						  intent.putExtra("callfrom","other");
		    			intent.putExtra("fromstation", entry.getsStationCode());
		    			intent.putExtra("timefilter", "");
		    			intent.putExtra("tostation", entry.getsdesCode());
		    			intent.setAction(Intent.ACTION_VIEW);
		    			
		    				intent.putExtra("filterbytime", 0);
		    			
		    			startActivity(intent);
		    			
		       	    	
		       	      }
		       	    });
        	}
        	else
        		noRecordFound();
		       	
		       	
		       	if (pDialog.isShowing())
	                pDialog.dismiss();
        }
 
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();

		Intent intent = new Intent(getApplicationContext(), LocalTrain.class);
		intent.putExtra("callfrom","other");
		startActivity(intent);
		finish();
        //overridePendingTransition(R.anim.lefttoright, R.anim.righttoleft);
    }
    
    
    
        
    public void noRecordFound()
    {
  	
  	  AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(
 		        SugesstionStation.this);

 		// Setting Dialog Title
 		alertDialog2.setTitle("Alert");

 		// Setting Dialog Message
 		alertDialog2.setMessage("To Station is Invalid!!Try again");

 		// Setting Icon to Dialog
 		

 		// Setting Positive "Yes" Btn
 		alertDialog2.setNeutralButton("OK",
 		        new DialogInterface.OnClickListener() {
 		            public void onClick(DialogInterface dialog, int which) {
 		                // Write your code here to execute after dialog
 		            
 		            	Intent i3 = new Intent(SugesstionStation.this, LocalTrain.class);
						i3.putExtra("callfrom","other");
 	                    startActivity(i3);
 	                    finish();
 	                 

 		                
 		            }
 		        });
 		alertDialog2.show();
 		
    }  
  
    
    
    @Override
    public void onDestroy()
    {
        super.onDestroy();
        dbHelper.close();
    }  
  
   
}