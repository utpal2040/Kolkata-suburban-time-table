package com.matainja.runingstatus;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.kunzisoft.switchdatetime.SwitchDateTimeDialogFragment;
import com.matainja.runingstatus.Adapter.CancelTrainAdapter;
import com.matainja.runingstatus.Common.ConnectionDetector;
import com.matainja.runingstatus.Connectivity.HttpConnection;
import com.matainja.runingstatus.Database.DatabaseAdapterSaveSearch;
import com.matainja.runingstatus.Model.CancelTrain;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by matainja on 23-Mar-17.
 */

public class ActivityCancelTrain extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    Toolbar toolbar;
    Context context;
    int currentapiVersion=0;
    DrawerLayout drawer;
    private DatabaseAdapterSaveSearch dbHelpersave;
    ListView cancellist;
    RelativeLayout date_select_lay;
    private static final String TAG_DATETIME_FRAGMENT = "TAG_DATETIME_FRAGMENT";
    private SwitchDateTimeDialogFragment dateTimeFragment;
    private static final String TAG = "Matainja";
    TextView datetxt;
    ConnectionDetector cd;
    ArrayList<CancelTrain> cancelArrList;
    CancelTrainAdapter cta;
    ProgressBar progressBar2;
    String email;
    List<String>  calArr= Arrays.asList(new String[] {"Jan","Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"});

    SharedPreferences sp;
    CircleImageView profile_image;
    TextView nameTxt;
    RelativeLayout headerFrame;
    //private MenuItem mSearchAction;
    //private boolean isSearchOpened = false;
    //private EditText edtSeach;

    private SearchView mSearchView;
    private MenuItem searchMenuItem;
    SearchView.OnQueryTextListener listener;
    String error_msg = "Server Busy! Please try after some time...";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_cancel_train);
        overridePendingTransition(R.anim.anim_slide_in_left,
                R.anim.anim_slide_out_left);
        context = getApplicationContext();
        currentapiVersion = android.os.Build.VERSION.SDK_INT;

        dbHelpersave = new DatabaseAdapterSaveSearch(this);
        dbHelpersave.open();
        cd = new ConnectionDetector(getApplicationContext());
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        //handleIntent(getIntent());

        /*toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });*/

        //datetxt = (TextView) findViewById(R.id.date);
        //date_select_lay = (RelativeLayout) findViewById(R.id.date_select_lay);
       /* date_select_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateTimeFragment.show(getSupportFragmentManager(), TAG_DATETIME_FRAGMENT);

            }
        });*/

        sp = getSharedPreferences("themeColor", Context.MODE_PRIVATE);
        if(sp.getInt("expressColor",0)!=0) {
            toolbar.setBackgroundColor(sp.getInt("expressColor",0));

            int[] demo_colors = getResources().getIntArray(R.array.demo_colors);
            int[] demo_colors_mat = getResources().getIntArray(R.array.demo_colors_mat);

            ArrayList<String> colorString = new ArrayList<String>();
            ArrayList<String> colorString_mat = new ArrayList<String>();

            for (int i = 0; i < demo_colors.length; i++) {
                Log.e("Test", demo_colors[i] + "");
                //Log.e("Test", demo_colors[i] + "");
                colorString.add(String.valueOf(demo_colors[i]));

            }

            for (int i = 0; i < demo_colors_mat.length; i++) {
                Log.e("Test", demo_colors[i] + "");
                //Log.e("Test", demo_colors[i] + "");
                colorString_mat.add(String.valueOf(demo_colors_mat[i]));

            }
            Log.e("position ",colorString.indexOf(String.valueOf(sp.getInt("expressColor",0))) + "");
            int color_mat_val = Integer.parseInt(colorString_mat.get(colorString.indexOf(String.valueOf(sp.getInt("expressColor", 0)))));

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(color_mat_val);
            }
        }else{
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(getResources().getColor(R.color.colorExpressStatus));
            }
        }
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.setDrawerIndicatorEnabled(true);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View headerview = navigationView.getHeaderView(0);
        profile_image = (CircleImageView) headerview.findViewById(R.id.profile_image);
        nameTxt = (TextView) headerview.findViewById(R.id.name);
        headerFrame = (RelativeLayout) headerview.findViewById(R.id.headerFrame);
        headerFrame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            if(email.equalsIgnoreCase("")){
                Intent i = new Intent(ActivityCancelTrain.this, WelcomeActivity.class);
                startActivity(i);
                finish();
            }else {
                Intent i = new Intent(ActivityCancelTrain.this, SettingsActivity.class);
                startActivity(i);
            }


            }
        });

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AdView mAdView = (AdView) findViewById(R.id.adView);
                AdRequest adRequest = new AdRequest.Builder().build();
                mAdView.loadAd(adRequest);
            }
        });

        cancellist = (ListView) findViewById(R.id.cancellist);
        progressBar2 = (ProgressBar) findViewById(R.id.progressBar2);

        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int thisMonth = calendar.get(Calendar.MONTH)+1;
        int date = calendar.get(Calendar.DATE);

        String month;
        int monthcal = calArr.indexOf(thisMonth);
        if(thisMonth < 10){

            month = "0"+thisMonth;
        } else{
            month = String.valueOf(thisMonth);
        }
        Log.e("date==",""+date+"-"+month+"-"+year);
        String newDate = date+"-"+month+"-"+year;

        if (cd.isConnectingToInternet()) {

            new GetCancelTrains().execute(newDate.toString());

        }
        else
        {
            String msglert="Please Check your Network Connection !!";
            NotificationAlert(msglert);
        }

        // Construct SwitchDateTimePicker
        /*dateTimeFragment = (SwitchDateTimeDialogFragment) getSupportFragmentManager().findFragmentByTag(TAG_DATETIME_FRAGMENT);
        if(dateTimeFragment == null) {
            dateTimeFragment = SwitchDateTimeDialogFragment.newInstance(
                    getString(R.string.label_datetime_dialog),
                    getString(R.string.positive_button_datetime_picker),
                    getString(R.string.negative_button_datetime_picker)
            );
        }

        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int thisMonth = calendar.get(Calendar.MONTH);
        int date = calendar.get(Calendar.DATE);
        int min = calendar.get(Calendar.MINUTE);
        int hr = calendar.get(Calendar.HOUR);
        // Assign values we want
        //HH:mm
        final SimpleDateFormat myDateFormat = new SimpleDateFormat("dd MMM yyyy", java.util.Locale.getDefault());
        dateTimeFragment.startAtCalendarView();
        dateTimeFragment.set24HoursMode(true);
        dateTimeFragment.setMinimumDateTime(new GregorianCalendar(2015, Calendar.JANUARY, 1).getTime());
        dateTimeFragment.setMaximumDateTime(new GregorianCalendar(2025, Calendar.DECEMBER, 31).getTime());
        dateTimeFragment.setDefaultDateTime(new GregorianCalendar(year, thisMonth, date, hr, min).getTime());
        // Or assign each element, default element is the current moment
        // dateTimeFragment.setDefaultHourOfDay(15);
        // dateTimeFragment.setDefaultMinute(20);
        // dateTimeFragment.setDefaultDay(4);
        // dateTimeFragment.setDefaultMonth(Calendar.MARCH);
        // dateTimeFragment.setDefaultYear(2017);

        // Define new day and month format
        try {
            dateTimeFragment.setSimpleDateMonthAndDayFormat(new SimpleDateFormat("MMMM dd", Locale.getDefault()));
        } catch (SwitchDateTimeDialogFragment.SimpleDateMonthAndDayFormatException e) {
            Log.e(TAG, e.getMessage());
        }

        // Set listener for date
        dateTimeFragment.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonClickListener() {
            @Override
            public void onPositiveButtonClick(Date date) {
                datetxt.setText(myDateFormat.format(date));

                //myDateFormat.format(date.getDay());
                String[] newdate = myDateFormat.format(date).split(" ");
                String daydate = newdate[0];
                String daymonth = newdate[1];
                String dayyear = newdate[2];

                String month;
                int monthcal = calArr.indexOf(daymonth)+1;
                if(monthcal < 10){

                    month = "0"+monthcal;
                } else{
                    month = String.valueOf(monthcal);
                }
                Log.e("date==",""+daydate+"-"+month+"-"+dayyear);
                String newDate = daydate+"-"+month+"-"+dayyear;
                new GetCancelTrains().execute(newDate.toString());
            }

            @Override
            public void onNegativeButtonClick(Date date) {
                datetxt.setText("Select Date");
            }
        });*/

        /*listener = new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                // newText is text entered by user to SearchView
                Toast.makeText(getApplicationContext(), newText, Toast.LENGTH_LONG).show();
                return false;
            }
        };*/

        ApplyUserInfo();
    }


    public void ApplyUserInfo() {
        SharedPreferences userPref = getSharedPreferences("userCred", MODE_PRIVATE);
        String name = userPref.getString("name", "Guest User");
        email = userPref.getString("email", "");
        String imageUrl = userPref.getString("image_value", "");

        Log.e("imageUrl ", imageUrl);
        nameTxt.setText(name);
        if (!imageUrl.equalsIgnoreCase("")) {
            Picasso.with(context).load(imageUrl).memoryPolicy(MemoryPolicy.NO_CACHE).into(profile_image);
        }
    }



    private void showResults(String query) {
        // Query your data set and show results
        // ...
    }

    public class GetCancelTrains extends AsyncTask<String, String, JSONObject>{

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            progressBar2.setVisibility(View.VISIBLE);
        }
        @Override
        protected JSONObject doInBackground(String... params) {
            HttpConnection getcon = new HttpConnection();
            JSONObject json = getcon.getCancelTrain(params[0]);
            return json;
        }

        @Override
        protected void onPostExecute(JSONObject json) {
            Log.e("JSON==",""+json);

            if(json!= null) {
                try {
                    if (json.getString("ResponseCode").equals("200")) {

                        JSONArray trainArr = json.getJSONArray("Trains");

                        cancelArrList = new ArrayList<CancelTrain>();
                        for (int i = 0; i < trainArr.length(); i++) {

                            JSONObject trainJSOBJ = trainArr.getJSONObject(i);

                            CancelTrain cancel_train = new CancelTrain();

                            /*-------------------------- FOR DEST --------------------------*/
                            JSONObject dest_obj = new JSONObject(trainJSOBJ.getString("Destination"));
                            cancel_train.setDest_name(dest_obj.getString("Name"));
                            cancel_train.setDest_code(dest_obj.getString("Code"));


                            /*-------------------------- FOR source --------------------------*/
                            JSONObject source_obj = new JSONObject(trainJSOBJ.getString("Source"));
                            cancel_train.setSrc_name(source_obj.getString("Name"));
                            cancel_train.setSrc_code(source_obj.getString("Code"));

                            /*-------------------------- FOR train --------------------------*/
                            JSONObject train_obj = new JSONObject(trainJSOBJ.getString("train"));
                            cancel_train.setStart_time(train_obj.getString("StartTime"));
                            cancel_train.setTrain_no(train_obj.getString("Number"));
                            cancel_train.setTrain_name(train_obj.getString("Name"));

                            cancelArrList.add(cancel_train);
                        }
                        progressBar2.setVisibility(View.GONE);
                        cta = new CancelTrainAdapter(ActivityCancelTrain.this, cancelArrList);
                        cancellist.setAdapter(cta);
                    }else{

                        if (json.has("Message")) {
                            error_msg = json.getString("Message");
                            NotificationAlert(error_msg);
                            progressBar2.setVisibility(View.GONE);
                        }

                    }
                } catch (JSONException e) {
                    NotificationAlert(error_msg);
                    progressBar2.setVisibility(View.GONE);
                }
            }else{
                NotificationAlert(error_msg);
                progressBar2.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
            Intent i = new Intent(ActivityCancelTrain.this, LocalTrain.class);
            i.putExtra("callfrom","other");
            startActivity(i);
            finish();
            overridePendingTransition(R.anim.anim_slide_in_right,
                    R.anim.anim_slide_out_right);
        }
    }

    /*@Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        mSearchAction = menu.findItem(R.id.action_search);
        return super.onPrepareOptionsMenu(menu);
    }*/

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        searchMenuItem = menu.findItem(R.id.action_search);
        mSearchView = (SearchView) searchMenuItem.getActionView();
        mSearchView.setOnQueryTextListener(listener);
        return true;
    }*/

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }*/

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {

            case R.id.action_search:
                Toast.makeText(ActivityCancelTrain.this, "handleMenuSearch", Toast.LENGTH_SHORT).show();
                handleMenuSearch();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }*/

   /* protected void handleMenuSearch(){
        ActionBar action = getSupportActionBar(); //get the actionbar

        if(isSearchOpened){ //test if the search is open

            Toast.makeText(ActivityCancelTrain.this, "if", Toast.LENGTH_SHORT).show();
            action.setDisplayShowCustomEnabled(false); //disable a custom view inside the actionbar
            action.setDisplayShowTitleEnabled(true); //show the title in the action bar

            //hides the keyboard
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(edtSeach.getWindowToken(), 0);

            //add the search icon in the action bar
            mSearchAction.setIcon(getResources().getDrawable(R.mipmap.ic_search));

            isSearchOpened = false;
        } else { //open the search entry

            Toast.makeText(ActivityCancelTrain.this, "else", Toast.LENGTH_SHORT).show();
            action.setDisplayShowCustomEnabled(true); //enable it to display a
            // custom view in the action bar.
            action.setCustomView(R.layout.search_bar);//add the custom view
            action.setDisplayShowTitleEnabled(false); //hide the title

            edtSeach = (EditText)action.getCustomView().findViewById(R.id.edtSearch); //the text editor

            edtSeach.addTextChangedListener(new TextWatcher() {

                @Override
                public void afterTextChanged(Editable arg0) {
                    // TODO Auto-generated method stub
                    String text = edtSeach.getText().toString().toUpperCase(Locale.getDefault());
                    Log.e("TEXT==",""+text);
                    cta.filter(text);
                }

                @Override
                public void beforeTextChanged(CharSequence arg0, int arg1,
                                              int arg2, int arg3) {
                    // TODO Auto-generated method stub
                }

                @Override
                public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                    // TODO Auto-generated method stub
                }
            });

            //this is a listener to do a search when the user clicks on search button
            edtSeach.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                        doSearch();

                        edtSeach.addTextChangedListener(new TextWatcher() {

                            @Override
                            public void afterTextChanged(Editable arg0) {
                                // TODO Auto-generated method stub
                                String text = edtSeach.getText().toString().toUpperCase(Locale.getDefault());
                                Log.e("TEXT==",""+text);
                                cta.filter(text);
                            }

                            @Override
                            public void beforeTextChanged(CharSequence arg0, int arg1,
                                                          int arg2, int arg3) {
                                // TODO Auto-generated method stub
                            }

                            @Override
                            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                                      int arg3) {
                                // TODO Auto-generated method stub
                            }
                        });

                        return true;
                    }
                    return false;
                }
            });


            edtSeach.requestFocus();

            //open the keyboard focused in the edtSearch
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(edtSeach, InputMethodManager.SHOW_IMPLICIT);


            //add the close icon
            mSearchAction.setIcon(getResources().getDrawable(R.drawable.ic_close));

            isSearchOpened = true;
        }
    }*/

    @Override
    public boolean onCreateOptionsMenu( Menu menu) {
        getMenuInflater().inflate( R.menu.menu_main, menu);

        MenuItem myActionMenuItem = menu.findItem( R.id.action_search);
        final SearchView searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (TextUtils.isEmpty(newText)) {
                    cta.filter("");
                    cancellist.clearTextFilter();
                } else {
                    cta.filter(newText);
                }
                return true;
            }
        });

        return true;
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the camera action
            Intent intent = new Intent(getApplicationContext(), LocalTrain.class);
            intent.putExtra("callfrom","other");
            startActivity(intent);
            finish();

        } else if (id == R.id.nav_runningstatus) {

            Intent intent = new Intent(getApplicationContext(), Homepage.class);

            startActivity(intent);
            finish();

        } else if (id == R.id.nav_cancelled) {

            /*Intent intent = new Intent(getApplicationContext(), ActivityCancelTrain.class);
            startActivity(intent);
            finish();*/

        }  else if (id == R.id.nav_pnrstatus) {

            Intent intent = new Intent(getApplicationContext(), Pnr.class);
            startActivity(intent);
            finish();

        } else if (id == R.id.nav_saveroute) {

            Intent intent = new Intent(getApplicationContext(), SaveSearch.class);
            startActivity(intent);
            finish();

        } else if (id == R.id.nav_feedback) {
            Intent intent = new Intent(getApplicationContext(), feedback.class);

            startActivity(intent);
            finish();

        } else if (id == R.id.nav_fblike) {

            Intent notificationIntent =null;
            notificationIntent = new Intent(Intent.ACTION_VIEW);
            notificationIntent.setData(Uri.parse("https://www.facebook.com/localtimetable"));
            startActivity(notificationIntent);


        } else if (id == R.id.nav_theme) {

            if(email.equalsIgnoreCase("")){
                Intent i = new Intent(ActivityCancelTrain.this, WelcomeActivity.class);
                startActivity(i);
                finish();
            }else {
                Intent i = new Intent(ActivityCancelTrain.this, SettingsActivity.class);
                startActivity(i);
            }


        } else if (id == R.id.nav_rate) {

            Intent notificationIntent =null;
            notificationIntent = new Intent(Intent.ACTION_VIEW);
            notificationIntent.setData(Uri.parse("market://details?id="+getApplicationContext().getPackageName()));
            startActivity(notificationIntent);


        } else if (id == R.id.nav_aboutus) {

            Intent intent = new Intent(getApplicationContext(), BuildWebsite.class);
            startActivity(intent);
            finish();
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    public void doSearch(){
        Log.e("SEARCH","SUCCESS");
    }
    public void NotificationAlert(String Message)
    {

        AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(
                ActivityCancelTrain.this);

        // Setting Dialog Title
        alertDialog2.setTitle("Message");

        // Setting Dialog Message
        alertDialog2.setMessage(Message);

        // Setting Icon to Dialog


        // Setting Positive "Yes" Btn
        alertDialog2.setNeutralButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog


                        //finish();


                    }
                });
        try {
            alertDialog2.show();
        }catch(Exception e){

        }
    }

}
