package com.matainja.runingstatus;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.matainja.runingstatus.Fragment.ViewPagerAdapterTheme;

import java.util.ArrayList;

/**
 * Created by matainja on 13-Nov-17.
 */

public class SettingsActivity extends AppCompatActivity{

    public static Toolbar toolbar;
    View view1;
    public static TabLayout tabLayout;
    private ViewPager viewPager;
    private AppBarLayout appbar;
    SharedPreferences sp;
    int localColor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_settings);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(SettingsActivity.this,LocalTrain.class);
                i.putExtra("callfrom","settings");
                startActivity(i);
                finish();
            }
        });


        sp = getSharedPreferences("themeColor", Context.MODE_PRIVATE);
        localColor = sp.getInt("localColor",0);

        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        appbar = (AppBarLayout) findViewById(R.id.appbar);

        tabLayout.addTab(tabLayout.newTab().setText("Local Train"));
        tabLayout.addTab(tabLayout.newTab().setText("Express Train"));

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setTabMode(TabLayout.MODE_FIXED);

        tabLayout.setSelectedTabIndicatorColor(Color.WHITE);
        tabLayout.setSelectedTabIndicatorHeight(5);

        if(sp.getInt("localColor",0)!=0) {
            toolbar.setBackgroundColor(sp.getInt("localColor",0));
            tabLayout.setBackgroundColor(sp.getInt("localColor",0));

            int[] demo_colors = getResources().getIntArray(R.array.demo_colors);
            int[] demo_colors_mat = getResources().getIntArray(R.array.demo_colors_mat);

            ArrayList<String> colorString = new ArrayList<String>();
            ArrayList<String> colorString_mat = new ArrayList<String>();

            for (int i = 0; i < demo_colors.length; i++) {

                colorString.add(String.valueOf(demo_colors[i]));

            }

            for (int i = 0; i < demo_colors_mat.length; i++) {

                colorString_mat.add(String.valueOf(demo_colors_mat[i]));

            }
            Log.e("position ",colorString.indexOf(String.valueOf(sp.getInt("localColor",0))) + "");
            int color_mat_val = Integer.parseInt(colorString_mat.get(colorString.indexOf(String.valueOf(sp.getInt("localColor", 0)))));

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(color_mat_val);
            }
        }

        final ViewPagerAdapterTheme adapter = new ViewPagerAdapterTheme
                (getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());

                if (tab.getPosition() == 1) {
                    //mTitle.setText("Notes - "+getIntent().getStringExtra("SUB_MENU_NAME"));
                    Log.e("color 1 ",""+sp.getInt("expressColor",0));
                    if(sp.getInt("expressColor",0)!=0) {

                        int[] demo_colors = getResources().getIntArray(R.array.demo_colors);
                        int[] demo_colors_mat = getResources().getIntArray(R.array.demo_colors_mat);

                        ArrayList<String> colorString = new ArrayList<String>();
                        ArrayList<String> colorString_mat = new ArrayList<String>();

                        for (int i = 0; i < demo_colors.length; i++) {
                            Log.e("Test", demo_colors[i] + "");
                            //Log.e("Test", demo_colors[i] + "");
                            colorString.add(String.valueOf(demo_colors[i]));

                        }

                        for (int i = 0; i < demo_colors_mat.length; i++) {
                            Log.e("Test", demo_colors[i] + "");
                            //Log.e("Test", demo_colors[i] + "");
                            colorString_mat.add(String.valueOf(demo_colors_mat[i]));

                        }
                        Log.e("position ",colorString.indexOf(String.valueOf(sp.getInt("expressColor",0))) + "");
                        int color_mat_val = Integer.parseInt(colorString_mat.get(colorString.indexOf(String.valueOf(sp.getInt("expressColor", 0)))));


                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            Window window = getWindow();
                            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                            window.setStatusBarColor(color_mat_val);
                        }
                        //mTitle.setText("Bookmark - "+getIntent().getStringExtra("SUB_MENU_NAME"));
                        toolbar.setBackgroundColor(sp.getInt("expressColor",0));
                        tabLayout.setBackgroundColor(sp.getInt("expressColor",0));
                    }
                } else {

                    Log.e("color 0 ",""+localColor);
                    if(sp.getInt("localColor",0)!=0) {
                        toolbar.setBackgroundColor(sp.getInt("localColor",0));
                        tabLayout.setBackgroundColor(sp.getInt("localColor",0));

                        int[] demo_colors = getResources().getIntArray(R.array.demo_colors);
                        int[] demo_colors_mat = getResources().getIntArray(R.array.demo_colors_mat);

                        ArrayList<String> colorString = new ArrayList<String>();
                        ArrayList<String> colorString_mat = new ArrayList<String>();

                        for (int i = 0; i < demo_colors.length; i++) {
                            Log.e("Test", demo_colors[i] + "");
                            //Log.e("Test", demo_colors[i] + "");
                            colorString.add(String.valueOf(demo_colors[i]));

                        }

                        for (int i = 0; i < demo_colors_mat.length; i++) {
                            Log.e("Test", demo_colors[i] + "");
                            //Log.e("Test", demo_colors[i] + "");
                            colorString_mat.add(String.valueOf(demo_colors_mat[i]));

                        }
                        Log.e("position ",colorString.indexOf(String.valueOf(sp.getInt("localColor",0))) + "");
                        int color_mat_val = Integer.parseInt(colorString_mat.get(colorString.indexOf(String.valueOf(sp.getInt("localColor", 0)))));


                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            Window window = getWindow();
                            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                            window.setStatusBarColor(color_mat_val);
                        }
                    }

                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }


    @Override
    public void onBackPressed(){
        super.onBackPressed();
        Intent i = new Intent(SettingsActivity.this,LocalTrain.class);
        i.putExtra("callfrom","settings");
        startActivity(i);
        finish();
    }
}
