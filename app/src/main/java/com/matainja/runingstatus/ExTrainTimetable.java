package com.matainja.runingstatus;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.matainja.runingstatus.Adapter.TimeTableListAdapter;
import com.matainja.runingstatus.Common.Constants;
import com.matainja.runingstatus.Database.DatabaseAdapterEx;
import com.matainja.runingstatus.GoogleAnalyticsApp.TrackerName;
import com.matainja.runingstatus.Model.RecentSearch;
import com.matainja.runingstatus.Model.TimetableListBean;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;


public class ExTrainTimetable extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{
	ListView list;
    TextView source;
    TextView arrival;
    TextView departure;
    TextView status;
    Button Btngetdata;
    TextView constatus ;
    ArrayList<HashMap<String, String>> oslist = new ArrayList<HashMap<String, String>>();
    //URL to get JSON Array
    private static String url = "";
    //JSON Node Names
    int norecrod=0;
    ProgressDialog pDialog;
    public static final int DIALOG_DOWNLOAD_PROGRESS = 1;
    Bundle extras;

    String tostationcode;
    private DatabaseAdapterEx dbHelper;
    Cursor traincursor;
    ListView lv;
    private ProgressDialog mProgressDialog;
    ArrayList<TimetableListBean> listOftrain;
    LayoutInflater infalter;
	 ViewGroup header;
	 TimeTableListAdapter adapter;
    ProgressBar progressBar1;
	 Integer validstationfrom=0;
	 Integer validstationto=0;
	 Cursor fromstation;
	 Cursor tostation;
	 int currentapiVersion=0;
	 int isexpress=0;
	TextView sourcestation,stationname,totaltrain;
    String fromStationNAme="";
    String tostation_ = "";
    Toolbar toolbar;
    DrawerLayout drawer;
    String fromstationcode,fromstationname,tostationname;
    Context context;
    String email;
    SharedPreferences prefs;
    SharedPreferences.Editor editorUser;
    boolean flag = false;
    String fromcode, tocode;
    ArrayList<RecentSearch> recentArrayList;
    ArrayList<RecentSearch> recentSearchList;
    SharedPreferences sp;
    CircleImageView profile_image;
    TextView nameTxt;
    RelativeLayout headerFrame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.expresstimetablelist);
        overridePendingTransition(R.anim.anim_slide_in_left,
                R.anim.anim_slide_out_left);
        context = getApplicationContext();


        dbHelper = new DatabaseAdapterEx(this);
        dbHelper.open();

        recentSearchList = new ArrayList<RecentSearch>();
        recentArrayList = new ArrayList<RecentSearch>();

        sp = getSharedPreferences("themeColor", Context.MODE_PRIVATE);
        prefs = ExTrainTimetable.this.getSharedPreferences(Constants.USER_SHARED_PREF_VAR, Context.MODE_PRIVATE);
        editorUser = prefs.edit();
        //Toast.makeText(ExTrainTimetable.this,"this",Toast.LENGTH_LONG).show();

        currentapiVersion = android.os.Build.VERSION.SDK_INT;
        stationname = (TextView) findViewById(R.id.stationname);
        totaltrain = (TextView) findViewById(R.id.totaltrain);
        lv = (ListView) findViewById(R.id.traintlist);
        progressBar1 = (ProgressBar) findViewById(R.id.progressBar1);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorExpressStatus));
        }*/

        if(sp.getInt("expressColor",0)!=0) {
            toolbar.setBackgroundColor(sp.getInt("expressColor",0));

            int[] demo_colors = getResources().getIntArray(R.array.demo_colors);
            int[] demo_colors_mat = getResources().getIntArray(R.array.demo_colors_mat);

            ArrayList<String> colorString = new ArrayList<String>();
            ArrayList<String> colorString_mat = new ArrayList<String>();

            for (int i = 0; i < demo_colors.length; i++) {

                colorString.add(String.valueOf(demo_colors[i]));

            }

            for (int i = 0; i < demo_colors_mat.length; i++) {

                colorString_mat.add(String.valueOf(demo_colors_mat[i]));

            }
            Log.e("position ",colorString.indexOf(String.valueOf(sp.getInt("expressColor",0))) + "");
            int color_mat_val = Integer.parseInt(colorString_mat.get(colorString.indexOf(String.valueOf(sp.getInt("expressColor", 0)))));

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(color_mat_val);
            }
        }else{
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(getResources().getColor(R.color.colorExpressStatus));
            }
        }

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View headerview = navigationView.getHeaderView(0);
        profile_image = (CircleImageView) headerview.findViewById(R.id.profile_image);
        nameTxt = (TextView) headerview.findViewById(R.id.name);
        headerFrame = (RelativeLayout) headerview.findViewById(R.id.headerFrame);
        headerFrame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(email.equalsIgnoreCase("")){
                    Intent i = new Intent(ExTrainTimetable.this, WelcomeActivity.class);
                    startActivity(i);
                    finish();
                }else {
                    Intent i = new Intent(ExTrainTimetable.this, SettingsActivity.class);
                    startActivity(i);
                }
            }
        });

        extras = getIntent().getExtras();
        if(extras !=null) {
            fromstationcode = extras.getString("fromstation");
            tostationcode = extras.getString("tostation");

            tostationname = extras.getString("tostationname");
            fromstationname = extras.getString("fromstationname");

            stationname.setText(fromstationname+" TO "+tostationname);

            getSupportActionBar().setTitle(fromstationcode+" TO "+tostationcode);
        } else{
            getSupportActionBar().setTitle("Train List");
        }

        fromstation =dbHelper.getStationName(fromstationcode);
        tostation =dbHelper.getStationName(tostationcode);
        String fromStationNAme="";
        String tostation_ = "";
        String fromStationCode ="";
        String toStationCode ="";

        if(fromstation.getCount()>0)
        {

            while (fromstation.moveToNext()) {


                validstationfrom=1;

                fromStationNAme = fromstation.getString(fromstation.getColumnIndexOrThrow("stationName"));
                fromStationCode = fromstation.getString(fromstation.getColumnIndexOrThrow("stationCode"));


            }
        }
        fromstation.close();

        if(tostation.getCount()>0)
        {

            while (tostation.moveToNext()) {


                validstationto=1;

                tostation_ = tostation.getString(tostation.getColumnIndexOrThrow("stationName"));
                toStationCode = tostation.getString(tostation.getColumnIndexOrThrow("stationCode"));



            }
        }
        tostation.close();

        if(currentapiVersion>=android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH)
        {
            Tracker t = ((GoogleAnalyticsApp) getApplication()).getTracker(TrackerName.APP_TRACKER);
            t.setScreenName(fromStationNAme + " To "+ tostation_+"- Express");
            t.send(new HitBuilders.AppViewBuilder().build());

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    AdView mAdView = (AdView) findViewById(R.id.adView);
                    AdRequest adRequest = new AdRequest.Builder().build();
                    mAdView.loadAd(adRequest);
                }
            });
        }
        ApplyUserInfo();
        new LoadingTrain().execute();
    }

    public void ApplyUserInfo(){
        SharedPreferences userPref = getSharedPreferences("userCred",MODE_PRIVATE);
        String name = userPref.getString("name","Guest User");
        email = userPref.getString("email","");
        String imageUrl = userPref.getString("image_value","");

        Log.e("imageUrl ",imageUrl);
        nameTxt.setText(name);
        if(!imageUrl.equalsIgnoreCase("")) {
            Picasso.with(context).load(imageUrl).memoryPolicy(MemoryPolicy.NO_CACHE).into(profile_image);
        }
    }
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the camera action
            Intent intent = new Intent(getApplicationContext(), LocalTrain.class);
            intent.putExtra("callfrom","other");
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_runningstatus) {

            Intent intent = new Intent(getApplicationContext(), Homepage.class);
            startActivity(intent);
            finish();

        } else if (id == R.id.nav_cancelled) {

            Intent intent = new Intent(getApplicationContext(), ActivityCancelTrain.class);
            startActivity(intent);
            finish();

        }  else if (id == R.id.nav_pnrstatus) {

            Intent intent = new Intent(getApplicationContext(), Pnr.class);
            startActivity(intent);
            finish();

        } else if (id == R.id.nav_saveroute) {

            Intent intent = new Intent(getApplicationContext(), SaveSearch.class);
            startActivity(intent);
            finish();

        } else if (id == R.id.nav_feedback) {
            Intent intent = new Intent(getApplicationContext(), feedback.class);
            startActivity(intent);
            finish();

        } else if (id == R.id.nav_fblike) {

            Intent notificationIntent =null;
            notificationIntent = new Intent(Intent.ACTION_VIEW);
            notificationIntent.setData(Uri.parse("https://www.facebook.com/localtimetable"));
            startActivity(notificationIntent);


        } else if (id == R.id.nav_theme) {

            if(email.equalsIgnoreCase("")){
                Intent i = new Intent(ExTrainTimetable.this, WelcomeActivity.class);
                startActivity(i);
                finish();
            }else {
                Intent i = new Intent(ExTrainTimetable.this, SettingsActivity.class);
                startActivity(i);
            }


        } else if (id == R.id.nav_rate) {

            Intent notificationIntent =null;
            notificationIntent = new Intent(Intent.ACTION_VIEW);
            notificationIntent.setData(Uri.parse("market://details?id="+getApplicationContext().getPackageName()));
            startActivity(notificationIntent);


        } else if (id == R.id.nav_aboutus) {

            Intent intent = new Intent(getApplicationContext(), BuildWebsite.class);
            startActivity(intent);
            finish();
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private class LoadingTrain extends AsyncTask<Void, Void, Void> {
  	  
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(ExTrainTimetable.this);
            pDialog.setMessage("Loading Train List ...");
            pDialog.setCancelable(false);
            pDialog.show();
 
        }
 
        @Override
        protected Void doInBackground(Void... arg0) {
       	 

        	
        	
        	listOftrain = new ArrayList<TimetableListBean>();
		      
	          traincursor = dbHelper.getTrainList(fromstationcode,tostationcode);
	          
	          if(traincursor.getCount()>0)
	 	  	  {

                  savestation();
	 	     	  while (traincursor.moveToNext()) {
                      Integer  trainId=0;

	 	     		String remarks ="";
	 	     		 trainId = traincursor.getInt(traincursor.getColumnIndexOrThrow("trainId"));
                      Log.e("1st trn id = ",""+traincursor.getInt(traincursor.getColumnIndexOrThrow("trainId")));
	 	     		String trainNo = traincursor.getString(traincursor.getColumnIndexOrThrow("trainNo"));
	 	 				
	 	  				
	 	  				String trainName = traincursor.getString(traincursor.getColumnIndexOrThrow("trainName"));
	 	  				String SourceStation  = traincursor.getString(traincursor.getColumnIndexOrThrow("SourceStation"));
	 	  				
	 	  				String sourcestationcode  = traincursor.getString(traincursor.getColumnIndexOrThrow("sourcestationcode"));
	 	  				
	 	  				String sourcearrival  = traincursor.getString(traincursor.getColumnIndexOrThrow("sourcearrival"));
	 	  				
	 	  				String DestinationStation  = traincursor.getString(traincursor.getColumnIndexOrThrow("DestinationStation"));
	 	  				
	 	  				String destStationcode  = traincursor.getString(traincursor.getColumnIndexOrThrow("destStationcode"));
	 	  				
	 	  				String destarrival  = traincursor.getString(traincursor.getColumnIndexOrThrow("destarrival"));
	 	  				
	 	  				
	 	  				Integer sun=0;
	 	  			 Integer mon=0;
	 	  			 Integer tue=0;
	 	  			 Integer wed=0;
	 	  			 Integer thu=0;
	 	  			 Integer fri=0;
	 	  			 Integer sat=0;
                     String available_text="";
                    Integer off_days=0;
                    Integer only_sat=0;
	 	  						
	 	  						
	 	  						 sun = traincursor.getInt(traincursor.getColumnIndexOrThrow("sun"));
	 	  						 mon = traincursor.getInt(traincursor.getColumnIndexOrThrow("mon"));
	 	  						 tue = traincursor.getInt(traincursor.getColumnIndexOrThrow("tue"));
	 	  						 wed = traincursor.getInt(traincursor.getColumnIndexOrThrow("wed"));
	 	  						 thu = traincursor.getInt(traincursor.getColumnIndexOrThrow("thu"));
	 	  						 fri = traincursor.getInt(traincursor.getColumnIndexOrThrow("fri"));
	 	  						 sat = traincursor.getInt(traincursor.getColumnIndexOrThrow("sat"));
	 	  						
	 	  		 				
	 	  		 				Log.e("trainId",""+trainId);
                      int stops;
                      String takeTime="";
                      String src_time,dest_time;
                      Date src_localTime = null;
                      Date dest_localTime = null;
                      SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                      String[] stopsArr = dbHelper.getStationStop(trainId,fromstationcode,tostationcode);
                      //String stops = String.valueOf(stationList.getCount());
                      stops =Integer.parseInt(stopsArr[0]);

                      src_time = stopsArr[1];
                      dest_time = stopsArr[2];

                      try {
                              src_localTime = format.parse(src_time);
                              dest_localTime = format.parse(dest_time);

                              long diff = dest_localTime.getTime() - src_localTime.getTime();

                              long diffSeconds = diff / 1000 % 60;
                              long diffMinutes = diff / (60 * 1000) % 60;
                              long diffHours = diff / (60 * 60 * 1000) % 24;
                              long diffDays = diff / (24 * 60 * 60 * 1000);

                              Log.e(" days ",""+diffDays);
                              Log.e( " hours ",""+diffHours );
                              Log.e(" minutes ",""+diffMinutes);
                              Log.e(" seconds ",""+diffSeconds);

                          /*if(diffDays != 0 && diffHours != 0) {
                              takeTime = diffDays + " D " + diffHours + " H";
                          }else{
                              if(diffDays ==0){
                                  takeTime = diffHours + " H";
                              }else if(diffHours ==0){
                                  takeTime = diffDays + " D";
                              }
                          }*/
                          if(diffDays > 0){
                              takeTime += diffDays+ "D ";
                          }
                          if(diffHours > 0){
                              takeTime += diffHours+ "H ";
                          }

                          if(diffMinutes > 0){
                              takeTime += diffMinutes+ "M ";
                          }
                          /*src_localTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).parse(src_time);
                          Log.e("SRC TimeStamp is ","" +src_localTime.getTime());

                          dest_localTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).parse(dest_time);
                          Log.e("DEST TimeStamp is ","" +dest_localTime.getTime());*/
                      } catch (java.text.ParseException e) {
                          Log.e("e","" +e);
                          e.printStackTrace();
                      }

                      //takeTime =stopsArr[1];
	 	  			 if(sun==1 && mon==1 && tue==1 && wed==1 &&  thu==1 &&  fri==1 &&  sat==1   )
	 	  			 {
	 	  				 available_text +="All Days" ;
	 	  			 }
	 	  			 else if(sun==0 && mon==0 && tue==0 && wed==0 &&  thu==0 &&  fri==0 &&  sat==1)
	 	  			 {
	 	  				 available_text +="Only Saturday" ; 
	 	  				only_sat=1;
	 	  			 }
	 	  			 else
	 	  			 {
	 	  				  if(sun==0)
	 	  				 {
	 	  					 off_days=1;
	 	  				  available_text +="Sun " ;
	 	  				 }
	 	  				  if(mon==0)
	 	  				 {
	 	  					 off_days=1;
	 	  				  available_text +="Mon " ;
	 	  				 }
	 	  				 
	 	  				  if(tue==0)
	 	  				 {
	 	  					 off_days=1;
	 	  				  available_text +="Tue " ;
	 	  				 }
	 	  				 if(wed==0)
	 	  				 {
	 	  					 off_days=1;
	 	  				  available_text +="Wed " ;
	 	  				 }
	 	  				  
	 	  				  if(thu==0)
	 	  					 {
	 	  						 off_days=1;
	 	  					  available_text +="Thu " ;
	 	  					 }
	 	  				  if(fri==0)
	 	  					 {
	 	  						 off_days=1;
	 	  					  available_text +="Fri " ;
	 	  					 }
	 	  				  if(sat==0)
	 	  					 {
	 	  						 off_days=1;
	 	  					  available_text +="Sat " ;
	 	  					 }
	 	  			 }
	 	  				
	 	  				
	 	  				listOftrain.add(new TimetableListBean(trainId,trainNo,trainName,SourceStation,sourcestationcode,sourcearrival,
	 	  						DestinationStation,destStationcode,destarrival,available_text,off_days,only_sat,remarks,stops,takeTime));
	 	 			}
	 	     	norecrod=1;
	 	     	
	 	  	 } 
	          traincursor.close();
       	 
        	
       	
       	 
         
       
            
       	    
       		
			return null;}
 
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            lv.setVisibility(View.VISIBLE);
            progressBar1.setVisibility(View.GONE);
            Log.e("listOftrain count==",""+listOftrain.size());
            adapter = new TimeTableListAdapter(getApplicationContext(), listOftrain);
 	    	//  infalter = getLayoutInflater();
 	    	// header = (ViewGroup) infalter.inflate(R.layout.clientlistheader, lv, false);
 	        // lv.addHeaderView(header);
 	    	// lv.setOnItemClickListener(this);
          lv.setAdapter(adapter);
 	    	// lv.setOnClickListener(new ClickListenerList());
 	    	lv.setOnItemClickListener(new OnItemClickListener() {
 	    	      public void onItemClick(AdapterView<?> parent, View view,
 	    	          int position, long id) {
 	    	    	  
 	    	    	 final TimetableListBean entry = listOftrain.get(position);
 	    			int trainId = entry.getTrainId();
 	    			String trainNo =String.valueOf(entry.getTrainNo());
 	    			
 	    			Intent intent = new Intent(getApplicationContext(), ExTimeTableDetails.class);

 	    			intent.putExtra("trainId", trainId);
 	    			Log.e("tarinId","aa="+trainId+"==abb="+trainNo);
 	    			intent.putExtra("trainNo", trainNo);
 	    			intent.putExtra("fromstation", fromstationcode);
 	    			intent.putExtra("tostation", tostationcode);
 	    	    	 
 	    			
 	      	        startActivity(intent);

                      overridePendingTransition(R.anim.anim_slide_in_left,
                              R.anim.anim_slide_out_left);
 	    	      }
 	    	    });
 	    	try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
 	    	
 	    	if (pDialog.isShowing())
                pDialog.dismiss();
 	    	if(norecrod==0)
 	    	{
 	    		successAlert();	
 	    	}
            
        }
 
    }


    public void savestation (){


        //Toast.makeText(ExTrainTimetable.this,"dfgd",Toast.LENGTH_SHORT).show();

        /*---------------- GET SESSION ARRAY IF ANY ------------------*/
        String recentsearchjson = prefs.getString("recentsearchEx", "null");
        if(recentsearchjson.equals("null")){
            Log.e("Recent JSON==","NO VALUE SAVED");
        } else{
            Log.e("Recent JSON==",""+recentsearchjson);
            try {
                JSONArray recent_arr = new JSONArray(recentsearchjson);
                Log.e("recent_arr JSON==",""+recent_arr);

                int count=0;
                int arr_count = recent_arr.length();
                if(arr_count <4){
                    count = recent_arr.length();
                } else{
                    count = 4;
                }
                Log.e("COUNT==",""+count);
                for(int i=0;i<count;i++){
                    JSONObject obj = recent_arr.getJSONObject(i);
                    RecentSearch recentsearch = new RecentSearch();
                    recentsearch.setToStationName(obj.getString("ToStationName"));
                    recentsearch.setFromStationName(obj.getString("FromStationName"));



                    String[] fromstationarr,tostationarr;
                    if(obj.getString("FromStationName").contains("-")) {
                        fromstationarr = obj.getString("FromStationName").split("-");
                        fromcode = fromstationarr[0].trim().toUpperCase(); // this will contain "Fruit"
                        //fromstationname = fromstationarr[1].trim().toUpperCase();
                    } else{
                        //fromstationarr = fromstation.split("-");
                        fromcode = obj.getString("FromStationName").toUpperCase();
                    }

                    if(obj.getString("ToStationName").contains("-")) {
                        tostationarr = obj.getString("ToStationName").split("-");
                        tocode = tostationarr[0].trim().toUpperCase();
                        //tostationname = tostationarr[1].trim().toUpperCase();
                    } else{
                        //tostationarr = tostation.split("-");
                        tocode = obj.getString("ToStationName").toUpperCase();
                    }


                    if((obj.getString("ToStationName").equalsIgnoreCase(Constants.EX_TOSTATIONCODE)                   /*chk by name*/
                            && obj.getString("FromStationName").equalsIgnoreCase(Constants.EX_FROMSTATIONCODE))
                            /*|| (obj.getString("ToStationName").equalsIgnoreCase(Constants.EX_FROMSTATIONCODE)
                            && obj.getString("FromStationName").equalsIgnoreCase(Constants.EX_TOSTATIONCODE))*/

                            || (fromcode.equalsIgnoreCase(Constants.EX_FROMSTATIONCODE)                  /*chk by code*/
                            && tocode.equalsIgnoreCase(Constants.EX_TOSTATIONCODE))
                            /*|| (fromcode.equalsIgnoreCase(Constants.EX_TOSTATIONCODE)
                            && tocode.equalsIgnoreCase(Constants.EX_FROMSTATIONCODE))*/){

                        flag = true;
                    }
                    recentArrayList.add(recentsearch);
                }

                Log.e("recentArrayList===",""+recentArrayList.size());



            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        Log.e("TOSTATIONCODE_LOCAL",""+Constants.EX_TOSTATIONCODE);
        Log.e("FROMSTATIONCODE_LOCAL",""+Constants.EX_FROMSTATIONCODE);

        if(flag == false) {
            RecentSearch rs = new RecentSearch();
            rs.setFromStationName(Constants.EX_FROMSTATIONCODE);
            rs.setToStationName(Constants.EX_TOSTATIONCODE);
            recentSearchList.add(rs);

            if (recentArrayList.size() > 0) {
                recentSearchList.addAll(recentArrayList);

            }
            Log.e("SAVED STATION COUNT ", "" + recentSearchList.size());
            for (int i = 0; i < recentSearchList.size(); i++) {
                Log.e("SAVED STATION ", recentSearchList.get(i).getFromStationName() + "" + recentSearchList.get(i).getToStationName());

            }
            Gson gson = new Gson();
            String json = gson.toJson(recentSearchList);
            editorUser.putString("recentsearchEx", json);
            editorUser.commit();
        }
    }
    
    @Override
    public void onBackPressed() {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if(drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        }else {
            super.onBackPressed();
            if(isexpress==0)
            {
        /*Intent intent = new Intent(getApplicationContext(), ExpressTrainHome.class);

     	startActivity(intent);*/
                /*Intent i3 = new Intent(ExTrainTimetable.this, LocalTrain.class);
                i3.putExtra("callfrom","other");
                startActivity(i3);*/
                finish();
                overridePendingTransition(R.anim.anim_slide_in_right,
                        R.anim.anim_slide_out_right);
                Log.e("if","TRUE");
            }
            else
            {
                /*Intent i3 = new Intent(ExTrainTimetable.this, LocalTrain.class);
                i3.putExtra("callfrom","other");
                startActivity(i3);*/
                finish();
                overridePendingTransition(R.anim.anim_slide_in_right,
                        R.anim.anim_slide_out_right);
                Log.e("ELSE","TRUE");
            }
        }



       // overridePendingTransition(R.anim.lefttoright, R.anim.righttoleft);
    }
    
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
		case DIALOG_DOWNLOAD_PROGRESS:
			mProgressDialog = new ProgressDialog(this);
			mProgressDialog.setMessage("Loading Train List...");
			mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			mProgressDialog.setCancelable(false);
			mProgressDialog.show();
			return mProgressDialog;
		default:
			return null;
        }
   }
    
    public void successAlert()
    {
  	
  	  AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(
 		        ExTrainTimetable.this);

 		// Setting Dialog Title
 		alertDialog2.setTitle("Alert");

 		// Setting Dialog Message
 		alertDialog2.setMessage("No train found!!");

 		// Setting Icon to Dialog
 		

 		// Setting Positive "Yes" Btn
 		alertDialog2.setNeutralButton("OK",
 		        new DialogInterface.OnClickListener() {
 		            public void onClick(DialogInterface dialog, int which) {
 		                // Write your code here to execute after dialog
 		            
 		            	/*Intent i3 = new Intent(ExTrainTimetable.this, LocalTrain.class);
 		                i3.putExtra("callfrom","ExTrainTimetable");
 	                    startActivity(i3);*/
                        finish();
                       /* ExpressTrainFragment ex = new ExpressTrainFragment();
                        getSupportFragmentManager().beginTransaction().replace(R.id.drawer_layout, ex).commit();*//*

                        ExpressTrainFragment ex = new ExpressTrainFragment();
                        FragmentManager manager = getSupportFragmentManager();
                        FragmentTransaction transaction = manager.beginTransaction();
                        transaction.add(R.id.drawer_layout,ex,"tab2");
                        transaction.commit();
*/
 	                    //
 	                 

 		                
 		            }
 		        });
 		alertDialog2.show();
 		
    } 
   
   

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        dbHelper.close();
       
       
    }  
   
}