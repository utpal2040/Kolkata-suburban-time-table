package com.matainja.runingstatus;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

import com.matainja.runingstatus.Database.DataBaseHelperExpress;
import com.matainja.runingstatus.Database.DataBaseHelperFile;
import com.matainja.runingstatus.Database.DatabaseAdapter;

import java.io.IOException;

/**
 * Created by matainja on 13-Feb-17.
 */

public class landing  extends AppCompatActivity {

    protected boolean _active = true;
    protected int _splashTime = 1000;
    private DatabaseAdapter dbHelper;
    SharedPreferences userPref;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.landing);

        userPref = getSharedPreferences("userCred", Context.MODE_PRIVATE);
        DataBaseHelperFile myDbHelper = new DataBaseHelperFile(null);
        myDbHelper = new DataBaseHelperFile(this);

        if (Build.VERSION.SDK_INT > 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }

        try {

            myDbHelper.createDataBase();



        } catch (IOException ioe) {

            throw new Error("Unable to create database");

        }

        DataBaseHelperExpress myDbHelperEx = new DataBaseHelperExpress(null);
        myDbHelperEx = new DataBaseHelperExpress(this);

        try {

            myDbHelperEx.createDataBase();



        } catch (IOException ioe) {

            throw new Error("Unable to create database");

        }


        // Database Connection Open
        dbHelper = new DatabaseAdapter(this);
        dbHelper.open();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {

                //if(!userPref.getString("email","").equals("")){
                    Intent i3 = new Intent(landing.this, LocalTrain.class);
                    i3.putExtra("callfrom", "other");
                    startActivity(i3);
                    finish();
                /*}else {
                    Intent i3 = new Intent(landing.this, WelcomeActivity.class);
                    i3.putExtra("callfrom", "other");
                    startActivity(i3);
                    finish();
                }*/
                /*landing.this.overridePendingTransition(R.anim.lefttoright,
                        R.anim.righttoleft);*/
            }
        }, _splashTime);
    }
}
