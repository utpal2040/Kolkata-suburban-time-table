package com.matainja.runingstatus;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.matainja.runingstatus.Adapter.SaveSearchRecyclerAdapter;
import com.matainja.runingstatus.CustomView.ItemClickSupport;
import com.matainja.runingstatus.Database.DatabaseAdapterSaveSearch;
import com.matainja.runingstatus.GoogleAnalyticsApp.TrackerName;
import com.matainja.runingstatus.Model.SaveSearchBean;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

//import android.support.v7.app.AlertController;




public class SaveSearch extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{
	
    
	 ListView leftmenulist;
    int norecrod=0;
    ProgressDialog pDialog;
    public static final int DIALOG_DOWNLOAD_PROGRESS = 1;
   
   
   
    private DatabaseAdapterSaveSearch dbHelpersave;
    Cursor SaveSearchCursor;
    RecyclerView lv;
    Context context;
    private ProgressDialog mProgressDialog;
    ArrayList<SaveSearchBean> listOfsaveSearch;
    LayoutInflater infalter;
	 ViewGroup header;
	 //SimpleSideDrawer slide_menu_train_list;
	 ImageView leftmenu;
	 int currentapiVersion=0;

	DrawerLayout drawer;
	Toolbar toolbar;
    SharedPreferences sp;
    CircleImageView profile_image;
    TextView nameTxt;
    RelativeLayout headerFrame;
    String email;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	//Remove title bar
    	  //this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.savesearchlist);
        overridePendingTransition(R.anim.anim_slide_in_left,
                R.anim.anim_slide_out_left);
        currentapiVersion = android.os.Build.VERSION.SDK_INT;
       // Log.v("start activity","ooo");
        
     // Database Connection Open
        dbHelpersave = new DatabaseAdapterSaveSearch(this);
        dbHelpersave.open();

		toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.setDrawerIndicatorEnabled(true);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View headerview = navigationView.getHeaderView(0);
        profile_image = (CircleImageView) headerview.findViewById(R.id.profile_image);
        nameTxt = (TextView) headerview.findViewById(R.id.name);
        headerFrame = (RelativeLayout) headerview.findViewById(R.id.headerFrame);
        headerFrame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(email.equalsIgnoreCase("")){
                    Intent i = new Intent(SaveSearch.this, WelcomeActivity.class);
                    startActivity(i);
                    finish();
                }else {
                    Intent i = new Intent(SaveSearch.this, SettingsActivity.class);
                    startActivity(i);
                }
            }
        });
        ApplyUserInfo();
        sp = getSharedPreferences("themeColor", Context.MODE_PRIVATE);

        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorLocalStatus));
        }*/

        Handler handler = new Handler();
        Runnable r = new Runnable() {
            public void run() {
            	/* TRacking Code */
                if(currentapiVersion>=android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH)
                {
        	      Tracker t = ((GoogleAnalyticsApp) getApplication()).getTracker(TrackerName.APP_TRACKER);
        			t.setScreenName("Save Search");
        			t.send(new HitBuilders.AppViewBuilder().build());
        			
        			AdView mAdView = (AdView) findViewById(R.id.adView);
        	        AdRequest adRequest = new AdRequest.Builder().build();
        	        mAdView.loadAd(adRequest);
                }               
            }
        };
        handler.postDelayed(r, 200);
	      
        
        // left menu list and click
        
        /*leftmenu =(ImageView)findViewById(R.id.leftmenu);
        slide_menu_train_list = new SimpleSideDrawer(this);
        slide_menu_train_list.setLeftBehindContentView(R.layout.left_menu);
	     leftmenulist = (ListView)findViewById(R.id.menulist);*/
	     

	        //left menu
	        
	        /*leftmenu.setOnClickListener(new View.OnClickListener() {
	        	
	            @Override
	            public void onClick(View v) {
	                // TODO Auto-generated method stub
	            	
	            	
	            	
	            	//slide_menu_train_list.closeRightSide();
	            	//slide_menu_train_list.toggleLeftDrawer();
	            	
	            	
	            	
	            	
	            }
	        });*/
	        // drawer open
	     /*leftmenulist.setOnItemClickListener(new OnItemClickListener() {
	   	      public void onItemClick(AdapterView<?> parent, View view,
	   	          int position, long id) {
	   	        // When clicked, show a toast with the TextView text
	   	    	if(id == 0)
	   	    	  {
	   	    		Intent intent = new Intent(getApplicationContext(), LocalTrain.class);
	             	
	             	startActivity(intent);
	             	finish();
	   	    	  }
	   	    	  else if(id == 1)
	   	    	  {
	   	    		  
      Intent intent = new Intent(getApplicationContext(), ExpressTrainHome.class);
	             	
	             	startActivity(intent);  
	             	
	   	    	  }
	   	    	
	   	    	  
	   	    	else if(id == 2)
	   	    	  {
	   	    		  
  Intent intent = new Intent(getApplicationContext(), Homepage.class);
	             	
	             	startActivity(intent);  
	             	finish();
	   	    	  }
	   	    	  
	   	    	else if(id == 3)
	   	    	  {
	   	    		  
Intent intent = new Intent(getApplicationContext(), feedback.class);
	             	
	             	startActivity(intent);  
	             	finish();
	   	    	  }
	   	    	
	   	    	else if(id == 4)
	   	    	  {
	   	    		  
	   	    		Intent notificationIntent =null;
	   	    		notificationIntent = new Intent(Intent.ACTION_VIEW);
		   	        notificationIntent.setData(Uri.parse("https://www.facebook.com/localtimetable"));
		   	        startActivity(notificationIntent);
	   	    	  }
	   	    	
	   	    	else if(id == 5)
	   	    	  {
	   	    		  
	   	    		
	   	    		Intent notificationIntent =null;
	   	    		notificationIntent = new Intent(Intent.ACTION_VIEW);
		   	        notificationIntent.setData(Uri.parse("market://details?id="+getApplicationContext().getPackageName()));
		   	        startActivity(notificationIntent);
	   	    	  }
	   	    	
	   	    	else if(id == 6)
	   	    	  {
	   	    		  
	   	    		
Intent intent = new Intent(getApplicationContext(), SaveSearch.class);
	             	
	             	startActivity(intent);  
	             	finish();
	   	    	  }
	   	    	else if(id == 7)
	   	    	  {
	   	    		  
	   	    		
Intent intent = new Intent(getApplicationContext(), Pnr.class);
	             	
	             	startActivity(intent);  
	             	finish();
	   	    	  }
	   	    	else if(id == 8)
	   	    	  {
	   	    		  
	   	    		
Intent intent = new Intent(getApplicationContext(), BuildWebsite.class);
	             	
	             	startActivity(intent);  
	             	
	   	    	  }
	         			
	   	      }
	   	    });*/
	     
	     /// end left menu list 
	        /*Typeface custom_font = Typeface.createFromAsset(getAssets(),
	        	      "fonts/Bodoni-Italic.TTF");
	        TextView homepage_header_text = (TextView) findViewById(R.id.homepage_header_text);
	        homepage_header_text.setTypeface(custom_font);*/
	        
	       
		 	 /// Listview for train
		        lv = (RecyclerView)findViewById(R.id.list);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(SaveSearch.this);
                lv .setLayoutManager(mLayoutManager);
                lv.setNestedScrollingEnabled(false);

		         new LoadingSaveSearch().execute();

        if(sp.getInt("expressColor",0)!=0) {
            toolbar.setBackgroundColor(sp.getInt("expressColor", 0));

            int[] demo_colors = getResources().getIntArray(R.array.demo_colors);
            int[] demo_colors_mat = getResources().getIntArray(R.array.demo_colors_mat);

            ArrayList<String> colorString = new ArrayList<String>();
            ArrayList<String> colorString_mat = new ArrayList<String>();

            for (int i = 0; i < demo_colors.length; i++) {

                colorString.add(String.valueOf(demo_colors[i]));

            }

            for (int i = 0; i < demo_colors_mat.length; i++) {

                colorString_mat.add(String.valueOf(demo_colors_mat[i]));

            }
            Log.e("position ",colorString.indexOf(String.valueOf(sp.getInt("expressColor",0))) + "");
            int color_mat_val = Integer.parseInt(colorString_mat.get(colorString.indexOf(String.valueOf(sp.getInt("expressColor", 0)))));

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(color_mat_val);
            }
        }else{
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(getResources().getColor(R.color.colorExpress));
            }
        }
        
    }


    public void ApplyUserInfo(){
        SharedPreferences userPref = getSharedPreferences("userCred",MODE_PRIVATE);
        String name = userPref.getString("name","Guest User");
        email = userPref.getString("email","");
        String imageUrl = userPref.getString("image_value","");

        Log.e("imageUrl ",imageUrl);
        nameTxt.setText(name);
        if(!imageUrl.equalsIgnoreCase("")) {
            Picasso.with(context).load(imageUrl).memoryPolicy(MemoryPolicy.NO_CACHE).into(profile_image);
        }
    }

	@Override
	public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the camera action
            Intent intent = new Intent(getApplicationContext(), LocalTrain.class);
            intent.putExtra("callfrom","other");
            startActivity(intent);
            finish();

        } else if (id == R.id.nav_runningstatus) {

            Intent intent = new Intent(getApplicationContext(), Homepage.class);

            startActivity(intent);
            finish();

        } else if (id == R.id.nav_cancelled) {

            Intent intent = new Intent(getApplicationContext(), ActivityCancelTrain.class);
            startActivity(intent);
            finish();

        }  else if (id == R.id.nav_pnrstatus) {

            Intent intent = new Intent(getApplicationContext(), Pnr.class);
            startActivity(intent);
            finish();

        } else if (id == R.id.nav_saveroute) {

            /*Intent intent = new Intent(getApplicationContext(), Pnr.class);
            startActivity(intent);
            finish();*/

        } else if (id == R.id.nav_feedback) {
            Intent intent = new Intent(getApplicationContext(), feedback.class);

            startActivity(intent);
            finish();

        } else if (id == R.id.nav_fblike) {

            Intent notificationIntent =null;
            notificationIntent = new Intent(Intent.ACTION_VIEW);
            notificationIntent.setData(Uri.parse("https://www.facebook.com/localtimetable"));
            startActivity(notificationIntent);


        } else if (id == R.id.nav_theme) {

            if(email.equalsIgnoreCase("")){
                Intent i = new Intent(SaveSearch.this, WelcomeActivity.class);
                startActivity(i);
                finish();
            }else {
                Intent i = new Intent(SaveSearch.this, SettingsActivity.class);
                startActivity(i);
            }


        } else if (id == R.id.nav_rate) {

            Intent notificationIntent =null;
            notificationIntent = new Intent(Intent.ACTION_VIEW);
            notificationIntent.setData(Uri.parse("market://details?id="+getApplicationContext().getPackageName()));
            startActivity(notificationIntent);


        } else if (id == R.id.nav_aboutus) {

            Intent intent = new Intent(getApplicationContext(), BuildWebsite.class);
            startActivity(intent);
            finish();
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
	}


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if(drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        }else {
            super.onBackPressed();
            finish();
            overridePendingTransition(R.anim.anim_slide_in_right,
                    R.anim.anim_slide_out_right);
        }
    }

	private class LoadingSaveSearch extends AsyncTask<Void, Void, Void> {
  	  
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(SaveSearch.this);
            pDialog.setMessage("Loading Save Route List ...");
            pDialog.setCancelable(false);
            pDialog.show();
 
        }
 
        @Override
        protected Void doInBackground(Void... arg0) {
       	 

        	
        	
        	listOfsaveSearch = new ArrayList<SaveSearchBean>();
		      
	          SaveSearchCursor = dbHelpersave.getSaveSearch();
	          
	          if(SaveSearchCursor.getCount()>0)
	 	  	  {	 
	 	     	 
	 	     	  while (SaveSearchCursor.moveToNext()) {
	 	 				
	 	     		Integer Id = SaveSearchCursor.getInt(SaveSearchCursor.getColumnIndexOrThrow("_id"));
	 	     		String Name = SaveSearchCursor.getString(SaveSearchCursor.getColumnIndexOrThrow("searhcname"));
	 	 				
	 	     		
	 	  				String srcStation = SaveSearchCursor.getString(SaveSearchCursor.getColumnIndexOrThrow("sourcestation"));
	 	  				String desStation  = SaveSearchCursor.getString(SaveSearchCursor.getColumnIndexOrThrow("destinationstation"));
	 	  				
	 	  				
	 	  				
	 	  				Integer islocal = SaveSearchCursor.getInt(SaveSearchCursor.getColumnIndexOrThrow("islocal"));
	 	  				
	 	  				
	 	  						
	 	  		 				
	 	  		 				//Log.v("Station:name",stationName);
	 	  		 				
	 	  		 		
	 	  			 
	 	  				
	 	  				
	 	  				listOfsaveSearch.add(new SaveSearchBean(Id,Name,srcStation,desStation,islocal));
	 	 			}
	 	     	norecrod=1;
	 	     	
	 	  	 } 
	          SaveSearchCursor.close();
       	 
        	
       	
       	 
         
       
            
       	    
       		
			return null;}
 
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            SaveSearchRecyclerAdapter adapter = new SaveSearchRecyclerAdapter(SaveSearch.this, listOfsaveSearch);
 	    	
          lv.setAdapter(adapter);

            ItemClickSupport.addTo(lv).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
                @Override
                public void onItemClicked(RecyclerView recyclerView, int position, View v) {

                    final SaveSearchBean entry = listOfsaveSearch.get(position);
                    Integer Id = entry.getId();
                    String srcStation =entry.getSrcStation();
                    String desStation =entry.getDesstation();
                    Intent intent = new Intent(getApplicationContext(), TrainTimetable.class);

                    intent.putExtra("fromstation", srcStation);
                    intent.putExtra("timefilter", "");
                    intent.putExtra("tostation", desStation);
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.putExtra("filterbytime", 0);
                    intent.putExtra("callfrom","savesearch");

                    startActivity(intent);
                }
            });
 	    	// lv.setOnClickListener(new ClickListenerList());
 	    	/*lv.setOnItemClickListener(new OnItemClickListener() {
 	    	      public void onItemClick(AdapterView<?> parent, View view,
 	    	          int position, long id) {
 	    	    	 final SaveSearchBean entry = listOfsaveSearch.get(position);
  	    			Integer Id = entry.getId();
  	    			String srcStation =entry.getSrcStation();
  	    			String desStation =entry.getDesstation();
 	    	    	 Intent intent = new Intent(getApplicationContext(), TrainTimetable.class);
 	    	    	 
 	    	    	intent.putExtra("fromstation", srcStation);
 	   			intent.putExtra("timefilter", "");
 	   			intent.putExtra("tostation", desStation);
 	   			intent.setAction(Intent.ACTION_VIEW);
 	   			
 	   				intent.putExtra("filterbytime", 0);
 	   			
 	   			
 	   			startActivity(intent);
 	    	      }
 	    	    });*/
 	    	
 	    	
 	    	if (pDialog.isShowing())
                pDialog.dismiss();
 	    	if(norecrod==0)
 	    	{
 	    		successAlert();	
 	    	}
            
        }
 
    }

    
    
    
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
		case DIALOG_DOWNLOAD_PROGRESS:
			mProgressDialog = new ProgressDialog(this);
			mProgressDialog.setMessage("Loading Train List...");
			mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			mProgressDialog.setCancelable(false);
			mProgressDialog.show();
			return mProgressDialog;
		default:
			return null;
        }
   }
    
    public void successAlert()
    {
  	
  	  AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(
 		        SaveSearch.this);

 		// Setting Dialog Title
 		alertDialog2.setTitle("Alert");

 		// Setting Dialog Message
 		alertDialog2.setMessage("No Route Bookmarks Found");

 		// Setting Icon to Dialog
 		

 		// Setting Positive "Yes" Btn
 		alertDialog2.setNeutralButton("OK",
 		        new DialogInterface.OnClickListener() {
 		            public void onClick(DialogInterface dialog, int which) {
 		                // Write your code here to execute after dialog
 		            
 	                 

 		                
 		            }
 		        });
 		alertDialog2.show();
 		
    } 
   
    
    
    
    
    
    
   

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        dbHelpersave.close();
       
       
    }  
   
}