package com.matainja.runingstatus;

import android.app.ActionBar.LayoutParams;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.matainja.runingstatus.Adapter.CustomAdapterAge;
import com.matainja.runingstatus.Adapter.CustomAdapterClass;
import com.matainja.runingstatus.Adapter.CustomAdapterTypetckt;
import com.matainja.runingstatus.Adapter.TimeTableDetailsAdapter;
import com.matainja.runingstatus.Adapter.TimeTableListAdapter;
import com.matainja.runingstatus.Common.ConnectionDetector;
import com.matainja.runingstatus.Connectivity.HttpConnection;
import com.matainja.runingstatus.CustomView.Utility;
import com.matainja.runingstatus.Database.DatabaseAdapterEx;
import com.matainja.runingstatus.GoogleAnalyticsApp.TrackerName;
import com.matainja.runingstatus.Model.PeopleAge;
import com.matainja.runingstatus.Model.StationListBean;
import com.matainja.runingstatus.Model.TypeClass;
import com.matainja.runingstatus.Model.TypeTicket;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;


public class ExTimeTableDetails extends AppCompatActivity {
	ListView list;
    TextView source;
    TextView arrival;
    TextView departure;
    TextView status;
    Button Btngetdata;
    TextView constatus ;
    ArrayList<HashMap<String, String>> oslist = new ArrayList<HashMap<String, String>>();
    //URL to get JSON Array
    private static String url = "";
    //JSON Node Names
    
    ProgressDialog pDialog;
    public static final int DIALOG_DOWNLOAD_PROGRESS = 1;
    Bundle extras;
    int trainId;
    String  trainNo;
    String tostationcode;
    private DatabaseAdapterEx dbHelper;
    Cursor traincursor;
    Cursor stationList;
    ListView lv;
    private ProgressDialog mProgressDialog;
    ArrayList<StationListBean> stationListView;
    LayoutInflater infalter;
	 ViewGroup header;
	 TimeTableListAdapter adapter;
	 TextView train_name;
	 TextView deststation;
	 String trainName;
	 String fromstation;
	 String tostation;
	 int sun;
	 int mon;
	 int tue;
	 int wed;
	 int thu;
	 int fri;
	 int sat;
	 TextView days;
	 String TrainNo;
	 LinearLayout farebttn;
	 LinearLayout availabilty;
	 int currentapiVersion=0;
	 final Context context = this;
	 CustomAdapterClass spinnerClassadapter;
	 CustomAdapterAge spinnerAgeadapter;
	 CustomAdapterTypetckt spinnertcktadapter;
	 ExTimeTableDetails activity = null;
	 public  ArrayList<TypeClass> TypeclassSpinner;
	 public  ArrayList<PeopleAge> TypeageSpinner ;
	 public  ArrayList<TypeTicket> TypetcktSpinner;
	 String mClass="";
	 String mAge="";
	 String mTicket="";
	 LinearLayout farelayout;
	 LinearLayout adviewlayout ;
	 ConnectionDetector cd;
	 Toolbar toolbar;
	RelativeLayout Rlay;
	SharedPreferences sp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	//Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.extimetabledetails);
		/*overridePendingTransition(R.anim.anim_slide_in_left,
				R.anim.anim_slide_out_left);*/
        activity =this;
       // Log.v("start activity","ooo");

		sp = getSharedPreferences("themeColor", Context.MODE_PRIVATE);
        currentapiVersion = android.os.Build.VERSION.SDK_INT;
		Rlay = (RelativeLayout) findViewById(R.id.Rlay);
       // adviewlayout = (LinearLayout)findViewById(R.id.admoblayout);
	     
	     /* offline remove ads */
	     
	  cd = new ConnectionDetector(getApplicationContext());
        
        // Check if Internet present
        if (!cd.isConnectingToInternet()) {
       	 
       	 //adviewlayout.setVisibility(View.GONE);
			Rlay.setVisibility(View.GONE);
        }
        
     // Database Connection Open
	     dbHelper = new DatabaseAdapterEx(this);
	      dbHelper.open();

		toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
                overridePendingTransition(R.anim.anim_slide_in_right,
                        R.anim.anim_slide_out_right);
			}
		});

		/*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			Window window = getWindow();
			window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
			window.setStatusBarColor(getResources().getColor(R.color.colorExpressStatus));
		}*/


	      farebttn =(LinearLayout)findViewById(R.id.farebttn);
	      availabilty =(LinearLayout)findViewById(R.id.availablebttn);
	    //  farelayout =(LinearLayout)findViewById(R.id.farelayout);
	        /*Typeface custom_font = Typeface.createFromAsset(getAssets(),
	        	      "fonts/Bodoni-Italic.TTF");
	        TextView homepage_header_text = (TextView) findViewById(R.id.homepage_header_text);
	        homepage_header_text.setTypeface(custom_font);*/

		if(sp.getInt("expressColor",0)!=0) {
			toolbar.setBackgroundColor(sp.getInt("expressColor",0));
			farebttn.setBackgroundColor(sp.getInt("expressColor",0));
			availabilty.setBackgroundColor(sp.getInt("expressColor",0));

			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

				int[] demo_colors = getResources().getIntArray(R.array.demo_colors);
				int[] demo_colors_mat = getResources().getIntArray(R.array.demo_colors_mat);

				ArrayList<String> colorString = new ArrayList<String>();
				ArrayList<String> colorString_mat = new ArrayList<String>();

				for (int i = 0; i < demo_colors.length; i++) {

					colorString.add(String.valueOf(demo_colors[i]));

				}

				for (int i = 0; i < demo_colors_mat.length; i++) {

					colorString_mat.add(String.valueOf(demo_colors_mat[i]));

				}
				Log.e("position ",colorString.indexOf(String.valueOf(sp.getInt("expressColor",0))) + "");
				int color_mat_val = Integer.parseInt(colorString_mat.get(colorString.indexOf(String.valueOf(sp.getInt("expressColor", 0)))));

				Window window = getWindow();
				window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
				window.setStatusBarColor(color_mat_val);


			}
		}else{
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
				Window window = getWindow();
				window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
				window.setStatusBarColor(getResources().getColor(R.color.colorExpressStatus));
			}
		}


	        extras = getIntent().getExtras();
		      if(extras !=null) {
		    	  trainId = extras.getInt("trainId");
		    	  trainNo =extras.getString("trainNo"); 
		    	  fromstation = extras.getString("fromstation");
		    	  tostation = extras.getString("tostation");
		    	  
		    	  
				}
		      Log.e("form","=="+fromstation);
		      Log.e("to","=="+tostation);
		      if(fromstation.equalsIgnoreCase("") && tostation.equalsIgnoreCase(""))
		      {
		    	  Log.e("tofff","=="+tostation);
		    	 // farelayout.setVisibility(View.GONE);
		      }
		      availabilty.setOnClickListener(new OnClickListener() {
		        	
		            @Override
		            public void onClick(View v) {
		                // TODO Auto-generated method stub
		            	
		            	Intent intent = new Intent(activity, AvailableActivity.class);
		            	intent.putExtra("trainId", trainId);
		    			intent.putExtra("trainNo", trainNo);
		    			intent.putExtra("tostation", tostation);
		    			intent.putExtra("fromstation", fromstation);
		    			intent.putExtra("trainName", trainName);
		    			
	                    startActivity(intent);
		            }
		      });
		      farebttn.setOnClickListener(new OnClickListener() {
		        	
		            @Override
		            public void onClick(View v) {
		                // TODO Auto-generated method stub

                        Log.e("trainNo",""+trainNo);

                        Intent i = new Intent(ExTimeTableDetails.this, GetFareActivity.class);
                        i.putExtra("trainId",trainId);
                        i.putExtra("trainNo",trainNo);
                        i.putExtra("fromstation",fromstation);
                        i.putExtra("tostation",tostation);
                        i.putExtra("trainName",trainName);
                        startActivity(i);
                        finish();
		            	
		            	
		            	
		            }
		        });

		      Handler handler = new Handler();
		        Runnable r = new Runnable() {
		            public void run() {
		            	 /* TRacking Code */
		                currentapiVersion = android.os.Build.VERSION.SDK_INT;
		                if(currentapiVersion>=android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH)
		                {
		                	/* TRacking Code */
		      		      if(currentapiVersion>=android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH)
		      		        {
		      		      Tracker t = ((GoogleAnalyticsApp) getApplication()).getTracker(TrackerName.APP_TRACKER);
		      				t.setScreenName(trainNo+"- Express");
		      				t.send(new HitBuilders.AppViewBuilder().build());
		      				
		      		        }
							runOnUiThread(new Runnable() {
								@Override
								public void run() {
									AdView mAdView = (AdView) findViewById(R.id.adView);
									AdRequest adRequest = new AdRequest.Builder().build();
									mAdView.loadAd(adRequest);
								}
							});
		                }      
		            }
		        };
		        handler.postDelayed(r, 100);
		      
		   // Toast.makeText(getApplicationContext(), "trainNo==: " + trainNo, Toast.LENGTH_LONG).show();
		      stationListView = new ArrayList<StationListBean>();
	      /// Train Info retrireve
		      if(!(trainId>0))
		      {
		    	  Cursor getTrainIdcursor = dbHelper.getTrainId(trainNo); 
		    	  if(getTrainIdcursor.getCount()>0)
		 	  	  {	
		    	  while (getTrainIdcursor.moveToNext()) {
		    	 trainId =getTrainIdcursor.getInt(getTrainIdcursor.getColumnIndexOrThrow("_id"));
		    	 
		    	  }
		 	  	  }
		      }
		     
		     // trainId = 13149;
		      traincursor=  dbHelper.getLocalTrain(trainId);
		      if(traincursor.getCount()>0)
	 	  	  {	 
		    	  //Toast.makeText(getApplicationContext(), "trainNo get ==: " + trainNo, Toast.LENGTH_LONG).show();
	 	     	  while (traincursor.moveToNext()) {
	 	 				
	 	     		
	 	 				
	 	     		TrainNo =  traincursor.getString(traincursor.getColumnIndexOrThrow("trainNO"));
	 	  				 trainName = traincursor.getString(traincursor.getColumnIndexOrThrow("trainName"));
	 	  				trainId =traincursor.getInt(traincursor.getColumnIndexOrThrow("_id"));
	 	  				sun =traincursor.getInt(traincursor.getColumnIndexOrThrow("sun"));
	 	  				mon =traincursor.getInt(traincursor.getColumnIndexOrThrow("mon"));
	 	  				tue =traincursor.getInt(traincursor.getColumnIndexOrThrow("tue"));
	 	  				wed =traincursor.getInt(traincursor.getColumnIndexOrThrow("wed"));
	 	  				thu =traincursor.getInt(traincursor.getColumnIndexOrThrow("thu"));
	 	  				fri =traincursor.getInt(traincursor.getColumnIndexOrThrow("fri"));
	 	  				sat =traincursor.getInt(traincursor.getColumnIndexOrThrow("sat"));
	 	  				 
	 	  				
	 	  				
	 	  				
	 	  				
	 	 			}
	 	  	 } 
		      else
		        {
		        	NotFound();	
		        	return;
		        }
		    	   
			          // return null;
		       
		        
		       
		    	   traincursor.close();
	    
		        stationList = dbHelper.getStationList(trainId);
	    
	     
		        if(stationList.getCount()>0)
		 	  	  {	 
		 	     	 
		 	     	  while (stationList.moveToNext()) {
		 	 				
		 	     		
		 	     		
		 	 				
						  String arrival   = stationList.getString(stationList.getColumnIndexOrThrow("arrival"));
						  String stationName   = stationList.getString(stationList.getColumnIndexOrThrow("stationName"));
						  String stationCode  = stationList.getString(stationList.getColumnIndexOrThrow("stationCode"));
						  String journey_date  = stationList.getString(stationList.getColumnIndexOrThrow("sortarrival"));

						  String arrival_date = "";
						  DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
						  Date date = null;
						  try {
							  date = format.parse(journey_date);
							  DateFormat dateFormat_tom = new SimpleDateFormat("dd MMM yy");
							  arrival_date = dateFormat_tom.format(date);

						  } catch (ParseException e) {
							  e.printStackTrace();
						  }
						  System.out.println(date);

						  Log.e("arrival_date--",""+arrival_date);
		 	  				int iscolorBG=0;
		 	  				
		 	  				if(fromstation.equalsIgnoreCase(stationCode) || tostation.equalsIgnoreCase(stationCode))
		 	  				{
		 	  					iscolorBG=1;	
		 	  				}
		 	  				
		 	  				
		 	  				stationListView.add(new StationListBean(arrival,stationName,stationCode,fromstation,tostation,iscolorBG,arrival_date));
		 	 			}
		 	     	  
		 	     	String runningdays ="Running Days: ";
				      if(sun==1)
				      {
				    	  runningdays=runningdays+"| Sun";
				      }
				      if(mon==1)
				      {
				    	  runningdays=runningdays+" | Mon";
				      }
				      if(tue==1)
				      {
				    	  runningdays=runningdays+" | Tue";
				      }
				      if(wed==1)
				      {
				    	  runningdays=runningdays+" | Wed";
				      }
				      if(thu==1)
				      {
				    	  runningdays=runningdays+" | Thu";
				      }
				      if(fri==1)
				      {
				    	  runningdays=runningdays+" | Fri";
				      }
				      if(sat==1)
				      {
				    	  runningdays=runningdays+" | Sat ";
				      }
				 	 /// Listview for train
				        lv = (ListView)findViewById(R.id.list);
				       
				        train_name =   (TextView) findViewById(R.id.train_name);
				        days = (TextView) findViewById(R.id.runningday);
				        days.startAnimation((Animation)AnimationUtils.loadAnimation(getApplicationContext(),R.anim.textviewanimation));
				        train_name.setText(trainName+"(#"+trainNo+")");
				    	   days.setText(runningdays);
		 	  	 } 
		       
		        stationList.close();
      	
		        TimeTableDetailsAdapter adapter = new TimeTableDetailsAdapter(this, stationListView, "express");
		       	
		       	// lv.setOnItemClickListener(this);
		       	lv.setAdapter(adapter);
                Utility.setListViewHeightBasedOnChildren(lv);
		       	// lv.setOnClickListener(new ClickListenerList());
		       	lv.setOnItemClickListener(new OnItemClickListener() {
		       	      public void onItemClick(AdapterView<?> parent, View view,
		       	          int position, long id) {
		       	        // When clicked, show a toast with the TextView text
		       	    	
		       	      }
		       	    });
		       	
		       	/*ImageView  	menuback =(ImageView)findViewById(R.id.close);
		         menuback.setOnClickListener(new OnClickListener() {
		         	
		             @Override
		             public void onClick(View v) {
		                 // TODO Auto-generated method stub
		            	 //overridePendingTransition(R.anim.lefttoright, R.anim.righttoleft);
		             	finish();
		             	// overridePendingTransition(R.anim.lefttoright, R.anim.righttoleft);
		             }
		         });*/
		         
		         ImageView error =(ImageView)findViewById(R.id.error);
		         error.setOnClickListener(new OnClickListener() {
		         	
		             @Override
		             public void onClick(View v) {
		                 // TODO Auto-generated method stub

							String adminemail= getResources().getString(R.string.adminemail) ;
							String subject = "Express Time Schedule problems Train No : "+TrainNo;
							String Message = "HI The train No : "+TrainNo +" Problem in Schedule Please check Time";
							String[] TO = {adminemail};
						     // String[] CC = {"mcmohd@gmail.com"};
						      Intent emailIntent = new Intent(Intent.ACTION_SEND);
						      emailIntent.setData(Uri.parse("mailto:"));
						      emailIntent.setType("text/plain");


						      emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
						      //emailIntent.putExtra(Intent.EXTRA_CC, CC);
						      emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
						      emailIntent.putExtra(Intent.EXTRA_TEXT, Message);
						      
						      try {
							         startActivity(Intent.createChooser(emailIntent, "Sending email..."));
							         
							         
							        
							       //  successAlert();
							         //Log.i("Finished sending email...", "");
							      } catch (android.content.ActivityNotFoundException ex) {
							         Toast.makeText(ExTimeTableDetails.this, 
							         "There is no email client installed.", 

Toast.LENGTH_SHORT).show();
					    		CharSequence text = "success";
								int duration = Toast.LENGTH_SHORT;

								Toast toast = Toast.makeText(getApplicationContext(), text, 

duration);
								toast.setGravity(Gravity.CENTER, 0, 0);
								toast.show();	
					    	}
						      
						     	
				    	
		             }
		         });
		       
        
        
    }
    public void NotFound()
    {
   	
   	  AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(
   		        ExTimeTableDetails.this);

   		// Setting Dialog Title
   		alertDialog2.setTitle("Error");

   		// Setting Dialog Message
   		alertDialog2.setMessage("sorry!! Train Not found on our system");

   		// Setting Icon to Dialog
   		

   		// Setting Positive "Yes" Btn
   		alertDialog2.setNeutralButton("OK",
   		        new DialogInterface.OnClickListener() {
   		            public void onClick(DialogInterface dialog, int which) {
   		                // Write your code here to execute after dialog
   		            
   		            	// overridePendingTransition(R.anim.lefttoright, R.anim.righttoleft);
   	                    finish();
   	                 

   		                
   		            }
   		        });
   		alertDialog2.show();
   		
    }   
    
    /****** Function to set data in ArrayList *************/
    public void setListData()
    {
    	TypeclassSpinner = new ArrayList<TypeClass>();
    	/*1A or 2A or 3A or 3E or CC or FC or SL or 2S*/
    	String[][] typeclass = {
    			{ "Select", "" },
    			 { "Sleeper Class(SL)", "SL" },
    			 { "Second Sitting(2S)", "2S" },
    			 { "AC chair Car(CC)", "CC" },
    	        { "AC 2 Tier sleeper(2A)", "2A" },
    	        { "AC 3 Tier(3A)", "3A" },
    	        { "First class Air-Conditioned(1A)", "1A" },
    	        { "AC 3 Tier Economy(3E)", "3E" },
    	        { "First class(FC)", "FC" },
    	        };
        // Now i have taken static values by loop.
        // For further inhancement we can take data by webservice / json / xml;

        for (int ic = 0; ic < typeclass.length; ic++) {


        	final TypeClass sched = new TypeClass();

            /******* Firstly take data in model object ******/
        	sched.setName(typeclass[ic][0]);
             sched.setValue(typeclass[ic][1]);

             TypeclassSpinner.add(sched);




            /******** Take Model Object in ArrayList **********/

        }

    }

    public void setPeopleAge()
    {
    	TypeageSpinner = new ArrayList<PeopleAge>();
    	/*CH|AD|SM|SF for Child, Adult, Senior Male, Senior Female*/
    	String[][] typeage = {
    			{ "Select", "" },
    			 { "Adult", "AD" },
    			 { "Child", "CH" },

    	        { "Senior Male", "SM" },
    	        { "Senior Female", "SF" },

    	        };
        // Now i have taken static values by loop.
        // For further inhancement we can take data by webservice / json / xml;

        for (int ic = 0; ic < typeage.length; ic++) {


        	final PeopleAge sched = new PeopleAge();

            /******* Firstly take data in model object ******/
        	sched.setName(typeage[ic][0]);
             sched.setValue(typeage[ic][1]);

             TypeageSpinner.add(sched);




            /******** Take Model Object in ArrayList **********/

        }

    }

    public void setTicketType()
    {
    	/*GN|CK|PT for General, Tatkal, Premium Tatkal respectively.*/

    	TypetcktSpinner = new ArrayList<TypeTicket>();

    	String[][] tickettype = {
    			{ "Select", "" },
    			 { "General", "GN" },
    			 { "Tatkal", "CK" },

    	        { "Premium Tatkal", "PT" },
    	        { "PHYSICALLY HANDICAP", "HP" },
    	        { "Ladies Quota", "LD" },
    	        { "Senior Citizen", "SS" },


    	        };
        // Now i have taken static values by loop.
        // For further inhancement we can take data by webservice / json / xml;

        for (int ic = 0; ic < tickettype.length; ic++) {


        	final TypeTicket sched = new TypeTicket();

            /******* Firstly take data in model object ******/
        	sched.setName(tickettype[ic][0]);
             sched.setValue(tickettype[ic][1]);

             TypetcktSpinner.add(sched);




            /******** Take Model Object in ArrayList **********/

        }

    }
    
    
    private class GetFare extends AsyncTask<String, String, JSONObject> {
        private ProgressDialog pDialog;
       @Override
       protected void onPreExecute() {
           super.onPreExecute();
            
           pDialog = new ProgressDialog(ExTimeTableDetails.this);
           pDialog.setMessage("Getting Fare ...");
           pDialog.setIndeterminate(false);
           pDialog.setCancelable(false);
           pDialog.show();
       }
       @Override
       protected JSONObject doInBackground(String... args) {
       	HttpConnection searchFunctions = new HttpConnection();
	        JSONObject json = searchFunctions.getTrainFare(mAge, mClass, fromstation, mTicket, TrainNo, tostation);

	            return json;
       }
        @Override
        protected void onPostExecute(JSONObject json) {
            pDialog.dismiss();
            
            final Dialog dialogfare = new Dialog(context,R.style.cust_dialog);
			 dialogfare.setContentView(R.layout.fare_price_box);
            LinearLayout ll =(LinearLayout) dialogfare.findViewById(R.id.fare_get_layout);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
            	    LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
         if(json !=null)
         {
        	 
        	
        	 if(json.has("response_code") && json.has("fare"))
             {
        		try {
					String  farestatus = json.getString("response_code");
					
					if(farestatus.equals("200"))
	     			{
						
						 JSONArray fare_price = json.getJSONArray("fare");
						
						 for(int ik = 0; ik < fare_price.length(); ik++){
			                    JSONObject ca = fare_price.getJSONObject(ik);
			                    Log.e("json==", "llll");
			                    String cname=ca.getString("name");
			                    String code = ca.getString("code");
			                    String fare = ca.getString("fare");
			                    Log.e("json==name", cname);
			                    TextView tv = new TextView(activity);
			                   tv.setText(cname +"( "+code+ " ) : ₹"+fare);
			                   tv.setLayoutParams(params);
			                    ll.addView(tv);
			                    
						 }
						 
						
						 ImageView fare_getbttn_dismiss = (ImageView)dialogfare.findViewById(R.id.fare_get_close);
						 fare_getbttn_dismiss.setOnClickListener(new OnClickListener() {
								@Override
								public void onClick(View view) {
									dialogfare.dismiss();
								}
						 });
						 
						 dialogfare.show();
	     			}
					else
		        		 errorBox();
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
				}
 				
 				
             }
        	 else
        		 errorBox(); 
        	 //Log.e("json", json.toString()); 
         }
         else
       	  errorBox();  
         
         
		  Log.v("Data status3", "p=sshhh");     
		  // Getting JSON Array from URL 
		  
		  
		 
        }
   }
    
    public void  errorBox()
	  {
		  Log.v("status", "no");
  	// Internet Connection is not present
	  AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ExTimeTableDetails.this);

			// set title
			alertDialogBuilder.setTitle("Error");

			// set dialog message
			alertDialogBuilder
				.setMessage("Sorry!!somethings is Wrong Try again")
				.setCancelable(false)
				.setPositiveButton("OK",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						// if this button is clicked, close
						// current activity
						//MainActivity.this.finish();
						
					}
				  });
				

				// create alert dialog
				AlertDialog alertDialog = alertDialogBuilder.create();

				// show it
				alertDialog.show();	
				
				
	  }
    
    @Override
    public void onBackPressed() {
        super.onBackPressed();
      //  overridePendingTransition(R.anim.lefttoright, R.anim.righttoleft);
        overridePendingTransition(R.anim.anim_slide_in_right,
                R.anim.anim_slide_out_right);
    }
    
    
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
		case DIALOG_DOWNLOAD_PROGRESS:
			mProgressDialog = new ProgressDialog(this);
			mProgressDialog.setMessage("Loading Train List...");
			mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			mProgressDialog.setCancelable(false);
			mProgressDialog.show();
			return mProgressDialog;
		default:
			return null;
        }
        
       
   }
    
    
    @Override
    public void onDestroy()
    {
        super.onDestroy();
        dbHelper.close();
    }  
  
   
}