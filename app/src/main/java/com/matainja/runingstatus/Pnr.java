package com.matainja.runingstatus;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TextInputLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.matainja.runingstatus.Database.DatabaseAdapter;
import com.matainja.runingstatus.Database.DatabaseAdapterSaveSearch;
import com.matainja.runingstatus.GoogleAnalyticsApp.TrackerName;
import com.matainja.runingstatus.Model.DateList;
import com.matainja.runingstatus.Model.TimeList;
import com.matainja.runingstatus.Model.Train;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;


public class Pnr extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

	 ArrayList<Train> listOfTrain;
	 ArrayList<TimeList> TimelistArr;
	 Cursor traincursor;
	 TextView trainnotxt;
	 ArrayList<String> PNRs = new ArrayList<String>();
	 
	 private DatabaseAdapterSaveSearch dbHelpersave;

	SharedPreferences sp;
	 Button action,btn_api_link;
	 AutoCompleteTextView pnrinput;
	 TextInputLayout input_layout_name;
	 ListView lv;
	  private EditText trainlist; 
	 private DatabaseAdapter dbHelper;
	 private EditText searchtrainbox;
	 LinearLayout mainView_trainlist;
	 LinearLayout menu_layout_timelist;
	 View convertView;
	 ArrayList<DateList>  listOfDateList;
	  LinearLayout journey_date_select;  
	  ImageView leftmenu;
	  ListView leftmenulist;
	   TextView pnr_label;
	   Context context;
	   int currentapiVersion=0;
	   TextView savepnr;
	   LinearLayout ll=null;
    DrawerLayout drawer;
    Toolbar toolbar;
	CircleImageView profile_image;
	TextView nameTxt;
	RelativeLayout headerFrame;
	String email;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	//Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pnr_home);
		overridePendingTransition(R.anim.anim_slide_in_left,
				R.anim.anim_slide_out_left);
        currentapiVersion = android.os.Build.VERSION.SDK_INT;
        context =getApplicationContext();
        
      
        
       

        //Database Connection Open
         dbHelpersave = new DatabaseAdapterSaveSearch(this);
         dbHelpersave.open();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        /*toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(Pnr.this, LocalTrain.class);
				i.putExtra("callfrom","other");
                startActivity(i);
                finish();
            }
        });*/

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.setDrawerIndicatorEnabled(true);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


		View headerview = navigationView.getHeaderView(0);
		profile_image = (CircleImageView) headerview.findViewById(R.id.profile_image);
		nameTxt = (TextView) headerview.findViewById(R.id.name);
		headerFrame = (RelativeLayout) headerview.findViewById(R.id.headerFrame);
		headerFrame.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(email.equalsIgnoreCase("")){
					Intent i = new Intent(Pnr.this, WelcomeActivity.class);
					startActivity(i);
					finish();
				}else {
					Intent i = new Intent(Pnr.this, SettingsActivity.class);
					startActivity(i);
				}
			}
		});

		ApplyUserInfo();

		sp = getSharedPreferences("themeColor", Context.MODE_PRIVATE);


         Handler handler = new Handler();
         Runnable r = new Runnable() {
             public void run() {
                /* TRacking Code */
                 if(currentapiVersion>=android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH)
                   {
                     Tracker t = ((GoogleAnalyticsApp) getApplication()).getTracker(TrackerName.APP_TRACKER);
                        t.setScreenName("PNR Status");
                        t.send(new HitBuilders.AppViewBuilder().build());

                        //google adds
                        /*AdView mAdView = (AdView) findViewById(R.id.adView);
                           AdRequest adRequest = new AdRequest.Builder().build();
                           mAdView.loadAd(adRequest);*/
                   }
             }
         };
         handler.postDelayed(r, 200);

		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				AdView mAdView = (AdView) findViewById(R.id.adView);
				AdRequest adRequest = new AdRequest.Builder().build();
				mAdView.loadAd(adRequest);
			}
		});
        new AdMob().execute();
 
        currentapiVersion = android.os.Build.VERSION.SDK_INT;
 
 
        Log.e("Data value","");
 
        //ll = (LinearLayout)findViewById(R.id.pnrsentry);
 
        savepnr =(TextView)findViewById(R.id.savepnr);

 
 


	     
	      action =(Button)findViewById(R.id.action);

		/*if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
			action.setBackgroundColor(getResources().getColor(R.color.colorExpressStatus));

		}

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			Window window = getWindow();
			window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
			window.setStatusBarColor(getResources().getColor(R.color.colorExpressStatus));
		}*/

		pnrinput = (AutoCompleteTextView) findViewById(R.id.pnrinput);
		input_layout_name =(TextInputLayout) findViewById(R.id.input_layout_name);
      
       
       ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
               android.R.layout.simple_dropdown_item_1line, PNRs);
       pnrinput.setAdapter(adapter);

       // pnr_label.setTypeface(custom_font);
       
       action.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
			
				GetStatus();
			}

		});
     	
        btn_api_link=(Button)findViewById(R.id.btn_api_link);
		btn_api_link.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i= new Intent(Pnr.this,WebViewActivity.class);
				startActivity(i);
				overridePendingTransition(R.anim.anim_slide_in_left,
						R.anim.anim_slide_out_left);
			}
		});

		if(sp.getInt("expressColor",0)!=0) {
			toolbar.setBackgroundColor(sp.getInt("expressColor",0));
			//pnrinput.setHintTextColor(sp.getInt("expressColor",0));

			/*ColorStateList colorStateList = ColorStateList.valueOf(sp.getInt("expressColor",0));
			ViewCompat.setBackgroundTintList(pnrinput, colorStateList);
			input_layout_name.setHintTextAppearance(sp.getInt("expressColor",0));*/

			action.setBackgroundColor(sp.getInt("expressColor",0));
			btn_api_link.setBackgroundColor(sp.getInt("expressColor",0));

			int[] demo_colors = getResources().getIntArray(R.array.demo_colors);
			int[] demo_colors_mat = getResources().getIntArray(R.array.demo_colors_mat);

			ArrayList<String> colorString = new ArrayList<String>();
			ArrayList<String> colorString_mat = new ArrayList<String>();

			for (int i = 0; i < demo_colors.length; i++) {

				colorString.add(String.valueOf(demo_colors[i]));

			}

			for (int i = 0; i < demo_colors_mat.length; i++) {

				colorString_mat.add(String.valueOf(demo_colors_mat[i]));

			}
			Log.e("position ",colorString.indexOf(String.valueOf(sp.getInt("expressColor",0))) + "");
			int color_mat_val = Integer.parseInt(colorString_mat.get(colorString.indexOf(String.valueOf(sp.getInt("expressColor", 0)))));

			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
				Window window = getWindow();
				window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
				window.setStatusBarColor(color_mat_val);
			}
		}else{
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
				Window window = getWindow();
				window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
				window.setStatusBarColor(getResources().getColor(R.color.colorExpressStatus));
			}
		}

    }

	public void ApplyUserInfo(){
		SharedPreferences userPref = getSharedPreferences("userCred",MODE_PRIVATE);
		String name = userPref.getString("name","Guest User");
		email = userPref.getString("email","");
		String imageUrl = userPref.getString("image_value","");

		Log.e("imageUrl ",imageUrl);
		nameTxt.setText(name);
		if(!imageUrl.equalsIgnoreCase("")) {
			Picasso.with(context).load(imageUrl).memoryPolicy(MemoryPolicy.NO_CACHE).into(profile_image);
		}
	}

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
            Intent i = new Intent(Pnr.this, LocalTrain.class);
            i.putExtra("callfrom","other");
            startActivity(i);
            finish();
            overridePendingTransition(R.anim.anim_slide_in_right,
                    R.anim.anim_slide_out_right);
        }
    }
	
    private void GetStatus()
	{
    	String daysofjourneyval="";
    	if(pnrinput.getText().length()== 0)
		{
    		CharSequence text = "Please Enter PNR  No!";
			int duration = Toast.LENGTH_SHORT;

			Toast toast = Toast.makeText(getApplicationContext(), text, duration);
			toast.setGravity(Gravity.CENTER, 0, 0);
			toast.show();
		}
    	
    	else
    	{
    		 
    		// When clicked, show a toast with the TextView text
   	    	InputMethodManager imm = (InputMethodManager)getSystemService(
   	    	      Context.INPUT_METHOD_SERVICE);
   	    	imm.hideSoftInputFromWindow(pnrinput.getWindowToken(), 0);
    		String pnrno = pnrinput.getText().toString();
    		
    		
    		 
 			Intent intent = new Intent(getApplicationContext(), Pnrstatus.class);
	    	intent.putExtra("pnrno", pnrno);
			startActivity(intent);
    	}
	}

	@Override
	public boolean onNavigationItemSelected(MenuItem item) {

		int id = item.getItemId();

		if (id == R.id.nav_home) {
			// Handle the camera action
            Intent intent = new Intent(getApplicationContext(), LocalTrain.class);
            intent.putExtra("callfrom","other");
            startActivity(intent);
            finish();

		} else if (id == R.id.nav_runningstatus) {

			Intent intent = new Intent(getApplicationContext(), Homepage.class);

			startActivity(intent);
			finish();

		} else if (id == R.id.nav_cancelled) {

			Intent intent = new Intent(getApplicationContext(), ActivityCancelTrain.class);
			startActivity(intent);
			finish();

		}  else if (id == R.id.nav_pnrstatus) {



		} else if (id == R.id.nav_saveroute) {

			Intent intent = new Intent(getApplicationContext(), SaveSearch.class);
			startActivity(intent);
			finish();

		} else if (id == R.id.nav_feedback) {
			Intent intent = new Intent(getApplicationContext(), feedback.class);

			startActivity(intent);
			finish();

		} else if (id == R.id.nav_fblike) {

			Intent notificationIntent =null;
			notificationIntent = new Intent(Intent.ACTION_VIEW);
			notificationIntent.setData(Uri.parse("https://www.facebook.com/localtimetable"));
			startActivity(notificationIntent);


		} else if (id == R.id.nav_theme) {

			if(email.equalsIgnoreCase("")){
				Intent i = new Intent(Pnr.this, WelcomeActivity.class);
				startActivity(i);
				finish();
			}else {
				Intent i = new Intent(Pnr.this, SettingsActivity.class);
				startActivity(i);
			}


		} else if (id == R.id.nav_rate) {

			Intent notificationIntent =null;
			notificationIntent = new Intent(Intent.ACTION_VIEW);
			notificationIntent.setData(Uri.parse("market://details?id="+getApplicationContext().getPackageName()));
			startActivity(notificationIntent);


		} else if (id == R.id.nav_aboutus) {

			Intent intent = new Intent(getApplicationContext(), BuildWebsite.class);
			startActivity(intent);
            finish();
		}


		DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
		drawer.closeDrawer(GravityCompat.START);
		return true;
	}

	// add items into spinner dynamically
   
  
    private class AdMob extends AsyncTask<String, String, Cursor> {

    	/**
    	 * Defining Process dialog
    	 **/
            
            
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                   
                   
                  
            }

            @Override
            protected Cursor doInBackground(String... args) {
            	Cursor SavepnrCursor = dbHelpersave.getSavepnr();
            
                return SavepnrCursor;
            }
            
          
    	   protected void onPostExecute(Cursor SavepnrCursor) {
    		   
    		   
    		   Log.e("PNR search",SavepnrCursor.getCount()+"==");
    		   
    		   if(SavepnrCursor.getCount()>0)
    			  {	 
    			 savepnr.setVisibility(View.VISIBLE);
    		 	  while (SavepnrCursor.moveToNext()) {
    		 		 final String Name = SavepnrCursor.getString(SavepnrCursor.getColumnIndexOrThrow("pnr"));  
    		 		final String from_to  = SavepnrCursor.getString(SavepnrCursor.getColumnIndexOrThrow("fromto")); 
    		 		PNRs.add(Name);
					  Log.e("PNR search",Name +" -- ");
    		 		TextView tvv=new TextView(context);
    		 		tvv.setTextSize(15);
                      savepnr.setText(from_to+" "+Name);
    		 		 tvv.setText(from_to+" - "+Name);
    		 		tvv.setPadding(5,5,5,5);
    		 		tvv.setBackgroundColor(Color.GRAY);
                      savepnr.setOnClickListener(new View.OnClickListener() {
    		 		 	
    		 		     @Override
    		 		     public void onClick(View v) {
    		 		    	
    		 		    	Intent intent = new Intent(context, Pnrstatus.class);
    		 		    	//StringBuffer urlString = new StringBuffer();
    		 		    	//Clinets parentActivity = (Clinets)getParent();
    		 		    	//replaceContentView("addclients", intent);
    		 		    	// 1 for Add Clients
    		 				intent.putExtra("pnrno", Name);
    		 				
    		 				
    		 				
    		 		    	startActivity(intent);
    		 		     } });
    		 		     
    		 		 ll.addView(tvv);
    		 		TextView tvvspace=new TextView(context);
    		 		tvvspace.setText("  ");
    		 		 ll.addView(tvvspace);
    		 		 Log.v("================",Name);
    		 	  }
    			  }
    		   
           }
           }



}