package com.matainja.runingstatus;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gcm.GCMRegistrar;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.matainja.runingstatus.Adapter.StationAdapter;
import com.matainja.runingstatus.Common.ConnectionDetector;
import com.matainja.runingstatus.Common.Constants;
import com.matainja.runingstatus.Common.ServerUtilities;
import com.matainja.runingstatus.Common.WakeLocker;
import com.matainja.runingstatus.Connectivity.HttpConnection;
import com.matainja.runingstatus.Database.DatabaseAdapter;
import com.matainja.runingstatus.Database.DatabaseAdapterEx;
import com.matainja.runingstatus.Fragment.ExpressTrainFragment;
import com.matainja.runingstatus.Fragment.LocalTrainFragment;
import com.matainja.runingstatus.Fragment.ViewPagerAdapter;
import com.matainja.runingstatus.Model.StationLocal;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.provider.AlarmClock.EXTRA_MESSAGE;
import static android.text.format.DateUtils.DAY_IN_MILLIS;
import static android.text.format.DateUtils.HOUR_IN_MILLIS;
import static android.text.format.DateUtils.MINUTE_IN_MILLIS;
import static com.matainja.runingstatus.Common.CommonUtilities.DISPLAY_MESSAGE_ACTION;
import static com.matainja.runingstatus.Common.CommonUtilities.SENDER_ID;

public class LocalTrain extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    private TabLayout tabLayout;
    private ViewPager viewPager;
    private AppBarLayout appbar;
    Context context;
    int currentapiVersion = 0;
    int VersionCode;
    DrawerLayout drawer;
    EditText searchtrainbox;
    ListView stationlist;
    private DatabaseAdapter dbHelper;
    Cursor stationCursor;
    private DatabaseAdapterEx dbHelperEx;
    Cursor stationCursorEx;
    ArrayList<StationLocal> listOfStation;
    ImageView back;
    ConnectionDetector cd;
    String android_id ;
    AsyncTask<Void, Void, Void> mRegisterTask;
    RelativeLayout headerFrame;
    SharedPreferences sp;
    CircleImageView profile_image;
    TextView nameTxt;
    String email;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        setContentView(R.layout.localtrain);
        /*this.overridePendingTransition(R.anim.righttoleft,
                R.anim.lefttoright);*/
        /*this.overridePendingTransition(R.anim.lefttoright,
                R.anim.righttoleft);*/
        overridePendingTransition(R.anim.anim_slide_in_left,
                R.anim.anim_slide_out_left);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        context = getApplicationContext();
        dbHelper = new DatabaseAdapter(LocalTrain.this);
        dbHelper.open();

        dbHelperEx = new DatabaseAdapterEx(LocalTrain.this);
        dbHelperEx.open();

        sp = getSharedPreferences("themeColor", Context.MODE_PRIVATE);


        currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            Tracker t = ((GoogleAnalyticsApp) getApplication()).getTracker(GoogleAnalyticsApp.TrackerName.APP_TRACKER);
            t.setScreenName("Localtrain");
            t.send(new HitBuilders.AppViewBuilder().build());

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    AdView mAdView = (AdView) findViewById(R.id.adView);
                    AdRequest adRequest = new AdRequest.Builder().build();
                    mAdView.loadAd(adRequest);
                }
            });
        }

        try {
            VersionCode = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
        }
        // Database Connection Open
        Log.e("version code1 ", VersionCode + "");

        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorLocalStatus));
        }*/
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        appbar = (AppBarLayout) findViewById(R.id.appbar);
        back = (ImageView) findViewById(R.id.back);


        //create tabs title
        tabLayout.addTab(tabLayout.newTab().setText("Local Train"));
        tabLayout.addTab(tabLayout.newTab().setText("Express Train"));
        //tabLayout.removeTab(tabLayout.getTabAt(1));
        //attach tab layout with ViewPager
        //set gravity for tab bar
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setTabMode(TabLayout.MODE_FIXED);

        tabLayout.setSelectedTabIndicatorColor(Color.WHITE);
        tabLayout.setSelectedTabIndicatorHeight(5);

        if(sp.getInt("localColor",0)!=0) {
            toolbar.setBackgroundColor(sp.getInt("localColor",0));
            tabLayout.setBackgroundColor(sp.getInt("localColor",0));

            int[] demo_colors = getResources().getIntArray(R.array.demo_colors);
            int[] demo_colors_mat = getResources().getIntArray(R.array.demo_colors_mat);

            ArrayList<String> colorString = new ArrayList<String>();
            ArrayList<String> colorString_mat = new ArrayList<String>();

            for (int i = 0; i < demo_colors.length; i++) {
                Log.e("Test", demo_colors[i] + "");
                //Log.e("Test", demo_colors[i] + "");
                colorString.add(String.valueOf(demo_colors[i]));

            }

            for (int i = 0; i < demo_colors_mat.length; i++) {
                Log.e("Test", demo_colors[i] + "");
                //Log.e("Test", demo_colors[i] + "");
                colorString_mat.add(String.valueOf(demo_colors_mat[i]));

            }
            Log.e("position ",colorString.indexOf(String.valueOf(sp.getInt("localColor",0))) + "");
            int color_mat_val = Integer.parseInt(colorString_mat.get(colorString.indexOf(String.valueOf(sp.getInt("localColor", 0)))));

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(color_mat_val);
            }
        }else{
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(getResources().getColor(R.color.colorLocalStatus));
            }
        }

        final ViewPagerAdapter adapter = new ViewPagerAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                //Toast.makeText(BookmarkSessionActivity.this, ""+tab.getPosition(), Toast.LENGTH_LONG).show();
                if (tab.getPosition() == 1) {
                    //mTitle.setText("Notes - "+getIntent().getStringExtra("SUB_MENU_NAME"));

                    if(sp.getInt("expressColor",0)!=0) {
                        //Toast.makeText(LocalTrain.this, "IF  "+tab.getPosition(), Toast.LENGTH_LONG).show();
                        toolbar.setBackgroundColor(sp.getInt("expressColor",0));
                        tabLayout.setBackgroundColor(sp.getInt("expressColor",0));

                        ExpressTrainFragment.parentR.setBackgroundColor(sp.getInt("expressColor",0));
                        ExpressTrainFragment.bttntimetable.setTextColor(sp.getInt("expressColor",0));
                        ExpressTrainFragment.recentsearchtext.setTextColor(sp.getInt("expressColor",0));


                            int[] demo_colors = getResources().getIntArray(R.array.demo_colors);
                            int[] demo_colors_mat = getResources().getIntArray(R.array.demo_colors_mat);

                            ArrayList<String> colorString = new ArrayList<String>();
                            ArrayList<String> colorString_mat = new ArrayList<String>();

                            for (int i = 0; i < demo_colors.length; i++) {
                                Log.e("Test", demo_colors[i] + "");
                                //Log.e("Test", demo_colors[i] + "");
                                colorString.add(String.valueOf(demo_colors[i]));

                            }

                            for (int i = 0; i < demo_colors_mat.length; i++) {
                                Log.e("Test", demo_colors[i] + "");
                                //Log.e("Test", demo_colors[i] + "");
                                colorString_mat.add(String.valueOf(demo_colors_mat[i]));

                            }
                            Log.e("position ",colorString.indexOf(String.valueOf(sp.getInt("expressColor",0))) + "");
                            int color_mat_val = Integer.parseInt(colorString_mat.get(colorString.indexOf(String.valueOf(sp.getInt("expressColor", 0)))));

                            Log.e("color_mat_val ",""+color_mat_val);
                            ExpressTrainFragment.fromstationcode.setBackgroundColor(color_mat_val);
                            ExpressTrainFragment.tostationcode.setBackgroundColor(color_mat_val);
                            ExpressTrainFragment.traincode.setBackgroundColor(color_mat_val);

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            Window window = getWindow();
                            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                            window.setStatusBarColor(color_mat_val);
                        }
                    }else{
                        //Toast.makeText(LocalTrain.this, "ELSE "+tab.getPosition(), Toast.LENGTH_LONG).show();
                        toolbar.setBackgroundColor(getResources().getColor(R.color.colorExpress));
                        tabLayout.setBackgroundColor(getResources().getColor(R.color.colorExpress));
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            Window window = getWindow();
                            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                            window.setStatusBarColor(getResources().getColor(R.color.colorExpress));
                        }
                    }
                } else {
                    if(sp.getInt("localColor",0)!=0) {
                        //Toast.makeText(LocalTrain.this, ""+tab.getPosition(), Toast.LENGTH_LONG).show();

                        int[] demo_colors = getResources().getIntArray(R.array.demo_colors);
                        int[] demo_colors_mat = getResources().getIntArray(R.array.demo_colors_mat);

                        ArrayList<String> colorString = new ArrayList<String>();
                        ArrayList<String> colorString_mat = new ArrayList<String>();

                        for (int i = 0; i < demo_colors.length; i++) {
                            Log.e("Test", demo_colors[i] + "");
                            //Log.e("Test", demo_colors[i] + "");
                            colorString.add(String.valueOf(demo_colors[i]));

                        }

                        for (int i = 0; i < demo_colors_mat.length; i++) {
                            Log.e("Test", demo_colors[i] + "");
                            //Log.e("Test", demo_colors[i] + "");
                            colorString_mat.add(String.valueOf(demo_colors_mat[i]));

                        }
                        Log.e("position ",colorString.indexOf(String.valueOf(sp.getInt("localColor",0))) + "");
                        int color_mat_val = Integer.parseInt(colorString_mat.get(colorString.indexOf(String.valueOf(sp.getInt("localColor", 0)))));

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            Window window = getWindow();
                            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                            window.setStatusBarColor(color_mat_val);
                        }
                        //mTitle.setText("Bookmark - "+getIntent().getStringExtra("SUB_MENU_NAME"));
                        toolbar.setBackgroundColor(sp.getInt("localColor",0));
                        tabLayout.setBackgroundColor(sp.getInt("localColor",0));
                    }else{
                        toolbar.setBackgroundColor(getResources().getColor(R.color.colorLocalStatus));
                        tabLayout.setBackgroundColor(getResources().getColor(R.color.colorLocalStatus));
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            Window window = getWindow();
                            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                            window.setStatusBarColor(getResources().getColor(R.color.colorLocalStatus));
                        }
                    }
                }


            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        if (getIntent().getStringExtra("callfrom").equalsIgnoreCase("ExTrainTimetable")) {
            viewPager.setCurrentItem(1);
        }
        searchtrainbox = (EditText) findViewById(R.id.searchtrainbox);

        stationlist = (ListView) findViewById(R.id.stationlist);


        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View headerview = navigationView.getHeaderView(0);

        profile_image = (CircleImageView) headerview.findViewById(R.id.profile_image);
        nameTxt = (TextView) headerview.findViewById(R.id.name);
        headerFrame = (RelativeLayout) headerview.findViewById(R.id.headerFrame);
        headerFrame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(email.equalsIgnoreCase("")){
                    Intent i = new Intent(LocalTrain.this, WelcomeActivity.class);
                    startActivity(i);
                    finish();
                }else {
                    Intent i = new Intent(LocalTrain.this, SettingsActivity.class);
                    startActivity(i);
                }
            }
        });

        NavigationView search_view = (NavigationView) findViewById(R.id.search_view);
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, findViewById(R.id.search_view));
        //navigationView.setItemIconTintList(null);


        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        DrawerLayout.LayoutParams params = (DrawerLayout.LayoutParams) search_view.getLayoutParams();
        params.width = metrics.widthPixels;
        search_view.setLayoutParams(params);


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.closeDrawer(GravityCompat.END);
                hideSoftKeyboard(LocalTrain.this);
            }
        });

        /*CharSequence result = DateUtils.getRelativeTimeSpanString(1306767830, 1306767935, MINUTE_IN_MILLIS);
        Log.e("RESULT TIME-==", "" + result);//getTimeAgo(1306767830,LocalTrain.this)
*/

        /*long sarrival = 20170320103556;
        long diffInMillisec = 20170320103556 - 20170320113556;
        long diffInSec = TimeUnit.MILLISECONDS.toSeconds(diffInMillisec);
        long seconds = diffInSec % 60;
        diffInSec/= 60;
        long minutes =diffInSec % 60;
        diffInSec /= 60;
        long hours = diffInSec % 24;
        diffInSec /= 24;
        //days = diffInSec;
        Log.e("diffInSec",""+diffInSec);*/



        //GoGCM();
        ApplyUserInfo();
    }

    public void GoGCM(){

        registerReceiver(mHandleMessageReceiver, new IntentFilter(
                DISPLAY_MESSAGE_ACTION));

        cd = new ConnectionDetector(getApplicationContext());

        android_id = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        // Log.v("Id",android_id);




        if (cd.isConnectingToInternet()) {


            new Checkupdate().execute();
            // Make sure the device has the proper dependencies.
            //GCMRegistrar.checkDevice(this);

            // Make sure the manifest was properly set - comment out this line
            // while developing the app, then uncomment it when it's ready.
            //GCMRegistrar.checkManifest(this);


            // GCMRegistrar.setRegisteredOnServer(this, false);

            // Get GCM registration id
            final String regId = GCMRegistrar.getRegistrationId(this);

            // Check if regid already presents
            if (regId.equals("")) {

                // Registration is not present, register now with GCM
               // Toast.makeText(getApplicationContext(), "1st IF "+SENDER_ID, Toast.LENGTH_LONG).show();
                GCMRegistrar.register(this, SENDER_ID);

            } else {
                //ServerUtilities.unregister(getApplicationContext(), regId);
                // Device is already registered on GCM
                if (GCMRegistrar.isRegisteredOnServer(this)) {
                    // Skips registration.
                   // Toast.makeText(getApplicationContext(), "Already registered with GCM"+regId, Toast.LENGTH_LONG).show();
                } else {
                    // Try to register again, but not in the UI thread.
                    // It's also necessary to cancel the thread onDestroy(),
                    // hence the use of AsyncTask instead of a raw thread.
                    final Context context = this;
                    mRegisterTask = new AsyncTask<Void, Void, Void>() {

                        @Override
                        protected Void doInBackground(Void... params) {
                            // Register on our server
                            // On server creates a new user
                           // Toast.makeText(getApplicationContext(), " registered ID=="+regId, Toast.LENGTH_LONG).show();
                            ServerUtilities.register(context, "", "", regId);
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void result) {
                            mRegisterTask = null;
                        }

                    };
                    mRegisterTask.execute(null, null, null);
                }
            }


        }
    }

    private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String newMessage = intent.getExtras().getString(EXTRA_MESSAGE);
            // Waking up mobile if it is sleeping
            WakeLocker.acquire(getApplicationContext());

        }
    };
    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
    }

    public void getLayoutHide() {
        // Toast.makeText(LocalTrain.this,"HI!",Toast.LENGTH_SHORT).show();
        stationlist.setSelected(true);      // forcefully set focus
        drawer.openDrawer(GravityCompat.END);
        //drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_OPEN, findViewById(R.id.search_view));

        SearchStation("");

        searchtrainbox.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //here is your code

                if (searchtrainbox.getText().length() == 0) {
                    SearchStation("");
                } else {
                    String keywords = searchtrainbox.getText().toString();
                    SearchStation(keywords);
                }


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });

    }

    private void SearchStation(String searchQuery) {
        /// Listview for train
        //Toast.makeText(LocalTrain.this, "== CALLED", Toast.LENGTH_SHORT).show();
        //stationlist.setAdapter(null);
        int pageNum = viewPager.getCurrentItem();
        Log.e("pageNum===", "" + pageNum);
        if (pageNum == 1) {

            if (searchQuery.length() > 0) {
                stationCursor = dbHelperEx.getAllStation(searchQuery);
                //Toast.makeText(LocalTrain.this, "== "+stationCursor.getCount(), Toast.LENGTH_SHORT).show();
            } else {
                stationCursor = dbHelperEx.getAllStation(null);
            }

            listOfStation = new ArrayList<StationLocal>();
            if (stationCursor.getCount() > 0) {

                while (stationCursor.moveToNext()) {


                    Integer id = stationCursor.getInt(stationCursor.getColumnIndexOrThrow("_id"));
                    Integer popularity = stationCursor.getInt(stationCursor.getColumnIndexOrThrow("popularity"));

                    String stationCod = stationCursor.getString(stationCursor.getColumnIndexOrThrow("stationCode"));
                    String stationName = stationCursor.getString(stationCursor.getColumnIndexOrThrow("stationName"));

                    //Log.v("Station:name",stationName);

                    listOfStation.add(new StationLocal(id, stationCod, stationName, popularity));
                }
            }
            stationCursor.close();

            StationAdapter adapter = new StationAdapter(this, listOfStation);
            stationlist.setAdapter(adapter);

            stationlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    // When clicked, show a toast with the TextView text
                    InputMethodManager imm = (InputMethodManager) getSystemService(
                            Context.INPUT_METHOD_SERVICE);

                    final StationLocal entry = listOfStation.get(position);
                    Integer _id = entry.getId();

                    searchtrainbox.setText("");
                    hideSoftKeyboard(LocalTrain.this);
                    String StationCode = entry.getStationCode();
                    String StationName = entry.getStationName();

                    if (Constants.isSelectedFromEx == true) {
                        ExpressTrainFragment.fromstationcode.setText(StationCode + " - " + StationName);
                        Constants.isSelectedFromEx = false;
                    } else if (Constants.isSelectedtoEx) {
                        ExpressTrainFragment.tostationcode.setText(StationCode + " - " + StationName);
                        Constants.isSelectedtoEx = false;
                    }
                    drawer.closeDrawer(GravityCompat.END);


                }
            });

        } else {
            Log.e("ELSE===", "TRUE");
            if (searchQuery.length() > 0) {
                stationCursor = dbHelper.getAllStation(searchQuery);
                //Toast.makeText(LocalTrain.this, "== "+stationCursor.getCount(), Toast.LENGTH_SHORT).show();
            } else {
                stationCursor = dbHelper.getAllStation(null);
            }


            listOfStation = new ArrayList<StationLocal>();

            // Log.v("Station:name","SearchStation="+stationCursor.getCount());
            if (stationCursor.getCount() > 0) {

                while (stationCursor.moveToNext()) {


                    Integer id = stationCursor.getInt(stationCursor.getColumnIndexOrThrow("_id"));
                    Integer popularity = stationCursor.getInt(stationCursor.getColumnIndexOrThrow("popularity"));

                    String stationCod = stationCursor.getString(stationCursor.getColumnIndexOrThrow("stationCode"));
                    String stationName = stationCursor.getString(stationCursor.getColumnIndexOrThrow("stationName"));

                    //Log.v("Station:name",stationName);

                    listOfStation.add(new StationLocal(id, stationCod, stationName, popularity));
                }
            }
            stationCursor.close();

            StationAdapter adapter = new StationAdapter(this, listOfStation);
   	/* LayoutInflater infalter = getLayoutInflater();
   	 ViewGroup header = (ViewGroup) infalter.inflate(R.layout.clientlistheader, lv, false);
        lv.addHeaderView(header);*/
            // lv.setOnItemClickListener(this);
            stationlist.setAdapter(adapter);
            // lv.setOnClickListener(new ClickListenerList());
            stationlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    // When clicked, show a toast with the TextView text
                    InputMethodManager imm = (InputMethodManager) getSystemService(
                            Context.INPUT_METHOD_SERVICE);

                    final StationLocal entry = listOfStation.get(position);
                    Integer _id = entry.getId();

                    searchtrainbox.setText("");
                    hideSoftKeyboard(LocalTrain.this);
                    String StationCode = entry.getStationCode();
                    String StationName = entry.getStationName();

                    if (Constants.isSelectedFrom == true) {
                        LocalTrainFragment.fromstationcode.setText(StationCode + " - " + StationName);
                        Constants.isSelectedFrom = false;
                    } else if (Constants.isSelectedto) {
                        LocalTrainFragment.tostationcode.setText(StationCode + " - " + StationName);
                        Constants.isSelectedto = false;
                    }
                    drawer.closeDrawer(GravityCompat.END);


                }
            });
        }
    }


    public static String getTimeAgo(long time, Context ctx) {

        if (time < 1000000000000L) {
            // if timestamp given in seconds, convert to millis
            time *= 1000;
        }

        long now = System.currentTimeMillis();
        ;
   /* if (time > now || time <= 0) {
        return null;
    }*/
        Log.e("now",""+now);
        final long diff = time - now;
        if (diff < MINUTE_IN_MILLIS) {
            return "just now";
        } else if (diff < 2 * MINUTE_IN_MILLIS) {
            return "a minute ago";
        } else if (diff < 50 * MINUTE_IN_MILLIS) {
            return diff / MINUTE_IN_MILLIS + " minutes ago";
        } else if (diff < 90 * MINUTE_IN_MILLIS) {
            return "an hour ago";
        } else if (diff < 24 * HOUR_IN_MILLIS) {
            return diff / HOUR_IN_MILLIS + " hours ago";
        } else if (diff < 48 * HOUR_IN_MILLIS) {
            return "yesterday";
        } else {
            return diff / DAY_IN_MILLIS + " days ago";
        }


}


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if(drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        }else {
            super.onBackPressed();
            finish();
            overridePendingTransition(R.anim.anim_slide_in_right,
                    R.anim.anim_slide_out_right);
        }
    }


    public void ApplyUserInfo(){
        SharedPreferences userPref = getSharedPreferences("userCred",MODE_PRIVATE);
        String name = userPref.getString("name","Guest User");
        email = userPref.getString("email","");

        String imageUrl = userPref.getString("image_value","");

        Log.e("email ",email);
        //Toast.makeText(LocalTrain.this,email,Toast.LENGTH_LONG).show();
        nameTxt.setText(name);
        if(!imageUrl.equalsIgnoreCase("")) {
            Picasso.with(context).load(imageUrl).memoryPolicy(MemoryPolicy.NO_CACHE).into(profile_image);
        }
    }
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the camera action
        } else if (id == R.id.nav_runningstatus) {

            Intent intent = new Intent(getApplicationContext(), Homepage.class);
            startActivity(intent);
            finish();

        } else if (id == R.id.nav_cancelled) {

            Intent intent = new Intent(getApplicationContext(), ActivityCancelTrain.class);
            startActivity(intent);
            finish();

        }  else if (id == R.id.nav_pnrstatus) {

            Intent intent = new Intent(getApplicationContext(), Pnr.class);
            startActivity(intent);
            finish();

        } else if (id == R.id.nav_saveroute) {

            Intent intent = new Intent(getApplicationContext(), SaveSearch.class);
            startActivity(intent);
            finish();

        } else if (id == R.id.nav_feedback) {
            Intent intent = new Intent(getApplicationContext(), feedback.class);
            startActivity(intent);
            finish();

        } else if (id == R.id.nav_fblike) {

            Intent notificationIntent =null;
            notificationIntent = new Intent(Intent.ACTION_VIEW);
            notificationIntent.setData(Uri.parse("https://www.facebook.com/localtimetable"));
            startActivity(notificationIntent);


        } else if (id == R.id.nav_theme) {

            if(email.equalsIgnoreCase("")){
                Intent i = new Intent(LocalTrain.this, WelcomeActivity.class);
                startActivity(i);
                finish();
            }else {
                Intent i = new Intent(LocalTrain.this, SettingsActivity.class);
                startActivity(i);
            }


        } else if (id == R.id.nav_rate) {

            Intent notificationIntent =null;
            notificationIntent = new Intent(Intent.ACTION_VIEW);
            notificationIntent.setData(Uri.parse("market://details?id="+getApplicationContext().getPackageName()));
            startActivity(notificationIntent);


        } else if (id == R.id.nav_aboutus) {

            Intent intent = new Intent(getApplicationContext(), BuildWebsite.class);
            startActivity(intent);
            finish();
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    public void onDestroy()
    {


        /*if (mRegisterTask != null) {
            mRegisterTask.cancel(true);
        }
        try {
            unregisterReceiver(mHandleMessageReceiver);
            GCMRegistrar.onDestroy(this);
        } catch (Exception e) {
            Log.e("UnRegister Receiver Error", "> " + e.getMessage());
        }*/
        super.onDestroy();
        dbHelper.close();
    }

    public void NotificationAlert(String Message)
    {

        AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(
                LocalTrain.this);

        // Setting Dialog Title
        alertDialog2.setTitle("Message");

        // Setting Dialog Message
        alertDialog2.setMessage(Message);

        // Setting Icon to Dialog


        // Setting Positive "Yes" Btn
        alertDialog2.setNeutralButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog





                    }
                });
        alertDialog2.show();

    }

    @Override
    protected void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        if(currentapiVersion>=android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH)
        {
            GoogleAnalytics.getInstance(LocalTrain.this).reportActivityStart(this);
        }
    }


    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        if(currentapiVersion>=android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH)
        {
            GoogleAnalytics.getInstance(LocalTrain.this).reportActivityStop(this);
        }
    }

    private class Checkupdate extends AsyncTask<String, String, JSONObject> {


        private ProgressDialog pDialog;

        String email,password;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.v("Log da","+++++++++++++++++++++++");
        }

        @Override
        protected JSONObject doInBackground(String... args) {
            Log.v("version code ",VersionCode+"");
            HttpConnection searchFunctions = new HttpConnection();
            JSONObject json = searchFunctions.CheckUpdate(VersionCode);

            return json;

        }

        @Override
        protected void onPostExecute(JSONObject json) {
            boolean loginsucess=false;

            if(json!=null)
            {
                Log.v("Log da",json.toString());
                try {
                    String  vcode = json.getString("isupdate");

                    int version_code_diff = Integer.parseInt(vcode);

                    if(version_code_diff==1)
                    {
                        //updatecheck.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }




        }
    }
}
