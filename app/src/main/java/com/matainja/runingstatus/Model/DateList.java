package com.matainja.runingstatus.Model;

public class DateList {
	private String TextLabel;
	private String Textvalue;
	public DateList(String Textvalue,String TextLabel)
	{
		this.TextLabel = TextLabel;
		this.Textvalue = Textvalue;
	}
	
	
	public String getTextLabel()
	{
		return TextLabel;
	}
		public void  SetTextLabel(String TextLabel)
		{
			this.TextLabel = TextLabel;
		}

		
		public String getTextValue()
		{
			return Textvalue;
		}
		
		public void SetTextValue( String TextValue)
		{
			this.Textvalue = TextValue;
			
		}
}
