package com.matainja.runingstatus.Model;

public class StationListBean {
	
	
	private String arrival;
	private String stationName;
	private String stationCode;
	private String fromstation;
	private String tostation;
	private Integer iscolorBG;
	private String arrival_date;
	
	public StationListBean(String arrival,String stationName,String stationCode,String fromstation,String tostation,int iscolorBG,String arrival_date)
	{
		super();
		this.arrival = arrival;
		this.stationName = stationName;
		this.stationCode = stationCode;
		this.fromstation = fromstation;
		this.tostation = tostation;
		this.iscolorBG = iscolorBG;
		this.arrival_date = arrival_date;
		
	}
	
	
    	
    	
    	public String getArrival() {
            return arrival;
        	}
    	
    	
    	
        	
        	
        	public String getStationName() {
                return stationName;
            	}
        	
        	
        	
            	
            	
            	public String getStationCode() {
                    return stationCode;
                	}
            	
            	public String getfromStationCode() {
                    return fromstation;
                	}
            	public String gettoStationCode() {
                    return tostation;
                	}
            	
            	public int getColorback() {
            		
            			return iscolorBG;
            	      
                    
                	}


	public String getArrival_date() {
		return arrival_date;
	}

	public void setArrival_date(String arrival_date) {
		this.arrival_date = arrival_date;
	}
}
