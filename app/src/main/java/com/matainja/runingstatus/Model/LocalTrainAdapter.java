package com.matainja.runingstatus.Model;

import com.matainja.runingstatus.Database.DatabaseAdapter;

/*CREATE TABLE train_table(_id INTEGER PRIMARY KEY AUTOINCREMENT, trainNO text, trainName text,sun boolean,mon boolean,tue boolean,wed
		boolean,thu boolean,fri boolean,sat boolean, UNIQUE (trainNO))*/
public class LocalTrainAdapter {
	
	
    
    private int _id;
    private String trainNO;
    private   String trainName;
    private  int sun;
    private  int mon; 
    private int tue; 
    private int wed;
    private int thu; 
    private int fri; 
    private int sat;
    private DatabaseAdapter dbHelper;
    public LocalTrainAdapter(){}
     
    public LocalTrainAdapter(int _id, String trainNO, String trainName,int sun,int mon, 
    		int tue, int wed, int thu, int fri, int sat  ){
        this._id = _id;
        this.trainNO = trainNO;
        this.trainName = trainName;
        this.sun=sun;
        this.mon=mon;
        this.tue = tue;
        this.wed = wed;
        this.thu = thu;
        this.fri= fri;
        this.sat = sat;
    }
     
    
    public void setId(int id){
        this._id = id;
    }
     
    public void settrainNO(String trainNO){
        this.trainNO = trainNO;
    }
    public void settrainName(String trainName){
        this.trainName = trainName;
    }
    public void setSun(int sun)
    {
    	this.sun = sun;
    }
    public void setMon(int mon)
    {
    	this.mon = mon;
    }
    public void setTue(int tue)
    {
    	this.tue = tue;
    }
    public void setWed(int wed)
    {
    	this.wed = wed;
    }
    public void setThu(int thu)
    {
    	this.thu = thu;
    }
    public void setFri(int fri)
    {
    	this.fri = fri;
    }
    public void setSat(int sat)
    {
    	this.sat = sat;
    }
     
    public int getId(){
        return this._id;
    }
     
    /*private String trainNO;
    private   String trainName;
    private  boolean sun;
    private  boolean mon; 
    private boolean tue; 
    private boolean wed;
    private boolean thu; 
    private boolean fri; 
    private boolean sat;*/
    public String gettrainName(){
        return this.trainName;
    }
    
    public String gettrainNO(){
        return this.trainNO;
    }
    
    public int getSun(){
        return this.sun;
    }
    
    public int getMon(){
        return this.mon;
    }
    public int getTue(){
        return this.tue;
    }
    public int getWed(){
        return this.wed;
    }
    public int getThu(){
        return this.thu;
    }
    public int getFri(){
        return this.fri;
    }
    public int getSat(){
        return this.sat;
    }
 
}
