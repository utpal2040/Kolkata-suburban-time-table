package com.matainja.runingstatus.Model;

public class RouteLocalTrain {
	

	
	private int _id;
	private int trainId;
	private int stationId;
	private String arrival;
	private int datePlus;
	
	public RouteLocalTrain(){}
    
    public RouteLocalTrain(int _id, int trainId, int stationId,String arrival,int datePlus ){
        this._id = _id;
        this.trainId = trainId;
        this.stationId=stationId;
        this.arrival=arrival;
        this.datePlus=datePlus;
        
    }
    
    public int getId()
    {
    	return this._id;
    }
    
    public int getTrainId()
    {
    	return this.trainId;
    }
    
    public int getStationId()
    {
    	return this.stationId;
    }
    public String getArrival()
    {
    	return this.arrival;
    }
    public int getDatePlus()
    {
    	return this.datePlus;
    }

}
