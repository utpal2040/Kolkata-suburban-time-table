package com.matainja.runingstatus.Model;

public class Train {
	private String train_no;
	private String train_name;
	private String start_station;
	private String end_station;
	private int id;

	public Train(String train_no, String train_name, String start_station, String end_station)
	{
		super();
		this.train_no = train_no;
		this.train_name = train_name;
		this.start_station = start_station;
		this.end_station = end_station;
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}


	public String getTrainNo() {
        return train_no;
    	}
	
	
	
    	public void setTrainNo(String train_no) {
        this.train_no = train_no;
    	}
    	
    	
    	public String getTrainName() {
            return train_name;
        	}
    	
    	
    	
        	public void setTrainNAme(String train_name) {
            this.train_name = train_name;
        	}
        	
        	public String getstart_station() {
                return start_station;
            	}
        	
        	
        	
            	public void setstart_station(String start_station) {
                this.train_name = start_station;
            	}
            	
            	public String getend_station() {
                    return end_station;
                	}
            	
            	
            	
                	public void setend_station(String end_station) {
                    this.train_name = end_station;
                	}
            	
}
