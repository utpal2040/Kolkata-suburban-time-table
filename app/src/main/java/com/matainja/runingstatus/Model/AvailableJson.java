package com.matainja.runingstatus.Model;

public class AvailableJson {
	private String mdate;
	private String mstatus;
	
	
	public AvailableJson(String Idate,String Istatus)
	{
		super();
		this.mdate = Idate;
		this.mstatus = Istatus;
		
		
	}
	
	public String getDate() {
        return mdate;
    	}
	
	
	
    	public void setDate(String Idate) {
        this.mdate = Idate;
    	}
    	
    	
    	public String getStatus() {
            return mstatus;
        	}
    	
    	
    	
        	
            	
}
