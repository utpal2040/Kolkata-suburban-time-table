package com.matainja.runingstatus.Model;

public class StationLocal {
	
	private int _id;
	
	private String StationCode;
	private String stationName;
	private int popularity;
	public StationLocal(){}
    
    public StationLocal(int _id, String StationCode, String stationName,int popularity ){
        this._id = _id;
        this.StationCode = StationCode;
        this.stationName=stationName;
        this.popularity=popularity;
        
    }
    
    public int getId()
    {
    	return this._id;
    }
    
    public String getStationCode()
    {
    	return this.StationCode;
    }
    
    public String getStationName()
    {
    	return this.stationName;
    }
    public int getPopularity()
    {
    	return this.popularity;
    }

}
