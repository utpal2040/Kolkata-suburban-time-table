package com.matainja.runingstatus.Model;

public class SuggestStationBean {
	
	
	
	private String sStationName;
	private String sStationCode;
	private String sdesName;
	private String sdesCode;
    
    public SuggestStationBean(String sStationName, String sStationCode, String sdesName,String sdesCode ){
      
        this.sStationName = sStationName;
        this.sStationCode=sStationCode;
        this.sdesName=sdesName;
        this.sdesCode=sdesCode;
    }
    
    
    
    public String getsStationName()
    {
    	return this.sStationName;
    }
    
    public String getsdesName()
    {
    	return this.sdesName;
    }
    
    public String getsStationCode()
    {
    	return this.sStationCode;
    }
    
    public String getsdesCode()
    {
    	return this.sdesCode;
    }
    

}
