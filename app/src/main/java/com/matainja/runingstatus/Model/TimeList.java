package com.matainja.runingstatus.Model;



public class TimeList {
	
	private String text;
	private String val;
	
	
	public TimeList(String text,String val)
	{
		super();
		this.text = text;
		this.val = val;
		
		
	}
	
	
    	
    	
    	public String getText() {
            return text;
        	}
    	
    	
    	
        	public void setText(String text) {
            this.text = text;
        	}
        	
        	public String getVal() {
                return val;
            	}
        	
        	
        	
            	public void setVal(String val) {
                this.val = val;
            	}
            	
            	
            	
}
