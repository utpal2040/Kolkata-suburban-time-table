package com.matainja.runingstatus.Model;

import static com.matainja.runingstatus.R.id.StationName;

/**
 * Created by matainja on 24-Feb-17.
 */

public class RecentSearch {
    String FromStationName;
    String FromStastionCode;
    String ToStationName;

    public String getToStationCode() {
        return ToStationCode;
    }

    public void setToStationCode(String toStationCode) {
        ToStationCode = toStationCode;
    }

    public String getToStationName() {
        return ToStationName;
    }

    public void setToStationName(String toStationName) {
        ToStationName = toStationName;
    }

    public String getFromStastionCode() {
        return FromStastionCode;
    }

    public void setFromStastionCode(String fromStastionCode) {
        FromStastionCode = fromStastionCode;
    }

    public String getFromStationName() {
        return FromStationName;
    }

    public void setFromStationName(String fromStationName) {
        FromStationName = fromStationName;
    }

    String ToStationCode;


}

