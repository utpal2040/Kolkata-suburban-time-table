package com.matainja.runingstatus.Model;

public class TimetableListBean {
	private String trainNo;
	private Integer trainId;
	private String trainName;
	private String SourceStation;
	private String sourcestationcode;
	private String sourcearrival;
	private String DestinationStation;
	private String destStationcode;
	private String destarrival;
	private String avilabledays;
	private int isoffday=0;
	private int isonlysat=0;
	private String remarks;
    private int TotalStop;
	private String totaltime;

    public TimetableListBean(Integer trainId, String trainNo, String trainName, String SourceStation, String
			sourcestationcode, String sourcearrival, String DestinationStation, String
			destStationcode, String destarrival, String avilabledays, int isoffday,
							 int isonlysat, String remarks, int TotalStop,String totaltime)
	{
		super();
		this.trainId = trainId;
		this.trainNo = trainNo;
		this.trainName = trainName;
		this.SourceStation = SourceStation;
		this.sourcestationcode = sourcestationcode;
		this.sourcearrival = sourcearrival;
		this.DestinationStation = DestinationStation;
		this.destStationcode = destStationcode;
		this.destarrival = destarrival;
		this.avilabledays=avilabledays;
		this.isoffday = isoffday;
		this.isonlysat = isonlysat;
		this.remarks = remarks;
        this.TotalStop = TotalStop;
		this.totaltime = totaltime;
	}
	public String getavilabledays() {
        return avilabledays;
    	}
	public int getisoffday() {
        return isoffday;
    	}
	public int getisonlysat() {
        return isonlysat;
    	}
	public Integer getTrainId() {
        return trainId;
    	}
	public String getTrainNo() {
        return trainNo;
    	}
	
	
	
    	public void setTrainNo(String trainNo) {
        this.trainNo = trainNo;
    	}
    	
    	
    	public String getTrainName() {
            return trainName;
        	}
    	
    	
    	
        	public void setTrainNAme(String trainName) {
            this.trainName = trainName;
        	}
        	
        	public String getSourceStation() {
                return SourceStation;
            	}
        	
        	
        	
            	public void setSourceStation(String SourceStation) {
                this.SourceStation = SourceStation;
            	}
            	
            	public String getsourcestationcode() {
                    return sourcestationcode;
                	}
            	
            	
            	
                	public void setsourcestationcode(String sourcestationcode) {
                    this.sourcestationcode = sourcestationcode;
                	}
            	
                	public String getsourcearrival() {
                        return sourcearrival;
                    	}
                	
                	public String geDestinationStation() {
                        return DestinationStation;
                    	}
                	public String getdestStationcode() {
                        return destStationcode;
                    	}
                	public String getdestarrival() {
                        return destarrival;
                    	}
                	
                	public String getRemarks() {
                        return remarks;
                    	}
                	
                	public void setRemarks(String remarks) {
                        this.remarks=remarks;
                    	}

            public int getTotalStop() {
                return TotalStop;
            }
				public String gettotaltime() {
					return totaltime;
				}
            public void setTotalStop(int totalStop) {
                TotalStop = totalStop;
            }

}
