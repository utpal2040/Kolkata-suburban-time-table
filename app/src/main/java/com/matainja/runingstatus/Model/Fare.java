package com.matainja.runingstatus.Model;

/**
 * Created by matainja on 17-Mar-17.
 */

public class Fare {

    String name;
    String code;

    public String getFare() {
        return fare;
    }

    public void setFare(String fare) {
        this.fare = fare;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    String fare;
}
