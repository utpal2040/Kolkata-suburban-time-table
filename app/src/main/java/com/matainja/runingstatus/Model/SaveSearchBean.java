package com.matainja.runingstatus.Model;

public class SaveSearchBean {
	private String name;
	private String srcstation;
	private String desstation;
	private Integer islocal;
	private  Integer Id;
	
	public SaveSearchBean(Integer Id,String name,String srcstation,String desstation,Integer islocal)
	{
		super();
		this.Id = Id;
		this.name = name;
		this.srcstation = srcstation;
		this.desstation = desstation;
		this.islocal = islocal;
		
	}
	public Integer getId() {
        return Id;
    	}
	
	
	
    	public void setTId(Integer Id) {
        this.Id = Id;
    	}
    	
	public String getName() {
        return name;
    	}
	
	
	
    	public void setTName(String name) {
        this.name = name;
    	}
    	
    	
    	public String getSrcStation() {
            return srcstation;
        	}
    	
    	
    	
        	public void setSrcStation(String srcstation) {
            this.srcstation = srcstation;
        	}
        	
        	public String getDesstation() {
                return desstation;
            	}
        	
        	
        	
            	public void setDesstation(String desstation) {
                this.desstation = desstation;
            	}
            	
            	public Integer getIsLocal() {
                    return islocal;
                	}
            	
            	
            	
                	public void setIsLocal(Integer islocal) {
                    this.islocal = islocal;
                	}
            	
            	
            	
}
