package com.matainja.runingstatus.Model;



public class TypeTicket {
    
    private  String Name="";
    private  String mValue=""; 
    private  String Url="";
     
    /*********** Set Methods ******************/
    public void setName(String name)
    {
        this.Name = name;
    }
     
    public void setValue(String val)
    {
        this.mValue = val;
    }
     
   
     
    /*********** Get Methods ****************/
    public String getName()
    {
        return this.Name;
    }
     
    public String getValue()
    {
        return this.mValue;
    }
   
}