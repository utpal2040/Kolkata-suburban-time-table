package com.matainja.runingstatus;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.matainja.runingstatus.Adapter.CustomAdapterAge;
import com.matainja.runingstatus.Adapter.CustomAdapterClass;
import com.matainja.runingstatus.Adapter.CustomAdapterTypetckt;
import com.matainja.runingstatus.Common.ConnectionDetector;
import com.matainja.runingstatus.Connectivity.HttpConnection;
import com.matainja.runingstatus.CustomView.Utility;
import com.matainja.runingstatus.Database.DatabaseAdapterEx;
import com.matainja.runingstatus.Model.Fare;
import com.matainja.runingstatus.Model.PeopleAge;
import com.matainja.runingstatus.Model.StationListBean;
import com.matainja.runingstatus.Model.TypeClass;
import com.matainja.runingstatus.Model.TypeTicket;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by matainja on 16-Mar-17.
 */

public class GetFareActivity extends AppCompatActivity implements View.OnClickListener {

    String TrainNo;
    Bundle extras;
    int trainId;
    String  trainNo,trainName;
    String fromstation;
    String tostation;
    ArrayList<StationListBean> stationListView;
    private DatabaseAdapterEx dbHelper;
    public AlertDialog alertDialog;
    String mClass="SL";
    String mAge;
    String mTicket="GN";

    CustomAdapterClass spinnerClassadapter;
    CustomAdapterAge spinnerAgeadapter;
    CustomAdapterTypetckt spinnertcktadapter;

    public  ArrayList<TypeClass> TypeclassSpinner;
    public  ArrayList<PeopleAge> TypeageSpinner ;
    public  ArrayList<TypeTicket> TypetcktSpinner;
    Toolbar toolbar;
    public ProgressBar progressBar;
    Button fare_getbttn;
    CardView card2;
    LinearLayout ll;
    LinearLayout.LayoutParams params;
    public ListView list;
    ArrayList<Fare> farelist;
    EditText _age;

    TextView mstation,mdtation,trainno,Trainame;
    ImageView imageView2;
    RelativeLayout FCRLayuout,E3RLayout,A1RLayout,A3RLayout,A2RLayout,CCRlayout,S2RLayout,SLLLayout;
    TextView FC,E3,A1,A3,A2,CC,S2,SL;

    CheckBox gen,tat,pre_tat,phy_handi,ladQ,seniCity;
    String erro_msg ="Sorry!!somethings is Wrong Try again";

    public Dialog dialog;
    private static final String[] typeage = {
            "Adult", "Child", "Senior Male", "Senior Female",
    };
    SharedPreferences sp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout_getfare);
        overridePendingTransition(R.anim.anim_slide_in_left,
                R.anim.anim_slide_out_left);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        sp = getSharedPreferences("themeColor", Context.MODE_PRIVATE);
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorExpressStatus));
        }*/



        extras = getIntent().getExtras();
        if(extras !=null) {
            trainId = extras.getInt("trainId");
            TrainNo =extras.getString("trainNo");
            fromstation = extras.getString("fromstation");
            tostation = extras.getString("tostation");
            trainName = extras.getString("trainName");

            Log.e("fromstation==",fromstation + "  " + tostation);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(GetFareActivity.this, "fromstation=="+fromstation+" tostation== "+tostation, Toast.LENGTH_SHORT).show();
                Intent i = new Intent(GetFareActivity.this, ExTimeTableDetails.class);
                i.putExtra("trainId",trainId);
                i.putExtra("trainNo",TrainNo);
                i.putExtra("fromstation",fromstation);
                i.putExtra("tostation",tostation);
                i.putExtra("trainName",trainName);
                startActivity(i);
                finish();

                overridePendingTransition(R.anim.anim_slide_in_right,
                        R.anim.anim_slide_out_right);

                /*overridePendingTransition(R.anim.anim_slide_in_right,
                        R.anim.anim_slide_out_right);*/
            }
        });

        ll =(LinearLayout) findViewById(R.id.fare_get_layout);
        params = new LinearLayout.LayoutParams(
                ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.MATCH_PARENT);
        farelist = new ArrayList<Fare>();

        Trainame    = (TextView) findViewById(R.id.fare_trainname);
        trainno     = (TextView) findViewById(R.id.trainId);
        mstation    = (TextView) findViewById(R.id.fare_station);
        mdtation    = (TextView) findViewById(R.id.fare_dstation);
        fare_getbttn = (Button)findViewById(R.id.fare_get);
        imageView2 = (ImageView) findViewById(R.id.imageView2);

        //card2 = (CardView) findViewById(R.id.card2);
        Trainame.setText(trainName);
        trainno.setText("#"+TrainNo);
        mstation.setText(fromstation);
        mdtation.setText(tostation);

        /*FCRLayuout = (RelativeLayout) findViewById(R.id.FCRLayuout);
        E3RLayout = (RelativeLayout) findViewById(R.id.E3RLayout);
        A1RLayout = (RelativeLayout) findViewById(R.id.A1RLayout);
        A3RLayout = (RelativeLayout) findViewById(R.id.A3RLayout);
        A2RLayout = (RelativeLayout) findViewById(R.id.A2RLayout);
        CCRlayout = (RelativeLayout) findViewById(R.id.CCRlayout);
        S2RLayout = (RelativeLayout) findViewById(R.id.S2RLayout);
        //SLLLayout = (RelativeLayout) findViewById(R.id.SLLLayout);

        FC = (TextView) findViewById(R.id.FC);
        E3 = (TextView) findViewById(R.id.E3);
        A1 = (TextView) findViewById(R.id.A1);
        A3 = (TextView) findViewById(R.id.A3);
        A2 = (TextView) findViewById(R.id.A2);
        CC = (TextView) findViewById(R.id.cc);
        S2 = (TextView) findViewById(R.id.s2);
        SL = (TextView) findViewById(R.id.sl);

        FCRLayuout.setOnClickListener(this);
        E3RLayout.setOnClickListener(this);
        A1RLayout.setOnClickListener(this);
        A3RLayout.setOnClickListener(this);
        A2RLayout.setOnClickListener(this);
        CCRlayout.setOnClickListener(this);
        S2RLayout.setOnClickListener(this);
        //SLLLayout.setOnClickListener(this);*/

        gen = (CheckBox) findViewById(R.id.gen);
        tat = (CheckBox) findViewById(R.id.tat);
        pre_tat = (CheckBox) findViewById(R.id.pre_tat);
        phy_handi = (CheckBox) findViewById(R.id.phy_handi);
        ladQ = (CheckBox) findViewById(R.id.ladQ);
        seniCity = (CheckBox) findViewById(R.id.seniCity);
        _age = (EditText) findViewById(R.id.age);


        if(sp.getInt("expressColor",0)!=0) {
            toolbar.setBackgroundColor(sp.getInt("expressColor",0));
            fare_getbttn.setBackgroundColor(sp.getInt("expressColor",0));

            mstation.setTextColor(sp.getInt("expressColor",0));
            mdtation.setTextColor(sp.getInt("expressColor",0));
            imageView2.setColorFilter(sp.getInt("expressColor",0));
            _age.getBackground().setColorFilter(sp.getInt("expressColor",0), PorterDuff.Mode.SRC_ATOP);


            int[] demo_colors = getResources().getIntArray(R.array.demo_colors);
            int[] demo_colors_mat = getResources().getIntArray(R.array.demo_colors_mat);

            ArrayList<String> colorString = new ArrayList<String>();
            ArrayList<String> colorString_mat = new ArrayList<String>();

            for (int i = 0; i < demo_colors.length; i++) {

                colorString.add(String.valueOf(demo_colors[i]));

            }

            for (int i = 0; i < demo_colors_mat.length; i++) {

                colorString_mat.add(String.valueOf(demo_colors_mat[i]));

            }
            Log.e("position ",colorString.indexOf(String.valueOf(sp.getInt("expressColor",0))) + "");
            int color_mat_val = Integer.parseInt(colorString_mat.get(colorString.indexOf(String.valueOf(sp.getInt("expressColor", 0)))));

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(color_mat_val);


            }
        }else{
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(getResources().getColor(R.color.colorExpressStatus));
            }
        }

        /*---------------- TICKET TYPE EVENTS -----------------*/
        gen.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    // perform logic
                    mTicket = "GN";

                    tat.setChecked(false);
                    pre_tat.setChecked(false);
                    phy_handi.setChecked(false);
                    ladQ.setChecked(false);
                    seniCity.setChecked(false);
                }

            }
        });

        tat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    // perform logic
                    mTicket = "CK";

                    gen.setChecked(false);
                    pre_tat.setChecked(false);
                    phy_handi.setChecked(false);
                    ladQ.setChecked(false);
                    seniCity.setChecked(false);
                }

            }
        });

        pre_tat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    // perform logic
                    mTicket = "PT";

                    gen.setChecked(false);
                    tat.setChecked(false);
                    phy_handi.setChecked(false);
                    ladQ.setChecked(false);
                    seniCity.setChecked(false);
                }

            }
        });

        phy_handi.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    // perform logic
                    mTicket = "HP";

                    gen.setChecked(false);
                    tat.setChecked(false);
                    pre_tat.setChecked(false);
                    ladQ.setChecked(false);
                    seniCity.setChecked(false);
                }

            }
        });

        ladQ.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    // perform logic
                    mTicket = "LD";

                    gen.setChecked(false);
                    tat.setChecked(false);
                    pre_tat.setChecked(false);
                    phy_handi.setChecked(false);
                    seniCity.setChecked(false);
                }

            }
        });

        seniCity.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    // perform logic
                    mTicket = "SS";

                    gen.setChecked(false);
                    tat.setChecked(false);
                    pre_tat.setChecked(false);
                    phy_handi.setChecked(false);
                    ladQ.setChecked(false);
                }

            }
        });

        stationListView = new ArrayList<StationListBean>();
        /// Train Info retrireve
        if(!(trainId>0))
        {
            Cursor getTrainIdcursor = dbHelper.getTrainId(trainNo);
            if(getTrainIdcursor.getCount()>0)
            {
                while (getTrainIdcursor.moveToNext()) {
                    trainId =getTrainIdcursor.getInt(getTrainIdcursor.getColumnIndexOrThrow("_id"));

                }
            }
        }

       //Spinner Spinnerclass = (Spinner)findViewById(R.id.typeclass);

        //setListData();
        // Resources passed to adapter to get image

        //spinnerClassadapter = new CustomAdapterClass(GetFareActivity.this, R.layout.spinner_row, TypeclassSpinner);

        // Set adapter to spinner
        //Spinnerclass.setAdapter(spinnerClassadapter);
        /*Spinnerclass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View v, int position, long id) {
                // your code here

                // Get selected row data to show on screen

                TypeClass classtypeval = (TypeClass) TypeclassSpinner.get(position);
                mClass=classtypeval.getValue();
                Log.e("Select Item","hh="+mClass);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });*/
						 /* class drop  End down */


						 /* class Age drop down */

        /*MaterialSpinner SpinnerAge = (MaterialSpinner) findViewById(R.id.age);
        mAge = "AD";
        setPeopleAge();
        SpinnerAge.setItems(typeage);*/

       // if (!quantity.equals(null)) {
          /*  int spinnerPosition = ANDROID_VERSIONS.
            mSpinner.setSelection(spinnerPosition);*/
         /*int spinnerPosition =0;
            for (int i=0;i<typeage.length;i++) {
                if (typeage[i].equals("Senior Male")) {
                    spinnerPosition = i;
                    break;
                }
            }
        SpinnerAge.setSelectedIndex(spinnerPosition);*/
        //}

        /*SpinnerAge.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {

            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {

                //Double seleted_item = Double.parseDouble(item);
                String selected_age = item;
                Log.e("seleted_item=quantity=", "" + selected_age);
                if(selected_age.equalsIgnoreCase("Adult")){
                    mAge = "AD";
                }else if(selected_age.equalsIgnoreCase("Child")){
                    mAge = "CH";
                }else if(selected_age.equalsIgnoreCase("Senior Male")){
                    mAge = "SM";
                } else{
                    mAge = "SF";
                }

            }
        });*/
        /*Spinner  SpinnerAge = (Spinner)findViewById(R.id.age);



        spinnerAgeadapter = new CustomAdapterAge(GetFareActivity.this, R.layout.spinner_row, TypeageSpinner);

        // Set adapter to spinner
        SpinnerAge.setAdapter(spinnerAgeadapter);
        SpinnerAge.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View v, int position, long id) {

                PeopleAge classageval = (PeopleAge) TypeageSpinner.get(position);
                mAge=classageval.getValue();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });*/

        /*Spinner  Spinnertckt = (Spinner)findViewById(R.id.tckttype);

        setTicketType();
        // Resources passed to adapter to get image

        spinnertcktadapter = new CustomAdapterTypetckt(GetFareActivity.this, R.layout.spinner_row, TypetcktSpinner);

        // Set adapter to spinner
        Spinnertckt.setAdapter(spinnertcktadapter);
        Spinnertckt.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View v, int position, long id) {
                // your code here

                // Get selected row data to show on screen
                //  String Company    = ((TextView) v.findViewById(R.id.name)).getText().toString();

                TypeTicket classtypeval = (TypeTicket) TypetcktSpinner.get(position);
                mTicket=classtypeval.getValue();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });*/

        fare_getbttn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /*if(mClass.length()== 0)
                {
                    CharSequence text = "Please Select  Class!";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(getApplicationContext(), text, duration);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }*/
									/*else if(mEmail.getText().length()== 0)
									{
							    		CharSequence text = "Please Enter Email!";
										int duration = Toast.LENGTH_SHORT;

										Toast toast = Toast.makeText(getApplicationContext(), text, duration);
										toast.setGravity(Gravity.CENTER, 0, 0);
										toast.show();
									}*/
                /*else if(mAge.length()== 0)
                {
                    CharSequence text = "Please Select Age!";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(getApplicationContext(), text, duration);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }*/
                /*else if(mTicket.length()== 0)
                {
                    CharSequence text = "Please Select Ticket!";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(getApplicationContext(), text, duration);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }*/
                /*else
                {*/
                    ConnectionDetector cd = new ConnectionDetector(getApplicationContext());

                    // Check if Internet present
                    if (!cd.isConnectingToInternet()) {
                        Toast.makeText(getApplicationContext(),"Internet Problem!", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        GetFareDialog();
                        /*card2.setVisibility(View.GONE);
                        progressBar1.setVisibility(View.VISIBLE);
                        farelist.clear();*/

                        //tv.setText("");
                       // ll.removeView(ll);

                        //new GetFare().execute();
                    }



                //}
            }



        });
    }

    private void GetFareDialog() {


        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(GetFareActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = GetFareActivity.this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.getfaredialog, null);
        dialogBuilder.setView(dialogView);






        progressBar = (ProgressBar) dialogView.findViewById(R.id.progressBar);
        list = (ListView) dialogView.findViewById(R.id.list);

        mAge = _age.getText().toString().trim();

        new GetFare().execute();

        Button dialogButton = (Button) dialogView.findViewById(R.id.dialogButtonOK);

        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                farelist.clear();
                alertDialog.dismiss();
            }
        });


        if(sp.getInt("expressColor",0)!=0) {
            dialogButton.setBackgroundColor(sp.getInt("expressColor",0));
        }
        alertDialog = dialogBuilder.create();
        alertDialog.show();
        //alertDialog.getWindow().setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);






        /*dialog = new Dialog(GetFareActivity.this);

        dialog.setContentView(R.layout.getfaredialog);
        dialog.setTitle("Approx Fare");

        progressBar = (ProgressBar) dialog.findViewById(R.id.progressBar);
        list = (ListView) dialog.findViewById(R.id.list);

        new GetFare().execute();

        Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);

        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();*/
    }

    /*public void setListData()
    {
        TypeclassSpinner = new ArrayList<TypeClass>();
    	*//*1A or 2A or 3A or 3E or CC or FC or SL or 2S*//*
        String[][] typeclass = {
                { "Select", "" },
                { "Sleeper Class(SL)", "SL" },
                { "Second Sitting(2S)", "2S" },
                { "AC chair Car(CC)", "CC" },
                { "AC 2 Tier sleeper(2A)", "2A" },
                { "AC 3 Tier(3A)", "3A" },
                { "First class Air-Conditioned(1A)", "1A" },
                { "AC 3 Tier Economy(3E)", "3E" },
                { "First class(FC)", "FC" },
        };


        for (int ic = 0; ic < typeclass.length; ic++) {


            final TypeClass sched = new TypeClass();

            *//******* Firstly take data in model object ******//*
            sched.setName(typeclass[ic][0]);
            sched.setValue(typeclass[ic][1]);

            TypeclassSpinner.add(sched);

        }

    }*/

    public void setPeopleAge()
    {
        TypeageSpinner = new ArrayList<PeopleAge>();
    	//CH|AD|SM|SF for Child, Adult, Senior Male, Senior Female

        String[][] typeage = {
                { "Select", "" },
                { "Adult", "AD" },
                { "Child", "CH" },

                { "Senior Male", "SM" },
                { "Senior Female", "SF" },

        };

        for (int ic = 0; ic < typeage.length; ic++) {


            final PeopleAge sched = new PeopleAge();

            /****** Firstly take data in model object *****/
            sched.setName(typeage[ic][0]);
            sched.setValue(typeage[ic][1]);

            TypeageSpinner.add(sched);


        }

    }

    /*public void setTicketType()
    {
    	*//*GN|CK|PT for General, Tatkal, Premium Tatkal respectively.*//*

        TypetcktSpinner = new ArrayList<TypeTicket>();

        String[][] tickettype = {
                { "Select", "" },
                { "General", "GN" },
                { "Tatkal", "CK" },

                { "Premium Tatkal", "PT" },
                { "PHYSICALLY HANDICAP", "HP" },
                { "Ladies Quota", "LD" },
                { "Senior Citizen", "SS" },


        };


        for (int ic = 0; ic < tickettype.length; ic++) {


            final TypeTicket sched = new TypeTicket();

            *//******* Firstly take data in model object ******//*
            sched.setName(tickettype[ic][0]);
            sched.setValue(tickettype[ic][1]);

            TypetcktSpinner.add(sched);


        }

    }*/

    private class GetFare extends AsyncTask<String, String, JSONObject> {
        private ProgressDialog pDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pDialog = new ProgressDialog(GetFareActivity.this);
            pDialog.setMessage("Getting Fare ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            //pDialog.show();
            progressBar.setVisibility(View.VISIBLE);
            list.setVisibility(View.GONE);
        }
        @Override
        protected JSONObject doInBackground(String... args) {

            HttpConnection searchFunctions = new HttpConnection();
            JSONObject json = searchFunctions.getTrainFare(mAge, mClass, fromstation, mTicket, TrainNo, tostation);

            return json;
        }
        @Override
        protected void onPostExecute(JSONObject json) {
            pDialog.dismiss();

            /*final Dialog dialogfare = new Dialog(GetFareActivity.this,R.style.cust_dialog);
            dialogfare.setContentView(R.layout.fare_price_box);*/





            if(json !=null)
            {


                if(json.has("ResponseCode") )
                {
                    try {
                        String  farestatus = json.getString("ResponseCode");


                        if(farestatus.equals("204"))
                        {

                            JSONArray fare_price = json.getJSONArray("Fare");

                            for(int ik = 0; ik < fare_price.length(); ik++){
                                JSONObject ca = fare_price.getJSONObject(ik);
                                Fare fr = new Fare();
                               // Log.e("json==", "llll");
                                String cname=ca.getString("Name");
                                String code = ca.getString("Code");
                                String fare = ca.getString("Fare");
                               // Log.e("json==name", cname);
                                /*TextView tv = new TextView(GetFareActivity.this);
                                tv.setText(cname +"( "+code+ " ) : ₹"+fare);
                                tv.setLayoutParams(params);
                                ll.addView(tv);*/

                                fr.setName(cname);
                                fr.setCode(code);
                                fr.setFare(fare);

                                farelist.add(fr);
                            }

                            FareListAdapter fare_adapter =  new FareListAdapter(GetFareActivity.this,farelist);
                            list.setAdapter(fare_adapter);
                            Utility.setListViewHeightBasedOnChildren(list);
                            list.setVisibility(View.VISIBLE);
                            progressBar.setVisibility(View.GONE);
                            //card2.setVisibility(View.VISIBLE);

                            /*TranslateAnimation animate = new TranslateAnimation(0,0,0,-card2.getHeight());
                            animate.setDuration(1500);
                            animate.setFillAfter(true);
                            card2.startAnimation(animate);
                            card2.setVisibility(View.VISIBLE);*/

                            /*ImageView fare_getbttn_dismiss = (ImageView)dialogfare.findViewById(R.id.fare_get_close);
                            fare_getbttn_dismiss.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    dialogfare.dismiss();
                                }
                            });

                            dialogfare.show();*/
                        }
                        else {
                            if(json.has("Message") )
                            {
                                erro_msg = json.getString("Message");
                            }
                            errorBox();
                            progressBar.setVisibility(View.GONE);
                            alertDialog.dismiss();
                        }

                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        //e.printStackTrace();
                    }


                }
                else {
                    errorBox();
                    progressBar.setVisibility(View.GONE);
                   // dialog.dismiss();
                    alertDialog.dismiss();
                }
                //Log.e("json", json.toString());
            }
            else {
                errorBox();
                progressBar.setVisibility(View.GONE);
                //dialog.dismiss();
                alertDialog.dismiss();
            }

           // Log.v("Data status3", "p=sshhh");
            // Getting JSON Array from URL



        }
    }

    public void  errorBox()
    {
        Log.v("status", "no");
        // Internet Connection is not present
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(GetFareActivity.this);

        // set title
        alertDialogBuilder.setTitle("Error");

        // set dialog message
        alertDialogBuilder
                .setMessage(erro_msg)
                .setCancelable(false)
                .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        // if this button is clicked, close
                        // current activity
                        //MainActivity.this.finish();

                    }
                });


        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        try {
            alertDialog.show();
        }catch(Exception e){

        }

    }

    public class FareListAdapter extends BaseAdapter{

        ArrayList<Fare> listOfData;
        Context context;
        public FareListAdapter (Context context,ArrayList<Fare> listOfData){
            this.context = context;
            this.listOfData = listOfData;
        }
        @Override
        public int getCount() {
            return listOfData.size();
        }

        @Override
        public Object getItem(int position) {
            return listOfData.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.rowfarelist, null);
            }

            TextView item = (TextView) convertView.findViewById(R.id.item);
            TextView price = (TextView) convertView.findViewById(R.id.price);

            /*String string = "\u20B9";
            byte[] utf8 = null;
            try {
                utf8 = string.getBytes("UTF-8");
                string = new String(utf8, "UTF-8");
                Log.e("string",string);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }*/


            item.setText(listOfData.get(position).getName()+"("+listOfData.get(position).getCode()+")");
            //price.setText(R.string.Rs+" "+listOfData.get(position).getFare());
            price.setText("\u20B9 "+listOfData.get(position).getFare());
            //listOfData.get(position).getName()+"("+listOfData.get(position).getCode()+") :"+listOfData.get(position).getFare()
            return convertView;
        }
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();

        /*overridePendingTransition(R.anim.anim_slide_in_left,
                R.anim.anim_slide_out_left);
*/
        Intent i = new Intent(GetFareActivity.this, ExTimeTableDetails.class);
        i.putExtra("trainId",trainId);
        i.putExtra("trainNo",TrainNo);
        i.putExtra("fromstation",fromstation);
        i.putExtra("tostation",tostation);
        i.putExtra("trainName",trainName);
        startActivity(i);
        finish();
        overridePendingTransition(R.anim.anim_slide_in_right,
                R.anim.anim_slide_out_right);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            /*case R.id.FCRLayuout:
                FCRLayuout.setBackgroundColor(getResources().getColor(R.color.colorExpress));
                FC.setTextColor(getResources().getColor(R.color.white));

                E3RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                A1RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                A3RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                A2RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                CCRlayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                S2RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                SLLLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));

                E3.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                A1.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                A3.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                A2.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                CC.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                S2.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                SL.setTextColor(getResources().getColor(R.color.cardview_dark_background));

                mClass = "FC";
                break;

            case R.id.E3RLayout:
                E3RLayout.setBackgroundColor(getResources().getColor(R.color.colorExpress));
                E3.setTextColor(getResources().getColor(R.color.white));

                FCRLayuout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                A1RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                A3RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                A2RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                CCRlayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                S2RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                SLLLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));

                FC.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                A1.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                A3.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                A2.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                CC.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                S2.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                SL.setTextColor(getResources().getColor(R.color.cardview_dark_background));

                mClass = "E3";
                break;

            case R.id.A1RLayout:
                A1RLayout.setBackgroundColor(getResources().getColor(R.color.colorExpress));
                A1.setTextColor(getResources().getColor(R.color.white));

                FCRLayuout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                E3RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                A3RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                A2RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                CCRlayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                S2RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                SLLLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));

                FC.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                E3.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                A3.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                A2.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                CC.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                S2.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                SL.setTextColor(getResources().getColor(R.color.cardview_dark_background));

                mClass = "A1";
                break;

            case R.id.A3RLayout:
                A3RLayout.setBackgroundColor(getResources().getColor(R.color.colorExpress));
                A3.setTextColor(getResources().getColor(R.color.white));

                FCRLayuout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                E3RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                A1RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                A2RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                CCRlayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                S2RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                SLLLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));

                FC.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                E3.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                A1.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                A2.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                CC.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                S2.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                SL.setTextColor(getResources().getColor(R.color.cardview_dark_background));

                mClass = "A3";
                break;

            case R.id.A2RLayout:
                A2RLayout.setBackgroundColor(getResources().getColor(R.color.colorExpress));
                A2.setTextColor(getResources().getColor(R.color.white));

                FCRLayuout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                E3RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                A1RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                A3RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                CCRlayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                S2RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                SLLLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));

                FC.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                E3.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                A1.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                A3.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                CC.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                S2.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                SL.setTextColor(getResources().getColor(R.color.cardview_dark_background));

                mClass = "A2";
                break;

            case R.id.CCRlayout:
                CCRlayout.setBackgroundColor(getResources().getColor(R.color.colorExpress));
                CC.setTextColor(getResources().getColor(R.color.white));

                FCRLayuout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                E3RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                A1RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                A3RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                A2RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                S2RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                SLLLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));

                FC.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                E3.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                A1.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                A3.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                A2.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                S2.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                SL.setTextColor(getResources().getColor(R.color.cardview_dark_background));

                mClass = "CC";
                break;

            case R.id.S2RLayout:
                S2RLayout.setBackgroundColor(getResources().getColor(R.color.colorExpress));
                S2.setTextColor(getResources().getColor(R.color.white));

                FCRLayuout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                E3RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                A1RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                A3RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                A2RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                CCRlayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                SLLLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));

                FC.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                E3.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                A1.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                A3.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                A2.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                CC.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                SL.setTextColor(getResources().getColor(R.color.cardview_dark_background));

                mClass = "S2";
                break;

            case R.id.SLLLayout:
                SLLLayout.setBackgroundColor(getResources().getColor(R.color.colorExpress));
                SL.setTextColor(getResources().getColor(R.color.white));

                FCRLayuout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                E3RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                A1RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                A3RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                A2RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                CCRlayout.setBackgroundColor(getResources().getColor(R.color.background_layout));
                S2RLayout.setBackgroundColor(getResources().getColor(R.color.background_layout));

                FC.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                E3.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                A1.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                A3.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                A2.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                CC.setTextColor(getResources().getColor(R.color.cardview_dark_background));
                S2.setTextColor(getResources().getColor(R.color.cardview_dark_background));

                mClass = "SL";
                break;*/
        }
    }
}
