
package com.matainja.runingstatus;

import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.Window;
import android.view.WindowManager;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;



import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.graphics.Typeface;


import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.animation.Animation.AnimationListener;
import android.view.inputmethod.InputMethodManager;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.matainja.runingstatus.Database.DataBaseHelperExpress;


public class InstallDbExpress extends Activity   {
	EditText mName;
	EditText mEmail;
	EditText mfeedback;
	Button mSend;
	protected int _splashTime = 2000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	 super.onCreate(savedInstanceState);
    	
    	 setContentView(R.layout.installdbexpress);
    	 DataBaseHelperExpress myDbHelperEx;
	        myDbHelperEx = new DataBaseHelperExpress(getApplicationContext());
	       
	        	
	        try {
	         
	        	myDbHelperEx.updatecreateDataBase();
	        
	        
	         
	        } catch (IOException ioe) {
	         
	        throw new Error("Unable to create database");
	         
	        
	        
	        }
	        
	        Handler handler = new Handler();
	        handler.postDelayed(new Runnable() {
	            public void run() {
	                
	                Intent i3 = new Intent(InstallDbExpress.this, LocalTrain.class);
					i3.putExtra("callfrom","other");
					startActivity(i3);
					finish();
	            }
	        }, _splashTime);
    }
}