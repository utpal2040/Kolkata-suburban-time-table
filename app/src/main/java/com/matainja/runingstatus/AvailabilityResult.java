
package com.matainja.runingstatus;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.matainja.runingstatus.Adapter.AvailableAdapter;
import com.matainja.runingstatus.Common.ConnectionDetector;
import com.matainja.runingstatus.Connectivity.HttpConnection;
import com.matainja.runingstatus.GoogleAnalyticsApp.TrackerName;
import com.matainja.runingstatus.Model.AvailableJson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;




public class AvailabilityResult extends AppCompatActivity {
	
	AvailabilityResult activity;
	
	 String mClass="";
	
	 String mTicket="";
	
	 ConnectionDetector cd;
	 TextView tno;
	 Bundle extras;
	 String trainName;
	 String fromstation;
	 String tostation;
	 int trainId;
	 String trainNo,mjourneydate,jsonerror,ItrainName;
	 Button av_get;
	TextView Ltrainname,tclass,stationname,tquote;
	ListView lv;
	 ArrayList<AvailableJson> availableList;
	 int currentapiVersion=0;
	Toolbar toolbar;
	SharedPreferences sp;
	String erro_msg = "Something is wrong!! server not connected";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	//Remove title bar
        //this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.availableresult);
		overridePendingTransition(R.anim.anim_slide_in_left,
				R.anim.anim_slide_out_left);
		toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
				overridePendingTransition(R.anim.anim_slide_in_right,
						R.anim.anim_slide_out_right);
			}
		});

		sp = getSharedPreferences("themeColor", Context.MODE_PRIVATE);

		/*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			Window window = getWindow();
			window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
			window.setStatusBarColor(getResources().getColor(R.color.colorExpress));
		}*/

        Ltrainname = (TextView)findViewById(R.id.Ltrainname);
        tno = (TextView)findViewById(R.id.tno);
        stationname  = (TextView)findViewById(R.id.stationname);
        cd = new ConnectionDetector(getApplicationContext());
        tclass = (TextView)findViewById(R.id.tclass);
        tquote = (TextView)findViewById(R.id.tquote);
        activity = this;
        lv = (ListView)findViewById(R.id.list);


		if(sp.getInt("expressColor",0)!=0) {
			toolbar.setBackgroundColor(sp.getInt("expressColor",0));

			int[] demo_colors = getResources().getIntArray(R.array.demo_colors);
			int[] demo_colors_mat = getResources().getIntArray(R.array.demo_colors_mat);

			ArrayList<String> colorString = new ArrayList<String>();
			ArrayList<String> colorString_mat = new ArrayList<String>();

			for (int i = 0; i < demo_colors.length; i++) {

				colorString.add(String.valueOf(demo_colors[i]));

			}

			for (int i = 0; i < demo_colors_mat.length; i++) {

				colorString_mat.add(String.valueOf(demo_colors_mat[i]));

			}
			Log.e("position ",colorString.indexOf(String.valueOf(sp.getInt("expressColor",0))) + "");
			int color_mat_val = Integer.parseInt(colorString_mat.get(colorString.indexOf(String.valueOf(sp.getInt("expressColor", 0)))));


			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
				Window window = getWindow();
				window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
				window.setStatusBarColor(color_mat_val);
			}
		}else{
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
				Window window = getWindow();
				window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
				window.setStatusBarColor(getResources().getColor(R.color.colorExpress));
			}
		}
         extras = getIntent().getExtras();
	      if(extras !=null) {

	    	 /* intent.putExtra("mjourneydate", mjourneydate);
  			intent.putExtra("mClass", mClass);
  			intent.putExtra("fromstation", fromstation);
  			intent.putExtra("mTicket", mTicket);
  			intent.putExtra("trainNo", trainNo);
  			intent.putExtra("tostation", tostation);
              startActivity(intent);*/
	    	  mjourneydate =extras.getString("mjourneydate");
	    	  mClass = extras.getString("mClass");
	    	  fromstation =extras.getString("fromstation");
	    	  mTicket = extras.getString("mTicket");
	    	  trainNo = extras.getString("trainNo");
	    	  tostation = extras.getString("tostation");
			  ItrainName = extras.getString("trainName");

			  Ltrainname.setText(ItrainName);
			  tno.setText(trainNo);
			  stationname.setText(fromstation+" To "+ tostation);

	    	  if (cd.isConnectingToInternet()) {

	    		  new GetAvailable().execute();

		      }
	    	  else
	    	  {
	    		  String msglert="Please Check your Network Connection !!";
		    	  NotificationAlert(msglert);

	    	  }

			}
	      else
	      {
	    	  String msglert="Sorry!! not valid input";
	    	  NotificationAlert(msglert);
	      }


	      if (!cd.isConnectingToInternet()) {

	    	  String msglert="Please Check your Network Connection !!";
	    	  NotificationAlert(msglert);
	      }

	      /*ImageView  	menuback =(ImageView)findViewById(R.id.backbutton);
	         menuback.setOnClickListener(new View.OnClickListener() {

	             @Override
	             public void onClick(View v) {
	                 // TODO Auto-generated method stub
	            	// overridePendingTransition(R.anim.lefttoright, R.anim.righttoleft);
		             	finish();
		             	// overridePendingTransition(R.anim.lefttoright, R.anim.righttoleft);
	             }
	         });
		 */

	         Handler handler = new Handler();
		        Runnable r = new Runnable() {
		            public void run() {
		            	 /* TRacking Code */
		                currentapiVersion = android.os.Build.VERSION.SDK_INT;
		                if(currentapiVersion>=android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH)
		                {
		                	/* TRacking Code */

		      		      Tracker t = ((GoogleAnalyticsApp) getApplication()).getTracker(TrackerName.APP_TRACKER);
		      				t.setScreenName(trainNo+"- Details Localtrain");
		      				t.send(new HitBuilders.AppViewBuilder().build());
		        			AdView mAdView = (AdView) findViewById(R.id.adView);
		        	        AdRequest adRequest = new AdRequest.Builder().build();
		        	        mAdView.loadAd(adRequest);
		                }
		            }
		        };
		        handler.postDelayed(r, 200);



    	}










    private class GetAvailable extends AsyncTask<String, String, JSONObject> {
        private ProgressDialog pDialog;
       @Override
       protected void onPreExecute() {
           super.onPreExecute();

           pDialog = new ProgressDialog(activity);
           pDialog.setMessage("Please Wait ...");
           pDialog.setIndeterminate(false);
           pDialog.setCancelable(true);
           pDialog.show();
       }
       @Override
       protected JSONObject doInBackground(String... args) {
       	HttpConnection searchFunctions = new HttpConnection();
	        JSONObject json = searchFunctions.getTrainAvailability(mjourneydate, mClass, fromstation, mTicket, trainNo, tostation);

	            return json;
       }
        @Override
        protected void onPostExecute(JSONObject json) {
        	availableList = new ArrayList<AvailableJson>();
            pDialog.dismiss();

//            Log.e("json", json.toString());
            jsonerror ="Something is wrong!! server not connected";



         if(json !=null)
         {



        	 if(json.has("ResponseCode"))
             {
        		 try {

        		 //String error = json.getString("error");
        		 String responsecode = json.getString("ResponseCode");
 				if( responsecode.equals("200") )
     			{

						//JSONObject Javailabl = json.getJSONObject("availability");
 					    JSONObject Javailabl = json;
						String jtranname = Javailabl.getString("TrainName");
						String jtrain_number = Javailabl.getString("TrainNumber");
					     Ltrainname.setText(jtranname);
					     tno.setText(jtrain_number);

						JSONObject Jmfrom = Javailabl.getJSONObject("From");
						String jfrom=Jmfrom.getString("Name");
						JSONObject Jmto = Javailabl.getJSONObject("To");
						String jto=Jmto.getString("Name");

						JSONObject Jmclass = Javailabl.getJSONObject("Class");
						String jmtclass=Jmclass.getString("Name");
						tclass.setText(jmtclass);
						JSONObject Jmquota = Javailabl.getJSONObject("Quota");
						String jmvtquote=Jmquota.getString("Name");

						tquote.setText(jmvtquote);

						 JSONArray Aresult = null;

						 Aresult = Javailabl.getJSONArray("Availabile");

					Log.e("JsonAva",Aresult.toString());
						 for(int i = 0; i < Aresult.length(); i++){
			                    JSONObject ca = Aresult.getJSONObject(i);

			                    String rjdate=ca.getString("Date");
			                    String rjstatus = ca.getString("Status");
			                    availableList.add(new AvailableJson(rjdate, rjstatus));
						 }

						 AvailableAdapter adapter = new AvailableAdapter(getApplicationContext(), availableList);
						 lv.setAdapter(adapter);

        		 }
 				else {
					if (json.has("Message")) {
						erro_msg = json.getString("Message");
						errorBox();
					}
				}
        		 } catch (JSONException e) {


					}

        	 }
        	 else
             {

        		 try {

					 if(json.has("Message")) {
						 erro_msg = json.getString("Message");
						 errorBox();
					 }

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
             }

        	 //Log.e("json", json.toString());
         }
         else
       	  errorBox();


		  Log.v("Data status3", "p=sshhh");
		  // Getting JSON Array from URL



        }
   }

    public void  errorBox()
	  {
		  Log.v("status", "no");
  	// Internet Connection is not present
	  AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);

			// set title
			alertDialogBuilder.setTitle("Error");

			// set dialog message
			alertDialogBuilder
				.setMessage(erro_msg)
				.setCancelable(false)
				.setPositiveButton("OK",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						// if this button is clicked, close
						// current activity
						//MainActivity.this.finish();
						finish();

					}
				  });


				// create alert dialog
				AlertDialog alertDialog = alertDialogBuilder.create();

				// show it
		  try {
			  alertDialog.show();
		  }catch(Exception e){

		  }

	  }
    // add items into spinner dynamically


    public void NotificationAlert(String Message)
    {

  	  AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(
 		        AvailabilityResult.this);

 		// Setting Dialog Title
 		alertDialog2.setTitle("Message");

 		// Setting Dialog Message
 		alertDialog2.setMessage(Message);

 		// Setting Icon to Dialog


 		// Setting Positive "Yes" Btn
 		alertDialog2.setNeutralButton("OK",
 		        new DialogInterface.OnClickListener() {
 		            public void onClick(DialogInterface dialog, int which) {
 		                // Write your code here to execute after dialog


 	                 finish();


 		            }
 		        });
 		alertDialog2.show();

    }





    @Override
    public void onBackPressed() {
        super.onBackPressed();

     	finish();
		overridePendingTransition(R.anim.anim_slide_in_right,
				R.anim.anim_slide_out_right);
    }


    @Override
    public void onDestroy()
    {



		 super.onDestroy();

    }

    @Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();

	}


	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();

	}
}