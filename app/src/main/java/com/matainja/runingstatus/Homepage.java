package com.matainja.runingstatus;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.matainja.runingstatus.Adapter.DateListAdapter;
import com.matainja.runingstatus.Adapter.ExpressLiveCustomAdapter;
import com.matainja.runingstatus.Adapter.TrainListAdapter;
import com.matainja.runingstatus.Database.DatabaseAdapterEx;
import com.matainja.runingstatus.GoogleAnalyticsApp.TrackerName;
import com.matainja.runingstatus.Model.DateList;
import com.matainja.runingstatus.Model.Train;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import de.hdodenhof.circleimageview.CircleImageView;


public class Homepage extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

	ArrayList<Train> listOfTrain;
	//ArrayList<TimeList> TimelistArr;
	Cursor traincursor;
	TextView trainnotxt;
	TextView date_dropdown_val;
	TextView date_dropdown_val_hid;
	TextView  dayofjourney ;
	private Spinner spinner2;
	Button runningstatus;
	//EditText trainnoinput;
    Context context;
	ImageView searchBtn;
	ImageView menuback;
	ImageView selectdate;
	ImageView leftmenu;
	ListView lv;
	ListView leftmenulist;
	private EditText trainlist;
	private DatabaseAdapterEx dbHelper;
	private EditText searchtrainbox;
	LinearLayout mainView_trainlist;
	LinearLayout menu_layout_timelist;
	View convertView;
	//ArrayList<DateList>  listOfDateList;
	LinearLayout journey_date_select;
	TextView label_train_date;
	TextView label_train_no;
	int currentapiVersion=0;
    SharedPreferences sp;
	private TabLayout tabLayout;
	private ViewPager viewPager;
	private AppBarLayout appbar;
    DrawerLayout drawer;
    ArrayList<DateList>  listOfDateList;
    RelativeLayout parentR;
    ImageView searchImg,searchImg2;
    TextView date_val_dropdown,date_val_dropdown_hid;
    AutoCompleteTextView trainnoinput;
    FrameLayout frame1;
    ExpressLiveCustomAdapter adapter;
    ListView  dateListView;
    Button bttntimetable;
    CircleImageView profile_image;
    TextView nameTxt;
    RelativeLayout headerFrame;
    String email;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	//Remove title bar

        super.onCreate(savedInstanceState);
        setContentView(R.layout.homepage);
        overridePendingTransition(R.anim.anim_slide_in_left,
                R.anim.anim_slide_out_left);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        currentapiVersion = android.os.Build.VERSION.SDK_INT;
        // view width calculate
      
     // Database Connection Open
        dbHelper = new DatabaseAdapterEx(this);
        dbHelper.open();
	      
	      // drawer define
	      /* TRacking Code */
        sp = getSharedPreferences("themeColor", Context.MODE_PRIVATE);
        if(currentapiVersion>=android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH)
        {
          Tracker t = ((GoogleAnalyticsApp) getApplication()).getTracker(TrackerName.APP_TRACKER);
            t.setScreenName("Running Homepage");
            t.send(new HitBuilders.AppViewBuilder().build());
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AdView mAdView = (AdView) findViewById(R.id.adView);
                AdRequest adRequest = new AdRequest.Builder().build();
                mAdView.loadAd(adRequest);
            }
        });

        searchImg = (ImageView) findViewById(R.id.searchImg);
        searchImg2 = (ImageView) findViewById(R.id.searchImg2);
        trainnoinput = (AutoCompleteTextView) findViewById(R.id.trainnoinput);
        date_val_dropdown = (TextView) findViewById(R.id.date_val_dropdown);
        frame1 = (FrameLayout) findViewById(R.id.frame1);
        searchtrainbox = (EditText) findViewById(R.id.searchtrainbox);
        dateListView = (ListView)findViewById(R.id.DateList);
        lv = (ListView)findViewById(R.id.trainlist);
        date_dropdown_val_hid = (TextView) findViewById(R.id.date_val_dropdown_hid);

        bttntimetable = (Button) findViewById(R.id.bttntimetable);

        bttntimetable.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                GetRunningStatus();
            }
        });
        searchImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*((LocalTrain)getActivity()).getLayoutHide();
                Constants.isSelectedFrom = true;
                LocalTrain.hideSoftKeyboard(Homepage.this);*/

                drawer.openDrawer(GravityCompat.END);
                frame1.setVisibility(View.VISIBLE);
                hideSoftKeyboard(Homepage.this);
                lv.setVisibility(View.VISIBLE);
                dateListView.setVisibility(View.GONE);
                SearchTrain("");
            }
        });
        searchImg2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              /*  ((LocalTrain)getActivity()).getLayoutHide();
                Constants.isSelectedto = true;
                LocalTrain.hideSoftKeyboard(Homepage.this);*/
                drawer.openDrawer(GravityCompat.END);
                frame1.setVisibility(View.GONE);
                hideSoftKeyboard(Homepage.this);
                dateListView.setVisibility(View.VISIBLE);
                lv.setVisibility(View.GONE);
                addItemsOnSpinner2();
            }
        });

        date_val_dropdown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              /*  ((LocalTrain)getActivity()).getLayoutHide();
                Constants.isSelectedto = true;
                LocalTrain.hideSoftKeyboard(Homepage.this);*/
                drawer.openDrawer(GravityCompat.END);
                frame1.setVisibility(View.GONE);
                hideSoftKeyboard(Homepage.this);
                dateListView.setVisibility(View.VISIBLE);
                lv.setVisibility(View.GONE);
                addItemsOnSpinner2();
            }
        });

        searchtrainbox.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //here is your code

                if(searchtrainbox.getText().length()== 0)
                {
                    SearchTrain("");
                }
                else
                {
                    String keywords = searchtrainbox.getText().toString();
                    SearchTrain(keywords);
                }


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });

        trainnoinput.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //here is your code

                if(trainnoinput.getText().length()== 0)
                {
                    TrainSuggestion("");
                }
                else
                {
                    String keywords = trainnoinput.getText().toString();
                    TrainSuggestion(keywords);
                }


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });

        appbar = (AppBarLayout) findViewById(R.id.appbar);
        parentR = (RelativeLayout) findViewById(R.id.parentR);
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorExpressStatus));
        }*/

        if(sp.getInt("expressColor",0)!=0) {
            toolbar.setBackgroundColor(sp.getInt("expressColor",0));





            parentR.setBackgroundColor(sp.getInt("expressColor",0));
            bttntimetable.setTextColor(sp.getInt("expressColor",0));
            //bttntimetable.setBackgroundColor(sp.getInt("expressColor",0));


            int[] demo_colors = getResources().getIntArray(R.array.demo_colors);
            int[] demo_colors_mat = getResources().getIntArray(R.array.demo_colors_mat);

            ArrayList<String> colorString = new ArrayList<String>();
            ArrayList<String> colorString_mat = new ArrayList<String>();

            for (int i = 0; i < demo_colors.length; i++) {

                colorString.add(String.valueOf(demo_colors[i]));

            }

            for (int i = 0; i < demo_colors_mat.length; i++) {

                colorString_mat.add(String.valueOf(demo_colors_mat[i]));

            }
            int color_mat_val = Integer.parseInt(colorString_mat.get(colorString.indexOf(String.valueOf(sp.getInt("expressColor", 0)))));


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(color_mat_val);
            }

            trainnoinput.setBackgroundColor(color_mat_val);
            date_val_dropdown.setBackgroundColor(color_mat_val);

        }
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        NavigationView search_view = (NavigationView) findViewById(R.id.search_view);
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, findViewById(R.id.search_view));
        View headerview = navigationView.getHeaderView(0);
        profile_image = (CircleImageView) headerview.findViewById(R.id.profile_image);
        nameTxt = (TextView) headerview.findViewById(R.id.name);
        headerFrame = (RelativeLayout) headerview.findViewById(R.id.headerFrame);
        headerFrame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(email.equalsIgnoreCase("")){
                    Intent i = new Intent(Homepage.this, WelcomeActivity.class);
                    startActivity(i);
                    finish();
                }else {
                    Intent i = new Intent(Homepage.this, SettingsActivity.class);
                    startActivity(i);
                }
            }
        });

        ApplyUserInfo();
    }

    public void ApplyUserInfo(){
        SharedPreferences userPref = getSharedPreferences("userCred",MODE_PRIVATE);
        String name = userPref.getString("name","Guest User");
        email = userPref.getString("email","");
        String imageUrl = userPref.getString("image_value","");

        Log.e("imageUrl ",imageUrl);
        nameTxt.setText(name);
        if(!imageUrl.equalsIgnoreCase("")) {
            Picasso.with(context).load(imageUrl).memoryPolicy(MemoryPolicy.NO_CACHE).into(profile_image);
        }
    }
    @Override
    public void onDestroy()
    {
        super.onDestroy();
        dbHelper.close();
       // traincursor.close();
    }

    public void addItemsOnSpinner2() {
        // assign empty date List object array
        listOfDateList = new ArrayList<DateList>();
        // today date

        /// Listview for train

        SimpleDateFormat sdf = new SimpleDateFormat("d  MMMM, yyyy");
        SimpleDateFormat valdf = new SimpleDateFormat("yyyyMMd");

        Calendar cal=Calendar.getInstance();
        int currentDay=cal.get(Calendar.DAY_OF_MONTH);
        // today
        String currentDateandTime = sdf.format(cal.getTime());
        listOfDateList.add(new DateList("today",currentDateandTime + "  (Today)"));
        //yestraday
        cal.set(Calendar.DAY_OF_MONTH, currentDay-1);

        String yesterday = sdf.format(cal.getTime());

        listOfDateList.add(new DateList("yesterday",yesterday + " (Yesterday)"));
        // tomorrow
        cal.set(Calendar.DAY_OF_MONTH, currentDay+1);

        String tomorrow = sdf.format(cal.getTime());
        listOfDateList.add(new DateList("tomorrow",tomorrow + "  (Tomorrow)"));
        // 2 days ago
        cal.set(Calendar.DAY_OF_MONTH, currentDay-2);

        String days2ago = sdf.format(cal.getTime());
        String days2ago_val = valdf.format(cal.getTime());
        listOfDateList.add(new DateList(days2ago_val,days2ago));
        // 3 days ago
        cal.set(Calendar.DAY_OF_MONTH, currentDay-3);

        String days3ago = sdf.format(cal.getTime());
        String days3ago_val = valdf.format(cal.getTime());
        listOfDateList.add(new DateList(days3ago_val,days3ago));

        DateListAdapter adapter = new DateListAdapter(this, listOfDateList);

        dateListView.setAdapter(adapter);

        dateListView.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // When clicked, show a toast with the TextView text
                final DateList entry = listOfDateList.get(position);
                String dateLabel = entry.getTextLabel();
                String datelabel_val = entry.getTextValue();

                date_val_dropdown.setText(dateLabel);
                date_dropdown_val_hid.setText(datelabel_val);
                drawer.closeDrawer(GravityCompat.END);

            }
        });


    }

    private void SearchTrain(String searchQuery)
    {
        /// Listview for train

        if(searchQuery.length()>0)
        {
            traincursor = dbHelper.getAllTrainSearch(searchQuery);
        }
        else
        {
            traincursor = dbHelper.getAllTrain();
        }


        listOfTrain = new ArrayList<Train>();

        if(traincursor.getCount()>0)
        {
            while (traincursor.moveToNext()) {



                int trainId = traincursor.getInt(traincursor.getColumnIndexOrThrow("_id"));
                String train_no = traincursor.getString(traincursor.getColumnIndexOrThrow("trainNO"));

                String train_name = traincursor.getString(traincursor.getColumnIndexOrThrow("trainName"));

                String start_station = "";
                String end_station = "";


                listOfTrain.add(new Train(train_no,train_name,start_station,end_station));
            }
        }
        traincursor.close();
        TrainListAdapter adapter = new TrainListAdapter(this, listOfTrain);
   	    lv.setAdapter(adapter);

        lv.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // When clicked, show a toast with the TextView text
                final Train entry = listOfTrain.get(position);
                String train_no = entry.getTrainNo();
                String train_no_val = train_no.toString();

                trainnoinput.setText(train_no_val);
                drawer.closeDrawer(GravityCompat.END);
                hideSoftKeyboard(Homepage.this);
            }
        });

    }

    private void TrainSuggestion(String searchQuery)
    {
        /// Listview for train
        lv = (ListView)findViewById(R.id.trainlist);
        if(searchQuery.length()>0)
        {
            traincursor = dbHelper.getAllTrainSearch(searchQuery);
        }
        else
        {
            traincursor = dbHelper.getAllTrain();
        }


        listOfTrain = new ArrayList<Train>();

        if(traincursor.getCount()>0)
        {
            while (traincursor.moveToNext()) {



                int trainId = traincursor.getInt(traincursor.getColumnIndexOrThrow("_id"));
                String train_no = traincursor.getString(traincursor.getColumnIndexOrThrow("trainNO"));

                String train_name = traincursor.getString(traincursor.getColumnIndexOrThrow("trainName"));

                String start_station = "";
                String end_station = "";


                listOfTrain.add(new Train(train_no,train_name,start_station,end_station));
                adapter = new ExpressLiveCustomAdapter(Homepage.this,R.layout.simple_list_item,listOfTrain);
            }
        }
        traincursor.close();
        trainnoinput.setAdapter(adapter);

    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
    }

	@Override
	public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the camera action
            Intent intent = new Intent(getApplicationContext(), LocalTrain.class);
            intent.putExtra("callfrom","other");
            startActivity(intent);
            finish();

        } else if (id == R.id.nav_runningstatus) {



        } else if (id == R.id.nav_cancelled) {

            Intent intent = new Intent(getApplicationContext(), ActivityCancelTrain.class);
            startActivity(intent);
            finish();

        }  else if (id == R.id.nav_pnrstatus) {

            Intent intent = new Intent(getApplicationContext(), Pnr.class);

            startActivity(intent);
            finish();

        } else if (id == R.id.nav_saveroute) {

            Intent intent = new Intent(getApplicationContext(), SaveSearch.class);
            startActivity(intent);
            finish();

        } else if (id == R.id.nav_feedback) {
            Intent intent = new Intent(getApplicationContext(), feedback.class);

            startActivity(intent);
            finish();

        } else if (id == R.id.nav_fblike) {

            Intent notificationIntent =null;
            notificationIntent = new Intent(Intent.ACTION_VIEW);
            notificationIntent.setData(Uri.parse("https://www.facebook.com/localtimetable"));
            startActivity(notificationIntent);


        } else if (id == R.id.nav_theme) {

            if(email.equalsIgnoreCase("")){
                Intent i = new Intent(Homepage.this, WelcomeActivity.class);
                startActivity(i);
                finish();
            }else {
                Intent i = new Intent(Homepage.this, SettingsActivity.class);
                startActivity(i);
            }


        } else if (id == R.id.nav_rate) {

            Intent notificationIntent =null;
            notificationIntent = new Intent(Intent.ACTION_VIEW);
            notificationIntent.setData(Uri.parse("market://details?id="+getApplicationContext().getPackageName()));
            startActivity(notificationIntent);


        } else if (id == R.id.nav_aboutus) {

            Intent intent = new Intent(getApplicationContext(), BuildWebsite.class);
            startActivity(intent);
            finish();
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
	}

    private void GetRunningStatus()
    {
        String daysofjourneyval="";
        if(trainnoinput.getText().length()== 0)
        {
            CharSequence text = "Please Enter Train No!";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(getApplicationContext(), text, duration);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
        else if(date_dropdown_val_hid.getText().length() == 0)
        {
            CharSequence text = "Please Select journey Date!";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(getApplicationContext(), text, duration);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
        else
        {

            String trainno = trainnoinput.getText().toString();
            daysofjourneyval = date_dropdown_val_hid.getText().toString();


            Intent intent = new Intent(getApplicationContext(), Running.class);

            intent.putExtra("trainno", trainno);

            intent.putExtra("dayofjourney", daysofjourneyval);
            intent.setAction(Intent.ACTION_VIEW);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
        }
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        Intent intent = new Intent(getApplicationContext(), LocalTrain.class);
        intent.putExtra("callfrom","other");
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.anim_slide_in_right,
                R.anim.anim_slide_out_right);
    }
}