
package com.matainja.runingstatus;

import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;

import java.io.IOException;


import android.content.Intent;


import android.widget.Button;
import android.widget.EditText;

import com.matainja.runingstatus.Database.DataBaseHelperFile;


public class InstallDbLocal extends Activity   {
	EditText mName;
	EditText mEmail;
	EditText mfeedback;
	Button mSend;
	protected int _splashTime = 2000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	 super.onCreate(savedInstanceState);
    	
    	 setContentView(R.layout.installlocaldb);

		 DataBaseHelperFile myDbHelper = new DataBaseHelperFile(null);
	        myDbHelper = new DataBaseHelperFile(getApplicationContext());
	      
	    	   
	        try {
	         
	        myDbHelper.updatecreateDataBase();
	        
	       
	         
	        } catch (IOException ioe) {
	         
	        throw new Error("Unable to create database");
	         
	        }
	        
		
	        
	        Handler handler = new Handler();
	        handler.postDelayed(new Runnable() {
	            public void run() {
	                
	                Intent i3 = new Intent(InstallDbLocal.this, InstallDbExpress.class);
	               
	                    startActivity(i3);
	                    finish();
	            }
	        }, _splashTime);
    }
}