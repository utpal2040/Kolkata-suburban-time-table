package com.matainja.runingstatus.Fragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import com.matainja.runingstatus.LocalTrain;

import java.util.ArrayList;

/**
 * Created by Matainja on 13-Feb-17.
 */
public class ViewPagerAdapter extends FragmentPagerAdapter {

    int mNumOfTabs;
    public ViewPagerAdapter(FragmentManager fm,  int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;

        ArrayList<Fragment> FRAGMENTS = new ArrayList<Fragment>();

        /*Fragment fragA = new LocalTrainFragment();
        LocalTrain.ISetTextInFragment setText = (LocalTrain.ISetTextInFragment) fragA;
        FRAGMENTS.add(fragA);*/
    }



    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:

                LocalTrainFragment tab1 = new LocalTrainFragment();
                Log.e("dfhd","0");
                return tab1;
            case 1:

                ExpressTrainFragment  tab2 = new ExpressTrainFragment();
                Log.e("dfhd","1");
                return tab2;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
