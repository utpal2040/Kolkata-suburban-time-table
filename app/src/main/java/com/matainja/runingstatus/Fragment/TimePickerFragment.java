package com.matainja.runingstatus.Fragment;

import android.app.TimePickerDialog;
import android.os.Bundle;
import android.app.Fragment;
import android.text.format.DateFormat;
import android.util.Log;
import android.widget.TextView;
import android.app.DialogFragment;
import android.app.Dialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.widget.TimePicker;
import com.matainja.runingstatus.R;

/**
 * Created by matainja on 21-Feb-17.
 */

public class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener{

    private int CalendarHour, CalendarMinute;
    String format;
    Calendar calendar;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){
        //Use the current time as the default values for the time picker
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);


        //Create and return a new instance of TimePickerDialog
        return new TimePickerDialog(getActivity(),this, hour, minute,
                DateFormat.is24HourFormat(getActivity()));
    }

    //onTimeSet() callback method
    public void onTimeSet(TimePicker view, int hourOfDay, int minute){
        //Do something with the user chosen time
        //Get reference of host activity (XML Layout File) TextView widget
        TextView tv = (TextView) getActivity().findViewById(R.id.timehour);
        TextView tv1 = (TextView) getActivity().findViewById(R.id.timehour24);
        //Set a message for user
        //tv.setText("Your chosen time is...\n\n");
        //Display the user changed time on TextView
        tv1.setText(hourOfDay+" : "+minute);
        Log.e("hour24 dhfsd==",""+hourOfDay+" : "+minute);
        //tv1.setText();
        if (hourOfDay == 0) {

            hourOfDay += 12;

            format = "AM";
        }
        else if (hourOfDay == 12) {

            format = "PM";

        }
        else if (hourOfDay > 12) {

            hourOfDay -= 12;

            format = "PM";

        }
        else {

            format = "AM";
        }



        tv.setText(hourOfDay + ":" + minute + format);

        /*tv.setText(String.valueOf(hourOfDay)
                + ":" + String.valueOf(minute) + "\n");*/
    }
}
