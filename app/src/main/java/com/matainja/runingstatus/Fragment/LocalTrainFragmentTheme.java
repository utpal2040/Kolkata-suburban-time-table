package com.matainja.runingstatus.Fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.matainja.runingstatus.R;
import com.thebluealliance.spectrum.SpectrumPalette;

/**
 * Created by matainja on 13-Nov-17.
 */

public class LocalTrainFragmentTheme extends Fragment implements SpectrumPalette.OnColorSelectedListener{

    View fragmentView = null;
    SharedPreferences sp;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        fragmentView = inflater.inflate(R.layout.layout_localtrainfragment_theme, null);

        SpectrumPalette spectrumPalette = (SpectrumPalette)fragmentView.findViewById(R.id.palette);
        spectrumPalette.setOnColorSelectedListener(this);

        sp = getActivity().getSharedPreferences("themeColor", Context.MODE_PRIVATE);

        spectrumPalette.setSelectedColor(sp.getInt("localColor",0));
        return fragmentView;
    }


    @Override
    public void onColorSelected(@ColorInt int color) {
        //Toast.makeText(getActivity(), "Color selected: #" + Integer.toHexString(color).toUpperCase(), Toast.LENGTH_SHORT).show();

        //SettingsActivity.toolbar.setBackgroundColor(color);
        //SettingsActivity.tabLayout.setBackgroundColor(color);

        SharedPreferences.Editor editor = sp.edit();
        editor.putInt("localColor", color);
        editor.commit();
    }
}
