package com.matainja.runingstatus.Fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.matainja.runingstatus.Adapter.CustomerAdapter;
import com.matainja.runingstatus.Adapter.CustomerAdapterTrainNo;
import com.matainja.runingstatus.Adapter.RecentSearchAdapter;
import com.matainja.runingstatus.Common.Constants;
import com.matainja.runingstatus.Database.DatabaseAdapterEx;
import com.matainja.runingstatus.ExTimeTableDetails;
import com.matainja.runingstatus.ExTrainTimetable;
import com.matainja.runingstatus.LocalTrain;
import com.matainja.runingstatus.Model.RecentSearch;
import com.matainja.runingstatus.Model.StationLocal;
import com.matainja.runingstatus.Model.Train;
import com.matainja.runingstatus.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by matainja on 13-Feb-17.
 */

public class ExpressTrainFragment extends Fragment implements View.OnClickListener{

    View fragmentView = null;
    CheckBox Radio1;
    ListView lastsearchlist;
    public static Button bttntimetable;
    public static AutoCompleteTextView fromstationcode,tostationcode,traincode;
    ImageView updownImg,crossImg,clock,searchImg,searchImg2;
    String tostationcodeValue,fromstationcodeValue,fieldTo,fieldFrom;
    private int mYear, mMonth, mDay, mHour, mMinute;
    TextView timehour;
    public static RelativeLayout parentR,PrimaryRLayout;
    Context context;
    SimpleDateFormat timeFormatter;
    private DatabaseAdapterEx dbHelper;
    Cursor savestation;
    String fromsaveStation = null;
    String tosaveStation = null;
    ArrayList<String> stations = new ArrayList<String>();
    String recentsearchjson;
    public ArrayList<RecentSearch> recentSearchList;
    SharedPreferences prefs;
    SharedPreferences.Editor editorUser;
    ArrayList<RecentSearch> recentArrayList;
    Cursor stationCursor,stationCursorByTrainNO, stationCursorByTrainID;
    ArrayList<StationLocal> listOfStation;
    ArrayList<Train> listOfStationByTainNo;
    CustomerAdapter adapter;
    CustomerAdapterTrainNo adapter1;
    RecentSearchAdapter RSA;
    public static TextView recentsearchtext;
    SharedPreferences sp;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        fragmentView = inflater.inflate(R.layout.layout_expresstrainfragment, null);

        //Toast.makeText(getContext(),"Express",Toast.LENGTH_SHORT).show();


        recentSearchList = new ArrayList<RecentSearch>();
        prefs = getActivity().getSharedPreferences(Constants.USER_SHARED_PREF_VAR, Context.MODE_PRIVATE);
        editorUser = prefs.edit();
        recentArrayList = new ArrayList<RecentSearch>();

        /*Type type = new TypeToken<ArrayList<ArrayObject>>() {}.getType();
        ArrayList<ArrayObject> arrayList = gson.fromJson(json, type);*/

        //FragmentManager fm = getActivity().getFragmentManager();
        parentR = (RelativeLayout) fragmentView.findViewById(R.id.parentR);
        PrimaryRLayout  = (RelativeLayout) fragmentView.findViewById(R.id.PrimaryRLayout);
        //Radio1 = (CheckBox) fragmentView.findViewById(R.id.Radio1);
        lastsearchlist = (ListView) fragmentView.findViewById(R.id.lastsearchlist);
        bttntimetable = (Button) fragmentView.findViewById(R.id.bttntimetable);
        tostationcode = (AutoCompleteTextView) fragmentView.findViewById(R.id.tostationcode);
        fromstationcode = (AutoCompleteTextView) fragmentView.findViewById(R.id.fromstationcode);
        traincode = (AutoCompleteTextView) fragmentView.findViewById(R.id.traincode);
        crossImg = (ImageView) fragmentView.findViewById(R.id.crossImg);
        updownImg = (ImageView) fragmentView.findViewById(R.id.updownImg);
        //clock = (ImageView) fragmentView.findViewById(R.id.clock);
        //timehour = (TextView) fragmentView.findViewById(R.id.timehour);
        searchImg = (ImageView) fragmentView.findViewById(R.id.searchImg);
        searchImg2 = (ImageView) fragmentView.findViewById(R.id.searchImg2);
        recentsearchtext = (TextView) fragmentView.findViewById(R.id.recentsearchtext);


        sp = getActivity().getSharedPreferences("themeColor", Context.MODE_PRIVATE);


        // GET CURRENT TIME
        Calendar calander = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a");

        String time = simpleDateFormat.format(calander.getTime());

        /*Calendar now = Calendar.getInstance();
        String am_pm = "";
        int a = now.get(Calendar.AM_PM);

        if (now.get(Calendar.AM_PM) == Calendar.AM)
            am_pm = "AM";
        else if (now.get(Calendar.AM_PM) == Calendar.PM)
            am_pm = "PM";*/
        //Log.e("format",""+now.get(Calendar.HOUR)+":"+now.get(Calendar.MINUTE)+" "+am_pm);
        //timehour.setText(time);

        fieldTo = prefs.getString("ExTo", "null");
        fieldFrom = prefs.getString("ExFrom", "null");

        Log.e("fieldTo== fieldFrom",""+fieldTo+","+fieldFrom);
        if(!fieldTo.equals("null")){
            tostationcode.setText(fieldTo);
        }
        if(!fieldFrom.equals("null")){
            fromstationcode.setText(fieldFrom);
        }

        searchImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((LocalTrain)getActivity()).getLayoutHide();
                Constants.isSelectedFromEx = true;
                LocalTrain.hideSoftKeyboard(getActivity());
            }
        });
        searchImg2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((LocalTrain)getActivity()).getLayoutHide();
                Constants.isSelectedtoEx = true;
                LocalTrain.hideSoftKeyboard(getActivity());
            }
        });

        String[] languages={"Android ","java","IOS","SQL","JDBC","Web services"};


        bttntimetable.setOnClickListener(this);
        crossImg.setOnClickListener(this);
        updownImg.setOnClickListener(this);
        //clock.setOnClickListener(this);

        dbHelper = new DatabaseAdapterEx(getActivity());
        dbHelper.open();

        fromstationcode.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long rowId) {
                // String selection = (String)parent.getItemAtPosition(position);
                Log.e("SELECTION==",""+listOfStation.get(position).getStationCode());
                fromstationcode.setText(listOfStation.get(position).getStationCode()+" - "+listOfStation.get(position).getStationName());
                //TODO Do something with the selected text
            }
        });

        tostationcode.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long rowId) {
                // String selection = (String)parent.getItemAtPosition(position);
                Log.e("SELECTION==",""+listOfStation.get(position).getStationCode());
                tostationcode.setText(listOfStation.get(position).getStationCode()+" - "+listOfStation.get(position).getStationName());
                //TODO Do something with the selected text
            }
        });


        fromstationcode.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //here is your code

                if(fromstationcode.getText().length()== 0)
                {
                    SearchStation("");
                }
                else
                {
                    String keywords = fromstationcode.getText().toString();
                    SearchStation(keywords);
                }


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });

        tostationcode.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //here is your code

                if(tostationcode.getText().length()== 0)
                {
                    SearchStation("");
                }
                else
                {
                    String keywords = tostationcode.getText().toString();
                    SearchStation(keywords);
                }


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });

        traincode.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //here is your code

                if(traincode.getText().length()== 0)
                {
                    SearchStationByTrainNo("");
                }
                else
                {
                    String keywords = traincode.getText().toString();
                    SearchStationByTrainNo(keywords);
                }


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });

        savestation =dbHelper.getSaveStation();
        Log.e("savestation==",""+savestation.getCount());

        if(savestation.getCount()>0)
        {

            while (savestation.moveToNext()) {




                fromsaveStation = savestation.getString(savestation.getColumnIndexOrThrow("fromstation"));
                tosaveStation = savestation.getString(savestation.getColumnIndexOrThrow("tostation"));


            }
        }
        Log.e("STATIONS==",fromsaveStation+"----"+tosaveStation);

        savestation.close();

        Gson gson = new Gson();
        recentsearchjson = prefs.getString("recentsearchEx", "null");

        if(recentsearchjson.equals("null")){
            Log.e("Recent JSON==","NO VALUE SAVED");
        } else{
            Log.e("Recent JSON==",""+recentsearchjson);
            try {
                JSONArray recent_arr = new JSONArray(recentsearchjson);
                Log.e("recent_arr JSON==",""+recent_arr);

                int count=0;
                int arr_count = recent_arr.length();
                if(arr_count <4){
                    count = recent_arr.length();
                } else{
                    count = 4;
                }
                Log.e("COUNT==",""+count);
                for(int i=0;i<count;i++){
                    JSONObject obj = recent_arr.getJSONObject(i);
                    RecentSearch recentsearch = new RecentSearch();
                    recentsearch.setToStationName(obj.getString("ToStationName"));
                    recentsearch.setFromStationName(obj.getString("FromStationName"));

                    recentArrayList.add(recentsearch);
                }

                Log.e("recentArrayList===",""+recentArrayList.size());

                /*for(int k=0;k<recentArrayList.size();k++){
                    Log.e("KK==",""+recentArrayList.get(k).getFromStationName()+" "+recentArrayList.get(k).getToStationName());
                }*/
                recentsearchtext.setVisibility(View.VISIBLE);
                RSA = new RecentSearchAdapter(getActivity(),recentArrayList);
                lastsearchlist.setAdapter(RSA);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        lastsearchlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.e("code",recentArrayList.get(position).getFromStationName());

                String fromstation = recentArrayList.get(position).getFromStationName().toString();
                String tostation = recentArrayList.get(position).getToStationName().toString();
                //String time = timehour.getText().toString();

                Constants.EX_TOSTATIONCODE = tostation;
                Constants.EX_FROMSTATIONCODE = fromstation;

                String[] fromstationarr,tostationarr;
                String fromstationname = "", tostationname = "";

                if(fromstation.contains("-")) {
                    fromstationarr = fromstation.split("-");
                    fromstation = fromstationarr[0].trim().toUpperCase(); // this will contain "Fruit"
                    fromstationname = fromstationarr[1].trim().toUpperCase();
                }else{
                    fromstation = fromstation.toUpperCase();
                    fromstationname = fromstation;
                }

                if(tostation.contains("-")) {
                    tostationarr = tostation.split("-");
                    tostation = tostationarr[0].trim().toUpperCase();
                    tostationname = tostationarr[1].trim().toUpperCase();
                }else{
                    tostation = tostation.toUpperCase();
                    tostationname = tostation;
                }
                //Log.e("FROM--TO--",""+fromstation+" "+tostation);




                Intent intent = new Intent(getActivity(), ExTrainTimetable.class);
                intent.putExtra("fromstation", fromstation);
                intent.putExtra("timefilter", "");
                intent.putExtra("tostation", tostation);
                intent.putExtra("fromstationname", fromstationname);
                intent.putExtra("tostationname", tostationname);
                intent.setAction(Intent.ACTION_VIEW);
                startActivity(intent);

                //getActivity().finish();
            }
        });
        return fragmentView;
    }

    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        Log.e("CALLED","VISIBLE");
        if (visible) {
            // ...
            /*getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    SharedPreferences sp = getActivity().getSharedPreferences("themeColor", Context.MODE_PRIVATE);
                    if(sp.getInt("expresslColor",0)!=0) {

                        parentR.setBackgroundColor(sp.getInt("expresslColor",0));
                        bttntimetable.setTextColor(sp.getInt("expresslColor",0));
                        recentsearchtext.setTextColor(sp.getInt("expresslColor",0));

                        int[] demo_colors = getActivity().getResources().getIntArray(R.array.demo_colors);
                        int[] demo_colors_mat = getActivity().getResources().getIntArray(R.array.demo_colors_mat);

                        ArrayList<String> colorString = new ArrayList<String>();
                        ArrayList<String> colorString_mat = new ArrayList<String>();

                        for (int i = 0; i < demo_colors.length; i++) {
                            Log.e("Test", demo_colors[i] + "");
                            //Log.e("Test", demo_colors[i] + "");
                            colorString.add(String.valueOf(demo_colors[i]));

                        }

                        for (int i = 0; i < demo_colors_mat.length; i++) {
                            Log.e("Test", demo_colors[i] + "");
                            //Log.e("Test", demo_colors[i] + "");
                            colorString_mat.add(String.valueOf(demo_colors_mat[i]));

                        }
                        Log.e("position ",colorString.indexOf(String.valueOf(sp.getInt("expresslColor",0))) + "");
                        int color_mat_val = Integer.parseInt(colorString_mat.get(colorString.indexOf(String.valueOf(sp.getInt("expresslColor", 0)))));

                        Log.e("color_mat_val ",""+color_mat_val);
                        fromstationcode.setBackgroundColor(color_mat_val);
                        tostationcode.setBackgroundColor(color_mat_val);
                    }
                }
            });*/


        } else{
            Log.e("NOT","VISIBLE");
        }
    }
    @Override
    public void onResume(){
        super.onResume();
        // put your code here...
        try {
            lastsearchlist.setAdapter(RSA);
        }catch(NullPointerException ne){
            Log.e("ON RESUM==",""+ne);

        }
    }
    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.bttntimetable:
                String fromstation = null,tostation = null;
                if(traincode.getText().toString().isEmpty()) {
                    tostationcodeValue = tostationcode.getText().toString().replaceAll("\\s+$", "");
                    fromstationcodeValue = fromstationcode.getText().toString().replaceAll("\\s+$", "");

                    Log.e("To AnD FrOm==", tostationcodeValue + " == " + fromstationcodeValue);

                    if (!fromstationcodeValue.equals("")) {

                        if (!tostationcodeValue.equals("")) {

                            if (!fromstationcodeValue.equals(tostationcodeValue)) {

                                Constants.EX_TOSTATIONCODE = tostationcodeValue;
                                Constants.EX_FROMSTATIONCODE = fromstationcodeValue;

                                editorUser.putString("ExTo", tostationcodeValue);
                                editorUser.putString("ExFrom", fromstationcodeValue);
                                editorUser.commit();


                                fromstation = fromstationcode.getText().toString();
                                tostation = tostationcode.getText().toString();
                                //String time = timehour.getText().toString();

                                String fromstationname = "", tostationname = "";
                                String[] fromstationarr, tostationarr;

                                if (fromstation.contains("-")) {
                                    fromstationarr = fromstation.split("-");
                                    fromstation = fromstationarr[0].trim().toUpperCase(); // this will contain "Fruit"
                                    fromstationname = fromstationarr[1].trim().toUpperCase();
                                } else {
                                    //fromstationarr = fromstation.split("-");
                                    fromstation = fromstation.toUpperCase();
                                    fromstationname = fromstation;
                                }

                                if (tostation.contains("-")) {
                                    tostationarr = tostation.split("-");
                                    tostation = tostationarr[0].trim().toUpperCase();
                                    tostationname = tostationarr[1].trim().toUpperCase();
                                } else {
                                    //tostationarr = tostation.split("-");
                                    tostation = tostation.toUpperCase();
                                    tostationname = tostation;
                                }


                                Log.e("from station name==", fromstationname);
                                Intent intent = new Intent(getActivity(), ExTrainTimetable.class);
                                intent.putExtra("fromstation", fromstation);
                                //intent.putExtra("timefilter", time);
                                intent.putExtra("tostation", tostation);
                                intent.putExtra("fromstationname", fromstationname);
                                intent.putExtra("tostationname", tostationname);
                                intent.setAction(Intent.ACTION_VIEW);
                            /*if (Radio1.isChecked())
                                intent.putExtra("filterbytime", 1);
                            else
                                intent.putExtra("filterbytime", 0);*/

                                startActivity(intent);
                                //getActivity().finish();
                            } else {
                                ShowAlert("Please Select Differnt Source Or Destination");
                            }

                        } else {
                            ShowAlert(Constants.ERR_TOSTATION);
                        }

                    } else {
                        ShowAlert(Constants.ERR_FROMSTATION);
                    }

                }else{
                    Integer id = null;
                    String fromstationcode = null,tostationcode = null;

                    stationCursorByTrainNO = dbHelper.getTrainByNo(traincode.getText().toString().trim());

                    if (stationCursorByTrainNO.getCount() > 0) {

                        while (stationCursorByTrainNO.moveToNext()) {


                            id = stationCursorByTrainNO.getInt(stationCursorByTrainNO.getColumnIndexOrThrow("_id"));


                        }
                        stationCursorByTrainID = dbHelper.getTrainByID(id);
                        ArrayList<String> trainStationList = new ArrayList<String>();
                        while (stationCursorByTrainID.moveToNext()) {


                            String station  = stationCursorByTrainID.getString(stationCursorByTrainID.getColumnIndexOrThrow("stationName"));


                            trainStationList.add(station);

                        }

                        fromstationcode = trainStationList.get(0);
                        tostationcode = trainStationList.get(trainStationList.size()-1);
                    }

                    Intent intent = new Intent(getActivity(), ExTimeTableDetails.class);

                    intent.putExtra("trainId", id);

                    intent.putExtra("trainNo", traincode.getText().toString().trim());
                    intent.putExtra("fromstation", fromstationcode);
                    intent.putExtra("tostation", tostationcode);

                    startActivity(intent);
                    //getActivity().finish();

                }
                //Log.e("SAVED STATION ",recentSearchList.get(0).getFromStationName());

                break;

            case R.id.updownImg:
                tostationcodeValue = tostationcode.getText().toString();
                fromstationcodeValue = fromstationcode.getText().toString();

                tostationcode.setText(fromstationcodeValue);
                fromstationcode.setText(tostationcodeValue);
                break;

            case R.id.crossImg:
                tostationcode.setText("");
                fromstationcode.setText("");

                break;

            /*case R.id.clock:
                DialogFragment newFragment = new TimePickerFragment();
                newFragment.show(getActivity().getFragmentManager(),"TimePicker");
                break;*/
        }
    }


    private void SearchStation(String searchQuery)
    {
        /// Listview for train
        if (searchQuery.length() > 0) {
            stationCursor = dbHelper.getAllStation(searchQuery);
            //Toast.makeText(getActivity(), "== "+stationCursor.getCount(), Toast.LENGTH_SHORT).show();
            Log.e("IF","TRUE");
        } else {
            stationCursor = dbHelper.getAllStation(null);
            Log.e("ELSE","TRUE");
        }



        // Log.v("Station:name","SearchStation="+stationCursor.getCount());
        if (stationCursor.getCount() > 0) {
            listOfStation = new ArrayList<StationLocal>();
            while (stationCursor.moveToNext()) {


                Integer id = stationCursor.getInt(stationCursor.getColumnIndexOrThrow("_id"));
                Integer popularity = stationCursor.getInt(stationCursor.getColumnIndexOrThrow("popularity"));

                String stationCod = stationCursor.getString(stationCursor.getColumnIndexOrThrow("stationCode"));
                String stationName = stationCursor.getString(stationCursor.getColumnIndexOrThrow("stationName"));




                //Log.v("Station:name",stationName);
//new StationLocal(id, stationCod, stationName, popularity)
                listOfStation.add(new StationLocal(id, stationCod, stationName, popularity));
                //stationCursor.close();
                adapter = new CustomerAdapter(getActivity(),R.layout.simple_list_item,listOfStation);

            }
        }
        //Log.e("listOfStation COUNT===",""+listOfStation.size());

        fromstationcode.setAdapter(adapter);
        fromstationcode.setThreshold(1);

        tostationcode.setAdapter(adapter);
        tostationcode.setThreshold(1);
    }

    private void SearchStationByTrainNo(String searchQuery)
    {
        /// Listview for train
        if (searchQuery.length() > 0) {
            stationCursorByTrainNO = dbHelper.getTrainByNo(searchQuery);
            //Toast.makeText(getActivity(), "== "+stationCursor.getCount(), Toast.LENGTH_SHORT).show();
            Log.e("IF","TRUE "+stationCursorByTrainNO.getCount());
        } else {
            stationCursorByTrainNO = dbHelper.getTrainId(null);
            Log.e("ELSE","TRUE");
        }



        // Log.v("Station:name","SearchStation="+stationCursor.getCount());
        if (stationCursorByTrainNO.getCount() > 0) {
            listOfStationByTainNo = new ArrayList<Train>();
            while (stationCursorByTrainNO.moveToNext()) {


                Integer id = stationCursorByTrainNO.getInt(stationCursorByTrainNO.getColumnIndexOrThrow("_id"));
                String trainNo = stationCursorByTrainNO.getString(stationCursorByTrainNO.getColumnIndexOrThrow("trainNO"));
                String trainName = stationCursorByTrainNO.getString(stationCursorByTrainNO.getColumnIndexOrThrow("trainName"));

                /*String stationCod = stationCursorByTrainNO.getString(stationCursorByTrainNO.getColumnIndexOrThrow("stationCode"));
                String stationName = stationCursorByTrainNO.getString(stationCursorByTrainNO.getColumnIndexOrThrow*///("stationName"));




                //Log.v("Station:name",stationName);
//new StationLocal(id, stationCod, stationName, popularity)

                listOfStationByTainNo.add(new Train(trainNo, trainName, "", ""));
                //stationCursor.close();
                adapter1 = new CustomerAdapterTrainNo(getActivity(),R.layout.simple_list_item,listOfStationByTainNo);

            }
        }
        //Log.e("listOfStation COUNT===",""+listOfStation.size());

        traincode.setAdapter(adapter1);
        traincode.setThreshold(1);

    }

    public void ShowAlert(String msg){

        Snackbar snackbar = Snackbar.make(PrimaryRLayout, msg , Snackbar.LENGTH_LONG);
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        // snackbar.setDuration(2000);
        snackbar.show();

    }
}

