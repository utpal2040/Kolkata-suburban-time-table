package com.matainja.runingstatus.Fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.matainja.runingstatus.Adapter.ExpressTimeRecyclerAdapter;
import com.matainja.runingstatus.Adapter.TimeTableListAdapter;
import com.matainja.runingstatus.CustomView.ItemClickSupport;
import com.matainja.runingstatus.Database.DatabaseAdapterEx;
import com.matainja.runingstatus.ExTimeTableDetails;
import com.matainja.runingstatus.Model.TimetableListBean;
import com.matainja.runingstatus.R;
import com.matainja.runingstatus.TrainTimetable;
import com.tuyenmonkey.mkloader.MKLoader;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by matainja on 28-Feb-17.
 */

public class ExpressTrainListFragment extends Fragment {

    View fragmentView = null;
    String fromstationcode;
    String tostationcode;
    String timefilter;
    int filterbytime;
    private  DatabaseAdapterEx dbHelper;
    Cursor fromstation,traincursor;
    Cursor tostation;
    Integer validstationfrom=0;
    Integer validstationto=0;
    MKLoader progressBar1;
    //ListView lv;
    RecyclerView lv;
    ArrayList<TimetableListBean> listOftrain;
    int norecrod=0;
    TimeTableListAdapter adapter;
    /*SharedPreferences prefs;
    SharedPreferences.Editor editorUser;
    boolean flag = false;
    String fromcode, tocode;
    ArrayList<RecentSearch> recentArrayList;
    ArrayList<RecentSearch> recentSearchList;*/
    SharedPreferences sp;
    public ExpressTrainListFragment(){}
    @SuppressLint("ValidFragment")
    public ExpressTrainListFragment (String fromstationcode, String tostationcode, String timefilter, int filterbytime){
        this.fromstationcode = fromstationcode;
        this.tostationcode = tostationcode;
        this.timefilter = timefilter;
        this.filterbytime = filterbytime;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        fragmentView = inflater.inflate(R.layout.layout_expresstrainlistfragment, null);

        dbHelper = new DatabaseAdapterEx(getActivity());
        dbHelper.open();

        /*recentSearchList = new ArrayList<RecentSearch>();
        recentArrayList = new ArrayList<RecentSearch>();*/

        progressBar1 = (MKLoader) fragmentView.findViewById(R.id.progressBar1);
        //Toast.makeText(getActivity(), fromstationcode, Toast.LENGTH_SHORT).show();

       // lv = (ListView)fragmentView.findViewById(R.id.traintlist);
        lv = (RecyclerView) fragmentView.findViewById(R.id.traintlist);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        lv .setLayoutManager(mLayoutManager);
        lv.setNestedScrollingEnabled(false);
        listOftrain = new ArrayList<TimetableListBean>();
        //new LoadingTrain().execute();
        sp = getActivity().getSharedPreferences("themeColor", Context.MODE_PRIVATE);
        if(sp.getInt("expresslColor",0)!=0) {


        }

        fromstation =dbHelper.getStationName(fromstationcode);
        tostation =dbHelper.getStationName(tostationcode);
        String fromStationNAme="";
        String tostation_ = "";
        String fromStationCode ="";
        String toStationCode ="";

        if(fromstation.getCount()>0)
        {

            while (fromstation.moveToNext()) {


                validstationfrom=1;

                fromStationNAme = fromstation.getString(fromstation.getColumnIndexOrThrow("stationName"));
                fromStationCode = fromstation.getString(fromstation.getColumnIndexOrThrow("stationCode"));


            }
        }
        fromstation.close();

        if(tostation.getCount()>0)
        {

            while (tostation.moveToNext()) {


                validstationto=1;

                tostation_ = tostation.getString(tostation.getColumnIndexOrThrow("stationName"));
                toStationCode = tostation.getString(tostation.getColumnIndexOrThrow("stationCode"));



            }
        }
        tostation.close();

        return fragmentView;
    }

    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        if (visible) {
            // ...
            getActivity().runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    try {
                        TrainTimetable.totaltrain.setText(traincursor.getCount() + " Train(s)");
                    }catch(NullPointerException ne){

                    }
                    if(listOftrain.size() == 0) {
                        progressBar1.setVisibility(View.VISIBLE);
                        new LoadingTrain().execute();
                    }else{
                        Log.e("VISIBLE ELSE",""+listOftrain.size());
                    }
                    //NestedListView.total_list(traincursor.getCount());
                }
            });
        } else{
            Log.e("NOT","VISIBLE");
        }
    }

    private class LoadingTrain extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... arg0) {



            //Log.e("EXPRESS BACKGROUND","TRUE");


            traincursor = dbHelper.getTrainList(fromstationcode,tostationcode);


            if(traincursor.getCount()>0)
            {


                Log.e("called=**","TRUE");
                getActivity().runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        TrainTimetable.totaltrain.setText(traincursor.getCount()+" Train(s)");


                    }
                });
                while (traincursor.moveToNext()) {
                    String remarks ="";
                    Integer trainId = traincursor.getInt(traincursor.getColumnIndexOrThrow("trainId"));
                    String trainNo = traincursor.getString(traincursor.getColumnIndexOrThrow("trainNo"));


                    String trainName = traincursor.getString(traincursor.getColumnIndexOrThrow("trainName"));
                    String SourceStation  = traincursor.getString(traincursor.getColumnIndexOrThrow("SourceStation"));

                    String sourcestationcode  = traincursor.getString(traincursor.getColumnIndexOrThrow("sourcestationcode"));

                    String sourcearrival  = traincursor.getString(traincursor.getColumnIndexOrThrow("sourcearrival"));

                    String DestinationStation  = traincursor.getString(traincursor.getColumnIndexOrThrow("DestinationStation"));

                    String destStationcode  = traincursor.getString(traincursor.getColumnIndexOrThrow("destStationcode"));

                    String destarrival  = traincursor.getString(traincursor.getColumnIndexOrThrow("destarrival"));


                    Integer sun=0;
                    Integer mon=0;
                    Integer tue=0;
                    Integer wed=0;
                    Integer thu=0;
                    Integer fri=0;
                    Integer sat=0;
                    String available_text="";
                    Integer off_days=0;
                    Integer only_sat=0;


                    sun = traincursor.getInt(traincursor.getColumnIndexOrThrow("sun"));
                    mon = traincursor.getInt(traincursor.getColumnIndexOrThrow("mon"));
                    tue = traincursor.getInt(traincursor.getColumnIndexOrThrow("tue"));
                    wed = traincursor.getInt(traincursor.getColumnIndexOrThrow("wed"));
                    thu = traincursor.getInt(traincursor.getColumnIndexOrThrow("thu"));
                    fri = traincursor.getInt(traincursor.getColumnIndexOrThrow("fri"));
                    sat = traincursor.getInt(traincursor.getColumnIndexOrThrow("sat"));


                    //Log.v("Station:name",stationName);
                    int stops;
                    String takeTime="";
                    String src_time,dest_time;
                    Date src_localTime = null;
                    Date dest_localTime = null;
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                    String[] stopsArr = dbHelper.getStationStop(trainId,fromstationcode,tostationcode);

                    stops =Integer.parseInt(stopsArr[0]);

                    src_time = stopsArr[1];
                    dest_time = stopsArr[2];

                    try {
                        src_localTime = format.parse(src_time);
                        dest_localTime = format.parse(dest_time);

                        long diff = dest_localTime.getTime() - src_localTime.getTime();

                        long diffSeconds = diff / 1000 % 60;
                        long diffMinutes = diff / (60 * 1000) % 60;
                        long diffHours = diff / (60 * 60 * 1000) % 24;
                        long diffDays = diff / (24 * 60 * 60 * 1000);

                        Log.e(" days ",""+diffDays);
                        Log.e( " hours ",""+diffHours );
                        Log.e(" minutes ",""+diffMinutes);
                        Log.e(" seconds ",""+diffSeconds);

                        if(diffHours != 0 && diffMinutes != 0) {
                            takeTime = diffHours + " H : " + diffMinutes + " M";
                        }else{
                            if(diffHours == 0) {
                                takeTime = diffMinutes + " M";
                            }else if(diffMinutes == 0){
                                takeTime = diffHours + " H";
                            }
                        }
                          /*src_localTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).parse(src_time);
                          Log.e("SRC TimeStamp is ","" +src_localTime.getTime());

                          dest_localTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).parse(dest_time);
                          Log.e("DEST TimeStamp is ","" +dest_localTime.getTime());*/
                    } catch (java.text.ParseException e) {
                        Log.e("e","" +e);
                        e.printStackTrace();
                    }

                    if(sun==1 && mon==1 && tue==1 && wed==1 &&  thu==1 &&  fri==1 &&  sat==1   )
                    {
                        available_text +="All Days" ;
                    }
                    else if(sun==0 && mon==0 && tue==0 && wed==0 &&  thu==0 &&  fri==0 &&  sat==1)
                    {
                        available_text +="Only Saturday" ;
                        only_sat=1;
                    }
                    else
                    {
                        if(sun==0)
                        {
                            off_days=1;
                            available_text +="Sun " ;
                        }
                        if(mon==0)
                        {
                            off_days=1;
                            available_text +="Mon " ;
                        }

                        if(tue==0)
                        {
                            off_days=1;
                            available_text +="Tue " ;
                        }
                        if(wed==0)
                        {
                            off_days=1;
                            available_text +="Wed " ;
                        }

                        if(thu==0)
                        {
                            off_days=1;
                            available_text +="Thu " ;
                        }
                        if(fri==0)
                        {
                            off_days=1;
                            available_text +="Fri " ;
                        }
                        if(sat==0)
                        {
                            off_days=1;
                            available_text +="Sat " ;
                        }
                    }


                    listOftrain.add(new TimetableListBean(trainId,trainNo,trainName,SourceStation,sourcestationcode,sourcearrival,
                            DestinationStation,destStationcode,destarrival,available_text,off_days,only_sat,remarks,stops,takeTime));
                }
                norecrod=1;

            }else{
                Log.e("No REcord Found","TRUE");
                norecrod = 0;
            }
            traincursor.close();









            return null;}

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            /*adapter = new TimeTableListAdapter(getActivity(), listOftrain);
            lv.setAdapter(adapter);
            Utility.setListViewHeightBasedOnChildren(lv);

            // lv.setOnClickListener(new ClickListenerList());
            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {

                    final TimetableListBean entry = listOftrain.get(position);
                    int trainId = entry.getTrainId();
                    String trainNo =String.valueOf(entry.getTrainNo());

                    Intent intent = new Intent(getActivity(), ExTimeTableDetails.class);

                    intent.putExtra("trainId", trainId);
                    Log.v("tarinId","aa="+trainId+"==abb="+trainNo);
                    intent.putExtra("trainNo", trainNo);
                    intent.putExtra("fromstation", fromstationcode);
                    intent.putExtra("tostation", tostationcode);


                    startActivity(intent);
                }
            });*/

            ExpressTimeRecyclerAdapter adapter=new ExpressTimeRecyclerAdapter(getActivity(), listOftrain);
            lv.setAdapter(adapter);
            ItemClickSupport.addTo(lv).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
                @Override
                public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                    final TimetableListBean entry = listOftrain.get(position);
                    int trainId = entry.getTrainId();
                    String trainNo =String.valueOf(entry.getTrainNo());

                    Intent intent = new Intent(getActivity(), ExTimeTableDetails.class);

                    intent.putExtra("trainId", trainId);
                    Log.v("tarinId","aa="+trainId+"==abb="+trainNo);
                    intent.putExtra("trainNo", trainNo);
                    intent.putExtra("fromstation", fromstationcode);
                    intent.putExtra("tostation", tostationcode);


                    startActivity(intent);
                }
            });
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }


            Log.e("result==",""+norecrod);
            if(norecrod == 0){
                lv.setVisibility(View.GONE);
                progressBar1.setVisibility(View.GONE);
                //successAlert();
            } else{
                lv.setVisibility(View.VISIBLE);
                progressBar1.setVisibility(View.GONE);
            }

        }

    }

    /*public void savestation (){


        Toast.makeText(getActivity(),"dfgd",Toast.LENGTH_SHORT).show();

        *//*---------------- GET SESSION ARRAY IF ANY ------------------*//*
        String recentsearchjson = prefs.getString("recentsearchEx", "null");
        if(recentsearchjson.equals("null")){
            Log.e("Recent JSON==","NO VALUE SAVED");
        } else{
            Log.e("Recent JSON==",""+recentsearchjson);
            try {
                JSONArray recent_arr = new JSONArray(recentsearchjson);
                Log.e("recent_arr JSON==",""+recent_arr);

                int count=0;
                int arr_count = recent_arr.length();
                if(arr_count <4){
                    count = recent_arr.length();
                } else{
                    count = 4;
                }
                Log.e("COUNT==",""+count);
                for(int i=0;i<count;i++){
                    JSONObject obj = recent_arr.getJSONObject(i);
                    RecentSearch recentsearch = new RecentSearch();
                    recentsearch.setToStationName(obj.getString("ToStationName"));
                    recentsearch.setFromStationName(obj.getString("FromStationName"));



                    String[] fromstationarr,tostationarr;
                    if(obj.getString("FromStationName").contains("-")) {
                        fromstationarr = obj.getString("FromStationName").split("-");
                        fromcode = fromstationarr[0].trim().toUpperCase(); // this will contain "Fruit"
                        //fromstationname = fromstationarr[1].trim().toUpperCase();
                    } else{
                        //fromstationarr = fromstation.split("-");
                        fromcode = obj.getString("FromStationName").toUpperCase();
                    }

                    if(obj.getString("ToStationName").contains("-")) {
                        tostationarr = obj.getString("ToStationName").split("-");
                        tocode = tostationarr[0].trim().toUpperCase();
                        //tostationname = tostationarr[1].trim().toUpperCase();
                    } else{
                        //tostationarr = tostation.split("-");
                        tocode = obj.getString("ToStationName").toUpperCase();
                    }


                    if((obj.getString("ToStationName").equalsIgnoreCase(Constants.EX_TOSTATIONCODE)                   *//*chk by name*//*
                            && obj.getString("FromStationName").equalsIgnoreCase(Constants.EX_FROMSTATIONCODE))
                            || (obj.getString("ToStationName").equalsIgnoreCase(Constants.EX_FROMSTATIONCODE)
                            && obj.getString("FromStationName").equalsIgnoreCase(Constants.EX_TOSTATIONCODE))

                            || (fromcode.equalsIgnoreCase(Constants.EX_FROMSTATIONCODE)                  *//*chk by code*//*
                            && tocode.equalsIgnoreCase(Constants.EX_TOSTATIONCODE))
                            || (fromcode.equalsIgnoreCase(Constants.EX_TOSTATIONCODE)
                            && tocode.equalsIgnoreCase(Constants.EX_FROMSTATIONCODE))){

                        flag = true;
                    }
                    recentArrayList.add(recentsearch);
                }

                Log.e("recentArrayList===",""+recentArrayList.size());



            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        Log.e("TOSTATIONCODE_LOCAL",""+Constants.EX_TOSTATIONCODE);
        Log.e("FROMSTATIONCODE_LOCAL",""+Constants.EX_FROMSTATIONCODE);

        if(flag == false) {
            RecentSearch rs = new RecentSearch();
            rs.setFromStationName(Constants.EX_FROMSTATIONCODE);
            rs.setToStationName(Constants.EX_TOSTATIONCODE);
            recentSearchList.add(rs);

            if (recentArrayList.size() > 0) {
                recentSearchList.addAll(recentArrayList);

            }
            Log.e("SAVED STATION COUNT ", "" + recentSearchList.size());
            for (int i = 0; i < recentSearchList.size(); i++) {
                Log.e("SAVED STATION ", recentSearchList.get(i).getFromStationName() + "" + recentSearchList.get(i).getToStationName());

            }
            Gson gson = new Gson();
            String json = gson.toJson(recentSearchList);
            editorUser.putString("recentsearchEx", json);
            editorUser.commit();
        }
    }*/

    public void successAlert()
    {

        AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(
               getActivity());

        // Setting Dialog Title
        alertDialog2.setTitle("Alert");

        // Setting Dialog Message
        alertDialog2.setMessage("No train found!!");

        // Setting Icon to Dialog


        // Setting Positive "Yes" Btn
        alertDialog2.setNeutralButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog

                        /*Intent i3 = new Intent(ExTrainTimetable.this, ExpressTrainHome.class);

                        startActivity(i3);
                        finish();*/



                    }
                });
        alertDialog2.show();

    }
}
