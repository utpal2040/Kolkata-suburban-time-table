package com.matainja.runingstatus.Fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.matainja.runingstatus.Adapter.LocalTimeTableRecyclerAdapter;
import com.matainja.runingstatus.Adapter.TimeTableListAdapter;
import com.matainja.runingstatus.Common.Constants;
import com.matainja.runingstatus.CustomView.ItemClickSupport;
import com.matainja.runingstatus.CustomView.NestedListView;
import com.matainja.runingstatus.CustomView.Utility;
import com.matainja.runingstatus.Database.DatabaseAdapter;
import com.matainja.runingstatus.LocalTrain;
import com.matainja.runingstatus.Model.RecentSearch;
import com.matainja.runingstatus.Model.TimetableListBean;
import com.matainja.runingstatus.R;
import com.matainja.runingstatus.SugesstionStation;
import com.matainja.runingstatus.TimeTableDetails;
import com.matainja.runingstatus.TrainTimetable;
import com.tuyenmonkey.mkloader.MKLoader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by matainja on 28-Feb-17.
 */

public class LocalTrainListFragment extends Fragment {

    View fragmentView = null;
    String fromstationcode;
    String tostationcode;
    String timefilter;
    int filterbytime;
    int norecrod=0;
    Integer validstationto=0;
    Integer validstationfrom=0;
   // ListView lv;
    RecyclerView  lv;
    ArrayList<TimetableListBean> listOftrain;
    Cursor traincursor;
    private DatabaseAdapter dbHelper;

    String fromStationNAme="";
    String fromStationCode="";
    String tostation_ = "";
    String toStationCode= "";

    TimeTableListAdapter adapter;
    MKLoader progressBar1;
    Cursor fromstation,tostation;
    int listviewCount;

    ArrayList<RecentSearch> recentSearchList;
    ArrayList<RecentSearch> recentArrayList;
    SharedPreferences prefs;
    SharedPreferences.Editor editorUser;
    boolean flag = false;
    String fromcode, tocode;
    public static String callfrom = "";
    SharedPreferences sp;

    public LocalTrainListFragment(){}
    @SuppressLint("ValidFragment")
    public LocalTrainListFragment (String fromstationcode, String tostationcode, String timefilter, int filterbytime){
        this.fromstationcode = fromstationcode;
        this.tostationcode = tostationcode;
        this.timefilter = timefilter;
        this.filterbytime = filterbytime;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        fragmentView = inflater.inflate(R.layout.layout_localtrainlistfragment, null);
        dbHelper = new DatabaseAdapter(getActivity());
        dbHelper.open();

        recentSearchList = new ArrayList<RecentSearch>();
        recentArrayList = new ArrayList<RecentSearch>();
        prefs = getActivity().getSharedPreferences(Constants.USER_SHARED_PREF_VAR, Context.MODE_PRIVATE);
        editorUser = prefs.edit();

        sp = getActivity().getSharedPreferences("themeColor", Context.MODE_PRIVATE);

        progressBar1 = (MKLoader) fragmentView.findViewById(R.id.progressBar1);

        //Toast.makeText(getActivity(), fromstationcode, Toast.LENGTH_SHORT).show();

       // lv = (ListView)fragmentView.findViewById(R.id.traintlist);
        lv = (RecyclerView) fragmentView.findViewById(R.id.traintlist);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
            lv .setLayoutManager(mLayoutManager);
            lv.setNestedScrollingEnabled(false);
        new LoadingTrain().execute();

        fromstation =dbHelper.getStationName(fromstationcode);
        tostation =dbHelper.getStationName(tostationcode);


        if(fromstation.getCount()>0)
        {


            while (fromstation.moveToNext()) {


                validstationfrom=1;

                fromStationNAme = fromstation.getString(fromstation.getColumnIndexOrThrow("stationName"));
                fromStationCode = fromstation.getString(fromstation.getColumnIndexOrThrow("stationCode"));


            }
        }
        fromstation.close();

        if(tostation.getCount()>0)
        {

            while (tostation.moveToNext()) {


                validstationto=1;

                tostation_ = tostation.getString(tostation.getColumnIndexOrThrow("stationName"));
                toStationCode = tostation.getString(tostation.getColumnIndexOrThrow("stationCode"));



            }
        }
        tostation.close();

        TrainTimetable.stationname.setText(fromStationNAme+" To "+tostation_);

        if(sp.getInt("localColor",0)!=0) {


            /*parentR.setBackgroundColor(sp.getInt("localColor",0));
            bttntimetable.setTextColor(sp.getInt("localColor",0));
            recentsearchtext.setTextColor(sp.getInt("localColor",0));*/



        }
    return fragmentView;
    }

    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        if (visible) {
            // ...
            try {
                TrainTimetable.totaltrain.setText(traincursor.getCount() + " Train(s)");

            }catch(NullPointerException ne){

            }

        } else{
            Log.e("NOT","VISIBLE");
        }
    }

    private class LoadingTrain extends AsyncTask<Void, Void, Void> {


        @Override
        protected Void doInBackground(Void... arg0) {


            //Log.e("values==",fromstationcode+", "+ tostationcode+", "+timefilter+", "+filterbytime);

            listOftrain = new ArrayList<TimetableListBean>();

            traincursor = dbHelper.getTrainList(fromstationcode,tostationcode,timefilter,filterbytime);

            listviewCount = traincursor.getCount();


            if(traincursor.getCount()>0)
            {

                getActivity().runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        TrainTimetable.totaltrain.setText(traincursor.getCount()+" Train(s)");

                        if(callfrom.equalsIgnoreCase("localtrain")) {
                            Log.e("CALLFROM==",callfrom);
                            savestation();
                        }

                    }
                });


                while (traincursor.moveToNext()) {
                    String remarks ="";
                    Integer trainId = traincursor.getInt(traincursor.getColumnIndexOrThrow("trainId"));
                    String trainNo = traincursor.getString(traincursor.getColumnIndexOrThrow("trainNo"));


                    String trainName = traincursor.getString(traincursor.getColumnIndexOrThrow("trainName"));
                    String SourceStation  = traincursor.getString(traincursor.getColumnIndexOrThrow("SourceStation"));

                    String sourcestationcode  = traincursor.getString(traincursor.getColumnIndexOrThrow("sourcestationcode"));

                    String sourcearrival  = traincursor.getString(traincursor.getColumnIndexOrThrow("sourcearrival"));

                    String DestinationStation  = traincursor.getString(traincursor.getColumnIndexOrThrow("DestinationStation"));

                    String destStationcode  = traincursor.getString(traincursor.getColumnIndexOrThrow("destStationcode"));

                    String destarrival  = traincursor.getString(traincursor.getColumnIndexOrThrow("destarrival"));

                    String sarriable  = traincursor.getString(traincursor.getColumnIndexOrThrow("sarriable"));

                    String darriable  = traincursor.getString(traincursor.getColumnIndexOrThrow("darriable"));

                    Log.e("TrainName===",""+trainName);

                    cal_date_difference(sarriable,darriable);

                    /*long s = Long.valueOf(sarriable);
                    long d = Long.valueOf(darriable);
                    long diffInMillisec = s - d;
                    long diffInSec = TimeUnit.MILLISECONDS.toSeconds(diffInMillisec);
                    long seconds = diffInSec % 60;
                    diffInSec/= 60;
                    long minutes =diffInSec % 60;
                    diffInSec /= 60;
                    long hours = diffInSec % 24;
                    diffInSec /= 24;
                    //days = diffInSec;
                    Log.e("diffInSec",""+diffInSec);*/

                    remarks = traincursor.getString(traincursor.getColumnIndexOrThrow("remarks"));
                    Integer sun=0;
                    Integer mon=0;
                    Integer tue=0;
                    Integer wed=0;
                    Integer thu=0;
                    Integer fri=0;
                    Integer sat=0;
                    String available_text="";
                    Integer off_days=0;
                    Integer only_sat=0;


                    sun = traincursor.getInt(traincursor.getColumnIndexOrThrow("sun"));
                    mon = traincursor.getInt(traincursor.getColumnIndexOrThrow("mon"));
                    tue = traincursor.getInt(traincursor.getColumnIndexOrThrow("tue"));
                    wed = traincursor.getInt(traincursor.getColumnIndexOrThrow("wed"));
                    thu = traincursor.getInt(traincursor.getColumnIndexOrThrow("thu"));
                    fri = traincursor.getInt(traincursor.getColumnIndexOrThrow("fri"));
                    sat = traincursor.getInt(traincursor.getColumnIndexOrThrow("sat"));


                    //Log.v("Station:name",stationName);
                    int stops;
                    String takeTime="";
                    String src_time,dest_time;
                    Date src_localTime = null;
                    Date dest_localTime = null;
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                    String[] stopsArr = dbHelper.getStationStop(trainId,fromstationcode,tostationcode);
                    //String stops = String.valueOf(stationList.getCount());

                    stops =Integer.parseInt(stopsArr[0]);

                    src_time = stopsArr[1];
                    dest_time = stopsArr[2];

                    try {
                        src_localTime = format.parse(src_time);
                        dest_localTime = format.parse(dest_time);

                        long diff = dest_localTime.getTime() - src_localTime.getTime();

                        long diffSeconds = diff / 1000 % 60;
                        long diffMinutes = diff / (60 * 1000) % 60;
                        long diffHours = diff / (60 * 60 * 1000) % 24;
                        long diffDays = diff / (24 * 60 * 60 * 1000);

                        Log.e(" days ",""+diffDays);
                        Log.e( " hours ",""+diffHours );
                        Log.e(" minutes ",""+diffMinutes);
                        Log.e(" seconds ",""+diffSeconds);

                        if(diffHours != 0 && diffMinutes != 0) {
                            takeTime = diffHours + " H : " + diffMinutes + " M";
                        }else{
                            if(diffHours == 0) {
                                takeTime = diffMinutes + " M";
                            }else if(diffMinutes == 0){
                                takeTime = diffHours + " H";
                            }
                        }

                          /*src_localTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).parse(src_time);
                          Log.e("SRC TimeStamp is ","" +src_localTime.getTime());

                          dest_localTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).parse(dest_time);
                          Log.e("DEST TimeStamp is ","" +dest_localTime.getTime());*/
                    } catch (java.text.ParseException e) {
                        Log.e("e","" +e);
                        e.printStackTrace();
                    }

                    if(sun==1 && mon==1 && tue==1 && wed==1 &&  thu==1 &&  fri==1 &&  sat==1   )
                    {
                        available_text +="All Days" ;
                    }
                    else if(sun==0 && mon==0 && tue==0 && wed==0 &&  thu==0 &&  fri==0 &&  sat==1)
                    {
                        available_text +="Only Saturday" ;
                        only_sat=1;
                    }
                    else
                    {
                        if(sun==0)
                        {
                            off_days=1;
                            available_text +="Sun " ;
                        }
                        if(mon==0)
                        {
                            off_days=1;
                            available_text +="Mon " ;
                        }

                        if(tue==0)
                        {
                            off_days=1;
                            available_text +="Tue " ;
                        }
                        if(wed==0)
                        {
                            off_days=1;
                            available_text +="Wed " ;
                        }

                        if(thu==0)
                        {
                            off_days=1;
                            available_text +="Thu " ;
                        }
                        if(fri==0)
                        {
                            off_days=1;
                            available_text +="Fri " ;
                        }
                        if(sat==0)
                        {
                            off_days=1;
                            available_text +="Sat " ;
                        }
                    }


                    listOftrain.add(new TimetableListBean(trainId,trainNo,trainName,SourceStation,sourcestationcode,sourcearrival,
                            DestinationStation,destStationcode,destarrival,available_text,off_days,only_sat,remarks,stops,takeTime));


                }
                norecrod=1;

            } else{
                Log.e("No REcord Found","TRUE");
                norecrod = 0;

               /* getActivity().runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        //lv.setVisibility(View.VISIBLE);
                        progressBar1.setVisibility(View.GONE);
                    }
                });

                getActivity().runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        TrainTimetable.totaltrain.setText("");
                    }
                });*/
            }
            traincursor.close();









            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            Log.e("onpost","pppppppppppppppp");
            super.onPostExecute(result);


            progressBar1.setVisibility(View.GONE);
            lv.setVisibility(View.VISIBLE);

            //NestedListView.total_list(25);
          /*  adapter = new TimeTableListAdapter(getActivity(), listOftrain);
            lv.setAdapter(adapter);*/
            LocalTimeTableRecyclerAdapter adapter=new LocalTimeTableRecyclerAdapter(getActivity(), listOftrain);
            lv.setAdapter(adapter);
          //  Utility.setListViewHeightBasedOnChildren(lv);
           // lv.setLayoutParams(new LinearLayout.LayoutParams(RelativeLayout.LayoutParams.FILL_PARENT, 100));
            Log.e("result==",""+norecrod);
            if(norecrod == 0){
                lv.setVisibility(View.GONE);
               // progressBar1.setVisibility(View.GONE);
                successAlert();
            } else{
                lv.setVisibility(View.VISIBLE);
                //progressBar1.setVisibility(View.GONE);
            }
            ItemClickSupport.addTo(lv).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
                @Override
                public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                    final TimetableListBean entry = listOftrain.get(position);
                    Integer trainId = entry.getTrainId();
                    String trainNo =entry.getTrainNo();

                    Intent intent = new Intent(getActivity(), TimeTableDetails.class);
                    intent.putExtra("trainId", trainId);
                    intent.putExtra("trainNo", trainNo);
                    intent.putExtra("fromstation", fromstationcode);
                    intent.putExtra("tostation", tostationcode);

                    startActivity(intent);
                }
            });

         /*   getActivity().runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    lv.setVisibility(View.VISIBLE);
                    progressBar1.setVisibility(View.GONE);
                }
            });*/

            // lv.setOnClickListener(new ClickListenerList());
          /*  lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {

                    final TimetableListBean entry = listOftrain.get(position);
                    Integer trainId = entry.getTrainId();
                    String trainNo =entry.getTrainNo();

                    Intent intent = new Intent(getActivity(), TimeTableDetails.class);
                    intent.putExtra("trainId", trainId);
                    intent.putExtra("trainNo", trainNo);
                    intent.putExtra("fromstation", fromstationcode);
                    intent.putExtra("tostation", tostationcode);

                    startActivity(intent);
                }
            });*/
            /*try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }*/



            /*if(norecrod==0 && validstationto==1)
            {
                successAlert();
            }
            else if(norecrod==0)
            {
                noRecordFound();
            }*/

        }

    }

    public void savestation (){




        /*---------------- GET SESSION ARRAY IF ANY ------------------*/
        String recentsearchjson = prefs.getString("recentsearch", "null");
        if(recentsearchjson.equals("null")){
            Log.e("Recent JSON==","NO VALUE SAVED");
        } else{
            Log.e("Recent JSON==",""+recentsearchjson);
            try {
                JSONArray recent_arr = new JSONArray(recentsearchjson);
                Log.e("recent_arr JSON==",""+recent_arr);

                int count=0;
                int arr_count = recent_arr.length();
                if(arr_count <4){
                    count = recent_arr.length();
                } else{
                    count = 4;
                }
                Log.e("COUNT==",""+count);
                for(int i=0;i<count;i++){
                    JSONObject obj = recent_arr.getJSONObject(i);
                    RecentSearch recentsearch = new RecentSearch();
                    recentsearch.setToStationName(obj.getString("ToStationName"));
                    recentsearch.setFromStationName(obj.getString("FromStationName"));



                    String[] fromstationarr,tostationarr;
                    if(obj.getString("FromStationName").contains("-")) {
                        fromstationarr = obj.getString("FromStationName").split("-");
                        fromcode = fromstationarr[0].trim().toUpperCase(); // this will contain "Fruit"
                        //fromstationname = fromstationarr[1].trim().toUpperCase();
                    } else{
                        //fromstationarr = fromstation.split("-");
                        fromcode = obj.getString("FromStationName").toUpperCase();
                    }

                    if(obj.getString("ToStationName").contains("-")) {
                        tostationarr = obj.getString("ToStationName").split("-");
                        tocode = tostationarr[0].trim().toUpperCase();
                        //tostationname = tostationarr[1].trim().toUpperCase();
                    } else{
                        //tostationarr = tostation.split("-");
                        tocode = obj.getString("ToStationName").toUpperCase();
                    }


                    if((obj.getString("ToStationName").equalsIgnoreCase(Constants.TOSTATIONCODE_LOCAL)                   /*chk by name*/
                            && obj.getString("FromStationName").equalsIgnoreCase(Constants.FROMSTATIONCODE_LOCAL))
                            /*|| (obj.getString("ToStationName").equalsIgnoreCase(Constants.FROMSTATIONCODE_LOCAL)
                            && obj.getString("FromStationName").equalsIgnoreCase(Constants.TOSTATIONCODE_LOCAL))*/

                            || (fromcode.equalsIgnoreCase(Constants.FROMSTATIONCODE_LOCAL)                  /*chk by code*/
                            && tocode.equalsIgnoreCase(Constants.TOSTATIONCODE_LOCAL))
                            /*|| (fromcode.equalsIgnoreCase(Constants.TOSTATIONCODE_LOCAL)
                            && tocode.equalsIgnoreCase(Constants.FROMSTATIONCODE_LOCAL))*/){

                        flag = true;
                    }
                    recentArrayList.add(recentsearch);
                }

                Log.e("recentArrayList===",""+recentArrayList.size());



            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        Log.e("TOSTATIONCODE_LOCAL",""+Constants.TOSTATIONCODE_LOCAL);
        Log.e("FROMSTATIONCODE_LOCAL",""+Constants.FROMSTATIONCODE_LOCAL);

        if(flag == false) {
            RecentSearch rs = new RecentSearch();
            rs.setFromStationName(Constants.FROMSTATIONCODE_LOCAL);
            rs.setToStationName(Constants.TOSTATIONCODE_LOCAL);
            recentSearchList.add(rs);

            if (recentArrayList.size() > 0) {
                recentSearchList.addAll(recentArrayList);

            }
            Log.e("SAVED STATION COUNT ", "" + recentSearchList.size());
            for (int i = 0; i < recentSearchList.size(); i++) {
                Log.e("SAVED STATION ", recentSearchList.get(i).getFromStationName() + "" + recentSearchList.get(i).getToStationName());

            }
            Gson gson = new Gson();
            String json = gson.toJson(recentSearchList);
            editorUser.putString("recentsearch", json);
            editorUser.commit();
        }
    }

    public void cal_date_difference(String dateStart,String dateStop) {

        //Toast.makeText(getActivity(),"dateStart",Toast.LENGTH_LONG).show();
        Date d1, d2;
        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss", Locale.getDefault());
        try {
            d1 = format.parse(dateStart);
            d2 = format.parse(dateStop);

            Log.e("d1",""+d1);
            Log.e("d2",""+d2);
            //in milliseconds
            long diff = d2.getTime() - d1.getTime();

            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
            long diffDays = diff / (24 * 60 * 60 * 1000);

            if(diffDays!=0) {
                //txtdateago.setText(String.valueOf(diffDays + " days ago"));
                Log.e("diffDays==",""+diffDays);
            } else if(diffHours!=0) {
                //txtdateago.setText(String.valueOf(diffHours + " hours ago"));
                Log.e("diffHours==",""+diffHours);
            } else if(diffMinutes!=0) {
                //txtdateago.setText(String.valueOf(diffMinutes + " minutes ago"));
                Log.e("diffMinutes==",""+diffMinutes);
            } else {
               // txtdateago.setText(String.valueOf("Now"));
                Log.e("Now==","Now");
            }
        } catch (Exception e) {
            e.printStackTrace();

            Log.e("EXCEPTION",""+e);
        }

    }

    public void successAlert()
    {

        AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(
                getActivity());

        // Setting Dialog Title
        alertDialog2.setTitle("Alert");

        // Setting Dialog Message
        alertDialog2.setMessage("No train found between two stations!!");

        // Setting Icon to Dialog


        // Setting Positive "Yes" Btn
        alertDialog2.setNeutralButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog

                        Intent i3 = new Intent(getActivity(), LocalTrain.class);
                        i3.putExtra("callfrom","other");
                        startActivity(i3);
                        getActivity().finish();

                    }
                });

        alertDialog2.setNegativeButton("Suggestion", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                Intent i3 = new Intent(getActivity(), SugesstionStation.class);
                i3.putExtra("tostationName", tostation_);
                i3.putExtra("tostation", tostationcode);
                startActivity(i3);
                getActivity().finish();
            }
        });
        alertDialog2.show();

    }

    public void noRecordFound()
    {

        AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(
                getActivity());

        // Setting Dialog Title
        alertDialog2.setTitle("Alert");

        // Setting Dialog Message
        alertDialog2.setMessage("No train found between two station!!");

        // Setting Icon to Dialog


        // Setting Positive "Yes" Btn
        alertDialog2.setNeutralButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog

                        Intent i3 = new Intent(getActivity(), LocalTrain.class);
                        i3.putExtra("callfrom","other");
                        startActivity(i3);
                        getActivity().finish();



                    }
                });
        alertDialog2.show();

    }
}
