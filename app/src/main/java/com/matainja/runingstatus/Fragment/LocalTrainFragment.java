package com.matainja.runingstatus.Fragment;

import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.matainja.runingstatus.Adapter.CustomerAdapter;
import com.matainja.runingstatus.Adapter.RecentSearchAdapter;
import com.matainja.runingstatus.Common.Constants;
import com.matainja.runingstatus.Database.DatabaseAdapter;
import com.matainja.runingstatus.LocalTrain;
import com.matainja.runingstatus.Model.RecentSearch;
import com.matainja.runingstatus.Model.StationLocal;
import com.matainja.runingstatus.R;
import com.matainja.runingstatus.TrainTimetable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by matainja on 13-Feb-17.
 */

public class LocalTrainFragment extends Fragment implements View.OnClickListener {

    View fragmentView = null;
    CheckBox Radio1;
    ListView lastsearchlist;
    Button bttntimetable;
    public static AutoCompleteTextView fromstationcode,tostationcode;
    ImageView updownImg,crossImg,clock,searchImg,searchImg2;
    String tostationcodeValue,fromstationcodeValue;
    private int mYear, mMonth, mDay, mHour, mMinute;
    TextView timehour,recentsearchtext,timehour24;
    RelativeLayout parentR,PrimaryRLayout;
    Context context;
    SimpleDateFormat timeFormatter;
    private DatabaseAdapter dbHelper;
    Cursor savestation;
    String fromsaveStation = null;
    String tosaveStation = null;
    ArrayList<String> stations = new ArrayList<String>();
    String recentsearchjson,fieldTo,fieldFrom,timechkd;
    public ArrayList<RecentSearch> recentSearchList;
    SharedPreferences prefs;
    Editor editorUser;
    ArrayList<RecentSearch> recentArrayList;
    Cursor stationCursor;
    ArrayList<StationLocal> listOfStation;
    String hour24;
    SharedPreferences sp;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        fragmentView = inflater.inflate(R.layout.layout_localtrainfragment,  null);


        recentSearchList = new ArrayList<RecentSearch>();
        prefs = getActivity().getSharedPreferences(Constants.USER_SHARED_PREF_VAR, Context.MODE_PRIVATE);
        editorUser = prefs.edit();
        recentArrayList = new ArrayList<RecentSearch>();

        /*Type type = new TypeToken<ArrayList<ArrayObject>>() {}.getType();
        ArrayList<ArrayObject> arrayList = gson.fromJson(json, type);*/

        //FragmentManager fm = getActivity().getFragmentManager();
        parentR = (RelativeLayout) fragmentView.findViewById(R.id.parentR);
        PrimaryRLayout  = (RelativeLayout) fragmentView.findViewById(R.id.PrimaryRLayout);
        Radio1 = (CheckBox) fragmentView.findViewById(R.id.Radio1);
        lastsearchlist = (ListView) fragmentView.findViewById(R.id.lastsearchlist);
        bttntimetable = (Button) fragmentView.findViewById(R.id.bttntimetable);
        tostationcode = (AutoCompleteTextView) fragmentView.findViewById(R.id.tostationcode);
        fromstationcode = (AutoCompleteTextView) fragmentView.findViewById(R.id.fromstationcode);
        crossImg = (ImageView) fragmentView.findViewById(R.id.crossImg);
        updownImg = (ImageView) fragmentView.findViewById(R.id.updownImg);
        clock = (ImageView) fragmentView.findViewById(R.id.clock);
        timehour = (TextView) fragmentView.findViewById(R.id.timehour);
        timehour24 = (TextView) fragmentView.findViewById(R.id.timehour24);
        searchImg = (ImageView) fragmentView.findViewById(R.id.searchImg);
        searchImg2 = (ImageView) fragmentView.findViewById(R.id.searchImg2);
        recentsearchtext = (TextView) fragmentView.findViewById(R.id.recentsearchtext);

        sp = getActivity().getSharedPreferences("themeColor", Context.MODE_PRIVATE);
        Log.e("local session color" ,""+sp.getInt("localColor",0));
        if(sp.getInt("localColor",0)!=0) {

            parentR.setBackgroundColor(sp.getInt("localColor",0));
            bttntimetable.setTextColor(sp.getInt("localColor",0));
            recentsearchtext.setTextColor(sp.getInt("localColor",0));

            int[] demo_colors = getActivity().getResources().getIntArray(R.array.demo_colors);
            int[] demo_colors_mat = getActivity().getResources().getIntArray(R.array.demo_colors_mat);

            ArrayList<String> colorString = new ArrayList<String>();
            ArrayList<String> colorString_mat = new ArrayList<String>();

            for (int i = 0; i < demo_colors.length; i++) {
                Log.e("Test", demo_colors[i] + "");
                //Log.e("Test", demo_colors[i] + "");
                colorString.add(String.valueOf(demo_colors[i]));

            }

            for (int i = 0; i < demo_colors_mat.length; i++) {
                Log.e("Test", demo_colors[i] + "");
                //Log.e("Test", demo_colors[i] + "");
                colorString_mat.add(String.valueOf(demo_colors_mat[i]));

            }
            Log.e("position ",colorString.indexOf(String.valueOf(sp.getInt("localColor",0))) + "");
            int color_mat_val = Integer.parseInt(colorString_mat.get(colorString.indexOf(String.valueOf(sp.getInt("localColor", 0)))));

            Log.e("color_mat_val ",""+color_mat_val);
            fromstationcode.setBackgroundColor(color_mat_val);
            tostationcode.setBackgroundColor(color_mat_val);

        }
        // GET CURRENT TIME

        Calendar calander = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a");
        SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
        String time = simpleDateFormat.format(calander.getTime());

        try {

            Date date = simpleDateFormat.parse(time);
            Log.e("datelsdjflsdf ",""+displayFormat.format(date));

            hour24 = displayFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        timehour24.setText(hour24);
        timehour.setText(time);


        fieldTo = prefs.getString("local", "null");
        fieldFrom = prefs.getString("from", "null");
        timechkd = String.valueOf(Constants.timechkd);//prefs.getString("timechkd", "false");

        Log.e("fieldTo== fieldFrom",""+fieldTo+","+fieldFrom);
        if(!fieldTo.equals("null")){
            tostationcode.setText(fieldTo);
        }
        if(!fieldFrom.equals("null")){
            fromstationcode.setText(fieldFrom);
        }
        if(timechkd.equalsIgnoreCase("true")){
            Radio1.setChecked(true);
        } else{
            Radio1.setChecked(false);
        }
        searchImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((LocalTrain)getActivity()).getLayoutHide();
                Constants.isSelectedFrom = true;
                LocalTrain.hideSoftKeyboard(getActivity());
            }
        });
        searchImg2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((LocalTrain)getActivity()).getLayoutHide();
                Constants.isSelectedto = true;
                LocalTrain.hideSoftKeyboard(getActivity());
            }
        });

        String[] languages={"Android ","java","IOS","SQL","JDBC","Web services"};


        bttntimetable.setOnClickListener(this);
        crossImg.setOnClickListener(this);
        updownImg.setOnClickListener(this);
        clock.setOnClickListener(this);

        dbHelper = new DatabaseAdapter(getActivity());
        dbHelper.open();

        fromstationcode.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long rowId) {
               // String selection = (String)parent.getItemAtPosition(position);
                Log.e("SELECTION==",""+listOfStation.get(position).getStationCode());
                fromstationcode.setText(listOfStation.get(position).getStationCode()+" - "+listOfStation.get(position).getStationName());
                //TODO Do something with the selected text
            }
        });

        tostationcode.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long rowId) {
                // String selection = (String)parent.getItemAtPosition(position);
                Log.e("SELECTION==",""+listOfStation.get(position).getStationCode());
                tostationcode.setText(listOfStation.get(position).getStationCode()+" - "+listOfStation.get(position).getStationName());
                //TODO Do something with the selected text
            }
        });


        fromstationcode.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //here is your code

               // Log.e("SearchStation","ppppppp");

                if(fromstationcode.getText().length()== 0)
                {
                    SearchStation("");
                }
                else
                {
                    String keywords = fromstationcode.getText().toString();
                    SearchStation(keywords);
                }


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });

        tostationcode.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //here is your code

                if(tostationcode.getText().length()== 0)
                {
                    SearchStation("");
                }
                else
                {
                    String keywords = tostationcode.getText().toString();
                    SearchStation(keywords);
                }


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });

        /*Cursor getAllStations = dbHelper.getAllStations();
        Log.e("getAllStations==",""+getAllStations.getCount());
        if (getAllStations.moveToFirst()) {
            do {
                Log.e("STATION NAME==",""+getAllStations.getString(2));
                String sname = getAllStations.getString(2);
                String stn_code = getAllStations.getString(1);
                String station_all = sname+"-"+stn_code;
                stations.add(station_all);
                //stations.add(stn_code);

            } while (getAllStations.moveToNext());
        }getAllStations.close();
        Log.e("stations ARRAY",""+stations.get(0));

        ArrayAdapter adapter = new
                ArrayAdapter(getActivity(),android.R.layout.simple_list_item_1,stations);*/

        //fromstationcode.setAdapter(adapter);
        //fromstationcode.setThreshold(1);

        /*tostationcode.setAdapter(adapter);
        tostationcode.setThreshold(1);*/

        savestation =dbHelper.getSaveStation();
        Log.e("savestation==",""+savestation.getCount());

        if(savestation.getCount()>0)
        {

            while (savestation.moveToNext()) {




                fromsaveStation = savestation.getString(savestation.getColumnIndexOrThrow("fromstation"));
                tosaveStation = savestation.getString(savestation.getColumnIndexOrThrow("tostation"));


            }
        }
        Log.e("STATIONS==",fromsaveStation+"----"+tosaveStation);
        /*if(fromsaveStation!="" && tosaveStation!="")
        {
            fromStationEdit.setText(fromsaveStation);

            tostationcode.setText(tosaveStation);
        }*/
        savestation.close();

        Gson gson = new Gson();
        recentsearchjson = prefs.getString("recentsearch", "null");

        if(recentsearchjson.equals("null")){
            Log.e("Recent JSON==","NO VALUE SAVED");
        } else{
            Log.e("Recent JSON==",""+recentsearchjson);
            try {
                JSONArray recent_arr = new JSONArray(recentsearchjson);
                Log.e("recent_arr JSON==",""+recent_arr);

                int count=0;
                int arr_count = recent_arr.length();
                if(arr_count <4){
                    count = recent_arr.length();
                } else{
                    count = 4;
                }
                Log.e("COUNT==",""+count);
                for(int i=0;i<count;i++){
                    JSONObject obj = recent_arr.getJSONObject(i);
                    RecentSearch recentsearch = new RecentSearch();
                    recentsearch.setToStationName(obj.getString("ToStationName"));
                    recentsearch.setFromStationName(obj.getString("FromStationName"));

                    recentArrayList.add(recentsearch);
                }

                Log.e("recentArrayList===",""+recentArrayList.size());

                /*for(int k=0;k<recentArrayList.size();k++){
                    Log.e("KK==",""+recentArrayList.get(k).getFromStationName()+" "+recentArrayList.get(k).getToStationName());
                }*/
                recentsearchtext.setVisibility(View.VISIBLE);
                RecentSearchAdapter RSA = new RecentSearchAdapter(getActivity(),recentArrayList);
                lastsearchlist.setAdapter(RSA);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        lastsearchlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.e("code",recentArrayList.get(position).getFromStationName());

                String fromstation = recentArrayList.get(position).getFromStationName().toString();
                String tostation = recentArrayList.get(position).getToStationName().toString();

                Constants.TOSTATIONCODE_LOCAL = tostation;
                Constants.FROMSTATIONCODE_LOCAL = fromstation;

                //String time = timehour.getText().toString();
                String[] fromstationarr,tostationarr;
                String fromstationname = "", tostationname = "";
                if(fromstation.contains("-")) {
                    fromstationarr = fromstation.split("-");
                    fromstation = fromstationarr[0].trim().toUpperCase(); // this will contain "Fruit"
                    fromstationname = fromstationarr[1].trim().toUpperCase();
                }else{
                    fromstation = fromstation.toUpperCase();
                    fromstationname = fromstation;
                }
                if(tostation.contains("-")) {
                    tostationarr = tostation.split("-");
                    tostation = tostationarr[0].trim().toUpperCase();
                    tostationname = tostationarr[1].trim().toUpperCase();
                } else{
                    tostation = tostation.toUpperCase();
                    tostationname = tostation;
                }
                Log.e("FROM--TO--",""+fromstation+" "+tostation);




                Intent intent = new Intent(getActivity(), TrainTimetable.class);
                intent.putExtra("fromstation", fromstation);
                intent.putExtra("timefilter", "");
                intent.putExtra("tostation", tostation);
                intent.putExtra("fromstationname", fromstationname);
                intent.putExtra("tostationname", tostationname);
                intent.putExtra("callfrom","localtrain");
                intent.setAction(Intent.ACTION_VIEW);
                startActivity(intent);

                getActivity().finish();
            }
        });

        return fragmentView;
    }


    private void SearchStation(String searchQuery) {
        /// Listview for train

        if (searchQuery.length() > 0) {
            stationCursor = dbHelper.getAllStation(searchQuery);
            //Toast.makeText(getActivity(), "== "+stationCursor.getCount(), Toast.LENGTH_SHORT).show();
        } else {
            stationCursor = dbHelper.getAllStation(null);
        }
        //Log.e("Stationlist",listOfStation.toString());

        //listOfStation = new ArrayList<StationLocal>();
        // Log.v("Station:name","SearchStation="+stationCursor.getCount());
        if (stationCursor.getCount() > 0) {
            listOfStation = new ArrayList<StationLocal>();
            while (stationCursor.moveToNext()) {


                Integer id = stationCursor.getInt(stationCursor.getColumnIndexOrThrow("_id"));
                Integer popularity = stationCursor.getInt(stationCursor.getColumnIndexOrThrow("popularity"));

                String stationCod = stationCursor.getString(stationCursor.getColumnIndexOrThrow("stationCode"));
                String stationName = stationCursor.getString(stationCursor.getColumnIndexOrThrow("stationName"));




                //Log.v("Station:name",stationName);
//new StationLocal(id, stationCod, stationName, popularity)
                listOfStation.add(new StationLocal(id, stationCod, stationName, popularity));
            }
            CustomerAdapter adapter = new CustomerAdapter(getActivity(),R.layout.simple_list_item,listOfStation);
            fromstationcode.setAdapter(adapter);
            fromstationcode.setThreshold(1);

            tostationcode.setAdapter(adapter);
            tostationcode.setThreshold(1);
        }

        stationCursor.close();

    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.bttntimetable:
                tostationcodeValue = tostationcode.getText().toString().replaceAll("\\s+$", "");
                fromstationcodeValue = fromstationcode.getText().toString().replaceAll("\\s+$", "");

                Log.e("To AnD FrOm==",tostationcodeValue+" == "+fromstationcodeValue);

                if(!fromstationcodeValue.equals("")){

                    if(!tostationcodeValue.equals("")){
                        if (!fromstationcodeValue.equals(tostationcodeValue)) {
                            //for(int i=0;i<2;i++) {

                        Constants.TOSTATIONCODE_LOCAL = tostationcodeValue;
                        Constants.FROMSTATIONCODE_LOCAL = fromstationcodeValue;

                            editorUser.putString("local", tostationcodeValue);
                            editorUser.putString("from", fromstationcodeValue);
                            editorUser.commit();




                            Log.e("hour24==",""+timehour24.getText().toString()+" == "+timehour.getText());
                        /*RecentSearchAdapter RSA = new RecentSearchAdapter(getActivity(),recentSearchList);
                        lastsearchlist.setAdapter(RSA);*/
                            //myString.replaceAll("\\s+$", "");
                            String fromstation = fromstationcode.getText().toString();
                            String tostation = tostationcode.getText().toString();
                            String time = timehour24.getText().toString();
                            String fromstationname = "", tostationname = "";
                            String[] fromstationarr,tostationarr;
                            if(fromstation.contains("-")) {
                                fromstationarr = fromstation.split("-");
                                fromstation = fromstationarr[0].trim().toUpperCase(); // this will contain "Fruit"
                                fromstationname = fromstationarr[1].trim().toUpperCase();
                            } else{
                                //fromstationarr = fromstation.split("-");
                                fromstation = fromstation.toUpperCase();
                                fromstationname = fromstation;
                            }

                            if(tostation.contains("-")) {
                                tostationarr = tostation.split("-");
                                tostation = tostationarr[0].trim().toUpperCase();
                                tostationname = tostationarr[1].trim().toUpperCase();
                            } else{
                                //tostationarr = tostation.split("-");
                                tostation = tostation.toUpperCase();
                                tostationname = tostation;
                            }



                            Log.e("from station name==", fromstationname);
                            Intent intent = new Intent(getActivity(), TrainTimetable.class);
                            intent.putExtra("fromstation", fromstation);
                            intent.putExtra("timefilter", time);
                            intent.putExtra("tostation", tostation);
                            intent.putExtra("fromstationname", fromstationname);
                            intent.putExtra("tostationname", tostationname);
                            intent.putExtra("callfrom","localtrain");
                            intent.setAction(Intent.ACTION_VIEW);
                            if (Radio1.isChecked()) {
                                intent.putExtra("filterbytime", 1);
                                /*editorUser.putString("timechkd", "true");
                                editorUser.commit();*/
                                Constants.timechkd = true;
                            }
                            else {
                                intent.putExtra("filterbytime", 0);
                                /*editorUser.putString("timechkd", "false");
                                editorUser.commit();*/
                                Constants.timechkd = false;
                            }
                            startActivity(intent);
                            getActivity().finish();
                        } else{
                            ShowAlert("Please Select Differnt Source Or Destination");
                        }
                    }else{
                        ShowAlert(Constants.ERR_TOSTATION);
                    }
                } else{
                    ShowAlert(Constants.ERR_FROMSTATION);
                }


                //Log.e("SAVED STATION ",recentSearchList.get(0).getFromStationName());

                break;

            case R.id.updownImg:
                tostationcodeValue = tostationcode.getText().toString();
                fromstationcodeValue = fromstationcode.getText().toString();

                tostationcode.setText(fromstationcodeValue);
                fromstationcode.setText(tostationcodeValue);
                break;

            case R.id.crossImg:
                tostationcode.setText("");
                fromstationcode.setText("");

                break;

            case R.id.clock:
                DialogFragment newFragment = new TimePickerFragment();
                newFragment.show(getActivity().getFragmentManager(),"TimePicker");
                break;
        }
    }

    /*------------ CALLED FROM LocalTrainListFragment AFTER GETTING TRAINS -----------------------*/


    public void ShowAlert(String msg){

        Snackbar snackbar = Snackbar.make(PrimaryRLayout, msg , Snackbar.LENGTH_LONG);
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        // snackbar.setDuration(2000);
        snackbar.show();

    }

}
