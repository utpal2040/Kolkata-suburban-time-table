package com.matainja.runingstatus.Fragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by matainja on 28-Feb-17.
 */

public class TrainListViewPagerAdapter extends FragmentPagerAdapter{

    int mNumOfTabs;
    String fromstationcode;
    String tostationcode;
    String timefilter;
    int filterbytime;
    public TrainListViewPagerAdapter(FragmentManager fm, int NumOfTabs, String fromstationcode,String tostationcode, String timefilter, int filterbytime) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
        this.fromstationcode = fromstationcode;
        this.tostationcode = tostationcode;
        this.timefilter = timefilter;
        this.filterbytime = filterbytime;

        ArrayList<Fragment> FRAGMENTS = new ArrayList<Fragment>();

        /*Fragment fragA = new LocalTrainFragment();
        LocalTrain.ISetTextInFragment setText = (LocalTrain.ISetTextInFragment) fragA;
        FRAGMENTS.add(fragA);*/
    }



    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:

                LocalTrainListFragment tab1 = new LocalTrainListFragment(fromstationcode,tostationcode,timefilter,filterbytime);
                Log.e("dfhd","0");
                return tab1;
            case 1:

                ExpressTrainListFragment  tab2 = new ExpressTrainListFragment(fromstationcode,tostationcode,timefilter,filterbytime);
                Log.e("dfhd","1");
                return tab2;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
