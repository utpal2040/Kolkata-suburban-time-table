package com.matainja.runingstatus.Fragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by matainja on 13-Nov-17.
 */

public class ViewPagerAdapterTheme extends FragmentPagerAdapter {

    int mNumOfTabs;
    public ViewPagerAdapterTheme(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;

        ArrayList<Fragment> FRAGMENTS = new ArrayList<Fragment>();

        /*Fragment fragA = new LocalTrainFragment();
        LocalTrain.ISetTextInFragment setText = (LocalTrain.ISetTextInFragment) fragA;
        FRAGMENTS.add(fragA);*/
    }



    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:

                LocalTrainFragmentTheme tab1 = new LocalTrainFragmentTheme();
                Log.e("dfhd","0");
                return tab1;
            case 1:

                ExpressTrainFragmentTheme  tab2 = new ExpressTrainFragmentTheme();
                Log.e("dfhd","1");
                return tab2;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
