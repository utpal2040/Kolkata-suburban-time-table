package com.matainja.runingstatus;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.matainja.runingstatus.Adapter.RunningRecyclerViewAdapter;
import com.matainja.runingstatus.Common.ConnectionDetector;
import com.matainja.runingstatus.Connectivity.HttpConnection;
import com.matainja.runingstatus.Database.DatabaseAdapterEx;
import com.matainja.runingstatus.GoogleAnalyticsApp.TrackerName;
import com.matainja.runingstatus.Model.Train;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

//import com.matainja.runingstatus.JSONParser;

public class Running extends AppCompatActivity {
    RecyclerView list;
    TextView source;
    TextView arrival;
    TextView departure;
    TextView status;
    Button Btngetdata;
    TextView constatus ;
    ArrayList<HashMap<String, String>> oslist = new ArrayList<HashMap<String, String>>();
    //URL to get JSON Array
    private static String url = "";
    
    JSONArray trainroute = null;
    Bundle extras;
    String trainno;
    String dayofjourney;
    ConnectionDetector cd;
    private DatabaseAdapterEx dbHelper;
    Cursor traincursor;
    ArrayList<Train> listOfTrain;
    TextView train_name;
    TextView Source_station;
    TextView CurrentPostion;
    TextView destination_station;
    TextView train_status;
    int currentapiVersion=0;
    Context context;
    String currentDateandTime;
    Toolbar toolbar;
    SharedPreferences sp;
    String error_msg="Something is wrong!! server not connected!!";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	//Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.running);
        overridePendingTransition(R.anim.anim_slide_in_left,
                R.anim.anim_slide_out_left);

        context = getApplicationContext();
        currentapiVersion = android.os.Build.VERSION.SDK_INT;

        toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        sp = getSharedPreferences("themeColor", Context.MODE_PRIVATE);
        if(sp.getInt("expressColor",0)!=0) {
            int[] demo_colors = getResources().getIntArray(R.array.demo_colors);
            int[] demo_colors_mat = getResources().getIntArray(R.array.demo_colors_mat);

            ArrayList<String> colorString = new ArrayList<String>();
            ArrayList<String> colorString_mat = new ArrayList<String>();

            for (int i = 0; i < demo_colors.length; i++) {

                colorString.add(String.valueOf(demo_colors[i]));

            }

            for (int i = 0; i < demo_colors_mat.length; i++) {

                colorString_mat.add(String.valueOf(demo_colors_mat[i]));

            }
            Log.e("position ", colorString.indexOf(String.valueOf(sp.getInt("expressColor", 0))) + "");
            int color_mat_val = Integer.parseInt(colorString_mat.get(colorString.indexOf(String.valueOf(sp.getInt("expressColor", 0)))));


            toolbar.setBackgroundColor(sp.getInt("expressColor",0));

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(color_mat_val);
            }
        }else{
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(getResources().getColor(R.color.colorExpressStatus));
            }
        }

     // Database Connection Open
	     dbHelper = new DatabaseAdapterEx(this);
         dbHelper.open();
	      
	      oslist = new ArrayList<HashMap<String, String>>();
          Btngetdata = (Button)findViewById(R.id.getdata);
	        
	        train_name = (TextView)findViewById(R.id.train_name);
	        Source_station = (TextView)findViewById(R.id.Source_station);
        CurrentPostion = (TextView)findViewById(R.id.current_postion);
	        //destination_station = (TextView)findViewById(R.id.destination_station);
	        //train_status = (TextView)findViewById(R.id.train_status);
	        
	        extras = getIntent().getExtras();
		      if(extras !=null) {
		    	  trainno = extras.getString("trainno");
		    	  dayofjourney = extras.getString("dayofjourney");
				}
		      
          /* TRacking Code */
		      if(currentapiVersion>=android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH)
		        {
		      Tracker t = ((GoogleAnalyticsApp) getApplication()).getTracker(TrackerName.APP_TRACKER);
				t.setScreenName(trainno+"- Running Status");
				t.send(new HitBuilders.AppViewBuilder().build());
		        }
		      /*ImageView  	menuback =(ImageView)findViewById(R.id.close);
		        menuback.setOnClickListener(new View.OnClickListener() {
		        	
		            @Override
		            public void onClick(View v) {
		                // TODO Auto-generated method stub
		           	 
		         	finish();
		            	
		            	 //overridePendingTransition(R.anim.lefttoright, R.anim.righttoleft);
		            }
		        });*/
	      /// Train Info retrireve
	      
	     
	    	   traincursor = dbHelper.getTrainInfo(trainno);   
	       
	    
	    
	      listOfTrain = new ArrayList<Train>();
	      String train_name_val = null;
	      String start_station = null;
	      String end_station = null;
	      Integer valid_name =0;
	      String train_no;
	      
	     
	      if(traincursor.getCount()>0)
	 	  {	 
				while (traincursor.moveToNext()) {
					
					
					
					/*Integer train_no = traincursor.getInt(traincursor.getColumnIndexOrThrow("train_no"));
	 				
					train_name_val = traincursor.getString(traincursor.getColumnIndexOrThrow("train_name"));*/
					
					
	 				/* start_station = traincursor.getString(traincursor.getColumnIndexOrThrow("start_station"));
	 				 end_station = traincursor.getString(traincursor.getColumnIndexOrThrow("end_station"));*/
	 				 
	 				int trainId = traincursor.getInt(traincursor.getColumnIndexOrThrow("_id"));
	 				train_no = traincursor.getString(traincursor.getColumnIndexOrThrow("trainNO"));
	  				
	 				train_name_val = traincursor.getString(traincursor.getColumnIndexOrThrow("trainName"));
	 				 start_station= dbHelper.GetSourceStation(trainId);
	 				end_station = dbHelper.GetDestinationStation(trainId);
	 				
	 				valid_name =1;
	 				
				}
	 	  }
	     
	      if(valid_name==1)
	      {
	      train_name.setText(train_name_val);
		  Source_station.setText(start_station+ " To "+end_station);


	      /*Source_station.setText("Source : "+start_station);
	      destination_station.setText("Destination : "+end_station);*/
	      }
	      
      	SimpleDateFormat valdf = new SimpleDateFormat("yyyyMMdhms");
      	
      	Calendar cal=Calendar.getInstance();
          int currentDay=cal.get(Calendar.DAY_OF_MONTH);
          // today
          currentDateandTime = valdf.format(cal.getTime());
          
	      url = "http://www.matainja.com/androidApp/Api/v3/livetrain.php?trainno="+trainno+"&dayofjourney="+dayofjourney+"&time="+currentDateandTime;
	      
	     Log.e("url",url);
	       cd = new ConnectionDetector(getApplicationContext());
	                
	              // Check if Internet present
	              if (!cd.isConnectingToInternet()) {
	                  // Internet Connection is not present
	            	 
	            	  AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(
	            		        Running.this);

	            		// Setting Dialog Title
	            		alertDialog2.setTitle("Problems");

	            		// Setting Dialog Message
	            		alertDialog2.setMessage(error_msg);

	            		// Setting Icon to Dialog
	            		

	            		// Setting Positive "Yes" Btn
	            		alertDialog2.setNeutralButton("OK",
	            		        new DialogInterface.OnClickListener() {
	            		            public void onClick(DialogInterface dialog, int which) {
	            		                // Write your code here to execute after dialog
	            		            	// overridePendingTransition(R.anim.lefttoright, R.anim.righttoleft);
	            		            	finish();
	            		                
	            		            }
	            		        });
	            		// Setting Negative "NO" Btn
	            		

	            		// Showing Alert Dialog
	            		alertDialog2.show();
	                  return;
	              }
	             
        new JSONParse().execute();
	           
       
        
        
    }
    public void NotFound()
    {
  	
  	  AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(
 		        Running.this);

 		// Setting Dialog Title
 		alertDialog2.setTitle("Error");

 		// Setting Dialog Message
 		alertDialog2.setMessage(error_msg);

 		// Setting Icon to Dialog
 		

 		// Setting Positive "Yes" Btn
 		alertDialog2.setNeutralButton("OK",
 		        new DialogInterface.OnClickListener() {
 		            public void onClick(DialogInterface dialog, int which) {
 		                // Write your code here to execute after dialog
 		            
 		            	 //overridePendingTransition(R.anim.lefttoright, R.anim.righttoleft);
 	                    finish();
 	                 

 		                
 		            }
 		        });
 		alertDialog2.show();
 		
    } 
   
    private class JSONParse extends AsyncTask<String, String, JSONObject> {
         private ProgressDialog pDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
             source = (TextView)findViewById(R.id.source);
             arrival = (TextView)findViewById(R.id.arrival);

            pDialog = new ProgressDialog(Running.this);
            pDialog.setMessage("Getting Train status ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected JSONObject doInBackground(String... args) {
            HttpConnection con = new HttpConnection();
            JSONObject json = con.getExpressStatus(trainno,dayofjourney,currentDateandTime);
            return json;
        }
         @Override
         protected void onPostExecute(JSONObject json) {
             pDialog.dismiss();
            // Log.v("OnPostExecute","OnPostExecute");
             
             if(json==null)
             {
            	 NotFound(); 
             }
             else
             {

          try {
              String CurrentPos="";
        	  if(json.has("ResponseCode") )
              {
        		  String responseCode = json.getString("ResponseCode");
                  if(json.has("CurrentPosition")) {
                       CurrentPos = json.getString("CurrentPosition");
                      CurrentPostion.setText(CurrentPos);
                  }

              
        		  if(responseCode.equals("200")  )
      			  {
        			  
        			  String position = json.getString("CurrentPosition");
                      if(json.has("CurrentStation")) {
                          JSONObject current_station = json.getJSONObject("CurrentStation");

                          String curr_station_name =current_station.getString("StationName");
                          String curr_station_arriavl = current_station.getString("ActualArrival");
                          //CurrentPostion.setText(CurrentPos+" "+curr_station_name+" Arrival at ("+curr_station_arriavl+")");
                      }

        			  //train_status.setText("#"+json.getString("train_number")+" " +position);
        			  Log.e("ll",position);
        			  trainroute = json.getJSONArray("TrainRoute");
                    // Getting JSON Array from URL
        	  if(trainroute.length()>0)
        	  {
                    for(int i = 0; i < trainroute.length(); i++){
                    JSONObject c = trainroute.getJSONObject(i);

                       // Log.e("jsonobject",c.toString());
                    
                   
                    //JSONObject stationObj = c.getJSONObject("station_");

                        //Log.e("jsonobject==",stationObj.toString());
                    /*{"day": 0, "actarr": "00:00", "has_departed": false, "schdep": "23:55", "status": "0 mins late",
                    	"scharr": "Source", "distance": 0, "no": 1, "actarr_date": "12 May 2016", "latemin": 0, 
                    	"scharr_date": "12 May 2016","actdep": "23:55", "station": "KLK", "has_arrived": false, 
                    	"station_": {"code": "KLK", "name": "KALKA"}
                    }*/
                    String scode =  c.getString("StationCode");
                    String sname = c.getString("StationName");
                    String mday = c.getString("Day");
                    String mactarr = c.getString("ActualArrival");
                    String mhas_departed = c.getString("IsDeparted");
                    String mschdep = c.getString("ScheduleDeparture");
                    String mstatus = c.getString("Status");
                    String mscharr_date = c.getString("ScheduleArrival_Date");
                    String mactdep = c.getString("ActualDeparture");
                    String mhas_arrived = c.getString("IsArrived");
                    String mdistance = c.getString("Distance");
                    String mscharr = c.getString("ScheduleArrival");
                    String has_arrived = c.getString("IsArrived");
                    String has_departed = c.getString("IsDeparted");
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("station_code", scode);
                    map.put("station_name", sname);
                    map.put("day", mday);
                    map.put("actarr", mactarr);
                    map.put("schdep", mschdep);
                    map.put("status", mstatus);
                    map.put("scharr_date", mscharr_date);
                    map.put("has_arrived", mhas_arrived);
                    map.put("actdep", mactdep);
                     map.put("distance", mdistance);
                     map.put("scharr", mscharr);
                     map.put("has_arrived", has_arrived);
                     map.put("has_departed", has_departed);
                     
                    oslist.add(map);
                    
                   // Log.e("kkkkkkkk",scode+"=="+sname);
                    // Storing  JSON item in a Variable
                    }
                    
                    list=(RecyclerView)findViewById(R.id.list);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(Running.this);
                    list .setLayoutManager(mLayoutManager);
                    list.setNestedScrollingEnabled(false);

                    RunningRecyclerViewAdapter adapter = new RunningRecyclerViewAdapter(context, oslist);
                    list.setAdapter(adapter);
                    //Utility.setListViewHeightBasedOnChildren(list);
        	  }
                    }
                    else
                    {
                    	 String msglert="Sorry!! Please Try Again later either Server not responding or not fetched The data for Given train no and date";
       		    	  NotificationAlert(msglert);	
                   
                    }
      			}
              }
            catch (JSONException e) {
                e.printStackTrace();
            } 
             
         }
    }
    }
    
    public void NotificationAlert(String Message)
    {
  	
  	  AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(
 		        Running.this);

 		// Setting Dialog Title
 		alertDialog2.setTitle("Message");

 		// Setting Dialog Message
 		alertDialog2.setMessage(Message);

 		// Setting Icon to Dialog
 		

 		// Setting Positive "Yes" Btn
 		alertDialog2.setNeutralButton("OK",
 		        new DialogInterface.OnClickListener() {
 		            public void onClick(DialogInterface dialog, int which) {
 		                // Write your code here to execute after dialog
 		            
 		            	
 	                 finish();

 		                
 		            }
 		        });
 		alertDialog2.show();
 		
    } 
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        
        
        finish();
        overridePendingTransition(R.anim.anim_slide_in_right,
                R.anim.anim_slide_out_right);
    }
    
    @Override
    public void onDestroy()
    {
        super.onDestroy();
       dbHelper.close();
       traincursor.close();
    }
    
}