package com.matainja.runingstatus;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.matainja.runingstatus.Adapter.PassengerListAdapter;
import com.matainja.runingstatus.Common.ConnectionDetector;
import com.matainja.runingstatus.Connectivity.HttpConnection;
import com.matainja.runingstatus.CustomView.Utility;
import com.matainja.runingstatus.Database.DatabaseAdapter;
import com.matainja.runingstatus.Database.DatabaseAdapterSaveSearch;
import com.matainja.runingstatus.GoogleAnalyticsApp.TrackerName;
import com.matainja.runingstatus.Model.Train;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;




public class Pnrstatus extends AppCompatActivity {
	ListView list;
    TextView source;
    TextView arrival;
    TextView departure;
    TextView status;
    Button Btngetdata;
    TextView constatus ;
    ArrayList<HashMap<String, String>> oslist = new ArrayList<HashMap<String, String>>();
    //URL to get JSON Array
    private static String url = "";
    //JSON Node Names
     static final String TAG_status = "ResponceCode";
     static final String TAG_data = "data";
     static final String TAG_train_name = "TrainName";
     
   SharedPreferences sp;
    Bundle extras;
    String pnrno;
    String dayofjourney;
    ConnectionDetector cd;
    private DatabaseAdapter dbHelper;
    Cursor traincursor;
    ArrayList<Train> listOfTrain;
    TextView train_name;
    TextView journey_date;
    TextView journey_class;
    TextView train_no,train_chart_label;
    
    TextView train_name_label;
    TextView journey_date_label;
    TextView journey_class_label;
    TextView train_no_label;
    
    
    TextView tckt_from;
    TextView tckt_to;
    TextView broading_from;
    TextView broading_to;
    
    
    TextView tckt_from_label;
    TextView tckt_to_label;
    TextView broading_from_label;
    TextView broading_to_label;
    
    TextView from_to_label_header;
    TextView board_to_label_header;
    
    // json val declare
    ScrollView pnrlayout;
    String json_train_name ;
    String json_train_number ;
    JSONObject json_from ;
    String json_from_name ;
    String json_from_code ;
    JSONArray json_to ;
    String json_to_name ;
    String json_to_code ;
    JSONArray json_board;
    String json_board_name ;
    String json_board_code ;
    String seat_class;
    String travel_date;
    JSONArray json_passenger;
    String Seat_status;
    String seat_number;
    Typeface custom_font;
    java.util.Date convertedDate;
    private DatabaseAdapterSaveSearch dbHelpersave;
    int currentapiVersion=0;
    ArrayList<HashMap<String, String>> plist = new ArrayList<HashMap<String, String>>();
    Toolbar toolbar;
    RelativeLayout list_lay;
    CardView cardLay;
    String error_msg="Try again Connection problems!!";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	//Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pnr_status);
        overridePendingTransition(R.anim.anim_slide_in_left,
                R.anim.anim_slide_out_left);
        cardLay = (CardView) findViewById(R.id.cardLay);
       // Log.v("start activity","ooo");
        sp = getSharedPreferences("themeColor", Context.MODE_PRIVATE);

        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorExpressStatus));
        }*/

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        Handler handler = new Handler();
        Runnable r = new Runnable() {
            public void run() {
            	 /* TRacking Code */
                currentapiVersion = android.os.Build.VERSION.SDK_INT;
                if(currentapiVersion>=android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH)
                {
        	      Tracker t = ((GoogleAnalyticsApp) getApplication()).getTracker(TrackerName.APP_TRACKER);
        			t.setScreenName("PNR Status");
        			t.send(new HitBuilders.AppViewBuilder().build());

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            AdView mAdView = (AdView) findViewById(R.id.adView);
                            AdRequest adRequest = new AdRequest.Builder().build();
                            mAdView.loadAd(adRequest);
                        }
                    });
                }      
            }
        };
        handler.postDelayed(r, 100);
       
        
     // Database Connection Open
	    // dbHelper = new DatabaseAdapter(this);
	     // dbHelper.open();
	      
      //Database Connection Open
        dbHelpersave = new DatabaseAdapterSaveSearch(this);
        dbHelpersave.open();
        
        //pnrlayout = (ScrollView) findViewById(R.id.pnrlayout);
        train_chart_label = (TextView) findViewById(R.id.train_chart_label);
        train_name = (TextView) findViewById(R.id.train_name);
        journey_date = (TextView) findViewById(R.id.jouney_date);
        journey_class = (TextView) findViewById(R.id.jouney_class);
        train_no = (TextView) findViewById(R.id.train_no);
        
        train_name_label = (TextView) findViewById(R.id.train_name_label);
        journey_date_label = (TextView) findViewById(R.id.jouney_date_label);
        journey_class_label = (TextView) findViewById(R.id.jouney_class_label);
        train_no_label = (TextView) findViewById(R.id.train_no_label);
        
        tckt_from = (TextView) findViewById(R.id.tckt_from);
        tckt_to = (TextView) findViewById(R.id.tckt_to);
        tckt_from_label = (TextView) findViewById(R.id.tckt_from_label);
        tckt_to_label = (TextView) findViewById(R.id.tckt_to_label);
        
        broading_from = (TextView) findViewById(R.id.broading_from);
       
        //broading_from_label = (TextView) findViewById(R.id.broading_from_label);
        
        //from_to_label_header = (TextView) findViewById(R.id.journey_from_header);
        board_to_label_header = (TextView) findViewById(R.id.journey_broading_from_header);
        TextView status_code_info = (TextView) findViewById(R.id.status_code_info);
      //  String codeinfo = getString(R.string.tckt_status_code_info);
       // status_code_info.setText(Html.fromHtml(codeinfo));

        list_lay = (RelativeLayout) findViewById(R.id.list_lay);
	         
	         
        ImageView  	menuback =(ImageView)findViewById(R.id.close);


        if(sp.getInt("expressColor",0)!=0) {
            toolbar.setBackgroundColor(sp.getInt("expressColor",0));
            train_chart_label.setTextColor(sp.getInt("expressColor",0));
            status_code_info.setTextColor(sp.getInt("expressColor",0));

            int[] demo_colors = getResources().getIntArray(R.array.demo_colors);
            int[] demo_colors_mat = getResources().getIntArray(R.array.demo_colors_mat);

            ArrayList<String> colorString = new ArrayList<String>();
            ArrayList<String> colorString_mat = new ArrayList<String>();

            for (int i = 0; i < demo_colors.length; i++) {

                colorString.add(String.valueOf(demo_colors[i]));

            }

            for (int i = 0; i < demo_colors_mat.length; i++) {

                colorString_mat.add(String.valueOf(demo_colors_mat[i]));

            }
            Log.e("position ",colorString.indexOf(String.valueOf(sp.getInt("expressColor",0))) + "");
            int color_mat_val = Integer.parseInt(colorString_mat.get(colorString.indexOf(String.valueOf(sp.getInt("expressColor", 0)))));

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(color_mat_val);
            }
        }else{
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(getResources().getColor(R.color.colorExpressStatus));
            }
        }

        /*menuback.setOnClickListener(new View.OnClickListener() {
               	
                   @Override
                   public void onClick(View v) {
                       // TODO Auto-generated method stub
                  	 //overridePendingTransition(R.anim.lefttoright, R.anim.righttoleft);
                  	Intent intent = new Intent(getApplicationContext(), Pnr.class);
                	
                	startActivity(intent);
                	finish();
                   	
                   	 //overridePendingTransition(R.anim.lefttoright, R.anim.righttoleft);
                   }
               });*/
	        
            
            
	          
	        extras = getIntent().getExtras();
		      if(extras !=null) {
		    	  pnrno = extras.getString("pnrno");
		    	 
				}
	      
	      
		      
		    //Log.v("url",url);
		       cd = new ConnectionDetector(getApplicationContext());
		                
		              // Check if Internet present
		              if (!cd.isConnectingToInternet()) {
		                  // Internet Connection is not present

                          cardLay.setVisibility(View.GONE);
		            	  AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(
		            		        Pnrstatus.this);

		            		// Setting Dialog Title
		            		alertDialog2.setTitle("Problems");

		            		// Setting Dialog Message
		            		alertDialog2.setMessage(error_msg);

		            		// Setting Icon to Dialog
		            		

		            		// Setting Positive "Yes" Btn
		            		alertDialog2.setNeutralButton("OK",
		            		        new DialogInterface.OnClickListener() {
		            		            public void onClick(DialogInterface dialog, int which) {
		            		                // Write your code here to execute after dialog
		            		            	// overridePendingTransition(R.anim.lefttoright, R.anim.righttoleft);
		            		            	finish();
		            		                
		            		            }
		            		        });
		            		// Setting Negative "NO" Btn
		            		

		            		// Showing Alert Dialog
		            		alertDialog2.show();
		                  return;
		              }
	             
        new JSONParse().execute();
	           
       
        
        
    }
    private class JSONParse extends AsyncTask<String, String, JSONObject> {
         private ProgressDialog pDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
             
            pDialog = new ProgressDialog(Pnrstatus.this);
            pDialog.setMessage("Getting Pnr status ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }
        @Override
        protected JSONObject doInBackground(String... args) {
        	HttpConnection searchFunctions = new HttpConnection();
	        JSONObject json = searchFunctions.PnrStatusRail(pnrno);

	            return json;
        }
         @Override
         protected void onPostExecute(JSONObject json) {
             pDialog.dismiss();
             String jsonStatus="";
             String from_to ="";
//          Log.e("json", json.toString());
    
          if(json !=null)
          {
          if(json.has("ResponceCode"))
          {
        	  String status;


			try {
				status = json.getString(TAG_status);

				if(status.equals("200") )
    			{
					
					//JSONObject data = json.getJSONObject("data");
                    json_train_number = json.getString("TrainNo");
                   
                    json_train_name = json.getString(TAG_train_name);
                    
                    travel_date = json.getString("DateOfJourny");
                    String dateString = travel_date;
                    
                    
                  
                    seat_class = json.getString("ClassType");
                   // Log.v("Train No +++", json_train_number+"jj="+travel_date+seat_class);
                    
                    train_name.setText(json_train_name);
                    journey_date.setText(travel_date);
                    train_no.setText(json_train_number);
                    journey_class.setText(seat_class);
                    
                   
                    
                    JSONObject from_object = json.getJSONObject("FromStation");
                    json_from_name = from_object.getString("Name");
                    json_from_code = from_object.getString("Code");
                    tckt_from.setText(json_from_name + " ( "+ json_from_code + " )");
                    
                    JSONObject to_object = json.getJSONObject("ToStation");
                    json_to_name = to_object.getString("Name");
                    json_to_code = to_object.getString("Code");
                    
                    tckt_to.setText(json_to_name + "( "+ json_to_code + " )");
                    
                    
                    JSONObject board_object = json.getJSONObject("BoardingStation");
                    json_board_name = board_object.getString("Name");
                    json_board_code = board_object.getString("Code");
                    
                    broading_from.setText(json_board_name + " ( "+ json_board_code + " )");
                    
               
                String Chart_prepare = json.getString("ChartPrepared");
                                   
                

                

                   
                    
                   
                
                    json_passenger = json.getJSONArray("PassengersList");
                    list=(ListView)findViewById(R.id.list);
                    list_lay.setVisibility(View.VISIBLE);
                    for(int passenger_i = 0; passenger_i < json_passenger.length(); passenger_i++){
                        JSONObject passenger_object = json_passenger.getJSONObject(passenger_i);
                        seat_number = passenger_object.getString("BookingStatus");
                        Seat_status = passenger_object.getString("CurrentStatus");
                        HashMap<String, String> map = new HashMap<String, String>();
                        String pNo = (passenger_i+1) + "";
                        map.put("passenger_no", pNo);
                        map.put("seat_number", seat_number);
                        map.put("status", Seat_status);
                       
                        plist.add(map);
                       
                        PassengerListAdapter adapter = new PassengerListAdapter(Pnrstatus.this, plist);
                        list.setAdapter(adapter);
                    }
                    Utility.setListViewHeightBasedOnChildren(list);
                    if(Chart_prepare.equals("N") || Chart_prepare.equals("n") )
                 	   train_chart_label.setText("Chart not prepared"); 
                    else
                 	   train_chart_label.setText("Chart  prepared");  
                   
                     
                    //pnrlayout.setVisibility(View.VISIBLE);
                    //pnrlayout.smoothScrollTo(0,0);
                    from_to = json_from_name+"-"+json_to_name;
                    
                    boolean hh1 = dbHelpersave.InsertPNRSave(pnrno,from_to);
    		
    			}
				else {
                    if (json.has("Message"))
                        error_msg = json.getString("Message");
                    errorBox();
                }
				
			} catch (JSONException e) {
				errorBox();
			}
              
              
              
        	
        	  
          }
          else
        	  errorBox();
          }
          else
        	  errorBox();  
          
          
		  Log.v("Data status3", "p=sshhh");     
		  // Getting JSON Array from URL 
		  
		  
		 
         }
    }
    
    public void  errorBox()
	  {
		  Log.v("status", "no");
    	// Internet Connection is not present
  	  AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Pnrstatus.this);
 
			// set title
			alertDialogBuilder.setTitle("Error");
 
			// set dialog message
			alertDialogBuilder
				.setMessage(error_msg)
				.setCancelable(false)
				.setPositiveButton("OK",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						// if this button is clicked, close
						// current activity
						//MainActivity.this.finish();
						Pnrstatus.this.finish();
					}
				  });
				
 
				// create alert dialog
				AlertDialog alertDialog = alertDialogBuilder.create();
 
				// show it
          try {
              alertDialog.show();
          }catch(Exception e){

          }
				
	  }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.anim_slide_in_right,
                R.anim.anim_slide_out_right);
    }
   
}