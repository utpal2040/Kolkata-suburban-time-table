
package com.matainja.runingstatus;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.matainja.runingstatus.Common.ConnectionDetector;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;


public class BuildWebsite extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
	
	Context context;
	String versionName;
	int currentapiVersion=0;
	private WebView webView;
	private ProgressDialog progressDialog;
    ConnectionDetector cd;
    DrawerLayout drawer;
    SharedPreferences sp;
    CircleImageView profile_image;
    TextView nameTxt;
    RelativeLayout headerFrame;
    String email;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	//Remove title bar
        //this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.buildwebsite);
        overridePendingTransition(R.anim.anim_slide_in_left,
                R.anim.anim_slide_out_left);
        context = getApplicationContext();

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        sp = getSharedPreferences("themeColor",MODE_PRIVATE);

        if(sp.getInt("localColor",0) !=0){
            toolbar.setBackgroundColor(sp.getInt("localColor",0));

            int[] demo_colors = getResources().getIntArray(R.array.demo_colors);
            int[] demo_colors_mat = getResources().getIntArray(R.array.demo_colors_mat);

            ArrayList<String> colorString = new ArrayList<String>();
            ArrayList<String> colorString_mat = new ArrayList<String>();

            for (int i = 0; i < demo_colors.length; i++) {

                colorString.add(String.valueOf(demo_colors[i]));

            }

            for (int i = 0; i < demo_colors_mat.length; i++) {

                colorString_mat.add(String.valueOf(demo_colors_mat[i]));

            }
            Log.e("position ",colorString.indexOf(String.valueOf(sp.getInt("localColor",0))) + "");
            int color_mat_val = Integer.parseInt(colorString_mat.get(colorString.indexOf(String.valueOf(sp.getInt("localColor", 0)))));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(color_mat_val);
            }
        }else{
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(getResources().getColor(R.color.colorExpressStatus));
            }
        }



        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View headerview = navigationView.getHeaderView(0);
        profile_image = (CircleImageView) headerview.findViewById(R.id.profile_image);
        nameTxt = (TextView) headerview.findViewById(R.id.name);
        headerFrame = (RelativeLayout) headerview.findViewById(R.id.headerFrame);
        headerFrame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(email.equalsIgnoreCase("")){
                    Intent i = new Intent(BuildWebsite.this, WelcomeActivity.class);
                    startActivity(i);
                    finish();
                }else {
                    Intent i = new Intent(BuildWebsite.this, SettingsActivity.class);
                    startActivity(i);
                }
            }
        });

        webView = (WebView) findViewById(R.id.mybooking_webView);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setVerticalScrollBarEnabled(true);
        webView.setHorizontalScrollBarEnabled(true);
        webView.requestFocusFromTouch();

      //  webView.addJavascriptInterface(new MybookingJavaScriptProxy(this), "androidAppProxy");
        progressDialog = ProgressDialog.show(this, "", "Please Wait ...");
        webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {              
                view.loadUrl(url);
                return true;
            }
            
            @Override
            public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
                if (progressDialog!=null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                    progressDialog = null;
                }
            }
            
            @SuppressWarnings("deprecation")
			@Override
            public void onReceivedError(WebView view, int errorCode,String description, String failingUrl) {
                view.loadUrl("about:blank");
                
                AlertDialog alertDialog = new AlertDialog.Builder(context).create();
 
		        // Setting Dialog Title
		        alertDialog.setTitle("Loading Problems");
		 
		        // Setting Dialog Message
		        alertDialog.setMessage("Something is wrong!! server not connected");
		 
		        // Setting Icon to Dialog
		        alertDialog.setIcon(R.drawable.appsicon);
		 
		        // Setting OK Button
		        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
		                public void onClick(DialogInterface dialog, int which) {
		                    // Write your code here to execute after dialog closed
		                	
							finish();
			                }
		        });
		 
		        // Showing Alert Message
                try {
                    alertDialog.show();
                }catch(Exception e){

                }
                super.onReceivedError(view, errorCode, description, failingUrl);
            }
            
        }); 
        webView.loadUrl("http://www.matainja.com/androidApp/kk0592/new/buildwebsite.php");
        
        /////////////////////////////////////////////////
        
        /*ImageView  	menuback =(ImageView)findViewById(R.id.close);
        menuback.setOnClickListener(new View.OnClickListener() {
        	
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
           	 //overridePendingTransition(R.anim.lefttoright, R.anim.righttoleft);
           	Intent intent = new Intent(getApplicationContext(), LocalTrain.class);
                intent.putExtra("callfrom","ExTrainTimetable");
         	startActivity(intent);
         	finish();
            	
            	 //overridePendingTransition(R.anim.lefttoright, R.anim.righttoleft);
            }
        });*/
        
       
        
       
        
	      // drawer define
	     
	      
	     
	     
	     /// end left menu list 
	     
	     
	     
	     
        
        
        
        
        
        // Auto suggest train search
        
       
        
        // drawer open
        
       
        
        
        
       
 
       // trainnotxt.setTypeface(custom_font);
      //  dayofjourney.setTypeface(custom_font);
       
        //Remove notification bar
        //this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    	//Set save button and action  onClick
        
        
      
        
      
       
/// list view End


        ApplyUserInfo();
        
        
    }

    public void ApplyUserInfo(){
        SharedPreferences userPref = getSharedPreferences("userCred",MODE_PRIVATE);
        String name = userPref.getString("name","Guest User");
        email = userPref.getString("email","");
        String imageUrl = userPref.getString("image_value","");

        Log.e("imageUrl ",imageUrl);
        nameTxt.setText(name);
        if(!imageUrl.equalsIgnoreCase("")) {
            Picasso.with(context).load(imageUrl).memoryPolicy(MemoryPolicy.NO_CACHE).into(profile_image);
        }
    }

    public void successAlert()
    {
  	
  	  AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(
 		        BuildWebsite.this);

 		// Setting Dialog Title
 		alertDialog2.setTitle("Success");

 		// Setting Dialog Message
 		alertDialog2.setMessage("Please check Your internet Connection");

 		// Setting Icon to Dialog
 		

 		// Setting Positive "Yes" Btn
 		alertDialog2.setNeutralButton("OK",
 		        new DialogInterface.OnClickListener() {
 		            public void onClick(DialogInterface dialog, int which) {
 		                // Write your code here to execute after dialog
 		            	
 		            	Intent intent = new Intent(getApplicationContext(), LocalTrain.class);
                        intent.putExtra("callfrom","other");
 		             	startActivity(intent);
 		             	finish();
 	                 

 		                
 		            }
 		        });
 		alertDialog2.show();
 		
    }
	
    // add items into spinner dynamically

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if(drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        }else {
            super.onBackPressed();
            Intent i = new Intent(BuildWebsite.this,LocalTrain.class);
            i.putExtra("callfrom","other");
            startActivity(i);
            finish();
            overridePendingTransition(R.anim.anim_slide_in_right,
                    R.anim.anim_slide_out_right);

        }
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the camera action

            Intent intent = new Intent(getApplicationContext(), LocalTrain.class);
            intent.putExtra("callfrom","other");
            startActivity(intent);
            finish();

        } else if (id == R.id.nav_runningstatus) {

            Intent intent = new Intent(getApplicationContext(), Homepage.class);
            startActivity(intent);
            finish();

        } else if (id == R.id.nav_cancelled) {

            Intent intent = new Intent(getApplicationContext(), ActivityCancelTrain.class);
            startActivity(intent);
            finish();

        }  else if (id == R.id.nav_pnrstatus) {

            Intent intent = new Intent(getApplicationContext(), Pnr.class);
            startActivity(intent);
            finish();

        } else if (id == R.id.nav_saveroute) {

            Intent intent = new Intent(getApplicationContext(), SaveSearch.class);
            startActivity(intent);
            finish();

        } else if (id == R.id.nav_feedback) {
            Intent intent = new Intent(getApplicationContext(), feedback.class);
            startActivity(intent);
            finish();

        } else if (id == R.id.nav_fblike) {

            Intent notificationIntent =null;
            notificationIntent = new Intent(Intent.ACTION_VIEW);
            notificationIntent.setData(Uri.parse("https://www.facebook.com/localtimetable"));
            startActivity(notificationIntent);


        } else if (id == R.id.nav_theme) {

            if(email.equalsIgnoreCase("")){
                Intent i = new Intent(BuildWebsite.this, WelcomeActivity.class);
                startActivity(i);
                finish();
            }else {
                Intent i = new Intent(BuildWebsite.this, SettingsActivity.class);
                startActivity(i);
            }


        } else if (id == R.id.nav_rate) {

            Intent notificationIntent =null;
            notificationIntent = new Intent(Intent.ACTION_VIEW);
            notificationIntent.setData(Uri.parse("market://details?id="+getApplicationContext().getPackageName()));
            startActivity(notificationIntent);


        } else if (id == R.id.nav_aboutus) {

            /*Intent intent = new Intent(getApplicationContext(), BuildWebsite.class);
            startActivity(intent);
            finish();*/
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}