package com.matainja.runingstatus.Database;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


public class DatabaseHelperSaveSearch extends SQLiteOpenHelper {
	private static final String DATABASE_NAME = "savesearch";
	private static String DB_PATH = "/data/data/com.matainja.runingstatus/databases/";
	
	private static final int DATABASE_VERSION = 200007;
	
	
	
	
	Context context;
	// Database creation sql statement
	/*private final String SQL_NEW_WINE="CREATE TABLE "+TABLE_WINE+" (id INTEGER PRIMARY KEY AUTOINCREMENT  NOT NULL ,name VARCHAR,producer VARCHAR DEFAULT Unknown,category " +
			"VARCHAR DEFAULT Unknown,scan_code_main VARCHAR,scan_code_2 VARCHAR,wine_year VARCHAR,can_age_year VARCHAR,initial_stock INTEGER,unit_cellar INTEGER," +
			"store_on DATETIME DEFAULT (CURRENT_DATE) ,location " +
			"VARCHAR DEFAULT Unknown,country VARCHAR DEFAULT Unknown,region " +
			"VARCHAR DEFAULT Unknown,sub_region VARCHAR DEFAULT Unknown,varietals VARCHAR DEFAULT Unknown,price DOUBLE," +
			"alcohol DOUBLE,rating FLOAT,photo_label_2 VARCHAR, photo_label_1 VARCHAR, photo_label_3 VARCHAR," +
			" personal_comment TEXT, boottle_empty INTEGER DEFAULT 0, favorite INTEGER DEFAULT 0," +
			"currency VARCHAR,volume VARCHAR,wine_date DATETIME,food_photo_url VARCHAR,ambience_photo_url " +
			"VARCHAR,friend_photo_url VARCHAR)"; */
			//AUTOINCREMENT

	 String SQl_save_pnr ="CREATE TABLE  IF NOT EXISTS savepnr (_id	INTEGER PRIMARY KEY AUTOINCREMENT," +
	 	   		"pnr TEXT,fromto TEXT )";
public DatabaseHelperSaveSearch(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		this.context = context;
		Log.v("Database","Database Helper Create"+DATABASE_NAME);
	}

	
	@Override
	public void onCreate(SQLiteDatabase database) {
		
		
 	   String SQl ="CREATE TABLE savesearch (_id	INTEGER PRIMARY KEY AUTOINCREMENT," +
 	   		"searhcname	TEXT,sourcestation	TEXT," +
 	   		"destinationstation	TEXT," +
 	   		"islocal INTEGER )";
 		Log.v("Sql==",""+SQl_save_pnr);
 	  database.execSQL(SQl);
 	 database.execSQL(SQl_save_pnr);
 	  
		
	}
	
	
	       

		//database.execSQL(SQL_WITHDRAW_BOTTLE);
		
		
	

	
	@Override
	public void onUpgrade(SQLiteDatabase database, int oldVersion,
			int newVersion) {
		
		database.execSQL(SQl_save_pnr);
		
		
	       
	}
	
	 
	

	 
	
	

}
