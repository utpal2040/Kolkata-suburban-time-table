package com.matainja.runingstatus.Database;

import java.util.ArrayList;
import java.util.Calendar;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.matainja.runingstatus.Model.LocalTrainAdapter;
import com.matainja.runingstatus.Model.RouteLocalTrain;
import com.matainja.runingstatus.Model.StationLocal;
import com.matainja.runingstatus.Model.SuggestStationBean;


public class DatabaseAdapter  {

	// Database fields
	static final String TABLE_TRAIN_NAME="train_name";
	static final String LTRAIN_NAME ="train_table";
	static final String LSTATION_NAME ="station_table";
	static final String LROUTE ="route_table";
	static final String LDBINFO ="db_info";
	static final String LSAVE_STATION ="save_station";
	
	
	private Context context;
	private SQLiteDatabase database;
	private DatabaseHelper dbHelper;

	public DatabaseAdapter(Context context) {
		this.context = context;
	}

	public DatabaseAdapter open() throws SQLException {
		dbHelper = new DatabaseHelper(context);
		database = dbHelper.getWritableDatabase();
		
		
		
		return this;
	}

public Cursor getAllTrain() {
		
	
		
		return database.rawQuery("SELECT * FROM " + TABLE_TRAIN_NAME + " where 1  order by visited desc LIMIT 0, 80", null);
		
	}

public Cursor getTrainInfo(String trainno) {
	
	
	trainno = replace(trainno);
	return database.rawQuery("SELECT * FROM " + TABLE_TRAIN_NAME + " where train_no="+ trainno +"  order by visited desc LIMIT 0, 1", null);
	
}

public void UpdateVisitedTrain(String trainNo) {
	
	
	//Log.v("lll","UPDATE  " + TABLE_TRAIN_NAME + " set visited=visited+1 where train_no ="+ trainNo);
	database.execSQL("UPDATE  " + TABLE_TRAIN_NAME + " set visited=visited+1 where train_no ="+trainNo);
	
	
}
	
public Cursor getAllTrainSearch(String keywords) {
	keywords = replace(keywords);
	
	
	return database.rawQuery("SELECT *,replace(train_name, ' ', '') as tname FROM " + TABLE_TRAIN_NAME + " where tname LIKE '%" + keywords +"%'   LIMIT 0, 80", null);
	
}

public void TruncateStationTable()
{
	database.execSQL("DELETE FROM "+ LSTATION_NAME);	
}

public void TruncateTrainTable()
{
	database.execSQL("DELETE FROM "+ LTRAIN_NAME);	
}
public void TruncateRouteTable()
{
	database.execSQL("DELETE FROM "+ LROUTE);	
}


public boolean InsertStationName(StationLocal station) {

    boolean createSuccessful = false;

    ContentValues values = new ContentValues();

  //  values.put(KEY_ID, information.getId());
    values.put("_id", station.getId());
//Log.v("_ID", station.getStationCode());
//Log.v("stationCode", station.getStationCode());
    values.put("stationCode", ""+station.getStationCode()+"");
    values.put("stationName", ""+station.getStationName()+"");
    values.put("popularity", station.getPopularity());

    createSuccessful = database.insert(LSTATION_NAME, null, values) > 0;
    //database.close();

    return createSuccessful;
}


public boolean InsertStationSave(String fromstation,String tosation) {

    boolean createSuccessful = false;

    ContentValues values = new ContentValues();

  //  values.put(KEY_ID, information.getId());
   
//Log.v("_ID", station.getStationCode());
//Log.v("stationCode", station.getStationCode());
    values.put("fromstation", ""+fromstation+"");
    values.put("tostation", ""+tosation+"");
   

    createSuccessful = database.insert(LSAVE_STATION, null, values) > 0;
    //database.close();

    return createSuccessful;
}

public boolean InsertTrainName(LocalTrainAdapter Train) {
	/*private final String train_table = "CREATE TABLE train_table(_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
	"trainNO text, trainName text,sun boolean,mon boolean,tue boolean,wed boolean,thu boolean,fri boolean,sat " +
	"boolean, UNIQUE (trainNO));";*/
    boolean createSuccessful = false;

    ContentValues values = new ContentValues();

  //  values.put(KEY_ID, information.getId());
    values.put("_id", Train.getId());
//Log.v("_ID", Train.gettrainName());
//Log.v("stationCode", Train.gettrainNO());
    values.put("trainNO", ""+Train.gettrainNO()+"");
    values.put("trainName", ""+Train.gettrainName()+"");
    values.put("sun", Train.getSun());
    values.put("mon", Train.getMon());
    values.put("tue", Train.getTue());
    values.put("wed", Train.getWed());
    values.put("thu", Train.getThu());
    values.put("fri", Train.getFri());
    values.put("sat", Train.getSat());
    

    createSuccessful = database.insert(LTRAIN_NAME, null, values) > 0;
    //database.close();

    return createSuccessful;
}

public boolean InsertTrainRoute(RouteLocalTrain TrainR) {
	/*private final String train_table = "CREATE TABLE train_table(_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
	"trainNO text, trainName text,sun boolean,mon boolean,tue boolean,wed boolean,thu boolean,fri boolean,sat " +
	"boolean, UNIQUE (trainNO));";*/
    boolean createSuccessful = false;

    ContentValues values = new ContentValues();

  //  values.put(KEY_ID, information.getId());
    values.put("_id", TrainR.getId());
//Log.v("_ID", ""+TrainR.getId());
//Log.v("stationCode", ""+TrainR.getTrainId());
    values.put("trainId", TrainR.getTrainId());
    values.put("stationId", TrainR.getStationId());
    values.put("arrival", TrainR.getArrival());
    values.put("datePlus", TrainR.getDatePlus());
    

    createSuccessful = database.insert(LROUTE, null, values) > 0;
    //database.close();

    return createSuccessful;
}

public Cursor getAllStation(String keywords) {
	keywords = replace(keywords);
	
	String Cond = "";
	if(keywords!= null)
		Cond ="AND sname  LIKE '" + keywords +"%' OR stationCode LIKE '" + keywords +"%'";
	//Log.v("Sql","SELECT * FROM " + LSTATION_NAME + " where 1  "+ Cond + "   LIMIT 0, 80");
	String Station_sql = "SELECT *,replace(stationName, ' ', '') as sname FROM " + LSTATION_NAME + " where 1  "+ Cond + "  order by popularity desc  LIMIT 0, 80";
	Log.e("station SQL",Station_sql);
	return database.rawQuery(Station_sql, null);
	
}

public Cursor getLocalTrain(int trainId) {
	
	
	return database.rawQuery("SELECT * FROM " + LTRAIN_NAME + " where 1 AND  _id = "+ trainId , null);
	
}
  /*private final String route_table = "CREATE TABLE route_table(_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
		"trainId INTEGER,stationId INTEGER,arrival text,datePlus INTEGER, FOREIGN KEY (trainId) " +
		"REFERENCES train_table(_id), FOREIGN KEY (stationId) REFERENCES station_table(_id));";*/

public void UpdateVisitedStation(Integer id) {
	
	
	//Log.v("update sql","UPDATE  " + LSTATION_NAME + " set popularity=popularity+1 where _id ="+id);
	database.execSQL("UPDATE  " + LSTATION_NAME + " set popularity=popularity+1 where _id ="+id);
	
	
}

public void DatabaseUpdate(String sql)
{
	database.execSQL(sql);
}
public Cursor getTrainList(String fromstation,String tostation,String timefilter,int isfilterbytime) {
	
	fromstation = replace(fromstation);
	tostation = replace(tostation);
	
	String condition = "";
	if(isfilterbytime==1)
		condition =" and sourcearrivalsort>=StrFTime('"+timefilter+"')";

	Log.e("timefilter--",""+timefilter);
	String SQL_L = "select  "+
			"CASE WHEN CAST (StrFTime('%H', src.arrival)AS INTEGER) > 12 THEN (StrFTime('%H', src.arrival) - 12) || ':' || StrFTime('%M', src.arrival) ||"+
			"' pm' ELSE ( select CASE WHEN CAST ((StrFTime('%H', src.arrival) - 12) AS INTEGER) ==0 THEN (StrFTime('%H', src.arrival)) || ':' || StrFTime('%M', src.arrival) ||"+
			"' pm'ELSE ( SELECT CASE WHEN CAST (StrFTime('%H', src.arrival)AS INTEGER) = 0 THEN '12' || ':' || StrFTime('%M', src.arrival) || ' am' "+
			"ELSE CAST (StrFTime('%H', src.arrival) AS INTEGER) || ':' || StrFTime('%M', src.arrival) || ' am' END) END )END AS sourcearrival,"+
			
			"CASE WHEN CAST (StrFTime('%H', dest.arrival)AS INTEGER) > 12 THEN (StrFTime('%H', dest.arrival) - 12) || ':' || StrFTime('%M', dest.arrival) ||"+
			"' pm' ELSE ( select CASE WHEN CAST ((StrFTime('%H', dest.arrival) - 12) AS INTEGER) ==0 THEN (StrFTime('%H', dest.arrival)) || ':' || StrFTime('%M', dest.arrival) ||"+
			"' pm'ELSE ( SELECT CASE WHEN CAST (StrFTime('%H', dest.arrival)AS INTEGER) = 0 THEN '12' || ':' || StrFTime('%M', dest.arrival) || ' am' "+
			"ELSE CAST (StrFTime('%H', dest.arrival) AS INTEGER) || ':' || StrFTime('%M', dest.arrival) || ' am' END) END )END AS destarrival,"+
			
			"trn._id as trainId,trn.trainName,trn.trainNo as trainNo,trn.remarks as remarks,trn.sun,trn.mon,trn.tue,trn.wed,trn.thu,trn.fri,trn.sat," +
			"srcSt.stationName as SourceStation, srcSt.stationCode as sourcestationcode" +
			",src.arrival sourcearrivalsort,destSt.stationName as DestinationStation," +
			"(strftime('%Y%m%d%H%M%S' ,(datetime(datetime('now'),'+'||src.datePlus||' days','+'||src.arrival)))) as sarriable ,(strftime('%Y%m%d%H%M%S' ,(datetime(datetime('now'),'+'||dest.datePlus||' days','+'||dest.arrival)))) as darriable ," +
			" destSt.stationCode as destStationcode from route_table src inner join route_table dest on " +
			" src.trainId = dest.trainId inner join station_table srcSt on src.stationId = srcSt._id inner join station_table destSt on " +
			" dest.stationId = destSt._id inner join train_table trn on dest.trainId= trn._id where srcSt.stationCode= '"+fromstation+"' and destSt.stationCode = '"+tostation+"'" +
			" and sarriable < darriable "+condition+" order by sarriable asc";
	
	Log.e("Sql", SQL_L);
	
	
	
	
	
	return database.rawQuery(SQL_L, null);
	
	
}

public Cursor getStationList(int trainId) {
	
	String SQL_L = "SELECT   "+
			"CASE WHEN CAST (StrFTime('%H', arrival)AS INTEGER) > 12 THEN (StrFTime('%H', arrival) - 12) || ':' || StrFTime('%M', arrival) ||"+
			"' pm' ELSE ( select CASE WHEN CAST ((StrFTime('%H', arrival) - 12) AS INTEGER) ==0 THEN (StrFTime('%H', arrival)) || ':' || StrFTime('%M', arrival) ||"+
			"' pm'ELSE ( SELECT CASE WHEN CAST (StrFTime('%H', arrival)AS INTEGER) = 0 THEN '12' || ':' || StrFTime('%M', arrival) || ' am' "+
			"ELSE CAST (StrFTime('%H', arrival) AS INTEGER) || ':' || StrFTime('%M', arrival) || ' am' END) END )END AS arrival,"+
			"st.stationCode,st.stationName," +
			" (strftime('%Y-%m-%d %H:%M:%S' ,(datetime(datetime('now'),'+'||rt.datePlus||' days','+'||rt.arrival)))) sortarrival "+
			"FROM route_table rt,station_table st  " +
			
			"where 1 and rt.stationId=st._id and rt.trainId="+trainId+" order by  sortarrival asc";
	
	Log.e("Sql", SQL_L);
	
	
	return database.rawQuery(SQL_L, null);
	
	
	
}


	public String[] getStationStop(int trainId, String fromcode, String tocode) {

		int stops_loop=0;
		int noofstops=0;
		String sourcetime ="";
		String destime ="";

		/*String SQL_L = "select * from route_table where trainId = "+trainId+" AND " +
				"((stationId between (select _id from station_table where stationCode='"+fromcode+"') AND " +
				"(select _id from station_table where stationCode='"+tocode+"')) OR " +
				"(stationId between (select _id from station_table where stationCode='"+tocode+"') AND " +
				"(select _id from station_table where stationCode='"+fromcode+"')))";*/


		String SQL_L = "SELECT   "+
				"CASE WHEN CAST (StrFTime('%H', arrival)AS INTEGER) > 12 THEN (StrFTime('%H', arrival) - 12) || ':' || StrFTime('%M', arrival) ||"+
				"' pm' ELSE ( select CASE WHEN CAST ((StrFTime('%H', arrival) - 12) AS INTEGER) ==0 THEN (StrFTime('%H', arrival)) || ':' || StrFTime('%M', arrival) ||"+
				"' pm'ELSE ( SELECT CASE WHEN CAST (StrFTime('%H', arrival)AS INTEGER) = 0 THEN '12' || ':' || StrFTime('%M', arrival) || ' am' "+
				"ELSE CAST (StrFTime('%H', arrival) AS INTEGER) || ':' || StrFTime('%M', arrival) || ' am' END) END )END AS arrival,"+
				"st.stationCode,st.stationName," +
				" (strftime('%Y-%m-%d %H:%M:%S' ,(datetime(datetime('now'),'+'||rt.datePlus||' days','+'||rt.arrival)))) sortarrival "+
				"FROM route_table rt,station_table st  " +

				"where 1 and rt.stationId=st._id and rt.trainId="+trainId+" order by  sortarrival asc";




		Log.e("Sql STOPS", SQL_L);

/*select * from route_table where trainId = "+trainId+" AND " +
		"((stationId between (select _id from station_table where stationCode='"+fromcode+"') AND " +
				"(select _id from station_table where stationCode='"+tocode+"') */
		Cursor getStops = database.rawQuery(SQL_L, null);
		if(getStops.getCount()>0) {
			while (getStops.moveToNext()) {

				String stationCode  = getStops.getString(getStops.getColumnIndexOrThrow("stationCode"));
				String timestation  = getStops.getString(getStops.getColumnIndexOrThrow("sortarrival"));

				if(fromcode.equalsIgnoreCase(stationCode))
				{
					stops_loop=1;
					sourcetime =timestation;
				}


				if(stops_loop==1)
				noofstops++;

				if(tocode.equalsIgnoreCase(stationCode))
				{
					stops_loop=0;
					destime =timestation;
					break;
				}

			}
		}

		Log.e("times",sourcetime+"---"+destime);
		String[] response;
		response = new String[3];
		response[0]=String.valueOf(noofstops);
		response[1]=sourcetime;
		response[2] = destime;
		return response;

	}

	public Cursor getStationName(String StationCode) {
	StationCode = replace(StationCode);
	String SQL_L = "SELECT *  " +
			"FROM station_table st  " +
			"where 1 and  st.stationCode='"+StationCode+"'";
	
	//Log.v("Sql", SQL_L);
	
	
	return database.rawQuery(SQL_L, null);

	}

	public Cursor getAllStations(){
        String Cond = "";

            Cond ="AND sname  LIKE '%' OR stationCode LIKE '%'";
        //Log.v("Sql","SELECT * FROM " + LSTATION_NAME + " where 1  "+ Cond + "   LIMIT 0, 80");
        String Station_sql = "SELECT *,replace(stationName, ' ', '') as sname FROM " + LSTATION_NAME + " where 1  "+ Cond + "  order by popularity desc  LIMIT 0, 80";
        Log.e("station SQL",Station_sql);
        return database.rawQuery(Station_sql, null);
	}

	public Cursor getSaveStation() {

	String SQL_L = "SELECT *  " +
			"FROM "+ LSAVE_STATION ;

	//Log.v("Sql", SQL_L);


	return database.rawQuery(SQL_L, null);
	
	
}

public Cursor getDBInfo() {
	
	String SQL_L = "SELECT *  " +
			"FROM "+ LDBINFO ;
	
	//Log.v("Sql", SQL_L);
	
	
	return database.rawQuery(SQL_L, null);
	
	
}

public boolean InsertDeviceId(String deviceId) {
	/*private final String train_table = "CREATE TABLE train_table(_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
	"trainNO text, trainName text,sun boolean,mon boolean,tue boolean,wed boolean,thu boolean,fri boolean,sat " +
	"boolean, UNIQUE (trainNO));";*/
    boolean createSuccessful = false;

    ContentValues values = new ContentValues();

    values.put("device_id", deviceId);
    

    createSuccessful = database.insert(LDBINFO, null, values) > 0;
    //database.close();

    return createSuccessful;
}
public String replace( String s )
{
	if(s!=null)
	{
	s = s.replaceAll("[^a-zA-Z0-9]", "");
	return s;
	}
	else
		return s;
}
	public void Indexing(String Sql)
	{
		
		
		database.execSQL(Sql);
	}
	
	public ArrayList<SuggestStationBean>  getSuggestion(String DestStation, String sdesName)
	{
		ArrayList<SuggestStationBean> list = new ArrayList<SuggestStationBean>();
		ArrayList<String> arrlist = new ArrayList<String>();
		Cursor listoftrain;
		Cursor stationList;
		Integer trainId = 0;
		String stationName;
		String stationCode;
		String SQL_TRAINID = "select  distinct(trn._id) as trainid from route_table src inner join route_table dest on " +
				" src.trainId = dest.trainId inner join station_table srcSt on src.stationId = srcSt._id inner " +
				"join station_table destSt on  dest.stationId = destSt._id inner join train_table trn on " +
				"dest.trainId= trn._id where  destSt.stationCode = '"+DestStation+"'";
		//Log.v("SQL train ",SQL_TRAINID);
		listoftrain = database.rawQuery(SQL_TRAINID, null);
		
		if(listoftrain.getCount()>0)
	  	  {	 
	     	 
	     	  while (listoftrain.moveToNext()) {
	 				
	     		 trainId=0;
	     		
	  				
	     		 trainId = listoftrain.getInt(listoftrain.getColumnIndexOrThrow("trainid"));
	     		 
	     		 if(trainId>0)
	     		 {
	     			 String SQL_STATION ="SELECT  st.stationName as name ,st.stationCode code,(strftime('%Y%m%d%H%M%S' ,(datetime(date('now'),'+'||rt.datePlus||' days','+'||rt.arrival)))) source  " +
	 	     				"FROM route_table rt,station_table st  where 1 and rt.stationId=st._id " +
		     				"and rt.trainId="+trainId+"  order by source asc limit 1";
	     			// Log.v("STATION LIST",SQL_STATION);
	     		stationList = database.rawQuery(SQL_STATION, null);
	     		
	     		if(stationList.getCount()>0)
	  	  	  {	 
	  	     	 
	  	     	  while (stationList.moveToNext()) {
	  	     		stationName ="";
	  	     		stationCode ="";
	  	     		stationName = stationList.getString(stationList.getColumnIndexOrThrow("name"));
	  	     		stationCode = stationList.getString(stationList.getColumnIndexOrThrow("code"));
	  	     		if(!arrlist.contains(stationCode) && !stationCode.equals(DestStation) ) {
	  	     			arrlist.add(stationCode);
		 		    list.add(new SuggestStationBean(stationName,stationCode,sdesName,DestStation));
		 		}
	  	     	  }
	  	  	  }
	     		
	     		 }
	     		 
	     		 
	     		
	     	  }
	  	  
	  	  }
		
	
	return list;
	}
	public void close() {
		database.close();
	}
}