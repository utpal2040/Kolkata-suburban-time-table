package com.matainja.runingstatus.Database;

import java.util.Calendar;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;



public class DatabaseAdapterSaveSearch  {

	// Database fields
	
	static final String LSAVE_STATION ="savesearch";
	
	static final String LSAVE_PNR ="savepnr";
	private Context context;
	private SQLiteDatabase database;
	private DatabaseHelperSaveSearch dbHelper;

	public DatabaseAdapterSaveSearch(Context context) {
		this.context = context;
	}

	public DatabaseAdapterSaveSearch open() throws SQLException {
		dbHelper = new DatabaseHelperSaveSearch(context);
		database = dbHelper.getWritableDatabase();
		
		
		
		return this;
	}

	public int getcount(String sourcestation,String desStation,Integer islocal) {
		
		String SQL_L = "SELECT *  " +
				"FROM "+LSAVE_STATION+" st  " +
				"where 1 and  st.sourcestation='"+sourcestation+"' AND st.destinationstation='"+desStation+"' AND st.islocal="+islocal;
		
		
		Cursor mCount=  database.rawQuery(SQL_L, null);
		
		
		mCount.moveToFirst();
		int count= mCount.getCount();
		mCount.close();
		return count;
		
		
	}
	
	public Cursor getSaveSearch()
	{
		return database.rawQuery("SELECT * FROM " + LSAVE_STATION + " where 1  order by _id", null);
	}
	
	public Cursor getSavepnr()
	{
		return database.rawQuery("SELECT * FROM " + LSAVE_PNR + " where 1  order by _id desc limit 0,10", null);
	}
	
	public void delSaveSearch(int id)
	{
		
		
		database.delete(LSAVE_STATION, "_id="+id , null);
		
	}
public boolean InsertStationSave(String searhcname,String sourcestation,String destinationstation,Integer islocal) {
	
    boolean createSuccessful = false;

    ContentValues values = new ContentValues();

    values.put("searhcname", ""+searhcname+"");
    values.put("sourcestation", ""+sourcestation+"");
    values.put("destinationstation", ""+destinationstation+"");
    values.put("islocal", islocal);
    

    createSuccessful = database.insert(LSAVE_STATION, null, values) > 0;
    

    return createSuccessful;
}

public boolean InsertPNRSave(String pnr,String fromto) {
	
	String SQL_L = "SELECT *  " +
			"FROM "+LSAVE_PNR+" st  " +
			"where 1 and  st.pnr='"+pnr+"'";
	boolean createSuccessful = false;
	
	Cursor mCount=  database.rawQuery(SQL_L, null);
	
	
	mCount.moveToFirst();
	int count= mCount.getCount();
	
	if(count==0)
	{
    

    ContentValues values = new ContentValues();

    values.put("pnr", pnr);
    
    values.put("fromto", fromto);

    createSuccessful = database.insert(LSAVE_PNR, null, values) > 0;
	}

    return createSuccessful;
}


















	public void close() {
		database.close();
	}
}