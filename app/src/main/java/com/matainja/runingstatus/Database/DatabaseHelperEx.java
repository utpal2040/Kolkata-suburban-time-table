package com.matainja.runingstatus.Database;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.matainja.runingstatus.InstallDbExpress;


public class DatabaseHelperEx extends SQLiteOpenHelper {
	private static final String DATABASE_NAME = "express_train";
	private static String DB_PATH = "/data/data/com.matainja.runingstatus/databases/";
	
	public static final int DATABASE_VERSION = 200007;
	
	
	static final String TABLE_TRAIN_NAME="train_name";
	Context context;
	// Database creation sql statement
	/*private final String SQL_NEW_WINE="CREATE TABLE "+TABLE_WINE+" (id INTEGER PRIMARY KEY AUTOINCREMENT  NOT NULL ,name VARCHAR,producer VARCHAR DEFAULT Unknown,category " +
			"VARCHAR DEFAULT Unknown,scan_code_main VARCHAR,scan_code_2 VARCHAR,wine_year VARCHAR,can_age_year VARCHAR,initial_stock INTEGER,unit_cellar INTEGER," +
			"store_on DATETIME DEFAULT (CURRENT_DATE) ,location " +
			"VARCHAR DEFAULT Unknown,country VARCHAR DEFAULT Unknown,region " +
			"VARCHAR DEFAULT Unknown,sub_region VARCHAR DEFAULT Unknown,varietals VARCHAR DEFAULT Unknown,price DOUBLE," +
			"alcohol DOUBLE,rating FLOAT,photo_label_2 VARCHAR, photo_label_1 VARCHAR, photo_label_3 VARCHAR," +
			" personal_comment TEXT, boottle_empty INTEGER DEFAULT 0, favorite INTEGER DEFAULT 0," +
			"currency VARCHAR,volume VARCHAR,wine_date DATETIME,food_photo_url VARCHAR,ambience_photo_url " +
			"VARCHAR,friend_photo_url VARCHAR)"; */
			//AUTOINCREMENT


public DatabaseHelperEx(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		this.context = context;
		Log.v("Database","Database Helperex Create "+DATABASE_NAME);
	}


	
	@Override
	public void onCreate(SQLiteDatabase database) {
		
		
		/*String Sql_ = "CREATE INDEX StationId ON route_table (stationId)";
	 	
		database.execSQL(Sql_);
	
 	
	Log.v("Tag","Helper On create Express");*/
	
		
	}
	
	
	       

		//database.execSQL(SQL_WITHDRAW_BOTTLE);
		
		
	

	
	@Override
	public void onUpgrade(SQLiteDatabase database, int oldVersion,
			int newVersion) {
		
		
		Log.v("On Upgrade","on helperEx");
		
		
		Log.v("On Upgrade","on");
		
		
		Intent intent = new Intent(context, InstallDbExpress.class);
     	
		context.startActivity(intent);
		
	
		
		
		
		
		
	}
	
	

}
