package com.matainja.runingstatus.Database;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DataBaseHelperExpress extends SQLiteOpenHelper{
    
//The Android's default system path of your application database.
private static String DB_PATH = "/data/data/com.matainja.runingstatus/databases/";
 
private static String DB_NAME = "express_train";
public static final int DATABASE_VERSION = 200007;
private SQLiteDatabase myDataBase;
 
private final Context myContext;

 
/**
  * Constructor
  * Takes and keeps a reference of the passed context in order to access to the application assets and resources.
  * @param context
  */
public DataBaseHelperExpress(Context context) {
 
super(context, DB_NAME, null, DATABASE_VERSION);
this.myContext = context;
}	
 


/**
  * Creates a empty database on the system and rewrites it with your own database.
  * */
public void createDataBase() throws IOException{
 
boolean dbExist = checkDataBase();
 
if(dbExist){
//do nothing - database already exist
}else{
 
//By calling this method and empty database will be created into the default system path
//of your application so we are gonna be able to overwrite that database with our database.
this.getReadableDatabase();
 
try {
 
copyDataBase();
 
} catch (IOException e) {
 
throw new Error("Error copying database");
 
}
}
 
}
 
/**
  * Check if the database already exist to avoid re-copying the file each time you open the application.
  * @return true if it exists, false if it doesn't
  */
private boolean checkDataBase(){
 
SQLiteDatabase checkDB = null;
 
try{
String myPath = DB_PATH + DB_NAME;
File dbfile = new File(myPath);
if( dbfile.exists())
checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
else
	checkDB=null;	
}catch(SQLiteException e){
 
Log.v("error","database express does't exist yet.");
 
}
 
if(checkDB != null){
 
checkDB.close();
 
}
 
return checkDB != null ? true : false;
}
 
public void updatecreateDataBase() throws IOException{
	 
boolean dbExist = checkDataBase();
 
if(dbExist){
	copyDataBase();
//do nothing - database already exist
}else{}
 
}
/**
  * Copies your database from your local assets-folder to the just created empty database in the
  * system folder, from where it can be accessed and handled.
  * This is done by transfering bytestream.
  * */
private void copyDataBase() throws IOException{
 
//Open your local db as the input stream
//InputStream myInput = myContext.getAssets().open(DB_NAME);
InputStream myInput = myContext.getAssets().open(DB_NAME+"aa");
InputStream myInputab = myContext.getAssets().open(DB_NAME+"ab");
InputStream myInputac = myContext.getAssets().open(DB_NAME+"ac");
InputStream myInputad = myContext.getAssets().open(DB_NAME+"ad");
InputStream myInputae = myContext.getAssets().open(DB_NAME+"ae");
InputStream myInputaf = myContext.getAssets().open(DB_NAME+"af");
InputStream myInputag = myContext.getAssets().open(DB_NAME+"ag");
InputStream myInputah = myContext.getAssets().open(DB_NAME+"ah");
InputStream myInputai = myContext.getAssets().open(DB_NAME+"ai");

// Path to the just created empty db
String outFileName = DB_PATH + DB_NAME;
 
//Open the empty db as the output stream
OutputStream myOutput = new FileOutputStream(outFileName);
 
//transfer bytes from the inputfile to the outputfile
byte[] buffer = new byte[1024];
int length;
while ((length = myInput.read(buffer))>0){
myOutput.write(buffer, 0, length);
}

while ((length = myInputab.read(buffer))>0){
myOutput.write(buffer, 0, length);
}

while ((length = myInputac.read(buffer))>0){
myOutput.write(buffer, 0, length);
}
 while ((length = myInputad.read(buffer))>0){
myOutput.write(buffer, 0, length);
}
 while ((length = myInputae.read(buffer))>0){
	 myOutput.write(buffer, 0, length);
	 }
 
 while ((length = myInputaf.read(buffer))>0){
	 myOutput.write(buffer, 0, length);
	 }
 while ((length = myInputag.read(buffer))>0){
	 myOutput.write(buffer, 0, length);
	 }
 while ((length = myInputah.read(buffer))>0){
	 myOutput.write(buffer, 0, length);
	 }
 while ((length = myInputai.read(buffer))>0){
	 myOutput.write(buffer, 0, length);
	 }
 
//Close the streams
myOutput.flush();
myOutput.close();
myInput.close();
myInputab.close();
myInputac.close();
myInputad.close();
myInputae.close();
myInputaf.close();
myInputag.close();
myInputah.close();
myInputai.close();


}
 
public void openDataBase() throws SQLException{
 
//Open the database
String myPath = DB_PATH + DB_NAME;
myDataBase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
 
}
 
@Override
public synchronized void close() {
 
if(myDataBase != null)
myDataBase.close();
 
super.close();
 
}
 
@Override
public void onCreate(SQLiteDatabase db) {
 
}
 
@Override
public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
 
}
 
// Add your public helper methods to access and get content from the database.
// You could return cursors by doing "return myDataBase.query(....)" so it'd be easy
// to you to create adapters for your views.
 
}