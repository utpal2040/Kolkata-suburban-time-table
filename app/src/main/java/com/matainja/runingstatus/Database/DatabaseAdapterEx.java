package com.matainja.runingstatus.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.matainja.runingstatus.Model.LocalTrainAdapter;
import com.matainja.runingstatus.Model.RouteLocalTrain;
import com.matainja.runingstatus.Model.StationLocal;


public class DatabaseAdapterEx  {

	// Database fields
	static final String TABLE_TRAIN_NAME="train_table";
	static final String LTRAIN_NAME ="train_table";
	static final String LSTATION_NAME ="station_table";
	static final String LROUTE ="route_table";
	static final String LDBINFO ="db_info";
	static final String LSAVE_STATION ="save_station";
	
	
	private Context context;
	private SQLiteDatabase database;
	private DatabaseHelperEx dbHelper;

	public DatabaseAdapterEx(Context context) {
		this.context = context;
	}

	public DatabaseAdapterEx open() throws SQLException {
		dbHelper = new DatabaseHelperEx(context);
		database = dbHelper.getWritableDatabase();
		
		
		
		return this;
	}

public Cursor getAllTrain() {
		
	
		
		return database.rawQuery("SELECT * FROM " + TABLE_TRAIN_NAME + " where 1   LIMIT 0, 80", null);
		
	}

public Cursor getTrainInfo(String trainno) {
	
	
	trainno = replace(trainno);
	return database.rawQuery("SELECT * FROM " + TABLE_TRAIN_NAME + " where trainNO="+ trainno +"   LIMIT 0, 1", null);
	
}

public void UpdateVisitedTrain(Integer trainNo) {
	
	
	//Log.v("lll","UPDATE  " + TABLE_TRAIN_NAME + " set visited=visited+1 where train_no ="+ trainNo);
	//database.execSQL("UPDATE  " + TABLE_TRAIN_NAME + " set visited=visited+1 where train_no ="+trainNo);
	
	
}
	
public String GetSourceStation(Integer TrainId)
{
	String station ="";
	String SQL_ ="select  rt.stationId, srcSt.stationName," +
			"(strftime('%Y%m%d%H%M%S' ,(datetime(date('now'),'+'||rt.datePlus||' days','+'||rt.arrival)))) as sarriable  " +
			"from route_table rt inner join station_table srcSt on rt.stationId = srcSt._id  " +
			"where rt.trainId="+TrainId+" order by  sarriable asc  limit 0,1";
	Cursor SourceStationC = database.rawQuery(SQL_, null);
	
	if(SourceStationC.getCount()>0)
	  {	 
			while (SourceStationC.moveToNext()) {
				
				 station = SourceStationC.getString(SourceStationC.getColumnIndexOrThrow("stationName"));
			}
			 
	  }
	return station;
}

public String GetDestinationStation(Integer TrainId)
{
	String station ="";
	String SQL_ ="select  rt.stationId, srcSt.stationName," +
			"(strftime('%Y%m%d%H%M%S' ,(datetime(date('now'),'+'||rt.datePlus||' days','+'||rt.arrival)))) as sarriable  " +
			"from route_table rt inner join station_table srcSt on rt.stationId = srcSt._id  " +
			"where rt.trainId="+TrainId+" order by  sarriable desc  limit 0,1";
	Cursor SourceStationC = database.rawQuery(SQL_, null);
	
	if(SourceStationC.getCount()>0)
	  {	 
			while (SourceStationC.moveToNext()) {
				
				 station = SourceStationC.getString(SourceStationC.getColumnIndexOrThrow("stationName"));
			}
			 
	  }
	return station;
}
public Cursor getAllTrainSearch(String keywords) {
	
	keywords = replace(keywords);
	
	return database.rawQuery("SELECT *,replace(trainName, ' ', '') as tname  FROM " + TABLE_TRAIN_NAME + " where tname LIKE '%" + keywords +"%'   LIMIT 0, 80", null);
	
}

public void TruncateStationTable()
{
	database.execSQL("DELETE FROM "+ LSTATION_NAME);	
}

public void TruncateTrainTable()
{
	database.execSQL("DELETE FROM "+ LTRAIN_NAME);	
}
public void TruncateRouteTable()
{
	database.execSQL("DELETE FROM "+ LROUTE);	
}


public boolean InsertStationName(StationLocal station) {

    boolean createSuccessful = false;

    ContentValues values = new ContentValues();

  //  values.put(KEY_ID, information.getId());
    values.put("_id", station.getId());
//Log.v("_ID", station.getStationCode());
//Log.v("stationCode", station.getStationCode());
    values.put("stationCode", ""+station.getStationCode()+"");
    values.put("stationName", ""+station.getStationName()+"");
    values.put("popularity", station.getPopularity());

    createSuccessful = database.insert(LSTATION_NAME, null, values) > 0;
    //database.close();

    return createSuccessful;
}


public boolean InsertStationSave(String fromstation,String tosation) {

    boolean createSuccessful = false;

    ContentValues values = new ContentValues();

  //  values.put(KEY_ID, information.getId());
   
//Log.v("_ID", station.getStationCode());
//Log.v("stationCode", station.getStationCode());
    values.put("fromstation", ""+fromstation+"");
    values.put("tostation", ""+tosation+"");
   

    createSuccessful = database.insert(LSAVE_STATION, null, values) > 0;
    //database.close();

    return createSuccessful;
}

public boolean InsertTrainName(LocalTrainAdapter Train) {
	/*private final String train_table = "CREATE TABLE train_table(_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
	"trainNO text, trainName text,sun boolean,mon boolean,tue boolean,wed boolean,thu boolean,fri boolean,sat " +
	"boolean, UNIQUE (trainNO));";*/
    boolean createSuccessful = false;

    ContentValues values = new ContentValues();

  //  values.put(KEY_ID, information.getId());
    values.put("_id", Train.getId());
//Log.v("_ID", Train.gettrainName());
//Log.v("stationCode", Train.gettrainNO());
    values.put("trainNO", ""+Train.gettrainNO()+"");
    values.put("trainName", ""+Train.gettrainName()+"");
    values.put("sun", Train.getSun());
    values.put("mon", Train.getMon());
    values.put("tue", Train.getTue());
    values.put("wed", Train.getWed());
    values.put("thu", Train.getThu());
    values.put("fri", Train.getFri());
    values.put("sat", Train.getSat());
    

    createSuccessful = database.insert(LTRAIN_NAME, null, values) > 0;
    //database.close();

    return createSuccessful;
}

public boolean InsertTrainRoute(RouteLocalTrain TrainR) {
	/*private final String train_table = "CREATE TABLE train_table(_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
	"trainNO text, trainName text,sun boolean,mon boolean,tue boolean,wed boolean,thu boolean,fri boolean,sat " +
	"boolean, UNIQUE (trainNO));";*/
    boolean createSuccessful = false;

    ContentValues values = new ContentValues();

  //  values.put(KEY_ID, information.getId());
    values.put("_id", TrainR.getId());
//Log.v("_ID", ""+TrainR.getId());
//Log.v("stationCode", ""+TrainR.getTrainId());
    values.put("trainId", TrainR.getTrainId());
    values.put("stationId", TrainR.getStationId());
    values.put("arrival", TrainR.getArrival());
    values.put("datePlus", TrainR.getDatePlus());
    

    createSuccessful = database.insert(LROUTE, null, values) > 0;
    //database.close();

    return createSuccessful;
}

public Cursor getAllStation(String keywords) {
	keywords = replace(keywords);
	String Cond = "";
	if(keywords!= null)
		Cond ="AND sname  LIKE '" + keywords +"%' OR stationCode LIKE '" + keywords +"%'";
	//Log.v("Sql","SELECT * FROM " + LSTATION_NAME + " where 1  "+ Cond + "   LIMIT 0, 80");
	return database.rawQuery("SELECT *,replace(stationName, ' ', '') as sname FROM " + LSTATION_NAME + " where 1  "+ Cond + "  order by popularity desc,stationName asc  LIMIT 0, 80", null);
	
}

/*
	public Cursor getAllStationByTrainNo(String keywords) {
		keywords = replace(keywords);
		String Cond = "";
		if(keywords!= null)
			Cond ="AND sname  LIKE '" + keywords +"%' OR stationCode LIKE '" + keywords +"%'";
		//Log.v("Sql","SELECT * FROM " + LSTATION_NAME + " where 1  "+ Cond + "   LIMIT 0, 80");
		return database.rawQuery("SELECT *,replace(stationName, ' ', '') as sname FROM " + LSTATION_NAME + " where 1  "+ Cond + "  order by popularity desc,stationName asc  LIMIT 0, 80", null);

	}*/


public Cursor getLocalTrain(Integer trainId) {
	
	String dd ="SELECT * FROM " + LTRAIN_NAME + " where 1 AND  _id = "+ trainId;
	Log.v("Sql- list",dd);
	return database.rawQuery("SELECT * FROM " + LTRAIN_NAME + " where 1 AND  _id = "+ trainId , null);
	
	
	
}

public Cursor getTrainId(String trainNo) {
	trainNo = replace(trainNo);
	//String Sqq = "SELECT * FROM " + LTRAIN_NAME + " where 1 AND  trainNO = '"+ trainNo+"'" ;

	return database.rawQuery("SELECT * FROM " + LTRAIN_NAME + " where 1 AND  trainNO = '"+ trainNo+"'" , null);
	
	
	
}

    public Cursor getTrainByNo(String trainNo) {
        trainNo = replace(trainNo);
        //String Sqq = "SELECT * FROM " + LTRAIN_NAME + " where 1 AND  trainNO = '"+ trainNo+"'" ;
		//Log.e("sql == ","SELECT * FROM " + LTRAIN_NAME + " where 1 AND  trainNO LIKE '"+ trainNo+"'");
        return database.rawQuery("SELECT * FROM " + LTRAIN_NAME + " where 1 AND  trainNO LIKE '%"+ trainNo+"%'" , null);



    }

	public Cursor getTrainByID(int id) {
		//trainNo = replace(trainNo);
		//String Sqq = "SELECT * FROM " + LTRAIN_NAME + " where 1 AND  trainNO = '"+ trainNo+"'" ;
		Log.e("sql == ","select s.* from "+ LSTATION_NAME +" s inner join route_table r on s._id = r.stationId\n" +
				"where r.trainId = '"+id+"' ORDER BY r.stationId ASC");
		return database.rawQuery("select s.* from "+ LSTATION_NAME +" s inner join route_table r on s._id = r.stationId\n" +
				"where r.trainId = '"+id+"' ORDER BY r.stationId ASC" , null);



	}
  /*private final String route_table = "CREATE TABLE route_table(_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
		"trainId INTEGER,stationId INTEGER,arrival text,datePlus INTEGER, FOREIGN KEY (trainId) " +
		"REFERENCES train_table(_id), FOREIGN KEY (stationId) REFERENCES station_table(_id));";*/

public void UpdateVisitedStation(Integer id) {
	
	
	
	database.execSQL("UPDATE  " + LSTATION_NAME + " set popularity=popularity+1 where _id ="+id);
	
	
	
}

public void DatabaseUpdate(String sql)
{
	database.execSQL(sql);
}
public Cursor getTrainList(String fromstation,String tostation) {
	fromstation = replace(fromstation);
	tostation = replace(tostation);
	
	String SQL_L = "select  "+
			"CASE WHEN CAST (StrFTime('%H', src.arrival)AS INTEGER) > 12 THEN (StrFTime('%H', src.arrival) - 12) || ':' || StrFTime('%M', src.arrival) ||"+
			"' pm' ELSE ( select CASE WHEN CAST ((StrFTime('%H', src.arrival) - 12) AS INTEGER) ==0 THEN (StrFTime('%H', src.arrival)) || ':' || StrFTime('%M', src.arrival) ||"+
			"' pm'ELSE ( SELECT CASE WHEN CAST (StrFTime('%H', src.arrival)AS INTEGER) = 0 THEN '12' || ':' || StrFTime('%M', src.arrival) || ' am' "+
			"ELSE CAST (StrFTime('%H', src.arrival) AS INTEGER) || ':' || StrFTime('%M', src.arrival) || ' am' END) END )END AS sourcearrival,"+
			
			"CASE WHEN CAST (StrFTime('%H', dest.arrival)AS INTEGER) > 12 THEN (StrFTime('%H', dest.arrival) - 12) || ':' || StrFTime('%M', dest.arrival) ||"+
			"' pm' ELSE ( select CASE WHEN CAST ((StrFTime('%H', dest.arrival) - 12) AS INTEGER) ==0 THEN (StrFTime('%H', dest.arrival)) || ':' || StrFTime('%M', dest.arrival) ||"+
			"' pm'ELSE ( SELECT CASE WHEN CAST (StrFTime('%H', dest.arrival)AS INTEGER) = 0 THEN '12' || ':' || StrFTime('%M', dest.arrival) || ' am' "+
			"ELSE CAST (StrFTime('%H', dest.arrival) AS INTEGER) || ':' || StrFTime('%M', dest.arrival) || ' am' END) END )END AS destarrival,"+
			
			"trn._id as trainId,trn.trainName,trn.trainNo as trainNo,trn.sun,trn.mon,trn.tue,trn.wed,trn.thu,trn.fri,trn.sat," +
			"srcSt.stationName as SourceStation, srcSt.stationCode as sourcestationcode" +
			",src.arrival sourcearrivalsort,destSt.stationName as DestinationStation," +
			"datetime(datetime('now'),'+'||src.datePlus||' days','+'||src.arrival) as sarriable ,datetime(datetime('now'),'+'||dest.datePlus||' days','+'||dest.arrival) as darriable ," +
			" destSt.stationCode as destStationcode from route_table src inner join route_table dest on " +
			" src.trainId = dest.trainId inner join station_table srcSt on src.stationId = srcSt._id inner join station_table destSt on " +
			" dest.stationId = destSt._id inner join train_table trn on dest.trainId= trn._id where srcSt.stationCode= '"+fromstation+"' and destSt.stationCode = '"+tostation+"'" +
			" and sarriable < darriable  order by sarriable asc";
	
	
	/*String SQL_L = "select  trn._id as trainId,trn.trainName,trn.trainNo as trainNo,trn.sun,trn.mon,trn.tue,trn.wed,trn.thu,trn.fri,trn.sat," +
			"srcSt.stationName as SourceStation, srcSt.stationCode as sourcestationcode" +
			",src.arrival sourcearrival,destSt.stationName as DestinationStation," +
			"src.arrival as sarriable ,dest.arrival as darriable ," +
			" destSt.stationCode as destStationcode,dest.arrival destarrival from route_table src inner join route_table dest on " +
			" src.trainId = dest.trainId inner join station_table srcSt on src.stationId = srcSt._id inner join station_table destSt on " +
			" dest.stationId = destSt._id inner join train_table trn on dest.trainId= trn._id where srcSt.stationCode= '"+fromstation+"' and destSt.stationCode = '"+tostation+"'" +
			"   order by sarriable  asc";*/
	
	
	Log.e("JJJJ",SQL_L);
	
	return database.rawQuery(SQL_L, null);
	
	
}

public Cursor isExtrainAvailable(String fromstation,String tostation) {
	fromstation = replace(fromstation);
	tostation = replace(tostation);
	String SQL_L = "select  trn._id as trainId, "+
	"datetime(datetime('now'),'+'||src.datePlus||' days','+'||src.arrival) as sarriable ,datetime(datetime('now'),'+'||dest.datePlus||' days','+'||dest.arrival) as darriable "+
			" from route_table src inner join route_table dest on " +
			" src.trainId = dest.trainId inner join station_table srcSt on src.stationId = srcSt._id inner join station_table destSt on " +
			" dest.stationId = destSt._id inner join train_table trn on dest.trainId= trn._id where srcSt.stationCode= '"+fromstation+"' and destSt.stationCode = '"+tostation+"'" +
			" and sarriable < darriable    limit 1";
	
	//Log.v("Sql", SQL_L);
	
	
	return database.rawQuery(SQL_L, null);
	
	
}

	public String[] getStationStop(int trainId, String fromcode, String tocode) {


		int stops_loop=0;
		int noofstops=0;
		String sourcetime ="";
		String destime ="";
        /*String SQL_L = "select * from route_table where trainId = " + trainId + " AND " +
                "((stationId between (select _id from station_table where stationCode='" + fromcode + "') AND " +
                "(select _id from station_table where stationCode='" + tocode + "')) OR " +
                "(stationId between (select _id from station_table where stationCode='" + tocode + "') AND " +
                "(select _id from station_table where stationCode='" + fromcode + "')))";*/

		String SQL_L = "SELECT   "+
				"CASE WHEN CAST (StrFTime('%H', arrival)AS INTEGER) > 12 THEN (StrFTime('%H', arrival) - 12) || ':' || StrFTime('%M', arrival) ||"+
				"' pm' ELSE ( select CASE WHEN CAST ((StrFTime('%H', arrival) - 12) AS INTEGER) ==0 THEN (StrFTime('%H', arrival)) || ':' || StrFTime('%M', arrival) ||"+
				"' pm'ELSE ( SELECT CASE WHEN CAST (StrFTime('%H', arrival)AS INTEGER) = 0 THEN '12' || ':' || StrFTime('%M', arrival) || ' am' "+
				"ELSE CAST (StrFTime('%H', arrival) AS INTEGER) || ':' || StrFTime('%M', arrival) || ' am' END) END )END AS arrival,"+
				"st.stationCode,st.stationName," +
				" (strftime('%Y-%m-%d %H:%M:%S' ,(datetime(date('now'),'+'||rt.datePlus||' days','+'||rt.arrival)))) sortarrival "+
				"FROM route_table rt,station_table st  " +

				"where 1 and rt.stationId=st._id and rt.trainId="+trainId+" order by  sortarrival asc";

        Log.e("Sql STOPS huhu", SQL_L);

/*select * from route_table where trainId = "+trainId+" AND " +
		"((stationId between (select _id from station_table where stationCode='"+fromcode+"') AND " +
				"(select _id from station_table where stationCode='"+tocode+"') */

		Cursor getStops = database.rawQuery(SQL_L, null);
		if(getStops.getCount()>0) {
			while (getStops.moveToNext()) {

				String stationCode  = getStops.getString(getStops.getColumnIndexOrThrow("stationCode"));
				String timestation  = getStops.getString(getStops.getColumnIndexOrThrow("sortarrival"));

				if(fromcode.equalsIgnoreCase(stationCode))
				{
					stops_loop=1;
					sourcetime =timestation;
				}


				if(stops_loop==1)
				{
					noofstops++;
				}

				if(tocode.equalsIgnoreCase(stationCode))
				{
					stops_loop=0;
					destime =timestation;
					break;
				}

			}
		}
Log.e("times",sourcetime+"---"+destime);
		String[] response;
		response = new String[3];
		response[0]=String.valueOf(noofstops);
		response[1]=sourcetime;
        response[2] = destime;
		return response;
    }

public Cursor getStationList(Integer trainId) {
	
	String SQL_L = "SELECT rt.arrival,st.stationCode,st.stationName,(strftime('%Y-%m-%d ' ,(datetime(date('now'),'+'||rt.datePlus||' days','+'||rt.arrival)))) sortarrival   " +
			"FROM route_table rt,station_table st  " +
			"where 1 and rt.stationId=st._id and rt.trainId="+trainId+" order by  rt._id asc";
	
	Log.e("Sql", SQL_L);
	
	
	return database.rawQuery(SQL_L, null);
	
	
}

public Cursor getStationName(String StationCode) {
	StationCode = replace(StationCode);
	String SQL_L = "SELECT *  " +
			"FROM station_table st  " +
			"where 1 and  st.stationCode='"+StationCode+"'";
	
	//Log.v("Sql", SQL_L);
	
	
	return database.rawQuery(SQL_L, null);
	
	
}
public Cursor getSaveStation() {
	
	String SQL_L = "SELECT *  " +
			"FROM "+ LSAVE_STATION ;
	
	//Log.v("Sql", SQL_L);
	
	
	return database.rawQuery(SQL_L, null);
	
	
}

public Cursor getDBInfo() {
	
	String SQL_L = "SELECT *  " +
			"FROM "+ LDBINFO ;
	
	//Log.v("Sql", SQL_L);
	
	
	return database.rawQuery(SQL_L, null);
	
	
}

public boolean InsertDeviceId(String deviceId) {
	/*private final String train_table = "CREATE TABLE train_table(_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
	"trainNO text, trainName text,sun boolean,mon boolean,tue boolean,wed boolean,thu boolean,fri boolean,sat " +
	"boolean, UNIQUE (trainNO));";*/
    boolean createSuccessful = false;

    ContentValues values = new ContentValues();

    values.put("device_id", deviceId);
    

    createSuccessful = database.insert(LDBINFO, null, values) > 0;
    //database.close();

    return createSuccessful;
}
public String replace( String s )
{
	if(s!=null)
	{
	s = s.replaceAll("[^a-zA-Z0-9]", "");
	return s;
	}
	else
		return s;
}
	public void Indexing(String Sql)
	{
		
		
		database.execSQL(Sql);
	}
	public void close() {
		database.close();
	}
}