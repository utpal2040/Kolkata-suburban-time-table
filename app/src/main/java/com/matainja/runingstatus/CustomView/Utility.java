package com.matainja.runingstatus.CustomView;

/**
 * Created by matainja on 10-Mar-17.
 */
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.MeasureSpec;
import android.widget.ListAdapter;
import android.widget.ListView;

public class Utility {
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(), MeasureSpec.AT_MOST);
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
            totalHeight += listItem.getMeasuredHeight();
        }

        Log.e("hieght===",""+totalHeight);
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        Log.e("CAL+=",""+totalHeight+"+("+listView.getDividerHeight()+"*("+listAdapter.getCount()+"-1))");
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        Log.e("Param===",""+params);
        listView.setLayoutParams(params);
        listView.requestLayout();
    }
}
