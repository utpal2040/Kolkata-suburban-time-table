package com.matainja.runingstatus.Adapter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.TextView;

import com.matainja.runingstatus.R;


public class ListviewAdapter extends BaseAdapter implements OnClickListener {
    
	private Context context;
    private String[] colors = new String[] { "008299", "2672EC","8C0095","5133AB","AC193D","D24726","008A00","094AB2",
    		"00A0B1","2E8DEF","A700AE","643EBF","BF1E4B","DC572E","00A600","00A600","2E1700","4E0000","4E0038","2D004E","1F0068","001E4E","004D60","004A00",
    		"15992A","E56C19","B81B1B","B81B6C","691BB8","1B58B8","569CE3","00AAAA",
    		"83BA1F","D39D09","E064B7","F3B200","77B900","2572EB","AD103C","632F00",
    		"B01E00","C1004F","7200AC","4617B4","006AC1","008287","199900","00C13F",
    		"FF981D","FF2E12","FF1D77","AA40FF","1FAEFF","56C5FF","91D100","E1B700",
    		"FF76BC","00A3A3","FE7C22","261300","380000","40002E","250040","180052"  };
    private List<HashMap<String, String>> listofstation;

    private static LayoutInflater inflater=null;
    

    public ListviewAdapter(Context context2,
			ArrayList<HashMap<String, String>> oslist) {
		// TODO Auto-generated constructor stub
    	
    	 this.context = context2;
         this.listofstation = oslist;
         inflater = (LayoutInflater)context2.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
        return listofstation.size();
    }

    @Override
	public Object getItem(int position) {
        return listofstation.get(position);
    }

    @Override
	public long getItemId(int position) {
        return position;
    }

    @Override
	public View getView(int position, View convertView, ViewGroup viewGroup) {
    	int colorPos = position % colors.length;
    	
    	int min = 1;
    	int max = colors.length-1;

    	Random r = new Random();
    	int  colorbox= r.nextInt(max - min + 1) + min;
    	  
    	
       
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.rowstyle, null);
        }
       HashMap<String, String> stations;
       stations = listofstation.get(position);
        TextView name = (TextView) convertView.findViewById(R.id.title);
        TextView arrival = (TextView) convertView.findViewById(R.id.tarrival);
        TextView departure = (TextView) convertView.findViewById(R.id.tdeparture);
       TextView reachtime = (TextView) convertView.findViewById(R.id.arrival_departure);
        TextView status = (TextView) convertView.findViewById(R.id.remarks);
        TextView day = (TextView) convertView.findViewById(R.id.day);
        TextView day_distance = (TextView) convertView.findViewById(R.id.day_distance);
        TextView depatured = (TextView) convertView.findViewById(R.id.reached);
/*
        map.put("station_code", scode);
        map.put("station_name", sname);
        map.put("day", mday);
        map.put("actarr", mactarr);
        map.put("schdep", mschdep);
        map.put("status", mstatus);
        map.put("scharr_date", mscharr_date);
        map.put("has_arrived", mhas_arrived);*/
        String stationcode=stations.get("station_code");
        String is_arrived=stations.get("has_arrived");
        String is_departure=stations.get("has_departed");
        String arrived_text="";
        String Eta = "ETA : ";
        Log.e("departured",stationcode+"=="+is_arrived+"/"+is_departure);
        if(is_arrived.compareTo("true")== 0 && is_departure.compareTo("true")==0 )
        {
        	arrived_text ="Arrived/Departure";
        	Eta = "Arrv/Dep : ";
        }
        else if (is_arrived.equalsIgnoreCase("true") && is_departure.equalsIgnoreCase("false"))
        	arrived_text ="Arrived";
        name.setText(stations.get("station_name")+ " ("+stationcode+")"); 
        arrival.setText(stations.get("scharr"));
        departure.setText(stations.get("schdep"));
        status.setText(stations.get("status"));
        reachtime.setText(Eta +stations.get("actarr")+ " /" + " "+stations.get("actdep"));
        day_distance.setText(stations.get("distance")+ " KM");
        day.setText(stations.get("day")+ " Days");
        depatured.setText(arrived_text);
        
        
       
       
       
		
       // convertView.setBackgroundColor(colors[colorPos]);
        
        
        
       
        return convertView;
    }
    
   
	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		
	}

    

   

}
