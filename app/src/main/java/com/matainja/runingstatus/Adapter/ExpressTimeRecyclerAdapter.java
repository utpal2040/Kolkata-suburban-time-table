package com.matainja.runingstatus.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.matainja.runingstatus.Model.TimetableListBean;
import com.matainja.runingstatus.R;
import com.matainja.runingstatus.TimeTableDetails;

import java.util.List;
import java.util.Random;

/**
 * Created by matainja on 27-Mar-17.
 */

public class ExpressTimeRecyclerAdapter extends RecyclerView.Adapter<ExpressTimeRecyclerAdapter.ExpressTimeHolder>{
    private Context context;
    private String[] colors = new String[] { "008299", "2672EC","8C0095","5133AB","AC193D","D24726","008A00","094AB2",
            "00A0B1","2E8DEF","A700AE","643EBF","BF1E4B","DC572E","00A600","00A600","2E1700","4E0000","4E0038","2D004E","1F0068","001E4E","004D60","004A00",
            "15992A","E56C19","B81B1B","B81B6C","691BB8","1B58B8","569CE3","00AAAA",
            "83BA1F","D39D09","E064B7","F3B200","77B900","2572EB","AD103C","632F00",
            "B01E00","C1004F","7200AC","4617B4","006AC1","008287","199900","00C13F",
            "FF981D","FF2E12","FF1D77","AA40FF","1FAEFF","56C5FF","91D100","E1B700",
            "FF76BC","00A3A3","FE7C22","261300","380000","40002E","250040","180052"  };
    private List<TimetableListBean> listoftrain;



    public ExpressTimeRecyclerAdapter(Context context,
                                         List<TimetableListBean>  trainlist) {
        // TODO Auto-generated constructor stub

        this.context = context;
        this.listoftrain = trainlist;



    }

    @Override
    public ExpressTimeRecyclerAdapter.ExpressTimeHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.timetablerow, parent, false);
        return new ExpressTimeRecyclerAdapter.ExpressTimeHolder(view);
    }



    @Override
    public void onBindViewHolder(ExpressTimeRecyclerAdapter.ExpressTimeHolder holder, int position) {
        int colorPos = position % colors.length;

        final TimetableListBean entry = listoftrain.get(position);
        int min = 1;
        int max = colors.length-1;
        String train_name_val = entry.getTrainName();
        String toarrival_val = entry.getdestarrival();
        String fromarrival_val = entry.getsourcearrival();
        String train_no_val = entry.getTrainNo();
        Integer off_days = entry.getisoffday();
        Integer on_satday = entry.getisonlysat();
        String available_text = entry.getavilabledays();

        Random r = new Random();
        int  colorbox= r.nextInt(max - min + 1) + min;

        holder.trainstop.setText(listoftrain.get(position).getTotalStop()+" Stop(s)");
        holder.totaltime.setText(listoftrain.get(position).gettotaltime());
        if(off_days==1)
        {
            available_text ="Off:"+available_text;
            holder.days.setTextColor(context.getResources().getColor(R.color.colorExpress));
        }
        else
        {
            if(on_satday==1)
                holder. days.setTextColor(context.getResources().getColor(R.color.colorAccent));
            else
                holder. days.setTextColor(Color.BLACK);
        }
        holder.train_name.setText(train_name_val);
        holder.fromarrival.setText(fromarrival_val);
        holder. toarrival.setText(toarrival_val);
        //holder. train_no.setText("Train No : "+train_no_val);
        holder. train_no.setText("Train No : "+entry.getTrainId());
        //holder. train_no.setText("Train No : "+entry.gettotaltime());
        holder.remarks.setText(entry.getRemarks());
        //raon_no.setText(train_no);
        holder.remarks.setTextColor(context.getResources().getColor(R.color.colorExpress));
        holder. days.setText(available_text);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer trainId = entry.getTrainId();
                String trainNo =entry.getTrainNo();

                Intent intent = new Intent(context, TimeTableDetails.class);

               /* intent.putExtra("trainId", trainId);
                intent.putExtra("trainNo", trainNo);
                intent.putExtra("fromstation", fromstationcode);
                intent.putExtra("tostation", tostationcode);

                startActivity(intent);*/

            }
        });
    }

    @Override
    public int getItemCount() {
        return listoftrain.size();
    }

    class ExpressTimeHolder extends RecyclerView.ViewHolder {
        TextView train_name,toarrival,fromarrival,train_no,days,remarks,trainstop,totaltime;

        public ExpressTimeHolder(View v) {
            super(v);

            train_name = (TextView) v.findViewById(R.id.title);
            toarrival = (TextView) v.findViewById(R.id.tdeparture);
            fromarrival = (TextView) v.findViewById(R.id.tarrival);
            train_no = (TextView) v.findViewById(R.id.trainno);
            days = (TextView) v.findViewById(R.id.days);
            remarks = (TextView) v.findViewById(R.id.remarks);
            trainstop = (TextView) v.findViewById(R.id.trainstop);
            totaltime = (TextView)v.findViewById(R.id.totaltime);
        }
    }

    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }
}