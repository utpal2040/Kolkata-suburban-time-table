package com.matainja.runingstatus.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;
import com.matainja.runingstatus.R;
import com.matainja.runingstatus.Model.StationLocal;

import java.util.ArrayList;

/**
 * Created by matainja on 27-Feb-17.
 */

public class CustomerAdapter extends ArrayAdapter<StationLocal> {
    private final String MY_DEBUG_TAG = "CustomerAdapter";
    private ArrayList<StationLocal> items;
    private ArrayList<StationLocal> itemsAll;
    private ArrayList<StationLocal> suggestions;
    private int viewResourceId;

    public CustomerAdapter(Context context, int viewResourceId, ArrayList<StationLocal> items) {
        super(context, viewResourceId, items);
        this.items = items;
        this.itemsAll = (ArrayList<StationLocal>) items.clone();
        this.suggestions = new ArrayList<StationLocal>();
        this.viewResourceId = viewResourceId;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(viewResourceId, null);
        }
        StationLocal stationLocal = items.get(position);
        if (stationLocal != null) {
            TextView text1 = (TextView) v.findViewById(R.id.text1);
            if (text1 != null) {
//              Log.i(MY_DEBUG_TAG, "getView Customer Name:"+customer.getName());
                text1.setText(stationLocal.getStationName()+"-"+stationLocal.getStationCode());
            }
        }
        return v;
    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    Filter nameFilter = new Filter() {
        @Override
        public String convertResultToString(Object resultValue) {
            String stn_name = ((StationLocal)(resultValue)).getStationName();
            String stn_code = ((StationLocal)(resultValue)).getStationCode();
            String str = stn_name+" "+stn_code;
            return str;
        }
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if(constraint != null) {
                suggestions.clear();
                for (StationLocal customer : itemsAll) {
                    if(customer.getStationName().toLowerCase().startsWith(constraint.toString().toLowerCase()) || customer.getStationCode().toLowerCase().startsWith(constraint.toString().toLowerCase())){
                        suggestions.add(customer);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            ArrayList<StationLocal> filteredList = (ArrayList<StationLocal>) results.values;
            if(results != null && results.count > 0) {
                clear();
                for (StationLocal c : filteredList) {
                    add(c);
                }
                notifyDataSetChanged();
            }
        }
    };

}