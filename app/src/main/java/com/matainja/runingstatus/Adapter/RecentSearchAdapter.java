package com.matainja.runingstatus.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.matainja.runingstatus.R;
import com.matainja.runingstatus.Model.RecentSearch;

import java.util.ArrayList;

/**
 * Created by matainja on 24-Feb-17.
 */

public class RecentSearchAdapter extends BaseAdapter {

    Context mcontext;
    ArrayList<RecentSearch> listOfData;
    String fromname, toname;
    public RecentSearchAdapter (Context context, ArrayList<RecentSearch> lisOfData){

        this.mcontext = context;
        this.listOfData = lisOfData;

        Log.e("LIST ADAPTER====","CALLED");
    }
    @Override
    public int getCount() {
        return listOfData.size();
    }

    @Override
    public Object getItem(int position) {
        return listOfData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mcontext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.row_recentsearch, null);


        }
        Log.e("ADAPTER POSITION==",""+position);
        TextView fromstation = (TextView) convertView.findViewById(R.id.fromstation);
        TextView fromstationCode = (TextView) convertView.findViewById(R.id.fromstationCode);
        TextView tostationCode = (TextView) convertView.findViewById(R.id.tostationCode);

        String fromst = listOfData.get(position).getFromStationName();
        String tost = listOfData.get(position).getToStationName();

        String[] fromstationarr, tostationarr;
        if(fromst.contains("-")) {
            fromstationarr = fromst.split("-");
            fromst = fromstationarr[0].trim().toUpperCase(); // this will contain "Fruit"
            fromname = fromstationarr[1].trim();

        } else{
            fromname = fromst;
        }
        if(tost.contains("-")) {
            tostationarr = tost.split("-");
            tost = tostationarr[0].trim().toUpperCase();
            toname = tostationarr[1].trim();
        } else{
            toname = tost;
        }



        fromstation.setText(fromname+" To "+toname);
        fromstationCode.setText(fromst);
        tostationCode.setText(tost);
        return convertView;
    }
    /*@Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        //listOfData.addAll(listOfData);//--insert object to list
    }*/
}
