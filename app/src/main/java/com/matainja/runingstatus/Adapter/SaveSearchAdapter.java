
package com.matainja.runingstatus.Adapter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;



import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.matainja.runingstatus.Database.DatabaseAdapterSaveSearch;
import com.matainja.runingstatus.Model.SaveSearchBean;
import com.matainja.runingstatus.R;


public class SaveSearchAdapter extends BaseAdapter implements OnClickListener {
	
    
	private Context context;
    
    private List<SaveSearchBean> SaveSearchlist;

    private static LayoutInflater inflater=null;
    

    public SaveSearchAdapter(Context context,
    		List<SaveSearchBean>  SaveSearchlist) {
		// TODO Auto-generated constructor stub
    	
    	 this.context = context;
         this.SaveSearchlist = SaveSearchlist;
         inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
         
        
	}

	@Override
	public int getCount() {
        return SaveSearchlist.size();
    }

    @Override
	public Object getItem(int position) {
        return SaveSearchlist.get(position);
    }

    @Override
	public long getItemId(int position) {
        return position;
    }

    @Override
	public View getView(final int position, View convertView, ViewGroup viewGroup) {
    	// Log.v("error","error0");
    	
    	final Context context = viewGroup.getContext();
    	SaveSearchBean entry = SaveSearchlist.get(position);
    
    	
    	String searchName = entry.getName();
    	String fromStation = entry.getSrcStation();
    	String toStation = entry.getDesstation();
// Database Connection Open
		

			  
    	
    	
    	  
    	
       
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.savesearchrow, null);
        }
       
       TextView viewSearchName = (TextView) convertView.findViewById(R.id.title);
       TextView Viewtoarrival = (TextView) convertView.findViewById(R.id.tdeparture);
       TextView Viewfromarrival = (TextView) convertView.findViewById(R.id.tarrival);
       
       
       
       viewSearchName.setText(searchName); 
       Viewtoarrival.setText(toStation);
       Viewfromarrival.setText(fromStation);
     
       
      
     
    
        /*Typeface custom_font = Typeface.createFromAsset(context.getAssets(),
      	      "fonts/Bodoni-Italic.TTF");
       // train_name.setTypeface(custom_font);
       
        Viewfromarrival.setTypeface(custom_font);
        Viewtoarrival.setTypeface(custom_font);*/
       
       // convertView.setBackgroundColor(colors[colorPos]);
        
       
        final ImageView imageView = (ImageView) convertView.findViewById(R.id.delete);
       // final LinearLayout LinearLayoutImg = (LinearLayout) convertView.findViewById(R.id.layoutdelete);
        imageView.setTag(new Integer(position));
       
        imageView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View view) {
            	// Database Connection Open
                DatabaseAdapterSaveSearch dbHelpersave = new DatabaseAdapterSaveSearch(context);
                dbHelpersave.open();
                SaveSearchBean delentry = SaveSearchlist.get(position);
                dbHelpersave.delSaveSearch(delentry.getId());
            	SaveSearchlist.remove(SaveSearchlist.get(position));
            	SaveSearchAdapter.this.notifyDataSetChanged();
            	dbHelpersave.close();
                Toast.makeText(context, "Sucessfully Deleted Route", Toast.LENGTH_SHORT).show();
            }
        });
       
       
        return convertView;
    }
    
   
	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		
	}

    

   

}
