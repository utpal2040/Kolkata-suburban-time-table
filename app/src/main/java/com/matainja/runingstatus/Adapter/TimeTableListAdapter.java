package com.matainja.runingstatus.Adapter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;



import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.TextView;

import com.matainja.runingstatus.Model.TimetableListBean;
import com.matainja.runingstatus.R;


public class TimeTableListAdapter extends BaseAdapter implements OnClickListener {
	
    
	private Context context;
    private String[] colors = new String[] { "008299", "2672EC","8C0095","5133AB","AC193D","D24726","008A00","094AB2",
    		"00A0B1","2E8DEF","A700AE","643EBF","BF1E4B","DC572E","00A600","00A600","2E1700","4E0000","4E0038","2D004E","1F0068","001E4E","004D60","004A00",
    		"15992A","E56C19","B81B1B","B81B6C","691BB8","1B58B8","569CE3","00AAAA",
    		"83BA1F","D39D09","E064B7","F3B200","77B900","2572EB","AD103C","632F00",
    		"B01E00","C1004F","7200AC","4617B4","006AC1","008287","199900","00C13F",
    		"FF981D","FF2E12","FF1D77","AA40FF","1FAEFF","56C5FF","91D100","E1B700",
    		"FF76BC","00A3A3","FE7C22","261300","380000","40002E","250040","180052"  };
    private List<TimetableListBean> listoftrain;

    private static LayoutInflater inflater=null;
    

    public TimeTableListAdapter(Context context,
    		List<TimetableListBean>  trainlist) {
		// TODO Auto-generated constructor stub
    	
    	 this.context = context;
         this.listoftrain = trainlist;
         inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
         
        
	}

	@Override
	public int getCount() {
        return listoftrain.size();
    }

    @Override
	public Object getItem(int position) {
        return listoftrain.get(position);
    }

    @Override
	public long getItemId(int position) {
        return position;
    }

    @Override
	public View getView(int position, View convertView, ViewGroup viewGroup) {
    	// Log.v("error","error0");
    	int colorPos = position % colors.length;
    	Context context = viewGroup.getContext();
    	TimetableListBean entry = listoftrain.get(position);
    	int min = 1;
    	int max = colors.length-1;
        String train_name_val = entry.getTrainName();
        String toarrival_val = entry.getdestarrival();
        String fromarrival_val = entry.getsourcearrival();
        String train_no_val = entry.getTrainNo();
        Integer off_days = entry.getisoffday();
        Integer on_satday = entry.getisonlysat();
        String available_text = entry.getavilabledays();
// Database Connection Open
		

			  
    	Random r = new Random();
    	int  colorbox= r.nextInt(max - min + 1) + min;
    	  
    	
       
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.timetablerow, null);
        }
       
       TextView train_name = (TextView) convertView.findViewById(R.id.title);
       TextView toarrival = (TextView) convertView.findViewById(R.id.tdeparture);
       TextView fromarrival = (TextView) convertView.findViewById(R.id.tarrival);
       TextView train_no = (TextView) convertView.findViewById(R.id.trainno);
       TextView days = (TextView) convertView.findViewById(R.id.days);
       TextView remarks = (TextView) convertView.findViewById(R.id.remarks);
       TextView trainstop = (TextView) convertView.findViewById(R.id.trainstop);
       TextView totaltime = (TextView)convertView.findViewById(R.id.totaltime);

        Log.e("title====",""+listoftrain.get(position).getTrainName());
        Log.e("STOP in adapter====",""+listoftrain.get(position).getTotalStop());
        trainstop.setText(listoftrain.get(position).getTotalStop()+" Stop(s)");


       if(off_days==1)
		  {
			  available_text ="Off:"+available_text; 
			  days.setTextColor(Color.RED);
		  }
       else
       {
    	   if(on_satday==1)
    		   days.setTextColor(Color.RED);
    	   else
    	   days.setTextColor(Color.BLACK);
       }
       
      train_name.setText(train_name_val);
      fromarrival.setText(fromarrival_val);
      toarrival.setText(toarrival_val);
      train_no.setText("Train No : "+train_no_val);
      totaltime.setText(listoftrain.get(position).gettotaltime());
        //train_no.setText("Train No : "+listoftrain.get(position).gettotaltime());
      remarks.setText(entry.getRemarks());
       //raon_no.setText(train_no);
      
      days.setText(available_text);
    
        /*Typeface custom_font = Typeface.createFromAsset(context.getAssets(),
      	      "fonts/Bodoni-Italic.TTF");
       // train_name.setTypeface(custom_font);
       
        fromarrival.setTypeface(custom_font);
        toarrival.setTypeface(custom_font);
        train_no.setTypeface(custom_font);
        days.setTypeface(custom_font);*/
       // convertView.setBackgroundColor(colors[colorPos]);
        
        
       
       
        return convertView;
    }
    
   
	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		
	}

    

   

}
