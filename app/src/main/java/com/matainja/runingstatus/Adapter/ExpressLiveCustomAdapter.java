package com.matainja.runingstatus.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.matainja.runingstatus.Model.StationLocal;
import com.matainja.runingstatus.Model.Train;
import com.matainja.runingstatus.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by matainja on 13-Mar-17.
 */

public class ExpressLiveCustomAdapter extends ArrayAdapter<Train> {

    private final String MY_DEBUG_TAG = "CustomerAdapter";
    private ArrayList<Train> items;
    private ArrayList<Train> itemsAll;
    private ArrayList<Train> suggestions;
    private int viewResourceId;

    public ExpressLiveCustomAdapter(Context context, int viewResourceId, ArrayList<Train> items) {
        super(context, viewResourceId, items);
        this.items = items;
        this.itemsAll = (ArrayList<Train>) items.clone();
        this.suggestions = new ArrayList<Train>();
        this.viewResourceId = viewResourceId;

    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(viewResourceId, null);
        }
        Train train = items.get(position);
        if (train != null) {
            TextView text1 = (TextView) v.findViewById(R.id.text1);
            if (text1 != null) {
//              Log.i(MY_DEBUG_TAG, "getView Customer Name:"+customer.getName());
                text1.setText(train.getTrainName()+"-"+train.getTrainNo());
            }
        }
        return v;
    }


    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    Filter nameFilter = new Filter() {
        @Override
        public String convertResultToString(Object resultValue) {
            String trn_name = ((Train)(resultValue)).getTrainName();
            String trn_code = ((Train)(resultValue)).getTrainNo();
            String str = trn_name+" "+trn_code;
            return str;
        }
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if(constraint != null) {
                suggestions.clear();
                for (Train train : itemsAll) {
                    if(train.getTrainName().toLowerCase().startsWith(constraint.toString().toLowerCase()) || train.getTrainNo().toLowerCase().startsWith(constraint.toString().toLowerCase())){
                        suggestions.add(train);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            ArrayList<Train> filteredList = (ArrayList<Train>) results.values;
            if(results != null && results.count > 0) {
                clear();
                for (Train c : filteredList) {
                    add(c);
                }
                notifyDataSetChanged();
            }
        }
    };

}
