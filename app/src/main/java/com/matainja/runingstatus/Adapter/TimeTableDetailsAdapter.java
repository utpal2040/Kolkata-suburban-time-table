package com.matainja.runingstatus.Adapter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;



import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.matainja.runingstatus.Database.DatabaseAdapter;
import com.matainja.runingstatus.Model.StationListBean;
import com.matainja.runingstatus.R;


public class TimeTableDetailsAdapter extends BaseAdapter implements OnClickListener {
	 private DatabaseAdapter dbHelper;
	 private Cursor traininfo;
    
	private Context context;
    private String[] colors = new String[] { "008299", "2672EC","8C0095","5133AB","AC193D","D24726","008A00","094AB2",
    		"00A0B1","2E8DEF","A700AE","643EBF","BF1E4B","DC572E","00A600","00A600","2E1700","4E0000","4E0038","2D004E","1F0068","001E4E","004D60","004A00",
    		"15992A","E56C19","B81B1B","B81B6C","691BB8","1B58B8","569CE3","00AAAA",
    		"83BA1F","D39D09","E064B7","F3B200","77B900","2572EB","AD103C","632F00",
    		"B01E00","C1004F","7200AC","4617B4","006AC1","008287","199900","00C13F",
    		"FF981D","FF2E12","FF1D77","AA40FF","1FAEFF","56C5FF","91D100","E1B700",
    		"FF76BC","00A3A3","FE7C22","261300","380000","40002E","250040","180052"  };
    private List<StationListBean> listoftrain;

    private static LayoutInflater inflater=null;
    String callfrom;

    public TimeTableDetailsAdapter(Context context,
    		List<StationListBean>  trainlist, String callfrom) {
		// TODO Auto-generated constructor stub
    	
    	 this.context = context;
         this.listoftrain = trainlist;
         this.callfrom = callfrom;
         inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
        return listoftrain.size();
    }

    @Override
	public Object getItem(int position) {
        return listoftrain.get(position);
    }

    @Override
	public long getItemId(int position) {
        return position;
    }

    @Override
	public View getView(int position, View convertView, ViewGroup viewGroup) {
    	int iscolorbg=0;
    	int colorPos = position % colors.length;
    	Context context = viewGroup.getContext();
    	StationListBean entry = listoftrain.get(position);
    	String station_name = entry.getStationName();
    	String stationcode = entry.getStationCode();
        String arrival_date_val = entry.getArrival_date();

     iscolorbg = entry.getColorback();
    	String arrival = entry.getArrival();
    	int min = 1;
    	int max = colors.length-1;

// Database Connection Open
		
		 
		 
			  
    	Random r = new Random();
    	int  colorbox= r.nextInt(max - min + 1) + min;
    	  
    	
       
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.timetabledetailsrow, null);
        }
       
       // Log.v("Log","fromstation="+fromstation +"Stationname="+station_name+"stationCode1="+stationcode);
        
       TextView station_name_field = (TextView) convertView.findViewById(R.id.title);
       
       TextView arrival_field = (TextView) convertView.findViewById(R.id.arrival);
       TextView stationcode_field = (TextView) convertView.findViewById(R.id.stationcode);
       TextView arrival_date = (TextView)convertView.findViewById(R.id.arrival_date);

       station_name_field.setText(station_name);
     
       stationcode_field.setText(stationcode);
       
       arrival_field.setText(arrival);

        if(!arrival_date_val.equalsIgnoreCase("")) {
            arrival_date.setVisibility(View.VISIBLE);
            arrival_date.setText(arrival_date_val);
        }

   //	Log.v("Log","match="+iscolorbg);
        LinearLayout relative=(LinearLayout) convertView.findViewById(R.id.rowid);
        //RelativeLayout rowrightid = (RelativeLayout) convertView.findViewById(R.id.rowrightid);
         if(iscolorbg == 1)
         {

            if(callfrom.equalsIgnoreCase("local")) {
                relative.setBackgroundColor(context.getResources().getColor(R.color.station_selected_color));
                //rowrightid.setBackgroundResource(R.drawable.boxleft_border_color_local);
                station_name_field.setTextColor(context.getResources().getColor(R.color.cardview_dark_background));
                arrival_field.setTextColor(context.getResources().getColor(R.color.cardview_dark_background));
                stationcode_field.setTextColor(context.getResources().getColor(R.color.cardview_dark_background));
                arrival_date.setTextColor(context.getResources().getColor(R.color.cardview_shadow_start_color));
            } else{
                relative.setBackgroundColor(context.getResources().getColor(R.color.station_selected_color));
                //rowrightid.setBackgroundResource(R.drawable.boxleft_border_color);
                station_name_field.setTextColor(context.getResources().getColor(R.color.cardview_dark_background));
                arrival_field.setTextColor(context.getResources().getColor(R.color.cardview_dark_background));
                stationcode_field.setTextColor(context.getResources().getColor(R.color.cardview_dark_background));
                arrival_date.setTextColor(context.getResources().getColor(R.color.cardview_shadow_start_color));
            }

         }
         else
         {
             relative.setBackgroundColor(Color.WHITE);
             //rowrightid.setBackgroundResource(R.drawable.boxleft_border);
             station_name_field.setTextColor(context.getResources().getColor(R.color.cardview_dark_background));
             arrival_field.setTextColor(context.getResources().getColor(R.color.cardview_dark_background));
             arrival_date.setTextColor(context.getResources().getColor(R.color.cardview_shadow_start_color));
             stationcode_field.setTextColor(context.getResources().getColor(R.color.sub_text));
         }



            return convertView;
        }
    
   
	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		
	}

    

   

}
