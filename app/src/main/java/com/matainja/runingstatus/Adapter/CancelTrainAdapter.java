package com.matainja.runingstatus.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.matainja.runingstatus.Model.CancelTrain;
import com.matainja.runingstatus.R;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by matainja on 05-Apr-17.
 */

public class CancelTrainAdapter extends BaseAdapter {

    ArrayList<CancelTrain> listOfData;
    private ArrayList<CancelTrain> arraylist;
    Context context;
    public CancelTrainAdapter(Context context , ArrayList<CancelTrain> cancelArrList){
        this.context = context;
        this.listOfData = cancelArrList;
        this.arraylist = new ArrayList<>();
        this.arraylist.addAll(cancelArrList);
    }
    @Override
    public int getCount() {
        return listOfData.size();
    }

    @Override
    public Object getItem(int position) {
        return listOfData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.rowcanceltrain, null);
        }

        TextView trainNo = (TextView) convertView.findViewById(R.id.trainNo);
        TextView trainName = (TextView) convertView.findViewById(R.id.trainName);
        TextView stn_name = (TextView) convertView.findViewById(R.id.stn_name);
        TextView start_time = (TextView) convertView.findViewById(R.id.start_time);

        trainNo.setText(listOfData.get(position).getTrain_no());
        trainName.setText(listOfData.get(position).getTrain_name());
        stn_name.setText(listOfData.get(position).getSrc_name()+" TO "+listOfData.get(position).getDest_name());
        start_time.setText(listOfData.get(position).getStart_time());
        return convertView;
    }

    public void filter(String charText) {
        charText = charText.toUpperCase(Locale.getDefault());
        listOfData.clear();
        if (charText.length() == 0) {
            listOfData.addAll(arraylist);
        } else {
            for (CancelTrain s : arraylist) {
                /*if (s.toLowerCase(Locale.getDefault()).contains(charText)) {
                    listOfData.add(s);
                }*/
                if(s.getTrain_name().toLowerCase().startsWith(charText.toString().toLowerCase()) || s.getTrain_name().toUpperCase().startsWith(charText.toString().toUpperCase())){
                    listOfData.add(s);
                }
            }
        }
        notifyDataSetChanged();
    }
}
