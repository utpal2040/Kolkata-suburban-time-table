package com.matainja.runingstatus.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.matainja.runingstatus.Model.TimetableListBean;
import com.matainja.runingstatus.R;
import com.matainja.runingstatus.TimeTableDetails;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

/**
 * Created by matainja on 28-Mar-17.
 */

public class RunningRecyclerViewAdapter extends RecyclerView.Adapter<RunningRecyclerViewAdapter.RunningRecyclerHolder>{

    private Context context;
    private String[] colors = new String[] { "008299", "2672EC","8C0095","5133AB","AC193D","D24726","008A00","094AB2",
            "00A0B1","2E8DEF","A700AE","643EBF","BF1E4B","DC572E","00A600","00A600","2E1700","4E0000","4E0038","2D004E","1F0068","001E4E","004D60","004A00",
            "15992A","E56C19","B81B1B","B81B6C","691BB8","1B58B8","569CE3","00AAAA",
            "83BA1F","D39D09","E064B7","F3B200","77B900","2572EB","AD103C","632F00",
            "B01E00","C1004F","7200AC","4617B4","006AC1","008287","199900","00C13F",
            "FF981D","FF2E12","FF1D77","AA40FF","1FAEFF","56C5FF","91D100","E1B700",
            "FF76BC","00A3A3","FE7C22","261300","380000","40002E","250040","180052"  };
    private List<HashMap<String, String>> listofstation;

    private static LayoutInflater inflater=null;



    public RunningRecyclerViewAdapter(Context context2,
                                      ArrayList<HashMap<String, String>> oslist) {
        // TODO Auto-generated constructor stub

        this.context = context2;
        this.listofstation = oslist;



    }

    @Override
    public RunningRecyclerViewAdapter.RunningRecyclerHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rowstyle, parent, false);
        return new RunningRecyclerViewAdapter.RunningRecyclerHolder(view);
    }

    @Override
    public void onBindViewHolder(RunningRecyclerViewAdapter.RunningRecyclerHolder holder, int position) {
        int colorPos = position % colors.length;

        int min = 1;
        int max = colors.length-1;

        Random r = new Random();
        int  colorbox= r.nextInt(max - min + 1) + min;



        HashMap<String, String> stations;
        stations = listofstation.get(position);

        String stationcode=stations.get("station_code");
        String is_arrived=stations.get("has_arrived");
        String is_departure=stations.get("has_departed");
        String arrived_text="";
        String Eta = "ETA : ";
        Log.e("departured",stationcode+"=="+is_arrived+"/"+is_departure);
        if(is_arrived.compareTo("true")== 0 && is_departure.compareTo("true")==0 )
        {
            arrived_text ="Arrived/Departure";
            Eta = "Arrv/Dep : ";
        }
        else if (is_arrived.equalsIgnoreCase("true") && is_departure.equalsIgnoreCase("false"))
            arrived_text ="Arrived";
        holder.name.setText(stations.get("station_name")+ " ("+stationcode+")");
        holder.arrival.setText(stations.get("scharr"));
        holder.departure.setText(stations.get("schdep"));
        holder.status.setText(stations.get("status"));
        holder.reachtime.setText(Eta +stations.get("actarr")+ " /" + " "+stations.get("actdep"));
        holder.day_distance.setText(stations.get("distance")+ " KM");
        holder.day.setText(stations.get("day")+ " Days");
        holder.depatured.setText(arrived_text);
    }

    @Override
    public int getItemCount() {
        return listofstation.size();
    }

    class RunningRecyclerHolder extends RecyclerView.ViewHolder {
        TextView name,arrival,departure,reachtime,status,day,day_distance,depatured;

        public RunningRecyclerHolder(View v) {
            super(v);

            name = (TextView) v.findViewById(R.id.title);
            arrival = (TextView) v.findViewById(R.id.tarrival);
            departure = (TextView) v.findViewById(R.id.tdeparture);
            reachtime = (TextView) v.findViewById(R.id.arrival_departure);
            status = (TextView) v.findViewById(R.id.remarks);
            day = (TextView) v.findViewById(R.id.day);
            day_distance = (TextView) v.findViewById(R.id.day_distance);
            depatured = (TextView) v.findViewById(R.id.reached);
        }
    }
}
