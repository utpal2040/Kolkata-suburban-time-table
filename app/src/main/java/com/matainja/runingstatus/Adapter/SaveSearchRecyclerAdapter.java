package com.matainja.runingstatus.Adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.matainja.runingstatus.Database.DatabaseAdapterSaveSearch;
import com.matainja.runingstatus.Model.SaveSearchBean;
import com.matainja.runingstatus.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

/**
 * Created by matainja on 05-Apr-17.
 */

public class SaveSearchRecyclerAdapter extends RecyclerView.Adapter<SaveSearchRecyclerAdapter.SaveSearchRecyclerHolder> {

    private List<SaveSearchBean> SaveSearchlist;
    private Context context;


    public SaveSearchRecyclerAdapter(Context context,
                                     List<SaveSearchBean>  SaveSearchlist) {
        // TODO Auto-generated constructor stub

        this.context = context;
        this.SaveSearchlist = SaveSearchlist;



    }

    @Override
    public SaveSearchRecyclerAdapter.SaveSearchRecyclerHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.savesearchrow, parent, false);
        return new SaveSearchRecyclerAdapter.SaveSearchRecyclerHolder(view);
    }


    @Override
    public void onBindViewHolder(SaveSearchRecyclerAdapter.SaveSearchRecyclerHolder holder, final int position) {


        SaveSearchBean entry = SaveSearchlist.get(position);


        String searchName = entry.getName();
        String fromStation = entry.getSrcStation();
        String toStation = entry.getDesstation();

        holder.viewSearchName.setText(searchName);
        holder.Viewtoarrival.setText(toStation);
        holder.Viewfromarrival.setText(fromStation);

        holder.imageView.setTag(new Integer(position));

        holder.imageView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                // Database Connection Open

                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case DialogInterface.BUTTON_POSITIVE:
                                //Yes button clicked
                                DatabaseAdapterSaveSearch dbHelpersave = new DatabaseAdapterSaveSearch(context);
                                dbHelpersave.open();
                                SaveSearchBean delentry = SaveSearchlist.get(position);
                                dbHelpersave.delSaveSearch(delentry.getId());
                                SaveSearchlist.remove(SaveSearchlist.get(position));
                                SaveSearchRecyclerAdapter.this.notifyDataSetChanged();
                                dbHelpersave.close();
                                Toast.makeText(context, "Sucessfully Deleted Route", Toast.LENGTH_SHORT).show();
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                //No button clicked
                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage("Are you sure?").setPositiveButton("Yes", dialogClickListener)
                        .setNegativeButton("No", dialogClickListener).show();



            }
        });

    }

    @Override
    public int getItemCount() {
        return SaveSearchlist.size();
    }

    class SaveSearchRecyclerHolder extends RecyclerView.ViewHolder {
        TextView viewSearchName,Viewtoarrival,Viewfromarrival;
        ImageView imageView;

        public SaveSearchRecyclerHolder(View v) {
            super(v);

            viewSearchName = (TextView) v.findViewById(R.id.title);
            Viewtoarrival = (TextView) v.findViewById(R.id.tdeparture);
            Viewfromarrival = (TextView) v.findViewById(R.id.tarrival);
            imageView = (ImageView) v.findViewById(R.id.delete);
        }
    }
}
