
package com.matainja.runingstatus.Adapter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;



import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.matainja.runingstatus.Model.AvailableJson;
import com.matainja.runingstatus.R;


public class AvailableAdapter extends BaseAdapter implements OnClickListener {
	
    
	private Context context;
    
    private List<AvailableJson> jtrainstatuslist;

    private static LayoutInflater inflater=null;
    

    public AvailableAdapter(Context context,
    		List<AvailableJson>  jtrainstatuslist) {
		// TODO Auto-generated constructor stub
    	
    	 this.context = context;
         this.jtrainstatuslist = jtrainstatuslist;
         inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
         
        
	}

	@Override
	public int getCount() {
        return jtrainstatuslist.size();
    }

    @Override
	public Object getItem(int position) {
        return jtrainstatuslist.get(position);
    }

    @Override
	public long getItemId(int position) {
        return position;
    }

    @Override
	public View getView(final int position, View convertView, ViewGroup viewGroup) {
    	// Log.v("error","error0");
    	
    	final Context context = viewGroup.getContext();
    	AvailableJson entry = jtrainstatuslist.get(position);
    
    	
    	
// Database Connection Open
		

			  
    	
    	
    	  
    	
       
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.availablerow, null);
        }
       
       TextView jstatus = (TextView) convertView.findViewById(R.id.jstatus);
       TextView jdate = (TextView) convertView.findViewById(R.id.jdate);
     
       
       
       jstatus.setText(entry.getStatus());
     
       jdate.setText(entry.getDate());
       
      
     
    
       
        
       
       
       
       
        return convertView;
    }
    
   
	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		
	}

    

   

}
