package com.matainja.runingstatus.Adapter;


import java.util.ArrayList;
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.matainja.runingstatus.Model.PeopleAge;
import com.matainja.runingstatus.R;

/***** Adapter class extends with ArrayAdapter ******/
public class CustomAdapterAge extends ArrayAdapter<String>{
     
    private Activity activity;
    private ArrayList data;
    public Resources res;
    PeopleAge tempValues=null;
    LayoutInflater inflater;
     
    /*************  CustomAdapter Constructor *****************/
    public CustomAdapterAge(
    		Activity activitySpinner, 
                          int textViewResourceId,   
                          ArrayList objects
                      
                         ) 
     {
        super(activitySpinner, textViewResourceId, objects);
         
        /********** Take passed values **********/
        activity = activitySpinner;
        data     = objects;
       
    
        /***********  Layout inflator to call external xml layout () **********************/
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
         
      }
 
    @Override
    public View getDropDownView(int position, View convertView,ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }
 
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }
 
    // This funtion called for each row ( Called data.size() times )
    public View getCustomView(int position, View convertView, ViewGroup parent) {
 
        /********** Inflate spinner_rows.xml file for each row ( Defined below ) ************/
        View row = inflater.inflate(R.layout.spinner_row, parent, false);
         
        /***** Get each Model object from Arraylist ********/
        tempValues = null;
        tempValues = (PeopleAge) data.get(position);
         
        TextView label        = (TextView)row.findViewById(R.id.name);
        
         
        if(position==0){
             
            // Default selected Spinner item 
            label.setText("Please select Age");
           
        }
        else
        {
            // Set values for spinner each row 
            label.setText(tempValues.getName());
            
             
        }   
 
        return row;
      }
 }