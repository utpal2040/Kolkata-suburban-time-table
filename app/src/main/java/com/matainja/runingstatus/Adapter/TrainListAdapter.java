package com.matainja.runingstatus.Adapter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;



import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.TextView;

import com.matainja.runingstatus.Model.Train;
import com.matainja.runingstatus.R;


public class TrainListAdapter extends BaseAdapter implements OnClickListener {
    
	private Context context;
    private String[] colors = new String[] { "008299", "2672EC","8C0095","5133AB","AC193D","D24726","008A00","094AB2",
    		"00A0B1","2E8DEF","A700AE","643EBF","BF1E4B","DC572E","00A600","00A600","2E1700","4E0000","4E0038","2D004E","1F0068","001E4E","004D60","004A00",
    		"15992A","E56C19","B81B1B","B81B6C","691BB8","1B58B8","569CE3","00AAAA",
    		"83BA1F","D39D09","E064B7","F3B200","77B900","2572EB","AD103C","632F00",
    		"B01E00","C1004F","7200AC","4617B4","006AC1","008287","199900","00C13F",
    		"FF981D","FF2E12","FF1D77","AA40FF","1FAEFF","56C5FF","91D100","E1B700",
    		"FF76BC","00A3A3","FE7C22","261300","380000","40002E","250040","180052"  };
    private List<Train> listoftrain;

    private static LayoutInflater inflater=null;
    

    public TrainListAdapter(Context context,
    		List<Train>  trainlist) {
		// TODO Auto-generated constructor stub
    	
    	 this.context = context;
         this.listoftrain = trainlist;
         inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
        return listoftrain.size();
    }

    @Override
	public Object getItem(int position) {
        return listoftrain.get(position);
    }

    @Override
	public long getItemId(int position) {
        return position;
    }

    @Override
	public View getView(int position, View convertView, ViewGroup viewGroup) {
    	int colorPos = position % colors.length;
    	Train entry = listoftrain.get(position);
    	int min = 1;
    	int max = colors.length-1;

    	Random r = new Random();
    	int  colorbox= r.nextInt(max - min + 1) + min;
    	  
    	
       
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.trainrowstyle, null);
        }
      
        TextView train_name = (TextView) convertView.findViewById(R.id.train_name);
        TextView source_station = (TextView) convertView.findViewById(R.id.source);
        TextView des_station = (TextView) convertView.findViewById(R.id.destination);
       
        String train_name_val = entry.getTrainName();
        String train_no=entry.getTrainNo(); 
        String Source_station = entry.getstart_station();
        String des_station_code = entry.getend_station();
        train_name.setText(train_name_val+"( " + train_no +" )"); 
       //raon_no.setText(train_no);
       // source_station.setText("Source : "+Source_station);
      // des_station.setText("Destination : "+des_station_code);
      
       /* Typeface custom_font = Typeface.createFromAsset(context.getAssets(),
      	      "fonts/Bodoni-Italic.TTF");
        train_name.setTypeface(custom_font);
        source_station.setTypeface(custom_font);
        des_station.setTypeface(custom_font);*/
       // convertView.setBackgroundColor(colors[colorPos]);
        
        
        
       
        return convertView;
    }
    
   
	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		
	}

    

   

}
