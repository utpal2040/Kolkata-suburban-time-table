package com.matainja.runingstatus.Adapter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;



import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.matainja.runingstatus.Database.DatabaseAdapter;
import com.matainja.runingstatus.Model.SuggestStationBean;
import com.matainja.runingstatus.R;


public class SuggestionStationsAdapter extends BaseAdapter implements OnClickListener {
	 private DatabaseAdapter dbHelper;
	 private Cursor traininfo;
    
	private Context context;
    
    private List<SuggestStationBean> suggestionlist;

    private static LayoutInflater inflater=null;
    

    public SuggestionStationsAdapter(Context context,
    		List<SuggestStationBean>  suggestionlist) {
		// TODO Auto-generated constructor stub
    	
    	 this.context = context;
         this.suggestionlist = suggestionlist;
         inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
        return suggestionlist.size();
    }

    @Override
	public Object getItem(int position) {
        return suggestionlist.get(position);
    }

    @Override
	public long getItemId(int position) {
        return position;
    }

    @Override
	public View getView(int position, View convertView, ViewGroup viewGroup) {
    	
    	Context context = viewGroup.getContext();
    	SuggestStationBean entry = suggestionlist.get(position);
    	String station_name = entry.getsStationName();
    	
    	
     


		
		 
		 
			  
    	
    	  
    	
       
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.suggestionlistrow, null);
        }
       
       // Log.v("Log","fromstation="+fromstation +"Stationname="+station_name+"stationCode1="+stationcode);
        
       TextView station_name_field = (TextView) convertView.findViewById(R.id.title);
       station_name_field.setText(station_name);
        
      /* Typeface custom_font = Typeface.createFromAsset(context.getAssets(),
      	      "fonts/Bodoni-Italic.TTF");
       // train_name.setTypeface(custom_font);
       
       station_name_field.setTypeface(custom_font);*/
       
       // convertView.setBackgroundColor(colors[colorPos]);
       
   
      
        
       
        return convertView;
    }
    
   
	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		
	}

    

   

}
