package com.matainja.runingstatus.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.matainja.runingstatus.Model.StationListBean;
import com.matainja.runingstatus.Model.TimetableListBean;
import com.matainja.runingstatus.R;
import com.matainja.runingstatus.TimeTableDetails;

import java.util.List;
import java.util.Random;

/**
 * Created by matainja on 28-Mar-17.
 */

public class TimeTableDetailsRecyclerAdapter extends RecyclerView.Adapter<TimeTableDetailsRecyclerAdapter.TimeTableDetailsHolder>{
    private Context context;
    String callfrom;
    private String[] colors = new String[] { "008299", "2672EC","8C0095","5133AB","AC193D","D24726","008A00","094AB2",
            "00A0B1","2E8DEF","A700AE","643EBF","BF1E4B","DC572E","00A600","00A600","2E1700","4E0000","4E0038","2D004E","1F0068","001E4E","004D60","004A00",
            "15992A","E56C19","B81B1B","B81B6C","691BB8","1B58B8","569CE3","00AAAA",
            "83BA1F","D39D09","E064B7","F3B200","77B900","2572EB","AD103C","632F00",
            "B01E00","C1004F","7200AC","4617B4","006AC1","008287","199900","00C13F",
            "FF981D","FF2E12","FF1D77","AA40FF","1FAEFF","56C5FF","91D100","E1B700",
            "FF76BC","00A3A3","FE7C22","261300","380000","40002E","250040","180052"  };
    private List<StationListBean> listoftrain;



    public TimeTableDetailsRecyclerAdapter(Context context,
                                         List<StationListBean>  trainlist,String callfrom) {
        // TODO Auto-generated constructor stub

        this.context = context;
        this.listoftrain = trainlist;
        this.callfrom=callfrom;


    }

    @Override
    public TimeTableDetailsRecyclerAdapter.TimeTableDetailsHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.timetabledetailsrow, parent, false);
        return new TimeTableDetailsRecyclerAdapter.TimeTableDetailsHolder(view);
    }

    @Override
    public void onBindViewHolder(TimeTableDetailsRecyclerAdapter.TimeTableDetailsHolder holder, int position) {
        int colorPos = position % colors.length;
        int iscolorbg=0;

        final StationListBean entry = listoftrain.get(position);
        int min = 1;
        int max = colors.length-1;
        Random r = new Random();

        iscolorbg = entry.getColorback();
        int  colorbox= r.nextInt(max - min + 1) + min;
        String station_name = entry.getStationName();
        String stationcode = entry.getStationCode();
        String arrival = entry.getArrival();

        holder.station_name_field.setText(station_name);
        holder.stationcode_field.setText(stationcode);
        holder. arrival_field.setText(arrival);

        if(iscolorbg == 1)
        {

            if(callfrom.equalsIgnoreCase("local")) {
                holder.relative.setBackgroundColor(context.getResources().getColor(R.color.station_selected_color));
                //holder.rowrightid.setBackgroundResource(R.drawable.boxleft_border_color_local);
                holder.station_name_field.setTextColor(context.getResources().getColor(R.color.cardview_dark_background));
                holder.arrival_field.setTextColor(context.getResources().getColor(R.color.cardview_dark_background));
                holder.stationcode_field.setTextColor(context.getResources().getColor(R.color.sub_text));
            } else{
                holder.relative.setBackgroundColor(context.getResources().getColor(R.color.station_selected_color));
                //holder.rowrightid.setBackgroundResource(R.drawable.boxleft_border_color);
                holder. station_name_field.setTextColor(context.getResources().getColor(R.color.cardview_dark_background));
                holder.arrival_field.setTextColor(context.getResources().getColor(R.color.cardview_dark_background));
                holder.stationcode_field.setTextColor(context.getResources().getColor(R.color.sub_text));
            }

        }
        else
        {
            holder.relative.setBackgroundColor(Color.WHITE);
            //holder.rowrightid.setBackgroundResource(R.drawable.boxleft_border);
            holder.station_name_field.setTextColor(context.getResources().getColor(R.color.cardview_dark_background));
            holder.arrival_field.setTextColor(context.getResources().getColor(R.color.cardview_dark_background));
            holder.stationcode_field.setTextColor(context.getResources().getColor(R.color.sub_text));
        }

    }

    @Override
    public int getItemCount() {
        return listoftrain.size();
    }

    class TimeTableDetailsHolder extends RecyclerView.ViewHolder {
        TextView station_name_field,arrival_field,stationcode_field;
        RelativeLayout rowrightid;
        LinearLayout relative;
        public TimeTableDetailsHolder(View v) {
            super(v);
            relative=(LinearLayout) v.findViewById(R.id.rowid);
            rowrightid = (RelativeLayout) v.findViewById(R.id.rowrightid);

             station_name_field = (TextView) v.findViewById(R.id.title);

             arrival_field = (TextView) v.findViewById(R.id.arrival);
             stationcode_field = (TextView) v.findViewById(R.id.stationcode);
        }
    }
}
