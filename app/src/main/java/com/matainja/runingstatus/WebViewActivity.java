package com.matainja.runingstatus;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.matainja.runingstatus.Common.ConnectionDetector;

import java.util.ArrayList;

/**
 * Created by matainja on 20-Apr-17.
 */

public class WebViewActivity extends AppCompatActivity {
    Toolbar toolbar;
    WebView webview;
    String url;
    ConnectionDetector cd;
    ProgressDialog pDialog;
    SharedPreferences sp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.webview_content);


        initilizeToolbar();
        FindViews();

       // url="http://www.indianrail.gov.in/enquiry/PnrEnquiry.html#element";
        url= "http://www.indianrail.gov.in/pnr_Enq.html#element";
        if (cd.isConnectingToInternet()) {
            WebSettings settings = webview.getSettings();
            settings.setJavaScriptEnabled(true);
            webview.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
            webview.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView wv, String url) {


                    wv.loadUrl(url);

                    return true; // let WebView handle this event
                }

                @Override
                public void onPageFinished(WebView view, String url) {


                }

                @Override
                public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(WebViewActivity.this);
                    String message = "SSL Certificate error.";
                    switch (error.getPrimaryError()) {
                        case SslError.SSL_UNTRUSTED:
                            message = "The certificate authority is not trusted.";
                            break;
                        case SslError.SSL_EXPIRED:
                            message = "The certificate has expired.";
                            break;
                        case SslError.SSL_IDMISMATCH:
                            message = "The certificate Hostname mismatch.";
                            break;
                        case SslError.SSL_NOTYETVALID:
                            message = "The certificate is not yet valid.";
                            break;
                    }
                    message += " Do you want to continue anyway?";

                    builder.setTitle("SSL Certificate Error");
                    builder.setMessage(message);
                    builder.setPositiveButton("continue", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            handler.proceed();
                        }
                    });
                    builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            handler.cancel();
                        }
                    });
                    final AlertDialog dialog = builder.create();
                    dialog.show();
                }

            });
            webview.loadUrl(url);
        }else{
            String msglert="Please Check your Network Connection !!";
            NotificationAlert(msglert);
        }
       }

    private void FindViews() {
       webview=(WebView)findViewById(R.id.webview);
        cd = new ConnectionDetector(getApplicationContext());
    }

    private void initilizeToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //Show back icon
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //set Title
        getSupportActionBar().setTitle("PASSENGER RESERVATION ENQUIRY");
        //set Logo
        //  getSupportActionBar().setLogo(R.drawable.ic_action_navigation_close);
        //back icon Click Event
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.anim_slide_in_right,
                        R.anim.anim_slide_out_right);
            }
        });
        sp = getSharedPreferences("themeColor", Context.MODE_PRIVATE);
        if(sp.getInt("expressColor",0)!=0) {
            toolbar.setBackgroundColor(sp.getInt("expressColor",0));
            int[] demo_colors = getResources().getIntArray(R.array.demo_colors);
            int[] demo_colors_mat = getResources().getIntArray(R.array.demo_colors_mat);

            ArrayList<String> colorString = new ArrayList<String>();
            ArrayList<String> colorString_mat = new ArrayList<String>();

            for (int i = 0; i < demo_colors.length; i++) {
                Log.e("Test", demo_colors[i] + "");
                //Log.e("Test", demo_colors[i] + "");
                colorString.add(String.valueOf(demo_colors[i]));

            }

            for (int i = 0; i < demo_colors_mat.length; i++) {
                Log.e("Test", demo_colors[i] + "");
                //Log.e("Test", demo_colors[i] + "");
                colorString_mat.add(String.valueOf(demo_colors_mat[i]));

            }
            Log.e("position ", colorString.indexOf(String.valueOf(sp.getInt("expressColor", 0))) + "");
            int color_mat_val = Integer.parseInt(colorString_mat.get(colorString.indexOf(String.valueOf(sp.getInt("expressColor", 0)))));

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(color_mat_val);
            }
        }else{
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(getResources().getColor(R.color.colorExpressStatus));
            }
        }

    }
    public void NotificationAlert(String Message)
    {

        android.app.AlertDialog.Builder alertDialog2 = new android.app.AlertDialog.Builder(
                WebViewActivity.this);

        // Setting Dialog Title
        alertDialog2.setTitle("Message");

        // Setting Dialog Message
        alertDialog2.setMessage(Message);

        // Setting Icon to Dialog


        // Setting Positive "Yes" Btn
        alertDialog2.setNeutralButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog


                        //finish();


                    }
                });
        alertDialog2.show();

    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // Check if the key event was the Back button and if there's history
        if ((keyCode == KeyEvent.KEYCODE_BACK) && webview.canGoBack()) {
            webview.goBack();
            return true;
        }
        else
        {
            finish();
            overridePendingTransition(R.anim.anim_slide_in_right,
                    R.anim.anim_slide_out_right);
            // finish the activity
        }
        // If it wasn't the Back key or there's no web page history, bubble up to the default
        // system behavior (probably exit the activity)
        return super.onKeyDown(keyCode, event);
    }
}
