package com.matainja.runingstatus;


import android.accounts.AccountManager;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.ColorStateList;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TextInputLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.AccountPicker;
import com.matainja.runingstatus.Common.ConnectionDetector;
import com.matainja.runingstatus.Connectivity.HttpConnection;
import com.matainja.runingstatus.Database.DatabaseAdapter;
import com.matainja.runingstatus.GoogleAnalyticsApp.TrackerName;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;


public class feedback extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{
	private static final int SOME_REQUEST_CODE = 1;
	EditText mName;
	EditText mEmail;
	EditText mfeedback;
	Button mSend;
	TextView mfeedbacktx;
	String subject;
	EditText mlocation;
	EditText memail;
	String msg;
	String possibleEmail;
	Context context;
	String versionName;
	int currentapiVersion = 0;
	private DatabaseAdapter dbHelper;
    Toolbar toolbar;
	DrawerLayout drawer;
	SharedPreferences sp;
	CircleImageView profile_image;
	TextView nameTxt;
	RelativeLayout headerFrame;
	String email;
	TextInputLayout input_layout_subject,input_layout_Email,input_layout_location,input_layout_feedback;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		//Remove title bar
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.feedback);
		overridePendingTransition(R.anim.anim_slide_in_left,
				R.anim.anim_slide_out_left);
		context = getApplicationContext();
		toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
        toolbar.setTitle("Feedback");
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		/*toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
                Intent i = new Intent(feedback.this,LocalTrain.class);
				i.putExtra("callfrom","other");
                startActivity(i);
				finish();
			}
		});*/

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.setDrawerIndicatorEnabled(true);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

		View headerview = navigationView.getHeaderView(0);
		profile_image = (CircleImageView) headerview.findViewById(R.id.profile_image);
		nameTxt = (TextView) headerview.findViewById(R.id.name);
		headerFrame = (RelativeLayout) headerview.findViewById(R.id.headerFrame);
		headerFrame.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(email.equalsIgnoreCase("")){
					Intent i = new Intent(feedback.this, WelcomeActivity.class);
					startActivity(i);
					finish();
				}else {
					Intent i = new Intent(feedback.this, SettingsActivity.class);
					startActivity(i);
				}
			}
		});

		sp = getSharedPreferences("themeColor", Context.MODE_PRIVATE);


		// view width calculate
		currentapiVersion = android.os.Build.VERSION.SDK_INT;
        /* TRacking Code */
		if (currentapiVersion >= android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
			Tracker t = ((GoogleAnalyticsApp) getApplication()).getTracker(TrackerName.APP_TRACKER);
			t.setScreenName("Feedback");
			t.send(new HitBuilders.AppViewBuilder().build());
		}

		mName = (EditText) findViewById(R.id.name);
		mlocation = (EditText) findViewById(R.id.location);
		memail = (EditText) findViewById(R.id.Email);
		mfeedback = (EditText) findViewById(R.id.feedback);
		mSend = (Button) findViewById(R.id.send);

		input_layout_subject = (TextInputLayout) findViewById(R.id.input_layout_subject);
		input_layout_Email = (TextInputLayout) findViewById(R.id.input_layout_Email);
		input_layout_location = (TextInputLayout) findViewById(R.id.input_layout_location);
		input_layout_feedback = (TextInputLayout) findViewById(R.id.input_layout_feedback);

/// font face define and set

		/*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			Window window = getWindow();
			window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
			window.setStatusBarColor(getResources().getColor(R.color.colorLocalStatus));
		}*/

		if(sp.getInt("localColor",0)!=0) {
			toolbar.setBackgroundColor(sp.getInt("localColor",0));
			mSend.setBackgroundColor(sp.getInt("localColor",0));

			ColorStateList colorStateList = ColorStateList.valueOf(sp.getInt("localColor",0));
			ViewCompat.setBackgroundTintList(mName, colorStateList);
			ViewCompat.setBackgroundTintList(mlocation, colorStateList);
			ViewCompat.setBackgroundTintList(memail, colorStateList);
			ViewCompat.setBackgroundTintList(mfeedback, colorStateList);

			input_layout_subject.setHintTextAppearance(sp.getInt("localColor",0));
			input_layout_Email.setHintTextAppearance(sp.getInt("localColor",0));
			input_layout_location.setHintTextAppearance(sp.getInt("localColor",0));
			input_layout_feedback.setHintTextAppearance(sp.getInt("localColor",0));



			int[] demo_colors = getResources().getIntArray(R.array.demo_colors);
			int[] demo_colors_mat = getResources().getIntArray(R.array.demo_colors_mat);

			ArrayList<String> colorString = new ArrayList<String>();
			ArrayList<String> colorString_mat = new ArrayList<String>();

			for (int i = 0; i < demo_colors.length; i++) {

				colorString.add(String.valueOf(demo_colors[i]));

			}

			for (int i = 0; i < demo_colors_mat.length; i++) {

				colorString_mat.add(String.valueOf(demo_colors_mat[i]));

			}
			Log.e("position ",colorString.indexOf(String.valueOf(sp.getInt("localColor",0))) + "");
			int color_mat_val = Integer.parseInt(colorString_mat.get(colorString.indexOf(String.valueOf(sp.getInt("localColor", 0)))));

			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
				Window window = getWindow();
				window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
				window.setStatusBarColor(color_mat_val);
			}
		}

		Intent intent = AccountPicker.newChooseAccountIntent(null, null, new String[]{"com.google"}, false, null, null, null, null);
		startActivityForResult(intent, SOME_REQUEST_CODE);

		// email auto populate
		/*if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.GET_ACCOUNTS) != PackageManager.PERMISSION_GRANTED) {
			// TODO: Consider calling
			//    ActivityCompat#requestPermissions
			// here to request the missing permissions, and then overriding
			//   public void onRequestPermissionsResult(int requestCode, String[] permissions,
			//                                          int[] grantResults)
			// to handle the case where the user grants the permission. See the documentation
			// for ActivityCompat#requestPermissions for more details.
			return;
		}
		Account[] accounts = AccountManager.get(this).getAccounts();
        for (Account account : accounts) 
        {

          // TODO: Check possibleEmail against an email regex or treat

          // account.name as an email address only for certain account.type values.
            possibleEmail = account.name;

        }*/
        
        try {
			 versionName = context.getPackageManager()
				    .getPackageInfo(context.getPackageName(), 0).versionName;
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
        //memail.setText(possibleEmail);
       /* Typeface custom_font = Typeface.createFromAsset(getAssets(),
      	      "fonts/Bodoni-Italic.TTF");
        mfeedbacktx.setTypeface(custom_font);*/
        //TextView homepage_header_text = (TextView) findViewById(R.id.homepage_header_text);
        //homepage_header_text.setTypeface(custom_font);
        /*ImageView  	menuback =(ImageView)findViewById(R.id.close);
        menuback.setOnClickListener(new View.OnClickListener() {
        	
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
           	 //overridePendingTransition(R.anim.lefttoright, R.anim.righttoleft);
           	Intent intent = new Intent(getApplicationContext(), LocalTrain.class);
         	
         	startActivity(intent);
         	finish();
            	
            	// overridePendingTransition(R.anim.lefttoright, R.anim.righttoleft);
            }
        });*/
        
        mSend.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
			
				if(mName.getText().length()== 0)
				{
		    		CharSequence text = "Please Enter  Subject!";
					int duration = Toast.LENGTH_SHORT;

					Toast toast = Toast.makeText(getApplicationContext(), text, duration);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				}
				/*else if(mEmail.getText().length()== 0)
				{
		    		CharSequence text = "Please Enter Email!";
					int duration = Toast.LENGTH_SHORT;

					Toast toast = Toast.makeText(getApplicationContext(), text, duration);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				}*/
				else if(mfeedback.getText().length()== 0)
				{
		    		CharSequence text = "Please Enter feedback!";
					int duration = Toast.LENGTH_SHORT;

					Toast toast = Toast.makeText(getApplicationContext(), text, duration);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				}
				else
		    	{
					 ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
		                
		              // Check if Internet present
		              if (!cd.isConnectingToInternet()) {
		            	  Toast.makeText(getApplicationContext(),"Internet Problem!", Toast.LENGTH_SHORT).show(); 
		              }
		              else
		            	  
					new ProcessEmail().execute();
					
					
		    	}
			}

		});
        
       
        
	      // drawer define
	     
	      
	     
	     
	     /// end left menu list 
	     
	     
	     
	     
        
        
        
        
        
        // Auto suggest train search
        
       
        
        // drawer open
        
       
        
        
        
       
 
       // trainnotxt.setTypeface(custom_font);
      //  dayofjourney.setTypeface(custom_font);
       
        //Remove notification bar
        //this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    	//Set save button and action  onClick
        
        
      
        
      
       
/// list view End


		ApplyUserInfo();
        
        
    }

	public void ApplyUserInfo(){
		SharedPreferences userPref = getSharedPreferences("userCred",MODE_PRIVATE);
		String name = userPref.getString("name","Guest User");
		email = userPref.getString("email","");
		String imageUrl = userPref.getString("image_value","");

		Log.e("imageUrl ",imageUrl);
		nameTxt.setText(name);
		if(!imageUrl.equalsIgnoreCase("")) {
			Picasso.with(context).load(imageUrl).memoryPolicy(MemoryPolicy.NO_CACHE).into(profile_image);
		}
	}

	protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
		if (requestCode == SOME_REQUEST_CODE && resultCode == RESULT_OK) {
			String emailAddress = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);

			Log.e("emailAddress==",emailAddress);
			memail.setText(emailAddress);
			// do something...
		}
	}
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the camera action
            Intent intent = new Intent(getApplicationContext(), LocalTrain.class);
            intent.putExtra("callfrom","other");
            startActivity(intent);
            finish();

        } else if (id == R.id.nav_runningstatus) {

            Intent intent = new Intent(getApplicationContext(), Homepage.class);

            startActivity(intent);
            finish();

        } else if (id == R.id.nav_cancelled) {

			Intent intent = new Intent(getApplicationContext(), ActivityCancelTrain.class);
			startActivity(intent);
			finish();

        }  else if (id == R.id.nav_pnrstatus) {

            Intent intent = new Intent(getApplicationContext(), Pnr.class);
            startActivity(intent);
            finish();

        } else if (id == R.id.nav_saveroute) {

			Intent intent = new Intent(getApplicationContext(), SaveSearch.class);
			startActivity(intent);
			finish();

        } else if (id == R.id.nav_feedback) {
            /*Intent intent = new Intent(getApplicationContext(), feedback.class);

            startActivity(intent);
            finish();
*/
        } else if (id == R.id.nav_fblike) {

            Intent notificationIntent =null;
            notificationIntent = new Intent(Intent.ACTION_VIEW);
            notificationIntent.setData(Uri.parse("https://www.facebook.com/localtimetable"));
            startActivity(notificationIntent);
            //finish();

        } else if (id == R.id.nav_theme) {

			if(email.equalsIgnoreCase("")){
				Intent i = new Intent(feedback.this, WelcomeActivity.class);
				startActivity(i);
				finish();
			}else {
				Intent i = new Intent(feedback.this, SettingsActivity.class);
				startActivity(i);
			}


		} else if (id == R.id.nav_rate) {

            Intent notificationIntent =null;
            notificationIntent = new Intent(Intent.ACTION_VIEW);
            notificationIntent.setData(Uri.parse("market://details?id="+getApplicationContext().getPackageName()));
            startActivity(notificationIntent);
            //finish();

        } else if (id == R.id.nav_aboutus) {

            Intent intent = new Intent(getApplicationContext(), BuildWebsite.class);
            startActivity(intent);
            finish();
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private class ProcessEmail extends AsyncTask<String, String, JSONObject> {

		/**
		 * Defining Process dialog
		 **/
	        private ProgressDialog pDialog;

	        String email,password,fname,lname,phone_val,location,useremail;
	        
	        @Override
	        protected void onPreExecute() {
	            super.onPreExecute();
	               
	               
	               String adminemail= getResources().getString(R.string.adminemail) ;
					 subject = mName.getText().toString();
				 msg = mfeedback.getText().toString();
				      location = mlocation.getText().toString();
				      useremail = memail.getText().toString();

		            pDialog = new ProgressDialog(feedback.this);
		            pDialog.setMessage("Sending fedback ...");
		            pDialog.setIndeterminate(false);
		            pDialog.setCancelable(true);
		            pDialog.show();
	        }

	        @Override
	        protected JSONObject doInBackground(String... args) {

	        HttpConnection searchFunctions = new HttpConnection();
	        JSONObject json = searchFunctions.EmailSend(subject, msg,location,useremail,versionName);

	            return json;
	        }
	        
	       @SuppressWarnings("deprecation")
		   @Override
	        protected void onPostExecute(JSONObject json) {
		       /**
		        * Checks for success message.
		        **/
	    	      try {
	                    if (json.getString(HttpConnection.KEY_SUCCESS) != null) {
	                        //registerErrorMsg.setText("");
	                        String res = json.getString("success");
	                        String msg = json.getString("message");
	                       // String red = json.getString(KEY_ERROR);
Log.v("success"," == "+res);
	                        if(Integer.parseInt(res) == 1){
	                        	Log.v("success1"," == true");
	                        	// pDialog.setTitle("Getting Data");
		                         //pDialog.setMessage("Loading Info");
	                            //Toast.makeText(getApplicationContext(),"Successfully Registered", Toast.LENGTH_SHORT).show();
	                            AlertDialog alertDialog = new AlertDialog.Builder(
	                                    feedback.this).create();
	             
			                    // Setting Dialog Title
			                    alertDialog.setTitle("Message");
			             
			                    // Setting Dialog Message
			                    alertDialog.setMessage(msg);
			             
			                    // Setting Icon to Dialog
			                    alertDialog.setIcon(R.drawable.appsicon);
			             
			                    // Setting OK Button
			                    alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
			                            public void onClick(DialogInterface dialog, int which) {
			                            // Write your code here to execute after dialog closed
			                            	dialog.cancel();
			                            	mName.setText("");
						                    mfeedback.setText("");
				                           
			                            }
			                    });
			             
			                    // Showing Alert Message
			                    alertDialog.show();
			                    
	                        }

	                        else {
	                        	
	                            Toast.makeText(getApplicationContext(),msg, Toast.LENGTH_SHORT).show();
	                        }
	                    }
	                    else{
	                        Toast.makeText(getApplicationContext(),"Sending error!!", Toast.LENGTH_SHORT).show();
	                        }

	                } catch (JSONException e) {
	                    e.printStackTrace();
	                }
	    	      
	    	      if ((pDialog != null) && pDialog.isShowing()) { 
	    	    	  pDialog.dismiss();
	    	     }
	    	      	   
	           }
	       }  
    public void successAlert()
    {
  	
  	  AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(
 		        feedback.this);

 		// Setting Dialog Title
 		alertDialog2.setTitle("Success");

 		// Setting Dialog Message
 		alertDialog2.setMessage("Sent feedback, we will contact shortly!!");

 		// Setting Icon to Dialog
 		

 		// Setting Positive "Yes" Btn
 		alertDialog2.setNeutralButton("OK",
 		        new DialogInterface.OnClickListener() {
 		            public void onClick(DialogInterface dialog, int which) {
 		                // Write your code here to execute after dialog
 		            	
 		            	Intent intent = new Intent(getApplicationContext(), LocalTrain.class);
						intent.putExtra("callfrom","other");
 		             	startActivity(intent);
 		             	finish();
 	                 

 		                
 		            }
 		        });
 		alertDialog2.show();
 		
    }
	
    // add items into spinner dynamically
   
    @Override
    public void onBackPressed() {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();

            Intent intent = new Intent(getApplicationContext(), LocalTrain.class);
            intent.putExtra("callfrom", "other");
            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.anim_slide_in_right,
                    R.anim.anim_slide_out_right);
        }
    }

	 
}