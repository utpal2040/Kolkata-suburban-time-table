package com.matainja.runingstatus.Connectivity;

import android.util.Log;

import com.matainja.runingstatus.Common.Constants;

import org.json.JSONObject;

import java.util.Formatter;
import java.util.HashMap;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by matainja on 09-Jan-17.
 */

public class HttpConnection {
    private JSONParser jsonParser;
    public HttpConnection() {
        jsonParser = new JSONParser();
    }

    static String Base_URL = "http://www.matainja.com/androidApp/Api/v3/";
    String LOGIN_URL = Base_URL+"emailsend.php";
    public static String KEY_SUCCESS = "success";
    public static String KEY_ERROR = "error";
    private static String PNR_URL = Base_URL+"pnr_curl.php";
    private static String API = "qicde9951";
    private static String VERSION_CHECK = Base_URL+"version_check.php";

    public JSONObject getTrainAvailability(String adate,String Tclass,String fscode,String quote,String tnum,String tscode){

    	/*$str_order ="class"."date"."format"."fscode"."pbapikey"."quote"."tnum"."tscode"*/;

        String base_string = Tclass+adate+"json"+fscode+"314e3568e95c79f21f70f2f38c332778"+quote+tnum+tscode;
        base_string = base_string.toLowerCase();
        //String base_string = "json"+"314e3568e95c79f21f70f2f38c332778"+pnr;
        String key = "'e23377f3fe611f99b13c3f236cc9b6be";

        HashMap<String, String> params = new HashMap<>();
        params.put("tag", "");

        // Base 64 Encode the results

        String retVal =getHasvalFare(base_string);// "5f932d5f2f20baefcc8e77a6951c845c72172390";//Base64.encodeToString(enc.getBytes(), 0);
        // String Available__URL = "http://railpnrapi.com/api/check_avail/tnum/"+tnum+"/fscode/"+fscode+"/tscode/"+tscode+"/date/"+adate+"/class/"+Tclass+"/quota/"+quote+"/format/json/pbapikey/314e3568e95c79f21f70f2f38c332778/pbapisign/"+retVal;
        String Available__URL = Base_URL+"avilabilty.php?tnum="+tnum+"&fscode="+fscode+"&tscode="+tscode+"&date="+adate+"&class="+Tclass+"&quota="+quote;

        Log.e("error++++++",Available__URL);



        //  params.add(new BasicNameValuePair("pnr", pnr));
        JSONObject json = jsonParser.makeHttpRequest(Available__URL, "POST", params);
        //JSONObject json = jsonParser.GetUrl(Available__URL);
        return json;
    }

    public JSONObject AuthenticateUser(String email,String name,String photo){

        HashMap<String, String> params = new HashMap<>();
        params.put("email", email);
        params.put("name", name);
        params.put("photo", photo);
        params.put("token", "timetable0592");

        String Available__URL = "http://www.demoinja.com/suhrit/auth.php";

        //  params.add(new BasicNameValuePair("pnr", pnr));
        JSONObject json = jsonParser.makeHttpRequest(Constants.CREATE_USER, "POST", params);
        Log.e("JSON ",""+json);
        //JSONObject json = jsonParser.GetUrl(Available__URL);
        return json;
    }

    public String getHasvalFare(String sortorderText)
    {
        String hmasString="";
        String mykey = "e23377f3fe611f99b13c3f236cc9b6be";
        //String test = "json"+"314e3568e95c79f21f70f2f38c332778"+mpnr;


        try {
            Mac mac = Mac.getInstance("HmacSHA1");
            SecretKeySpec secret = new SecretKeySpec(mykey.getBytes(),
                    "HmacSHA1");
            mac.init(secret);
            byte[] digest = mac.doFinal(sortorderText.getBytes());

            hmasString = bytesToHexString(digest);





        } catch (Exception e) {

        }
        return hmasString;

    }


    public static String bytesToHexString(byte[] bytes) {
        StringBuilder sb = new StringBuilder(bytes.length * 2);

        Formatter formatter = new Formatter(sb);
        for (byte b : bytes) {
            formatter.format("%02x", b);
        }

        return sb.toString();
    }


    public JSONObject getTrainFare(String agecode,String Tclass,String fscode,String quote,String tnum,String tscode){

    	/*$str_order ="agecode"."class"."format"."fscode"."pbapikey"."quote"."tnum"."tscode"*/;

        String base_string = agecode+Tclass+"json"+fscode+"314e3568e95c79f21f70f2f38c332778"+quote+tnum+tscode;
        base_string = base_string.toLowerCase();
        //String base_string = "json"+"314e3568e95c79f21f70f2f38c332778"+pnr;
        String key = "'e23377f3fe611f99b13c3f236cc9b6be";



        // Base 64 Encode the results

        String retVal =getHasvalFare(base_string);// "5f932d5f2f20baefcc8e77a6951c845c72172390";//Base64.encodeToString(enc.getBytes(), 0);
        // String FARE__URL = "http://railpnrapi.com/api/fare/tnum/"+tnum+"/fscode/"+fscode+"/tscode/"+tscode+"/class/"+Tclass+"/agecode/"+agecode+"/quota/"+quote+"/format/json/pbapikey/314e3568e95c79f21f70f2f38c332778/pbapisign/"+retVal;
        String FARE__URL = Base_URL+"/fare.php?tnum="+tnum+"&fscode="+fscode+"&tscode="+tscode+"&class="+Tclass+"&quota="+quote+"&agecode="+agecode;


        Log.e("error++++++",FARE__URL);

        HashMap<String, String> params = new HashMap<>();
        params.put("tag", "");

        //  params.add(new BasicNameValuePair("pnr", pnr));
        JSONObject json = jsonParser.makeHttpRequest(FARE__URL, "POST", params);
       // JSONObject json = jsonParser.GetUrl(FARE__URL);
        return json;
    }

    @SuppressWarnings("deprecation")
    public JSONObject EmailSend(String subject, String msg,String location,String useremail,String versionName){
        // Building Parameters

        HashMap<String, String> params = new HashMap<>();
        params.put("subject", subject);
        params.put("msg", msg);
        params.put("location", location);
        params.put("useremail", useremail);
        params.put("versionName", versionName);


        Log.e("param==",""+params);
       /* List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("subject", subject));
        params.add(new BasicNameValuePair("msg", msg));
        params.add(new BasicNameValuePair("location", location));
        params.add(new BasicNameValuePair("useremail", useremail));
        params.add(new BasicNameValuePair("versionName", versionName));*/
        JSONObject json = jsonParser.makeHttpRequest(LOGIN_URL, "GET", params);
        //JSONObject json = jsonParser.getJSON(LOGIN_URL,params);
        return json;
    }

    public JSONObject PnrStatusRail(String pnr){

        PNR_URL ="";
        String base_string = "json"+"314e3568e95c79f21f70f2f38c332778"+pnr;
        String key = "'e23377f3fe611f99b13c3f236cc9b6be";



        // Base 64 Encode the results

        String retVal =getHasval(pnr);// "5f932d5f2f20baefcc8e77a6951c845c72172390";//Base64.encodeToString(enc.getBytes(), 0);
        // PNR_URL = "http://railpnrapi.com/api/check_pnr/pnr/"+pnr+"/format/json/pbapikey/314e3568e95c79f21f70f2f38c332778/pbapisign/"+retVal;

        PNR_URL = Base_URL+"pnr.php?pnr="+pnr;
        Log.e("error++++++",PNR_URL);
        HashMap<String, String> params = new HashMap<>();
        params.put("", "");


        //  params.add(new BasicNameValuePair("pnr", pnr));
        JSONObject json = jsonParser.makeHttpRequest(PNR_URL, "POST", params);
Log.e("json",""+json);
        return json;
    }

    public String getHasval(String mpnr)
    {
        String hmasString="";
        String mykey = "e23377f3fe611f99b13c3f236cc9b6be";
        String test = "json"+"314e3568e95c79f21f70f2f38c332778"+mpnr;


        try {
            Mac mac = Mac.getInstance("HmacSHA1");
            SecretKeySpec secret = new SecretKeySpec(mykey.getBytes(),
                    "HmacSHA1");
            mac.init(secret);
            byte[] digest = mac.doFinal(test.getBytes());

            hmasString = bytesToHexString(digest);





        } catch (Exception e) {

        }
        return hmasString;

    }

    public JSONObject getExpressStatus(String trainno, String dayofjourney, String currentDateandTime){
        HashMap<String, String> params = new HashMap<>();
        params.put("trainno", trainno);
        params.put("dayofjourney", dayofjourney);
        params.put("time", currentDateandTime);

        JSONObject json = jsonParser.makeHttpRequest(Constants.EX_GETSTATUS_URL, "GET", params);

        return json;
    }

    public JSONObject getCancelTrain(String date){

        //http://api.railwayapi.com/cancelled/date/25-03-2017/apikey/qicde9951/
        HashMap<String, String> params = new HashMap<>();
        params.put("tag", "");


        JSONObject json = jsonParser.makeHttpRequest(Constants.EX_GETCANCEL_URL, "GET", params);
        return json;
    }

    public JSONObject CheckUpdate(int versioncode){
        // Building Parameters
        HashMap<String, String> params = new HashMap<>();
        params.put("Vcode", String.valueOf(versioncode));

       /* List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("Vcode", String.valueOf(versioncode)));*/

        JSONObject json = jsonParser.makeHttpRequest(VERSION_CHECK,"GET",params);
        return json;
    }
}
